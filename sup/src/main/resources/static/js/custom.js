jQuery(document).ready(function($) {

	$('input, textarea').each(function()
	{
		if( $(this).val() ) {
			$(this).next('label').addClass('shrink');
		}else{
			$(this).next('label').removeClass('shrink');
		}
	});
	
	$('select').each(function()
	{
		if( $(this).val() ) {
			$(this).addClass('choosen');
		}
	});

	$('input, textarea').blur(function()
	{
		if( $(this).val() ) {
			$(this).next('label').addClass('shrink');
		}else{
			$(this).next('label').removeClass('shrink');
		}
	});

	$('select').click(function()
	{
		if( $(this).val() ) {
			$(this).addClass('choosen');
		}
	});
})