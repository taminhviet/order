jQuery(document).ready(function() {
	var tableunassign = $('#tableorder').DataTable({
		columnDefs: [{
			orderable: false,
			className: 'select-checkbox',
			targets: 0
		}],
		select: {
			style: 'multi',
			selector: 'td:first-child'
		},
		"pageLength": 25,
		"order": [[1, "desc"]]
	});
	//	$("#paselect").select2();
	$('#tableordersup').DataTable({
		"pageLength": 25,
		"order": [[0, "desc"]]
	});
	$('#tableproxycheck').DataTable({
		"pageLength": 25,
		"order": [[0, "desc"]]
	});
	var tableallorder  = $('#tableallorder').DataTable({
		columnDefs: [{
			orderable: false,
			className: 'select-checkbox',
			targets: 0
		}],
		select: {
			style: 'multi',
			selector: 'td:first-child'
		},
		dom: 'Bfrtip',
		buttons: ['copy', 'csv', 'excel'],		
		"pageLength": 50,
		"order": [[1, "desc"]]
	});
	$('#tableneworder').DataTable({
		"pageLength": 25,
		"order": [[0, "desc"]]
	});
	$('#tablestore').DataTable({
		"pageLength": 25,
		"order": [[0, "desc"]]
	});
	$('#tablepaypal').DataTable({
		"pageLength": 50,
		"order": [[0, "desc"]]
	});
	$('#tablegmc').DataTable({
		"pageLength": 100,
		"ordering": false
		//"order": [[6, "desc"],[0, "desc"],[9, "desc"]]
	});
	$('#tableallproductgmc').DataTable({
		columnDefs: [
		{
			render: function(data, type, full, meta) {
				return "<div class='width-300'>" + data + "</div>";
			},
			targets: [2]
		},
		{
			render: function(data, type, full, meta) {
				return "<div class='width-100'>" + data + "</div>";
			},
			targets: [6]
		},
		{
			render: function(data, type, full, meta) {
				return "<div class='width-500'>" + data + "</div>";
			},
			targets: [9]
		}
		],
		select: {
			style: 'multi'
		},
		"pageLength": 100,
		"order": [[1, "desc"]]
	});


	$('#tableproducttm').DataTable({
		"pageLength": 50,
		"order": [[0, "desc"]]
	});
	$('#tablestripe').DataTable({
		"pageLength": 50,
		"order": [[0, "desc"]]
	});
	$('#tableorderdispute').DataTable({
		"pageLength": 25,
		"ordering": false
		//		"order": [[ 0, "desc" ]]
	});
	var tablesupplier = $('#tablesupplier').DataTable({
		"pageLength": 25,
		"order": [[0, "desc"]]
	});
	$('#tablereturn').DataTable({
		"pageLength": 25,
		"order": [[0, "desc"]]
	});
	//	$('#click').click(function () {
	//        var ids = $.map(tableunassign.rows('.selected').data(), function (item) {
	//            return item[1]
	//        });
	//        if(ids.length == 0){
	//        	alert("No row is selected!");
	//        }else {
	//        	$("#hiddenrow").val(ids);
	//        }
	//    });
	$('#btnExportOrder').click(function () {
		var tableallorder = $('#tableallorder').DataTable();
        var ids = $.map(tableallorder.rows('.selected').data(), function (item) {
            return item[1]
        });
        if(ids.length == 0){
        	alert("No row is selected!");
        }else {
        	$("#hiddenroworder").val(ids);
        }
    });
	
	
	var tableproductgmcmain = $('#tableproductgmcmain').DataTable({
		//		'ajax': '/lab/jquery-datatables-checkboxes/ids-arrays.txt',
		columnDefs: [{
			'targets': 0,
			'checkboxes': {
				'selectRow': true
			}
		}],
		scrollY: "550px",
		scrollX: true,
		scrollCollapse: true,
		select: {
			style: 'multi'
			//selector: 'td:first-child'
		},
		"pageLength": 50,
		"order": [[1, "asc"]]
	});
	var tablefeedgmc = $('#tablefeedgmc').DataTable({
		"pageLength": 50,
		"order": [[1, "asc"]]
	});
	var tabletm = $('#tableproducttmfeed').DataTable({
		scrollY: "550px",
		scrollX: true,
		scrollCollapse: true,
		columnDefs: [{
			'targets': 0,
			'checkboxes': {
				'selectRow': true
			}
		},
		{
			render: function(data, type, full, meta) {
				return "<div class='width-300'>" + data + "</div>";
			},
			targets: [9, 12]
		}
		],
		select: {
			style: 'multi'
		},
		"pageLength": 50,
		"order": [[1, "asc"]]
	});
	var tableproductgmc = $('#tableproductmc').DataTable({
		scrollY: "550px",
		scrollX: true,
		scrollCollapse: true,
		columnDefs: [
			{
				'targets': 0,
				'checkboxes': {
					'selectRow': true
				}
			},
			{
				render: function(data, type, full, meta) {
					return "<div class=' wrapnormal width-400'>" + data + "</div>";
				},
				targets: 3
			},
			{
				render: function(data, type, full, meta) {
					return "<div class='width-250'>" + data + "</div>";
				},
				targets: [14, 15]
			},
			{
				render: function(data, type, full, meta) {
					return "<div class='width-100'>" + data + "</div>";
				},
				targets: [8, 9, 12]
			}
		],
		select: {
			style: 'multi'
			//selector: 'td:first-child'
		},
		"pageLength": 50,
		"order": [[1, "asc"]]
	});


	$('#btnfeedsuptm').click(function() {
		var ids = $.map(tabletm.rows('.selected').data(), function(item) {
			return item[1]
		});
		var gmcproductids = $.map(tableproductgmc.rows('.selected').data(), function(item) {
			return item[1]
		});
		if (ids.length == 0 || gmcproductids == 0) {
			alert("Must select both table!");
			return;
		} else {
			$("#hiddentm").val(ids);
			$("#hiddenproductgmc").val(gmcproductids);
		}

	});
	$('#btnupdateapigmc').click(function() {
		var gmcproductids = $.map(tableproductgmc.rows('.selected').data(), function(item) {
			return item[1]
		});
		if (gmcproductids == 0) {
			alert("Must select table GMC!");
			return;
		} else {
			$("#hiddenproductgmcmodal").val(gmcproductids);
		}

	});

	$('#btnfeedsuptm2').click(function() {
		var gmcproductids = $.map(tableproductgmc.rows('.selected').data(), function(item) {
			return item[1]
		});
		if (gmcproductids == 0) {
			alert("Must select table GMC!");
			return;
		} else {
			$("#hiddenproductgmc2").val(gmcproductids);
		}

	});
	
	$('#btnupdatepricegmc').click(function() {
		var gmcproductids = $.map(tableproductgmc.rows('.selected').data(), function(item) {
			return item[1]
		});
		if (gmcproductids == 0) {
			alert("Must select table GMC!");
			return;
		} else {
			$("#hiddenupdategmcproduct").val(gmcproductids);
		}

	});
	
	$('#btnnotm').click(function() {
		var gmcproductids = $.map(tableproductgmcmain.rows('.selected').data(), function(item) {
			return item[1]
		});
		if (gmcproductids == 0) {
			alert("Must select table products!");
			return;
		} else {
			$("#hiddenproductstore").val(gmcproductids);
		}
	});
	$('#btndeletesupfeed').click(function() {
		var gmcproductids = $.map(tableproductgmc.rows('.selected').data(), function(item) {
			return item[1]
		});
//		if (gmcproductids == 0) {
//			alert("Must select table GMC!");
//			return;
//		} else {
		$("#hiddenproductgmcdelete").val(gmcproductids);
//		}

	});
	$('#btndeletemaingmc').click(function() {
		var gmcproductids = $.map(tableproductgmc.rows('.selected').data(), function(item) {
			return item[1]
		});
		if (gmcproductids == 0) {
			alert("Must select table GMC!");
			return;
		} else {
			$("#hiddenproductgmcdeletemain").val(gmcproductids);
		}

	});
	$("#div_focus").focus();
	jQuery('.add-new').hide();
	jQuery('.btn-add-new').click(function() {
		jQuery('.add-new').show();
	});

	jQuery('.btn-close-new').click(function() {
		jQuery('.add-new').hide();
	});
	jQuery('.add-new-satellite').hide();
	jQuery('.btn-add-new-satellite').click(function() {
		jQuery('.add-new-satellite').show();
	});

	jQuery('.btn-close-new-satellite').click(function() {
		jQuery('.add-new-satellite').hide();
	});
	jQuery('.add-new-gallery').hide();
	jQuery('.btn-add-new-gallery').click(function() {
		jQuery('.add-new-gallery').show();
	});

	jQuery('.btn-close-new-gallery').click(function() {
		jQuery('.add-new-gallery').hide();
	});

	$(document).on('change', '.up', function() {
		var names = [];
		var length = $(this).get(0).files.length;
		for (var i = 0; i < $(this).get(0).files.length; ++i) {
			names.push($(this).get(0).files[i].name);
		}
		// $("input[name=file]").val(names);
		if (length > 2) {
			var fileName = names.join(', ');
			$(this).closest('.form-group').find('.form-control').attr("value", length + " files selected");
		}
		else {
			$(this).closest('.form-group').find('.form-control').attr("value", names);
		}
	});
	$(".btn[data-target='#editPartnerModal']").click(function() {
		var $row = $(this).closest("tr");
		var $partnerid = $row.find(".id").text();
		var $partnername = $row.find(".name").text();
		$("#partneridedit").val($partnerid);
		$("#partnernameedit").val($partnername);
	});
	$(".btn[data-target='#editgmcapi']").click(function() {
		var $currency = $.map(tableproductgmc.rows('.selected').data(), function(item) {
			return item[13]
		});
		var $feedTM =  $.map(tableproductgmc.rows('.selected').data(), function(item) {
			return item[5]
		});
		if ($feedTM[0].indexOf("1st") == -1) {
			if($currency.indexOf("GBP") != -1){
				$("#sizemodal").val("UK 9");
			}else if($currency.indexOf("EUR") != -1 || $currency.indexOf("PLN") != -1){
				$("#sizemodal").val("EU 44");
			}else {
				$("#sizemodal").val("US 10");
			}
		}
	});
	$(".btn[data-target='#showSatellite']").click(function() {
		var $row = $(this).closest("tr");
		var $partnerid = $row.find(".id").text();
		var $partnername = $row.find(".name").text();
		$("#ModalSatelliteLabel").val($partnername);
		$.ajax({
			type: "GET",
			contentType: "application/json",
			url: "/admin/satellite/" + $partnerid,
			dataType: 'text',
			success: function(data) {
				$('.modal-body-satellite').html(data);
			},
			error: function(e) {
			}
		});
	});

	$(".btn[data-target='#editppmodal']").click(function() {
		var $row = $(this).closest("tr");
		var $id = $row.find(".id").text();
		var $email = $row.find(".email").text();
		var $username = $row.find(".username").text();
		var $password = $row.find(".password").text();
		var $address = $row.find(".address").text();
		$("#idedit").val($id.trim());
		$("#emailedit").val($email.trim());
		$("#usernameedit").val($username.trim());
		$("#passwordedit").val($password.trim());
		$("#addressedit").val($address.trim());
	});
	$('#tablegmc').on('click', '.btnEditGMC', function(e) {
		var $row = $(this).closest("tr");
		var $id = $row.find(".merchantId").text();
		var $email = $row.find(".accountSampleUser").text();
		var $country = $row.find(".country").text();
		var $domaintm = $row.find(".domaintm").text();
		var $supfeedid = $row.find(".supfeedid").text();
		$("#idedit").val($id.trim());
		$("#emailedit").val($email.trim());
		$("#countryedit").val($country.trim());
		$("#domaintmedit").val($domaintm.trim());
		$("#supfeedapiedit").val($supfeedid.trim());
	});	
//	$(".btn[data-target='#editgmcmodal']").click(function() {
//		var $row = $(this).closest("tr");
//		var $id = $row.find(".merchantId").text();
//		var $email = $row.find(".accountSampleUser").text();
//		var $country = $row.find(".country").text();
//		var $domaintm = $row.find(".domaintm").text();
//		var $supfeedid = $row.find(".supfeedid").text();
//		$("#idedit").val($id.trim());
//		$("#emailedit").val($email.trim());
//		$("#countryedit").val($country.trim());
//		$("#domaintmedit").val($domaintm.trim());
//		$("#supfeedapiedit").val($supfeedid.trim());
//	});


	$('#tableallorder').on('click', '.btnRefundPP', function(e) {
		var $row = $(this).closest("tr");
		var orderid = $row.find(".id").text();
		var paypalid = $row.find(".paypalaccountid").text();
		if (confirm("Refund order " + paypalid + " ?")) {
		$.ajax({
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify(orderid),
			url: "/admin/ajaxrefundpp",
			dataType: 'text',
			success: function(data) {
				$row.find(".resultrefund").html(data);
			},
			error: function(e) {
			}
		});
		}else {
			$row.find(".resultrefund").html("Cancel Refund!");
		}
	});
	$('#tableallorder').on('click', '.btnRefundWoo', function(e) {
		var $row = $(this).closest("tr");
		var orderid = $row.find(".id").text();
		if (confirm("Refund order " + orderid + " ?")) {
		$.ajax({
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify(orderid),
			url: "/admin/ajaxrefundwoo",
			dataType: 'text',
			success: function(data) {
				$row.find(".resultrefund").html(data);
			},
			error: function(e) {
			}
		});
		}else {
			$row.find(".resultrefund").html("Cancel Refund!");
		}
	});

	$('#tableallorder').on('click', '.btnCapturePP', function(e) {
		var $row = $(this).closest("tr");
		var $orderid = $row.find(".id").text();
		var $partial = $row.find('.partial').val();
		if ($partial != '') {
			var json = {
				"id": $orderid,
				"partial": $partial
			};
		} else {
			var json = {
				"id": $orderid
			};
		}
		$.ajax({
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify(json),
			url: "/admin/ajaxcapturepp",
			dataType: 'text',
			success: function(data) {
				$row.find(".result").html(data);
			},
			error: function(e) {
			}
		});
	});
	$('#tableallorder').on('click', '.btnCaptureStripe', function(e) {
		var $row = $(this).closest("tr");
		var $orderid = $row.find(".id").text();
		var json = {
			"id": $orderid
		};
		$.ajax({
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify(json),
			url: "/admin/ajaxcapturestripe",
			dataType: 'text',
			success: function(data) {
				$row.find(".result").html(data);
			},
			error: function(e) {
			}
		});
	});
	$('#tableallorder').on('click', '.btnDelete', function(e) {
		var $row = $(this).closest("tr");
		var $orderid = $row.find(".id").text().trim();
		var json = {
			"id": $orderid
		};
		$.ajax({
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify(json),
			url: "/admin/ajaxdeleteorder",
			dataType: 'text',
			success: function(data) {
				$row.find(".resultdelete").html(data);
			},
			error: function(e) {
			}
		});
	});
	$('#tableallproductgmc').on('click', '.btnDelete', function(e) {
		var $row = $(this).closest("tr");
		var $orderid = $row.find(".merchantId").text().trim();
		var json = {
			"id": $orderid
		};
		$.ajax({
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify(json),
			url: "/admin/ajaxdeletegmcproduct",
			dataType: 'text',
			success: function(data) {
				$row.find(".resultdelete").html(data);
			},
			error: function(e) {
			}
		});
	});
	$('#tableallproductgmc').on('click', '.copyimage', function(e) {
		var $row = $(this).closest("tr");
		var imageurl = $row.find(".image").text().trim();
		// Create a temporary input element
        var tempInput = $('<input>');
        $('body').append(tempInput);

        // Set the value of the input to the text to be copied
        tempInput.val(imageurl).select();

        // Execute the copy command
        document.execCommand('copy');

        // Remove the temporary input
        tempInput.remove();
	});
	$('#tablefeedgmc').on('click', '.copylink', function(e) {
		var $row = $(this).closest("tr");
		var imageurl = $row.find(".feedlink").text().trim();
		// Create a temporary input element
        var tempInput = $('<input>');
        $('body').append(tempInput);

        // Set the value of the input to the text to be copied
        tempInput.val(imageurl).select();

        // Execute the copy command
        document.execCommand('copy');

        // Remove the temporary input
        tempInput.remove();
	});
	$('#tablestore').on('click', '.btndelete', function(e) {
		var $row = $(this).closest("tr");
		var $storeid = $row.find(".id").text().trim();
		var json = {
			"id": $storeid
		};
		$.ajax({
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify(json),
			url: "/admin/ajaxdeletestore",
			dataType: 'text',
			success: function(data) {
				$row.find(".resultdelete").html(data);
			},
			error: function(e) {
			}
		});
	});
	$('#tablepaypal').on('click', '.btnDeactivePaypal', function(e) {
		var $row = $(this).closest("tr");
		var $paypalid = $row.find(".id").text().trim();
		var json = {
			"id": $paypalid
		};
		$.ajax({
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify(json),
			url: "/admin/ajaxdeactivepaypal",
			dataType: 'text',
			success: function(data) {
				$row.find(".result").html(data);
			},
			error: function(e) {
			}
		});
	});
	$('#tablepaypal').on('click', '.btnQueue', function(e) {
		var $row = $(this).closest("tr");
		var $paypalid = $row.find(".id").text().trim();
		var json = {
			"id": $paypalid
		};
		$.ajax({
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify(json),
			url: "/admin/ajaxqueuepp",
			dataType: 'text',
			success: function(data) {
				$row.find(".result").html(data);
			},
			error: function(e) {
			}
		});
	});
	$('#tablepaypal').on('click', '.btnNotePaypal', function(e) {
		var $row = $(this).closest("tr");
		var $paypalid = $row.find(".id").text().trim();
		var $notetext = $row.find(".notetext").val();
		var json = {
			"id": $paypalid,
			"notetext": $notetext
		};
		$.ajax({
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify(json),
			url: "/admin/ajaxnotepp",
			dataType: 'text',
			success: function(data) {
				$row.find(".resultnotepp").html(data);
			},
			error: function(e) {
			}
		});
	});
	$('#tablepaypal').on('click', '.btnCheckPaypal', function(e) {
		var $row = $(this).closest("tr");
		var $paypalid = $row.find(".id").text().trim();
		var json = {
			"id": $paypalid
		};
		$.ajax({
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify(json),
			url: "/admin/ajaxcheckppstatus",
			dataType: 'text',
			success: function(data) {
				$row.find(".resultcheckpp").html(data);
			},
			error: function(e) {
			}
		});
	});
	$('#tablefeedgmc').on('click', '.btnfetch', function(e) {
		var $row = $(this).closest("tr");
		var $merchantid = $row.find(".gmcid").text().trim();
		var $feedid = $row.find(".feedid").text().trim();
		var json = {
			"merchantid": $merchantid,
			"feedid": $feedid
		};
		$.ajax({
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify(json),
			url: "/admin/fetchfeed",
			dataType: 'text',
			success: function(data) {
				$row.find(".messagefetch").html(data);
			},
			error: function(e) {
			}
		});
	});
	$('#tablefeedgmc').on('click', '.btncountry', function(e) {
		var $row = $(this).closest("tr");
		var $merchantid = $row.find(".gmcid").text().trim();
		var $feedid = $row.find(".feedid").text().trim();
		var $gmccountry = $row.find('.gmccountry').val();
		var json = {
			"merchantid": $merchantid,
			"feedid": $feedid,
			"gmccountry": $gmccountry
		};
		$.ajax({
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify(json),
			url: "/admin/updatefetchfeed",
			dataType: 'text',
			success: function(data) {
				$row.find(".resultupfetch").html(data);
			},
			error: function(e) {
			}
		});
	});
	$('#tablefeedgmc').on('click', '.btndelete', function(e) {
		var $row = $(this).closest("tr");
		var $merchantid = $row.find(".gmcid").text().trim();
		var $feedid = $row.find(".feedid").text().trim();
		var json = {
			"merchantid": $merchantid,
			"feedid": $feedid
		};
		$.ajax({
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify(json),
			url: "/admin/deletefeedgmc",
			dataType: 'text',
			success: function(data) {
				$row.find(".messagedelete").html(data);
			},
			error: function(e) {
			}
		});
	});
	$('#tableallorder').on('click', '.btncost', function(e) {
		var $row = $(this).closest("tr");
		var $orderid = $row.find(".id").text();
		var $ordercost = $row.find('.ordercost').val();
		var json = {
			"id": $orderid,
			"ordercost": $ordercost
		};
		$.ajax({
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify(json),
			url: "/admin/ajaxcost",
			dataType: 'text',
			success: function(data) {
				$row.find(".resultcost").html(data);
			},
			error: function(e) {
			}
		});
	});
	$('#tableallorder').on('click', '.btnnote', function(e) {
		var $row = $(this).closest("tr");
		var $orderid = $row.find(".id").text();
		var $ordercost = $row.find('.ordernote').val();
		var json = {
			"id": $orderid,
			"ordercost": $ordercost
		};
		$.ajax({
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify(json),
			url: "/admin/ajaxnote",
			dataType: 'text',
			success: function(data) {
				$row.find(".resultnote").html(data);
			},
			error: function(e) {
			}
		});
	});
	$('#tablegmc').on('click', '.btnsupfeedid', function(e) {
		var $row = $(this).closest("tr");
		var $id = $row.find(".merchantId").text();
		var $note = $row.find('.gmcsupfeedid').val();
		var json = {
			"id": $id,
			"note": $note
		};
		$.ajax({
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify(json),
			url: "/admin/ajaxsuppfeedgmc",
			dataType: 'text',
			success: function(data) {
				$row.find(".resultsupfeedid").html(data);
			},
			error: function(e) {
			}
		});
	});
	$('#tablegmc').on('click', '.btngmcbusiness', function(e) {
		var $row = $(this).closest("tr");
		var $id = $row.find(".merchantId").text();
		var $note = $row.find('.gmcbusiness').val();
		var json = {
			"id": $id,
			"note": $note
		};
		$.ajax({
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify(json),
			url: "/admin/ajaxchangegmcbusiness",
			dataType: 'text',
			success: function(data) {
				$row.find(".resultbusiness").html(data);
			},
			error: function(e) {
			}
		});
	});
	$('#tablegmc').on('click', '.btnnote', function(e) {
		var $row = $(this).closest("tr");
		var $id = $row.find(".merchantId").text();
		var $note = $row.find('.gmcnote').val();
		var json = {
			"id": $id,
			"note": $note
		};
		$.ajax({
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify(json),
			url: "/admin/ajaxnotegmc",
			dataType: 'text',
			success: function(data) {
				$row.find(".resultnote").html(data);
			},
			error: function(e) {
			}
		});
	});
	$('#tablegmc').on('click', '.btnsetcountry', function(e) {
		var $row = $(this).closest("tr");
		var $id = $row.find(".merchantId").text();
		var $note = $row.find('.gmcchangecountry').val();
		var json = {
			"id": $id,
			"note": $note
		};
		$.ajax({
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify(json),
			url: "/admin/ajaxgmccountry",
			dataType: 'text',
			success: function(data) {
				$row.find(".resultcountry").html(data);
			},
			error: function(e) {
			}
		});
	});
	$('#tableproductmc').on('click', '.btnpricegmc', function(e) {
		var $row = $(this).closest("tr");
		var $productgmcid = $row.find(".id").text();
		var $ordercost = $row.find('.pricegmc').val();
		var json = {
			"id": $productgmcid,
			"ordercost": $ordercost
		};
		$.ajax({
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify(json),
			url: "/admin/ajaxupdatepricegmc",
			dataType: 'text',
			success: function(data) {
				$row.find(".resultgmc").html(data);
			},
			error: function(e) {
			}
		});
	});
	$('#tableallorder').on('click', '.btnbalance', function(e) {
		var $row = $(this).closest("tr");
		var $orderid = $row.find(".id").text();
		var $ordercost = $row.find('.ordernote').val();
		var json = {
			"id": $orderid,
			"ordercost": $ordercost
		};
		$.ajax({
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify(json),
			url: "/admin/ajaxcheckbalance",
			dataType: 'text',
			success: function(data) {
				$row.find(".resultnote").html(data);
			},
			error: function(e) {
			}
		});
	});
	$('#tableallorder').on('click', '.btnSendMessage', function(e) {
		var $row = $(this).closest("tr");
		var $orderid = $row.find(".id").text();
		var $message = $row.find('.messagedipsute').val();
		var json = {
			"id": $orderid,
			"message": $message
		};
		$.ajax({
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify(json),
			url: "/admin/ajaxmessagedispute",
			dataType: 'text',
			success: function(data) {
				$row.find(".resultMessage").html(data);
			},
			error: function(e) {
			}
		});
	});

	$('#tableallorder').on('click', '.btnDeliver', function(e) {
		var $row = $(this).closest("tr");
		var $orderid = $row.find(".id").text();
		var $trackingnumber = $row.find('.trackingnumberinput').val();
		var $carrier = $row.find('.carrier').val();
		var $notecarrier = $row.find('.isemail').is(':checked')
		var $partial = $row.find('.partial').val();
		if ($partial != '') {
			var json = {
				"id": $orderid,
				"trackingnumber": $trackingnumber,
				"carrier": $carrier,
				"notecarrier": $notecarrier,
				"partial": $partial
			};
		} else {
			var json = {
				"id": $orderid,
				"trackingnumber": $trackingnumber,
				"carrier": $carrier,
				"notecarrier": $notecarrier
			};
		}
		if ($carrier == 'PLEASE_SELECT') {
			alert("NEED SELECT CARRIER");
			return;
		}
		$.ajax({
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify(json),
			url: "/admin/ajaxdeliver",
			dataType: 'text',
			success: function(data) {
				$row.find(".resultCapture").html(data);
			},
			error: function(e) {
			}
		});
	});
	$('#tablegmc').on('click', '.btnCheckGMC', function(e) {
		var $row = $(this).closest("tr");
		var $merchantId = $row.find(".merchantId").text().trim();
		var $notetext = $row.find(".countrytext").val();
		var json = {
			"id": $merchantId,
			"notetext": $notetext
		};
		$.ajax({
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify(json),
			url: "/admin/ajaxcheckgmc",
			dataType: 'text',
			success: function(data) {
				$row.find(".resultgmc").html(data);
			},
			error: function(e) {
			}
		});
	});
	$('#tablegmc').on('click', '.btnRequestReview', function(e) {
		var $row = $(this).closest("tr");
		var $merchantId = $row.find(".merchantId").text().trim();
		var $notetext = $row.find(".countrytext").val();
		var json = {
			"id": $merchantId,
			"notetext": $notetext
		};
		$.ajax({
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify(json),
			url: "/admin/ajaxreviewgmc",
			dataType: 'text',
			success: function(data) {
				$row.find(".resultrequest").html(data);
			},
			error: function(e) {
			}
		});
	});
	$('#tablegmc').on('click', '.btnDisableGMC', function(e) {
		var $row = $(this).closest("tr");
		var $merchantId = $row.find(".merchantId").text().trim();
		var json = {
			"id": $merchantId
		};
		$.ajax({
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify(json),
			url: "/admin/ajaxdisablegmc",
			dataType: 'text',
			success: function(data) {
				$row.find(".resultdisable").html(data);
			},
			error: function(e) {
			}
		});
	});
	$('#tablegmc').on('click', '.btnEnableGMC', function(e) {
		var $row = $(this).closest("tr");
		var $merchantId = $row.find(".merchantId").text().trim();
		var json = {
			"id": $merchantId
		};
		$.ajax({
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify(json),
			url: "/admin/ajaxenablegmc",
			dataType: 'text',
			success: function(data) {
				$row.find(".resultenable").html(data);
			},
			error: function(e) {
			}
		});
	});
	$('#tableallorder').on('click', '.btnDeliverStripe', function(e) {
		var $row = $(this).closest("tr");
		var $orderid = $row.find(".id").text();
		var $trackingnumber = $row.find('.trackingnumberinput').val();
		var $carrier = $row.find('.carrier').val();
		var $notecarrier = $row.find('.notecarrier').val();
		var $partial = $row.find('.partial').val();
		var json = {
			"id": $orderid,
			"trackingnumber": $trackingnumber,
			"carrier": $carrier,
			"notecarrier": $notecarrier
		};
		if ($carrier == 'PLEASE_SELECT') {
			alert("NEED SELECT CARRIER");
			return;
		}
		$.ajax({
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify(json),
			url: "/admin/deliverstripe",
			dataType: 'text',
			success: function(data) {
				$row.find(".resultCapture").html(data);
			},
			error: function(e) {
			}
		});
	});
	$(".fullstat").hide();
	$('input[type=radio][name=display]').change(function() {
		if (this.value == 'image') {
			$(".onlyimage").show();
			$(".fullstat").hide();
		} else if (this.value == 'full') {
			$(".fullstat").show();
			$(".onlyimage").hide();
		}
	});
});
