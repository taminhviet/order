$(document).ready(function() {

	$("#openbtn").click(function() {
		$("#mySidebar").css("width", "250");
		$("#main").css("margin-left", "250");
	});
	$("#closebtn").click(function() {
		$("#mySidebar").css("width", "0");
		$("#main").css("margin-left", "0");
	});

	$("#filtertext").on("keyup", function() {
		var value = $(this).val().toLowerCase();
		$("#menutext *").filter(function() {
			$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
		});
	});

});