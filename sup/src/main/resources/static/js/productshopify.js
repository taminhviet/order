jQuery(document).ready(function() {
	$("#storeselect").select2();
	var tableproductshopify = $('#tableproductshopify').DataTable({
		columnDefs: [{
			'targets': 0,
			'checkboxes': {
				'selectRow': true
			}
		}],
		scrollY: "750px",
		scrollX: true,
		scrollCollapse: true,
		select: {
			style: 'multi'
		},
		"pageLength": 50,
		"order": [[1, "desc"]]
	});

	$('#btndeleteshopify').click(function() {
		var gmcproductids = $.map(tableproductshopify.rows('.selected').data(), function(item) {
			return item[1]
		});
		if (gmcproductids == 0) {
			alert("Must select table products!");
			return;
		} else {
			$("#hiddenproductshopify").val(gmcproductids);
		}

	});
	
	$('#btnupwoo').click(function() {
		var gmcproductids = $.map(tableproductshopify.rows('.selected').data(), function(item) {
			return item[1]
		});
		if (gmcproductids == 0) {
			alert("Must select table products!");
			return;
		} else {
			$("#hiddenproductupload").val(gmcproductids);
		}

	});
});