package shopping.content.v2_1.samples.products;

import java.io.IOException;
import java.math.BigInteger;

import com.google.api.services.content.model.Price;
import com.google.api.services.content.model.Product;

import shopping.content.v2_1.samples.ContentSample;

/** Sample that inserts a product. The product created here is used in other samples. */
public class ProductInsertSample extends ContentSample {
	private static final String CHANNEL = "online";
	  private static final String CONTENT_LANGUAGE = "en";
	  private static final String TARGET_COUNTRY = "GB";
  public ProductInsertSample(String[] args) throws IOException {
    super(args);
  }

  @Override
  public void execute() throws IOException {
    checkNonMCA();

    // Create a product with the defaults defined within ExampleProductFactory.

//    List<Product> lsProduct =  ExampleProductFactory.GetAllWooProducts();
    try {
//      Product result = content.products().insert(this.config.getMerchantId(), product).execute();
//    	Product productexist =
//    			content
//    			.products()
//    			.get(this.config.getMerchantId(), "online:en:-:641c1e363c6324275731c910_812_506")
//    			.execute();
//    	String target = productexist.getTargetCountry();
    	Product product = new Product()
    			.setTitle("Ship 2 2023 03 24 4.5587 - Men's Premium T-Shirt purple").setPrice(new Price().setCurrency("USD").setValue("170"));
//    	productexist.setPrice(new Price().setCurrency("USD").setValue("170"));
//    	 product.setTitle("Air jordan 4 white oreo");
//      ProductUtils.printProduct(result);
//    	for (Product product : lsProduct) {
    		Long intvalue = Long.valueOf("10100725099");
//			System.out.println(product.getOfferId() + ":" + product.getItemGroupId() + ":" + product.getTitle() + ":" + product.getPrice() + ":" +  product.getLink());
//    		Product result = content.products().insert(this.config.getMerchantId(), product).set("feedId", BigInteger.valueOf(intvalue)).execute();
//			content.products().delete(this.config.getMerchantId(), product.getId()).set("feedId", BigInteger.valueOf(intvalue)).execute();
//    		Product result = content.products().insert(this.config.getMerchantId(), product).execute();
    		Product result = content.products().update(this.config.getMerchantId(), "online:en:-:641c1e363c6324275731c910_812_506", product).execute();
//    	}
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static void main(String[] args) throws IOException {
    new ProductInsertSample(args).execute();
  }
}
