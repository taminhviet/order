package shopping.content.v2_1.samples.accountstatuses;

import java.io.IOException;
import java.util.List;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.services.content.ShoppingContent.Shoppingadsprogram;
import com.google.api.services.content.ShoppingContentRequest;
import com.google.api.services.content.model.AccountStatus;
import com.google.api.services.content.model.AccountStatusAccountLevelIssue;
import com.google.api.services.content.model.RequestReviewShoppingAdsRequest;
import com.google.api.services.content.model.ShoppingAdsProgramStatus;

import shoes.sup.util.Constants;
import shopping.content.v2_1.samples.ContentSample;

/**
 * Sample that retrieves the accountstatuses information for the current
 * Merchant Center account.
 */
public class AccountstatusGetSample extends ContentSample {
	public AccountstatusGetSample(String[] args) throws IOException {
		super(args);
	}

	@Override
	public void execute() throws IOException {
		try {
			AccountStatus accountStatus = content.accountstatuses().get(config.getMerchantId(), config.getMerchantId())
					.execute();
			AccountstatusUtils.printAccountStatus(accountStatus);
//			RequestReviewShoppingAdsRequest request = new RequestReviewShoppingAdsRequest();
//			request.setRegionCode("GB");
//			ShoppingAdsProgramStatus status = content.shoppingadsprogram().get(Long.valueOf("732858542")).execute();
//			content.shoppingadsprogram().requestreview(Long.valueOf("732858542"), request).execute();
//			System.out.println(status);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getStatus() throws IOException {
		String status = Constants.ENABLE;
		try {
			AccountStatus accountStatus = content.accountstatuses().get(config.getMerchantId(), config.getMerchantId())
					.execute();
			List<AccountStatusAccountLevelIssue> lsIssue = accountStatus.getAccountLevelIssues();
			if(lsIssue != null && !lsIssue.isEmpty()) {
				for (AccountStatusAccountLevelIssue accountStatusAccountLevelIssue : lsIssue) {
					status = accountStatusAccountLevelIssue.getTitle();
					System.out.println(status);
				}
			}
		} catch (GoogleJsonResponseException e) {
			checkGoogleJsonResponseException(e);
		}
		return status;
	}

	public static void main(String[] args) throws IOException {
		new AccountstatusGetSample(args).execute();
	}
}
