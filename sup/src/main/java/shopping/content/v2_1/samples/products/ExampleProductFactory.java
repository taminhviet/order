package shopping.content.v2_1.samples.products;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.api.services.content.model.Price;
import com.google.api.services.content.model.Product;
import com.google.api.services.content.model.ProductShipping;
import com.google.api.services.content.model.ProductsCustomBatchRequest;
import com.google.api.services.content.model.ProductsCustomBatchRequestEntry;
import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.icoderman.woocommerce.ApiVersionType;
import com.icoderman.woocommerce.EndpointBaseType;
import com.icoderman.woocommerce.WooCommerce;
import com.icoderman.woocommerce.WooCommerceAPI;
import com.icoderman.woocommerce.oauth.OAuthConfig;

import shopping.common.model.Attribute;
import shopping.common.model.ProductMerchants;
import shopping.common.model.ProductsWoo;
import shopping.common.utils.Constants;
import shopping.content.v2_1.samples.ContentConfig;

/**
 * Factory for creating Products to be inserted by the ProductInsert and ProductBatchInsert samples.
 */
public class ExampleProductFactory {
  private static final String CHANNEL = "online";
  private static final String CONTENT_LANGUAGE = "en";
  private static final String TARGET_COUNTRY = "GB";
  private static final String OFFER_ID = "167118";
  private static final int PRODUCT_COUNT = 10;

  public static String sampleProductId() {
    return sampleProductId(OFFER_ID);
  }

  public static String sampleProductId(String offerId) {
    return CHANNEL + ":" + CONTENT_LANGUAGE + ":" + TARGET_COUNTRY + ":" + offerId;
  }

  public static Product create(ContentConfig config) {
    return create(config, OFFER_ID);
  }

  public static Product create(ContentConfig config, String offerId) {
    String websiteUrl = config.getWebsiteUrl();

    if (websiteUrl == null || websiteUrl.equals("")) {
      throw new IllegalStateException(
          "Cannot create example products without a configured website");
    }

    return new Product()
        .setOfferId(offerId)
        .setTitle("A Tale of Two Cities")
        .setDescription("A classic novel about the French Revolution")
        .setLink(websiteUrl + "/tale-of-two-cities.html")
        .setImageLink(websiteUrl + "/tale-of-two-cities.jpg")
        .setChannel(CHANNEL)
        .setContentLanguage(CONTENT_LANGUAGE)
        .setTargetCountry(TARGET_COUNTRY)
        .setAvailability("in stock")
        .setCondition("new")
        .setGoogleProductCategory("Media > Books")
        .setGtin("9780007350896")
        .setPrice(new Price().setValue("2.50").setCurrency("GBP"))
        .setShipping(
            ImmutableList.of(
                new ProductShipping()
                    .setPrice(new Price().setValue("0.99").setCurrency("GBP"))
                    .setCountry("GB")
                    .setService("1st class post")));
  }
  
  public static Product createSample(ContentConfig config, String offderID) {
	  String websiteUrl = config.getWebsiteUrl();

	    if (websiteUrl == null || websiteUrl.equals("")) {
	      throw new IllegalStateException(
	          "Cannot create example products without a configured website");
	    }
	    return new Product()
	            .setOfferId(offderID)
	            .setTitle("Air Jordan 4 Retro \"White Oreo\"")
	            .setDescription("The Air Jordan 4 \"White Oreo\" is a Summer 2021 release by Jordan Brand that combines two popular colorways of Michael Jordan’s fourth signature shoe into one design. The styles we’re referring to are the Jordan 4 \"Oreo\" and \"White/Cement,\" which are fused together to create the predominantly monochromatic white design of the \"White Oreo.\" Like the \"White/Cement,\" the \"White Oreo\" shares a white tumbled leather construction with cement grey speckling on the eyelets and on the painted midsole. White netting can be found on both the mid-panel and tongue. Jordan Brand paints the model’s signature \"wings\" white and adds a Fire Red embroidered Jumpman to the white leather tongue. A metallic silver Jumpman logo adorns the heel. A small visible Air unit can be found in the heel of the cement grey midsole to finish off the look. Release date: July 3, 2021")
	            .setLink(websiteUrl + "/product/air-jordan-4-retro-white-oreo/")
	            .setImageLink("https://cdn.shopify.com/s/files/1/0562/7576/3358/products/CT8527-100_1.png")
	            .setChannel(CHANNEL)
	            .setContentLanguage(CONTENT_LANGUAGE)
	            .setTargetCountry("VN")
	            .setAvailability("in stock")
	            .setCondition("new")
	            .setGoogleProductCategory("Apparel & Accessories > Shoes")
	            .setGtin("194956852025")
	            .setPrice(new Price().setValue("170.00").setCurrency("GBP"))
	            .setShipping(
	                ImmutableList.of(
	                    new ProductShipping()
	                        .setPrice(new Price().setValue("4.99").setCurrency("GBP"))
	                        .setCountry("GB")
	                        .setService("1st class post")));
  }

  public static ProductsCustomBatchRequest createBatch(ContentConfig config, String prefix) {
    List<ProductsCustomBatchRequestEntry> productsBatchRequestEntries = new ArrayList<>();
    for (int i = 0; i < PRODUCT_COUNT; i++) {
      productsBatchRequestEntries.add(
          new ProductsCustomBatchRequestEntry()
              .setBatchId((long) i)
              .setMerchantId(config.getMerchantId())
              .setProduct(create(config, prefix + i))
              .setMethod("insert"));
    }
    return new ProductsCustomBatchRequest().setEntries(productsBatchRequestEntries);
  }
  
	public static List<Product> GetAllWooProducts() {
//		String productDetail = Constants.EMPTY;
		Gson gson = new Gson();
		List<Product> lsPM = new ArrayList<>();
		try {
//			StringBuilder sb = new StringBuilder();
//			sb.append("https://");
//			sb.append("omniosis.fr");
//			sb.append("/wp-json/wc/v3/products");
//			sb.append("?consumer_key=");
//			sb.append(Constants.Woo_KEY);
//			sb.append("&consumer_secret=");
//			sb.append(Constants.Woo_Secret);
//			String urlProduct = sb.toString();
//			productDetail = getWooInfo(urlProduct);
			StringBuilder apiUrl = new StringBuilder();
			apiUrl.append("https://");
			apiUrl.append("omniosis.fr");
			String url = apiUrl.toString();
			OAuthConfig config = new OAuthConfig(url, Constants.Woo_KEY, Constants.Woo_Secret);
			WooCommerce wooCommerce = new WooCommerceAPI(config, ApiVersionType.V2);
			Map<String, String> params = new HashMap<>();
			params.put("per_page","10");
			params.put("offset","0");
			List products = wooCommerce.getAll(
					EndpointBaseType.PRODUCTS.getValue(), params);
			System.out.println(products.size());
			
			for(Object product: products) {
				String json = gson.toJson(product);
				ProductsWoo productWoo = gson.fromJson(json, ProductsWoo.class);
				int id = productWoo.getId();
				String name = productWoo.getName();
				String permalink = productWoo.getPermalink();
				String description = productWoo.getDescription();
				String image = productWoo.getImages().get(0).getSrc();
				String price = productWoo.getPrice();
				String[] variants = productWoo.getVariations();
				for (String va : variants) {
					ProductsWoo variant = getWooVariant(id, va);
					Attribute first = variant.getAttributes().get(0);
					ProductMerchants pm = new ProductMerchants(va, name + " " + first.getOption() , description, permalink, image, CHANNEL, CONTENT_LANGUAGE, "US", null, variant.getPrice());
					lsPM.add(new Product()
		            .setOfferId(pm.getOfferID())
		            .setItemGroupId(String.valueOf(id))
		            .setTitle(pm.getTitle())
		            .setDescription(pm.getDescription())
		            .setLink(pm.getLink())
		            .setImageLink(pm.getImagelink())
		            .setChannel(CHANNEL)
		            .setContentLanguage(CONTENT_LANGUAGE)
		            .setTargetCountry("US")
		            .setAvailability("in stock")
		            .setCondition("new")
		            .setGoogleProductCategory("Toys & Games > Toys")
		            .setGtin(null)
		            .setPrice(new Price().setValue(pm.getPrice()).setCurrency("USD"))
		            .setShipping(
		                ImmutableList.of(
		                    new ProductShipping()
		                        .setPrice(new Price().setValue("4.99").setCurrency("USD"))
		                        .setCountry("US")
		                        .setService("1st class post"))));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lsPM;
	}
	private static ProductsWoo getWooVariant(int productID, String variantID) {
		Gson gson  = new Gson();
		StringBuilder sb = new StringBuilder();
		sb.append("https://");
		sb.append("omniosis.fr");
		sb.append("/wp-json/wc/v3/products/");
		sb.append(productID);
		sb.append("/variations/");
		sb.append(variantID);
		sb.append("?consumer_key=");
		sb.append(Constants.Woo_KEY);
		sb.append("&consumer_secret=");
		sb.append(Constants.Woo_Secret);
		String urlProduct = sb.toString();
		String productDetail = getWooInfo(urlProduct);
		ProductsWoo product= gson.fromJson(productDetail, ProductsWoo.class);
		return product;
	}

  private static String getWooInfo(String urlWoo) {
		String responseString = Constants.EMPTY;
		try {
			URL url = new URL(urlWoo);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.addRequestProperty("User-Agent", 
					"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
			conn.setRequestMethod("GET");

			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String output;

			StringBuffer response = new StringBuffer();
			while ((output = in.readLine()) != null) {
				response.append(output);
			}

			in.close();
			// printing result from response
			responseString = response.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseString;
	}
}
