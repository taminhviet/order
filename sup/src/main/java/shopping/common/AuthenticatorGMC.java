package shopping.common;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.util.Set;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.services.AbstractGoogleClient;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.content.ShoppingContent;
import com.google.api.services.content.ShoppingContentScopes;
import com.google.gson.Gson;

import shoes.sup.model.GMC;
import shopping.common.model.MerchantsInfo;

/**
 * Class that contains all the authentication logic, including choosing between
 * service account credentials and OAuth2 client credentials.
 */
public class AuthenticatorGMC {

	private Set<String> scopes;
	private HttpTransport httpTransport;
	private JsonFactory jsonFactory;
	private HttpRequestInitializer initializer;
	protected ShoppingContent content;
	private GMC gmc;

	public AuthenticatorGMC(GMC gmc)
			throws IOException {
		this.scopes = ShoppingContentScopes.all();
		this.httpTransport = createHttpTransport();
		this.jsonFactory = JacksonFactory.getDefaultInstance();
		this.gmc = gmc;
		initializer = BaseOption.increaseTimeout(
		        BaseOption.installLogging(authenticate(), BaseOption.parseOptions(new String[] {})));
	}
	public ShoppingContent initBuilder() {
		ShoppingContent.Builder builder =
		        new ShoppingContent.Builder(httpTransport, jsonFactory, initializer)
		            .setApplicationName("Content API for Shopping Samples");
		content = createService(builder);
		return content;
	}
	protected <T extends AbstractGoogleClient> T createService(AbstractGoogleClient.Builder builder) {
	    return BaseWorkflowSample.createService(builder);
	  }

	protected HttpTransport createHttpTransport() throws IOException {
		try {
			return GoogleNetHttpTransport.newTrustedTransport();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
			System.exit(1);
		}
		return null;
	}

	public Credential authenticate() throws IOException {
		Gson gson = new Gson();
		GoogleCredential credential = null;
		try {
			credential = GoogleCredential.getApplicationDefault().createScoped(scopes);
			return credential;
		} catch (IOException e) {
			// No need to do anything, we'll fall back on other credentials.
		}

		try {
			MerchantsInfo info = new MerchantsInfo(gmc.getType(), gmc.getProject_id(), gmc.getPrivate_key_id(),
					gmc.getPrivate_key(), gmc.getClient_email(), gmc.getClient_id(), gmc.getAuth_uri(),
					gmc.getToken_uri(), gmc.getAuth_provider_x509_cert_url(), gmc.getClient_x509_cert_url(),
					gmc.getUniverse_domain());
			String stringData = gson.toJson(info);
			InputStream stream = new ByteArrayInputStream(stringData.getBytes(StandardCharsets.UTF_8));
			credential = GoogleCredential.fromStream(stream, httpTransport, jsonFactory).createScoped(scopes);
			// GoogleCredential.fromStream does NOT refresh, since scopes must be added
			// after.
			if (!credential.refreshToken()) {
				System.out.println("The service account access token could not be refreshed.");
				System.out.println("The service account key may have been deleted in the API Console.");
				throw new IOException("Failed to refresh service account token");
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return credential;
	}
}
