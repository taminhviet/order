package shopping.common.model;

import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.OAuthTokenCredential;
import com.paypal.base.rest.PayPalRESTException;

public class PayPalClient {

	private static final String CLIENT_ID = "AfxKRIQnXMH2FOC9mngFscs1s7Ue3p05JTTtr7dvApxlNMTnVnG9uu68AYa70xe5hc2fmdkbW6mLDicY";
    private static final String CLIENT_SECRET = "EIGkgwv92ZlaUJJJiPuR1rHtzTneQPbSGtEzH2AUbEcOS6ZkAjCUoorGPKtacq2a3Fbp8bfPAtlP4ivG";
    private static final String MODE = "live"; // Change to "live" for production

    public static APIContext getAPIContext() throws PayPalRESTException {
        OAuthTokenCredential authTokenCredential = new OAuthTokenCredential(CLIENT_ID, CLIENT_SECRET);
        String accessToken = authTokenCredential.getAccessToken();
        return new APIContext(accessToken);
    }
}
