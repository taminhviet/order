package shopping.common.model;

import java.util.List;

import shoes.sup.model.MetaWoo;

public class ProductsWooAPI {
	String name;
	String slug;
	String type;
	String description;
	String sku;
	String price;
	String regular_price;
	String sale_price;
	int stock_quantity;
	String permalink;
	String weight;
	String[] variations;
	List<CategoriesWoo> categories;
	List<ImageWoo> images;
	List<Attribute> attributes;
	List<MetaWoo> meta_data;
	String stock_status;
	String status;
	
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getPermalink() {
		return permalink;
	}
	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSlug() {
		return slug;
	}
	public void setSlug(String slug) {
		this.slug = slug;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getRegular_price() {
		return regular_price;
	}
	public void setRegular_price(String regular_price) {
		this.regular_price = regular_price;
	}
	public String getSale_price() {
		return sale_price;
	}
	public void setSale_price(String sale_price) {
		this.sale_price = sale_price;
	}
	public int getStock_quantity() {
		return stock_quantity;
	}
	public void setStock_quantity(int stock_quantity) {
		this.stock_quantity = stock_quantity;
	}
	public List<ImageWoo> getImages() {
		return images;
	}
	public void setImages(List<ImageWoo> images) {
		this.images = images;
	}
	public List<CategoriesWoo> getCategories() {
		return categories;
	}
	public void setCategories(List<CategoriesWoo> categories) {
		this.categories = categories;
	}
	public String[] getVariations() {
		return variations;
	}
	public void setVariations(String[] variations) {
		this.variations = variations;
	}
	public List<Attribute> getAttributes() {
		return attributes;
	}
	public void setAttributes(List<Attribute> attributes) {
		this.attributes = attributes;
	}
	public List<MetaWoo> getMeta_data() {
		return meta_data;
	}
	public void setMeta_data(List<MetaWoo> meta_data) {
		this.meta_data = meta_data;
	}
	public String getStock_status() {
		return stock_status;
	}
	public void setStock_status(String stock_status) {
		this.stock_status = stock_status;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
