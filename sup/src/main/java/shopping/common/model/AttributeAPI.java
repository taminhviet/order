package shopping.common.model;

import java.util.List;

public class AttributeAPI {
	private String name;
	private boolean visible;
	private List<String> options;
	private boolean variation;
	
	public AttributeAPI() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getOption() {
		return options;
	}

	public void setOption(List<String> option) {
		this.options = option;
	}

	public AttributeAPI(String name, List<String> option) {
		super();
		this.name = name;
		this.options = option;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public boolean isVariation() {
		return variation;
	}

	public void setVariation(boolean variation) {
		this.variation = variation;
	}
	
	

}
