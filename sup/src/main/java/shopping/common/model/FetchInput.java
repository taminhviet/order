package shopping.common.model;

public class FetchInput {
	private String merchantid;
	private String feedid;
	
	public FetchInput() {
		super();
	}
	public FetchInput(String merchantid, String feedid) {
		super();
		this.merchantid = merchantid;
		this.feedid = feedid;
	}
	public String getMerchantid() {
		return merchantid;
	}
	public void setMerchantid(String merchantid) {
		this.merchantid = merchantid;
	}
	public String getFeedid() {
		return feedid;
	}
	public void setFeedid(String feedid) {
		this.feedid = feedid;
	}
	
	
}
