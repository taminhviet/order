package shopping.common.model;

public class Attribute {
	private int id;
	private String name;
	private String option;
	
	public Attribute(int id, String name, String option) {
		super();
		this.id = id;
		this.name = name;
		this.option = option;
	}
	
	public Attribute() {
		super();
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOption() {
		return option;
	}
	public void setOption(String option) {
		this.option = option;
	}
	
	
}
