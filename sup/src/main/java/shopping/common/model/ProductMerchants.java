package shopping.common.model;

public class ProductMerchants {
	private String offerID;
	private String title;
	private String description;
	private String link;
	private String imagelink;
	private String channel;
	private String conntentLanguage;
	private String targetCountry;
	private String gtin;
	private String price;
	
	
	
	public ProductMerchants() {
		super();
	}
	public ProductMerchants(String offerID, String title, String description, String link, String imagelink,
			String channel, String conntentLanguage, String targetCountry, String gtin, String price) {
		super();
		this.offerID = offerID;
		this.title = title;
		this.description = description;
		this.link = link;
		this.imagelink = imagelink;
		this.channel = channel;
		this.conntentLanguage = conntentLanguage;
		this.targetCountry = targetCountry;
		this.gtin = gtin;
		this.price = price;
	}
	public String getOfferID() {
		return offerID;
	}
	public void setOfferID(String offerID) {
		this.offerID = offerID;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getImagelink() {
		return imagelink;
	}
	public void setImagelink(String imagelink) {
		this.imagelink = imagelink;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getConntentLanguage() {
		return conntentLanguage;
	}
	public void setConntentLanguage(String conntentLanguage) {
		this.conntentLanguage = conntentLanguage;
	}
	public String getTargetCountry() {
		return targetCountry;
	}
	public void setTargetCountry(String targetCountry) {
		this.targetCountry = targetCountry;
	}
	public String getGtin() {
		return gtin;
	}
	public void setGtin(String gtin) {
		this.gtin = gtin;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	
	
}
