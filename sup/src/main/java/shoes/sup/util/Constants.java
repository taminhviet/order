package shoes.sup.util;

public class Constants {

	public static final int ROLE_ADMIN = 1;
	public static final int ROLE_SUPPLIER = 2;
	public static final String ADMIN = "ADMIN";
	public static final String SUPPLIER = "SUPPLIER";
	public static final String DEFAULT_AUTHEN = "admin";
	// contact page
	public static final String DEFAULT_EMAIL = "taminhviet@gmail.com";
	public static final String DEFAULT_PHONE = "(84) 973400809";
	public static final String DEFAULT_ADDRESS = "Viet Tri Phu Tho";

	// menu

	public static final String HYPHEN = "-";
	public static final String COMMA = ",";
	public static final String SPACE = " ";
	public static final String COLON = ":";
	public static final String SLASH = "/";

	public static final String BREAK_LINE = " <br> ";

	// status
	public static final String NEW_ORDER = "New order";
	public static final String DELIVERED = "Delivered";
	public static final String CANCEL_SHOPIFY = "Cancel";
	public static final String CANCEL_SUPPLIER = "Cancel_Supplier";
	public static final String RETURNED = "Returned";
	public static final String READY = "Ready";
	public static final String ON_HOLD = "On-Hold";

	// paypal
	public static final String INELIGIBLE = "Ineligible";

	// reason
	public static final String OUT_OF_STOCK = "Out of stock";
	public static final String DO_NOT_HAVE = "I don't have this model";
	public static final String NO_SIZE = "I don't have this size";
	public static final String LOW_QUALITY = "Low quality";
	public static final String OTHER = "Other";
	public static final String VN_REQUEST = "VN_REQUEST";

	// paypal
	public static final String DISPUTED = "disputed";
	public static final String EXECUTION_MODE = "live";
	public static final String PAYPAL_BALANCE = "https://api.paypal.com/v1/reporting/balances";
	public static final String LIST_DISPUTE = "https://api.paypal.com/v1/customer/disputes";
	public static final String TRACK_PP = "https://api.paypal.com/v1/shipping/trackers-batch";
	public static final String TRACK_PP_NEW = "https://api.paypal.com/v1/shipping/trackers/";
	public static final String SALE_PP = "https://api.paypal.com/v1/payments/sale/";
	public static final String CAPTURE_PP = "https://api.paypal.com/v2/payments/authorizations/";
	public static final String CAPTURE_DETAIL = "https://api.paypal.com/v2/payments/captures/";
	public static final String REFUND_PP = "https://api.paypal.com/v2/payments/captures/";
	public static final String AUTHOR = "Authorization";
	public static final String CONTENT_TYPE = "Content-Type";
	public static final String APPLICATION_JSON = "application/json";
	public static final String MULTIPART_RELATED = "multipart/form-data";
	public static final String GET_METHOD = "GET";
	public static final String POST_METHOD = "POST";
	public static final String DISPUTE_RESOLVED = "RESOLVED";
	public static final String DISPUTE_APPEALABLE = "APPEALABLE";
	public static final String RETURN_NOTE = "Offer refund with return item.";
	public static final String REFUND_WITH_RETURN = "REFUND_WITH_RETURN";
	public static final String MERCHANDISE_OR_SERVICE_NOT_AS_DESCRIBED = "MERCHANDISE_OR_SERVICE_NOT_AS_DESCRIBED";
	public static final String acknowledge_return_item = "acknowledge_return_item";
	public static final String CUSTOMER_RETURNED = "CUSTOMER RETUNRED";
	public static final String SHIPPED = "SHIPPED";
	public static final String WAITING_FOR_SELLER_RESPONSE = "WAITING_FOR_SELLER_RESPONSE";
	public static final String OPEN_INQUIRIES = "OPEN_INQUIRIES";
	public static final String OPEN = "OPEN";
	public static final String PP_LIVE = "LIVE";
	public static final String PP_LIMIT = "LIMIT";
	public static final String RESOLVED_BUYER_FAVOUR = "RESOLVED_BUYER_FAVOUR";

	public static final int SHIPPING_FEE = 10;
	public static final String NO_WWW = "NO";
	public static final String FALSE_WWW = "FALSE";

	// Store type
	public static final int SHOPIFY = 1;
	public static final int WOO_COMMERCE = 2;
	public static final String SNEAKER = "SNEAKER";
	public static final String TOY = "TOY";
	public static final String PAID = "PAID";
	public static final String REFUNDED = "REFUNDED";
	public static final String DEACTIVE = "DEACTIVE";
	public static final String ACTIVE = "ACTIVE";
	public static final String FULFILLED = "FULFILLED";
	public static final String CAPTURED = "CAPTURED";
	public static final String SUCCESSED = "SUCCESSED";
	public static final String COMPLETED = "COMPLETED";
	public static final String CAPTURED_FAILED = "CAPTURED_FAILED";
	public static final String PAYER_CANNOT_PAY = "PAYER_CANNOT_PAY";
	public static final String INCORRECT_PAYPAL = "INCORRECT PAYPAL OR TRANSACTION";
	public static final String WAIT_21 = "WAIT_21";
	public static final String READY_QUEUE = "READY_QUEUE";
	public static final String DAYS_180 = "DAYS_180";
	
	public static final String WOO_PP_ID = "ppec_paypal";
	public static final String WOO_PPCP_GW = "ppcp-gateway";
	public static final String WOO_PP_GW = "payment_gateways";
	public static final String STRIPE = "Stripe";
	public static final String PUBLISHABLE_KEY = "publishable_key";
	public static final int SPS = 1;
	public static final int WOO = 2;
	public static final int VARIANT = 2;
	public static final int PARENT = 1;

	// status order woo
	public static final String WOO_ON_HOLD = "on-hold";
	public static final String WOO_CANCELLED = "cancelled";
	public static final String WOO_PROCESSING = "processing";
	public static final String WOO_REFUNDED = "refunded";
	public static final String WOO_FAILED = "failed";
	public static final String WOO_COMPLETED = "completed";

	// list supplier US
	public static final String Fedex = "Fedex";
	public static final String FedEx_Sameday = "FedEx Sameday";
	public static final String USPS = "USPS";
	public static final String DHL_US = "DHL US";
	public static final String DHL_Parcel = "DHL Parcel";
	public static final String TNT = "TNT";
	public static final String DHL_Inraship_DE = "DHL Intraship (DE)";
	
	//list supplier admin
	public static final String australia_post = "australia-post";
	public static final String evri = "evri";
	public static final String royal_mail = "royal-mail";
	public static final String usps = "usps";
	public static final String dhl = "dhl";
	public static final String dhl_paket = "dhl-paket";
	public static final String fedex = "fedex";
	public static final String canada_post = "canada-post";
	public static final String la_poste = "la-poste";
	public static final String ctt_express_spain = "ctt-express-spain";
	

	// list carrier PP
	public static final String PLEASE_SELECT = "PLEASE_SELECT";
	public static final String FEDEX = "FEDEX";
	public static final String DE_DHL= "DE_DHL";
	public static final String UPS = "UPS";
	public static final String EMS = "EMS";
	public static final String AIRBORNE_EXPRESS = "AIRBORNE_EXPRESS";
	public static final String DHL = "DHL";
	public static final String AIRSURE = "AIRSURE";
	public static final String ROYAL_MAIL = "ROYAL_MAIL";
	public static final String EVRi = "MYHERMES_UK_API";
	public static final String YODEL = "YODEL";
	public static final String PARCELFORCE = "PARCELFORCE";
	public static final String ARAMEX = "ARAMEX";
	public static final String SWIFTAIR = "SWIFTAIR";
	public static final String UK_PARCELFORCE = "UK_PARCELFORCE";
	public static final String UK_ROYALMAIL_SPECIAL = "UK_ROYALMAIL_SPECIAL";
	public static final String UK_ROYALMAIL_RECORDED = "UK_ROYALMAIL_RECORDED";
	public static final String UK_ROYALMAIL_INT_SIGNED = "UK_ROYALMAIL_INT_SIGNED";
	public static final String UK_ROYALMAIL_AIRSURE = "UK_ROYALMAIL_AIRSURE";
	public static final String UK_UPS = "UK_UPS";
	public static final String UK_FEDEX = "UK_FEDEX";
	public static final String UK_AIRBORNE_EXPRESS = "UK_AIRBORNE_EXPRESS";
	public static final String UK_DHL = "UK_DHL";
	public static final String UK_OTHER = "UK_OTHER";
	public static final String UK_CANNOT_PROV_TRACK = "UK_CANNOT_PROV_TRACK";
	public static final String CA_CANADA_POST = "CA_CANADA_POST";
	public static final String CA_PUROLATOR = "CA_PUROLATOR";
	public static final String CA_CANPAR = "CA_CANPAR";
	public static final String CA_LOOMIS = "CA_LOOMIS";
	public static final String CA_TNT = "CA_TNT";
	public static final String CA_OTHER = "CA_OTHER";
	public static final String CA_CANNOT_PROV_TRACK = "CA_CANNOT_PROV_TRACK";
	public static final String DE_DP_DHL_WITHIN_EUROPE = "DE_DP_DHL_WITHIN_EUROPE";
	public static final String DE_DP_DHL_T_AND_T_EXPRESS = "DE_DP_DHL_T_AND_T_EXPRESS";
	public static final String DE_DHL_DP_INTL_SHIPMENTS = "DE_DHL_DP_INTL_SHIPMENTS";
	public static final String DE_GLS = "DE_GLS";
	public static final String DE_DPD_DELISTACK = "DE_DPD_DELISTACK";
	public static final String DE_HERMES = "DE_HERMES";
	public static final String DE_UPS = "DE_UPS";
	public static final String DE_FEDEX = "DE_FEDEX";
	public static final String DE_TNT = "DE_TNT";
	public static final String DE_OTHER = "DE_OTHER";
	public static final String FR_CHRONOPOST = "FR_CHRONOPOST";
	public static final String FR_COLIPOSTE = "FR_COLIPOSTE";
	public static final String FR_DHL = "FR_DHL";
	public static final String LAPOSTE = "LAPOSTE";
	public static final String FR_UPS = "FR_UPS";
	public static final String FR_FEDEX = "FR_FEDEX";
	public static final String FR_TNT = "FR_TNT";
	public static final String FR_GLS = "FR_GLS";
	public static final String FR_OTHER = "FR_OTHER";
	public static final String IT_POSTE_ITALIA = "IT_POSTE_ITALIA";
	public static final String IT_DHL = "IT_DHL";
	public static final String IT_UPS = "IT_UPS";
	public static final String IT_FEDEX = "IT_FEDEX";
	public static final String IT_TNT = "IT_TNT";
	public static final String IT_GLS = "IT_GLS";
	public static final String IT_OTHER = "IT_OTHER";
	public static final String AU_AUSTRALIA_POST_EP_PLAT = "AU_AUSTRALIA_POST_EP_PLAT";
	public static final String AU_AUSTRALIA_POST_EPARCEL = "AU_AUSTRALIA_POST_EPARCEL";
	public static final String AU_AUSTRALIA_POST_EMS = "AU_AUSTRALIA_POST_EMS";
	public static final String AU_AUSTRALIA_POST = "AUSTRALIA_POST_API";
	public static final String AU_DHL = "AU_DHL";
	public static final String AU_STAR_TRACK_EXPRESS = "AU_STAR_TRACK_EXPRESS";
	public static final String AU_UPS = "AU_UPS";
	public static final String AU_FEDEX = "AU_FEDEX";
	public static final String AU_TNT = "AU_TNT";
	public static final String AU_TOLL_IPEC = "AU_TOLL_IPEC";
	public static final String AU_OTHER = "AU_OTHER";
	public static final String FR_SUIVI = "FR_SUIVI";
	public static final String IT_EBOOST_SDA = "IT_EBOOST_SDA";
	public static final String ES_CORREOS_DE_ESPANA = "ES_CORREOS_DE_ESPANA";
	public static final String ES_DHL = "ES_DHL";
	public static final String ES_UPS = "ES_UPS";
	public static final String ES_FEDEX = "ES_FEDEX";
	public static final String ES_TNT = "ES_TNT";
	public static final String ES_OTHER = "ES_OTHER";
	public static final String AT_AUSTRIAN_POST_EMS = "AT_AUSTRIAN_POST_EMS";
	public static final String AT_AUSTRIAN_POST_PPRIME = "AT_AUSTRIAN_POST_PPRIME";
	public static final String CN_CHINA_POST_EMS = "EMS_CN";
	public static final String CN_FEDEX = "CN_FEDEX";
	public static final String CN_TNT = "CN_TNT";
	public static final String CN_UPS = "CN_UPS";
	public static final String CN_OTHER = "CN_OTHER";
	public static final String FOUR_PX_EXPRESS = "FOUR_PX_EXPRESS";

	public static final String EMPTY = "";
	public static final String FAILED = "FAILED";
	public static final String SUCCESS = "SUCCESS";
	public static final String REVIEWED = "REVIEWED";
	public static final String REFUND_MESSAGE = "Full refund to the customer.";
	public static final int PREFIX = 4;
	
	//status GMC
	public static final String ENABLE = "ENABLE";
	public static final String ENABLED = "ENABLED";
	public static final String APPROVED = "APPROVED";
	public static final String PENDING_REVIEW = "PENDING_REVIEW";
	public static final String DISAPPROVED = "DISAPPROVED";
	public static final String INELIGIBLE_GMC = "INELIGIBLE";
	public static final String ELIGIBLE = "ELIGIBLE";
	public static final String DISABLE = "DISABLE";
	public static final String SUSPENDED = "suspended";
	public static final String REACTIVE = "REACTIVE";
	public static final String NO_ADS = "No Google Ads account linked";
	public static final String MISPRESENTATION = "Misrepresentation";
	public static final String FEEDED1ST = "Feeded TM 1st";
	public static final String FEEDED = "Feeded";
	public static final String FEEDEDTM = "Feeded TM Redirect";
	public static final String CHANNEL = "online";
	public static final String CONTENT_LANGUAGE = "en";
	public static final String FREE_LISTING = "1";
	public static final String GOOGLE_ADS = "2";
	
	//Shopify
	public static final String X_Shopify_Access_Token = "X-Shopify-Access-Token";
	public static final String ACTIVE_PRODUCT = "active";
	public static final String SHOPIFY_API_VERSION = "2023-04";
	
	//ProductWoo Attribute
	public static final String VARIABLE = "variable";
	public static final String WHITE_COLOR = "white";
	public static final String GTIN = "gtin";
	public static final String INSTOCK = "instock";
	public static final String PUBLISH = "publish";
	public static final String SIMPLE = "simple";
	public static final String ADULT = "adult";
	public static final String UNISEX = "unisex";
	public static final String US = "US";
	public static final String FR = "FR";
	public static final String DE = "DE";
	public static final String AU = "AU";
	public static final String GB = "GB";
	public static final String USD = "USD";
	public static final String GBP = "GBP";
	public static final String AUD = "AUD";
	public static final String EUR = "EUR";
}
