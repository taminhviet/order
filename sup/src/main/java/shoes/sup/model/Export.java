package shoes.sup.model;

public class Export {
	private String handle;
	private String title;
	private String body;
	private String vendor;
	private String type;
	private String published;
	private String option1name;
	private String option1value;
	private String variantSKU;
	private String variantTracker;
	private String quantity;
	private String variantPolicy;
	private String variantFulfillment;
	private String price;
	private String compare;
	private String requireShipping;
	private String required_options;
	private String variantTax;
	private String variantBarcode;
	private String imageURL;
	private String ImagePosition;
	private String weightUnit;
	private String store;
	private String website;
	private String attSet;
	private String type_simple;
	private String categoryId;
	private String hasOption;
	private String metatitle;
	private String meta_description;
	private String image;
	private String small_image;
	private String thumbnail;
	private String url_key;
	private String url_path;
	private String custom_design;
	private String page_layout;
	private String options_container;
	private String image_label;
	private String small_image_label;
	private String thumbnail_label;
	private String country_of_manufacture;
	private String msrp_enabled;
	private String msrp_display_actual_price_type;
	private String gift_message_available;
	private String googleshopping_category;
	private String msrp;
	private String special_price;
	private String weight;
	private String manufacturer;
	private String status;
	private String is_recurring;
	private String visibility;
	private String tax_class_id;
	private String googleshopping_exclude;
	private String googleshopping_condition;
	private String  description;
	private String short_description;
	private String meta_keyword;
	private String custom_layout_update;
	private String special_from_date;
	private String special_to_date;
	private String news_from_date;
	private String news_to_date;
	private String custom_design_from;
	private String custom_design_to;
	private String qty;
	private String min_qty;
	private String use_config_min_qty;
	private String is_qty_decimal;
	private String backorders;
	private String use_config_backorders;
	private String min_sale_qty;
	private String use_config_min_sale_qty;
	private String max_sale_qty;
	private String use_config_max_sale_qty;
	private String is_in_stock;
	private String low_stock_date;
	private String notify_stock_qty;
	private String use_config_notify_stock_qty;
	private String manage_stock;
	private String use_config_manage_stock;
	private String stock_status_changed_auto;
	private String use_config_qty_increments;
	private String qty_increments;
	private String use_config_enable_qty_inc;
	private String enable_qty_increments;
	private String is_decimal_divided;
	private String stock_status_changed_automatically;
	private String use_config_enable_qty_increments;
	private String product_name;
	private String store_id;
	private String product_type_id;
	private String product_status_changed;
	private String product_changed_websites;
	private String _media_image;
	private String _media_lable;
	private String _media_position;
	private String _media_is_disabled;
	private String media_attribute_id;
	private String _root_category;
	private String woocommerce;
	
	
	public Export() {
		super();
	}
	public String getHandle() {
		return handle;
	}
	public void setHandle(String handle) {
		this.handle = handle;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getPublished() {
		return published;
	}
	public void setPublished(String published) {
		this.published = published;
	}
	public String getOption1name() {
		return option1name;
	}
	public void setOption1name(String option1name) {
		this.option1name = option1name;
	}
	public String getOption1value() {
		return option1value;
	}
	public void setOption1value(String option1value) {
		this.option1value = option1value;
	}
	public String getVariantSKU() {
		return variantSKU;
	}
	public void setVariantSKU(String variantSKU) {
		this.variantSKU = variantSKU;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getVariantPolicy() {
		return variantPolicy;
	}
	public void setVariantPolicy(String variantPolicy) {
		this.variantPolicy = variantPolicy;
	}
	public String getVariantFulfillment() {
		return variantFulfillment;
	}
	public void setVariantFulfillment(String variantFulfillment) {
		this.variantFulfillment = variantFulfillment;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getCompare() {
		return compare;
	}
	public void setCompare(String compare) {
		this.compare = compare;
	}
	public String isRequireShipping() {
		return requireShipping;
	}
	public void setRequireShipping(String requireShipping) {
		this.requireShipping = requireShipping;
	}
	public String isVariantTax() {
		return variantTax;
	}
	public void setVariantTax(String variantTax) {
		this.variantTax = variantTax;
	}
	public String getVariantBarcode() {
		return variantBarcode;
	}
	public void setVariantBarcode(String variantBarcode) {
		this.variantBarcode = variantBarcode;
	}
	public String getImageURL() {
		return imageURL;
	}
	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}
	public String getImagePosition() {
		return ImagePosition;
	}
	public void setImagePosition(String imagePosition) {
		ImagePosition = imagePosition;
	}
	public String getWeightUnit() {
		return weightUnit;
	}
	public void setWeightUnit(String weightUnit) {
		this.weightUnit = weightUnit;
	}
	public String getVariantTracker() {
		return variantTracker;
	}
	public void setVariantTracker(String variantTracker) {
		this.variantTracker = variantTracker;
	}
	public String getStore() {
		return store;
	}
	public void setStore(String store) {
		this.store = store;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getAttSet() {
		return attSet;
	}
	public void setAttSet(String attSet) {
		this.attSet = attSet;
	}
	public String getType_simple() {
		return type_simple;
	}
	public void setType_simple(String type_simple) {
		this.type_simple = type_simple;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getHasOption() {
		return hasOption;
	}
	public void setHasOption(String hasOption) {
		this.hasOption = hasOption;
	}
	public String getMetatitle() {
		return metatitle;
	}
	public void setMetatitle(String metatitle) {
		this.metatitle = metatitle;
	}
	public String getMeta_description() {
		return meta_description;
	}
	public void setMeta_description(String meta_description) {
		this.meta_description = meta_description;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getSmall_image() {
		return small_image;
	}
	public void setSmall_image(String small_image) {
		this.small_image = small_image;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	public String getUrl_key() {
		return url_key;
	}
	public void setUrl_key(String url_key) {
		this.url_key = url_key;
	}
	public String getUrl_path() {
		return url_path;
	}
	public void setUrl_path(String url_path) {
		this.url_path = url_path;
	}
	public String getCustom_design() {
		return custom_design;
	}
	public void setCustom_design(String custom_design) {
		this.custom_design = custom_design;
	}
	public String getPage_layout() {
		return page_layout;
	}
	public void setPage_layout(String page_layout) {
		this.page_layout = page_layout;
	}
	public String getOptions_container() {
		return options_container;
	}
	public void setOptions_container(String options_container) {
		this.options_container = options_container;
	}
	public String getImage_label() {
		return image_label;
	}
	public void setImage_label(String image_label) {
		this.image_label = image_label;
	}
	public String getSmall_image_label() {
		return small_image_label;
	}
	public void setSmall_image_label(String small_image_label) {
		this.small_image_label = small_image_label;
	}
	public String getThumbnail_label() {
		return thumbnail_label;
	}
	public void setThumbnail_label(String thumbnail_label) {
		this.thumbnail_label = thumbnail_label;
	}
	public String getCountry_of_manufacture() {
		return country_of_manufacture;
	}
	public void setCountry_of_manufacture(String country_of_manufacture) {
		this.country_of_manufacture = country_of_manufacture;
	}
	public String getMsrp_enabled() {
		return msrp_enabled;
	}
	public void setMsrp_enabled(String msrp_enabled) {
		this.msrp_enabled = msrp_enabled;
	}
	public String getMsrp_display_actual_price_type() {
		return msrp_display_actual_price_type;
	}
	public void setMsrp_display_actual_price_type(
			String msrp_display_actual_price_type) {
		this.msrp_display_actual_price_type = msrp_display_actual_price_type;
	}
	public String getGift_message_available() {
		return gift_message_available;
	}
	public void setGift_message_available(String gift_message_available) {
		this.gift_message_available = gift_message_available;
	}
	public String getGoogleshopping_category() {
		return googleshopping_category;
	}
	public void setGoogleshopping_category(String googleshopping_category) {
		this.googleshopping_category = googleshopping_category;
	}
	public String getMsrp() {
		return msrp;
	}
	public void setMsrp(String msrp) {
		this.msrp = msrp;
	}
	public String getSpecial_price() {
		return special_price;
	}
	public void setSpecial_price(String special_price) {
		this.special_price = special_price;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getIs_recurring() {
		return is_recurring;
	}
	public void setIs_recurring(String is_recurring) {
		this.is_recurring = is_recurring;
	}
	public String getVisibility() {
		return visibility;
	}
	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}
	public String getTax_class_id() {
		return tax_class_id;
	}
	public void setTax_class_id(String tax_class_id) {
		this.tax_class_id = tax_class_id;
	}
	public String getGoogleshopping_exclude() {
		return googleshopping_exclude;
	}
	public void setGoogleshopping_exclude(String googleshopping_exclude) {
		this.googleshopping_exclude = googleshopping_exclude;
	}
	public String getGoogleshopping_condition() {
		return googleshopping_condition;
	}
	public void setGoogleshopping_condition(String googleshopping_condition) {
		this.googleshopping_condition = googleshopping_condition;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getShort_description() {
		return short_description;
	}
	public void setShort_description(String short_description) {
		this.short_description = short_description;
	}
	public String getMeta_keyword() {
		return meta_keyword;
	}
	public void setMeta_keyword(String meta_keyword) {
		this.meta_keyword = meta_keyword;
	}
	public String getCustom_layout_update() {
		return custom_layout_update;
	}
	public void setCustom_layout_update(String custom_layout_update) {
		this.custom_layout_update = custom_layout_update;
	}
	public String getSpecial_from_date() {
		return special_from_date;
	}
	public void setSpecial_from_date(String special_from_date) {
		this.special_from_date = special_from_date;
	}
	public String getSpecial_to_date() {
		return special_to_date;
	}
	public void setSpecial_to_date(String special_to_date) {
		this.special_to_date = special_to_date;
	}
	public String getNews_from_date() {
		return news_from_date;
	}
	public void setNews_from_date(String news_from_date) {
		this.news_from_date = news_from_date;
	}
	public String getNews_to_date() {
		return news_to_date;
	}
	public void setNews_to_date(String news_to_date) {
		this.news_to_date = news_to_date;
	}
	public String getCustom_design_from() {
		return custom_design_from;
	}
	public void setCustom_design_from(String custom_design_from) {
		this.custom_design_from = custom_design_from;
	}
	public String getCustom_design_to() {
		return custom_design_to;
	}
	public void setCustom_design_to(String custom_design_to) {
		this.custom_design_to = custom_design_to;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getMin_qty() {
		return min_qty;
	}
	public void setMin_qty(String min_qty) {
		this.min_qty = min_qty;
	}
	public String getUse_config_min_qty() {
		return use_config_min_qty;
	}
	public void setUse_config_min_qty(String use_config_min_qty) {
		this.use_config_min_qty = use_config_min_qty;
	}
	public String getIs_qty_decimal() {
		return is_qty_decimal;
	}
	public void setIs_qty_decimal(String is_qty_decimal) {
		this.is_qty_decimal = is_qty_decimal;
	}
	public String getBackorders() {
		return backorders;
	}
	public void setBackorders(String backorders) {
		this.backorders = backorders;
	}
	public String getUse_config_backorders() {
		return use_config_backorders;
	}
	public void setUse_config_backorders(String use_config_backorders) {
		this.use_config_backorders = use_config_backorders;
	}
	public String getMin_sale_qty() {
		return min_sale_qty;
	}
	public void setMin_sale_qty(String min_sale_qty) {
		this.min_sale_qty = min_sale_qty;
	}
	public String getUse_config_min_sale_qty() {
		return use_config_min_sale_qty;
	}
	public void setUse_config_min_sale_qty(String use_config_min_sale_qty) {
		this.use_config_min_sale_qty = use_config_min_sale_qty;
	}
	public String getMax_sale_qty() {
		return max_sale_qty;
	}
	public void setMax_sale_qty(String max_sale_qty) {
		this.max_sale_qty = max_sale_qty;
	}
	public String getUse_config_max_sale_qty() {
		return use_config_max_sale_qty;
	}
	public void setUse_config_max_sale_qty(String use_config_max_sale_qty) {
		this.use_config_max_sale_qty = use_config_max_sale_qty;
	}
	public String getIs_in_stock() {
		return is_in_stock;
	}
	public void setIs_in_stock(String is_in_stock) {
		this.is_in_stock = is_in_stock;
	}
	public String getLow_stock_date() {
		return low_stock_date;
	}
	public void setLow_stock_date(String low_stock_date) {
		this.low_stock_date = low_stock_date;
	}
	public String getNotify_stock_qty() {
		return notify_stock_qty;
	}
	public void setNotify_stock_qty(String notify_stock_qty) {
		this.notify_stock_qty = notify_stock_qty;
	}
	public String getUse_config_notify_stock_qty() {
		return use_config_notify_stock_qty;
	}
	public void setUse_config_notify_stock_qty(String use_config_notify_stock_qty) {
		this.use_config_notify_stock_qty = use_config_notify_stock_qty;
	}
	public String getManage_stock() {
		return manage_stock;
	}
	public void setManage_stock(String manage_stock) {
		this.manage_stock = manage_stock;
	}
	public String getUse_config_manage_stock() {
		return use_config_manage_stock;
	}
	public void setUse_config_manage_stock(String use_config_manage_stock) {
		this.use_config_manage_stock = use_config_manage_stock;
	}
	public String getStock_status_changed_auto() {
		return stock_status_changed_auto;
	}
	public void setStock_status_changed_auto(String stock_status_changed_auto) {
		this.stock_status_changed_auto = stock_status_changed_auto;
	}
	public String getUse_config_qty_increments() {
		return use_config_qty_increments;
	}
	public void setUse_config_qty_increments(String use_config_qty_increments) {
		this.use_config_qty_increments = use_config_qty_increments;
	}
	public String getQty_increments() {
		return qty_increments;
	}
	public void setQty_increments(String qty_increments) {
		this.qty_increments = qty_increments;
	}
	public String getUse_config_enable_qty_inc() {
		return use_config_enable_qty_inc;
	}
	public void setUse_config_enable_qty_inc(String use_config_enable_qty_inc) {
		this.use_config_enable_qty_inc = use_config_enable_qty_inc;
	}
	public String getEnable_qty_increments() {
		return enable_qty_increments;
	}
	public void setEnable_qty_increments(String enable_qty_increments) {
		this.enable_qty_increments = enable_qty_increments;
	}
	public String getIs_decimal_divided() {
		return is_decimal_divided;
	}
	public void setIs_decimal_divided(String is_decimal_divided) {
		this.is_decimal_divided = is_decimal_divided;
	}
	public String getStock_status_changed_automatically() {
		return stock_status_changed_automatically;
	}
	public void setStock_status_changed_automatically(
			String stock_status_changed_automatically) {
		this.stock_status_changed_automatically = stock_status_changed_automatically;
	}
	public String getUse_config_enable_qty_increments() {
		return use_config_enable_qty_increments;
	}
	public void setUse_config_enable_qty_increments(
			String use_config_enable_qty_increments) {
		this.use_config_enable_qty_increments = use_config_enable_qty_increments;
	}
	public String getProduct_name() {
		return product_name;
	}
	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}
	public String getStore_id() {
		return store_id;
	}
	public void setStore_id(String store_id) {
		this.store_id = store_id;
	}
	public String getProduct_type_id() {
		return product_type_id;
	}
	public void setProduct_type_id(String product_type_id) {
		this.product_type_id = product_type_id;
	}
	public String getProduct_status_changed() {
		return product_status_changed;
	}
	public void setProduct_status_changed(String product_status_changed) {
		this.product_status_changed = product_status_changed;
	}
	public String getProduct_changed_websites() {
		return product_changed_websites;
	}
	public void setProduct_changed_websites(String product_changed_websites) {
		this.product_changed_websites = product_changed_websites;
	}
	public String get_media_image() {
		return _media_image;
	}
	public void set_media_image(String _media_image) {
		this._media_image = _media_image;
	}
	public String get_media_lable() {
		return _media_lable;
	}
	public void set_media_lable(String _media_lable) {
		this._media_lable = _media_lable;
	}
	public String get_media_position() {
		return _media_position;
	}
	public void set_media_position(String _media_position) {
		this._media_position = _media_position;
	}
	public String get_media_is_disabled() {
		return _media_is_disabled;
	}
	public void set_media_is_disabled(String _media_is_disabled) {
		this._media_is_disabled = _media_is_disabled;
	}
	public String getRequireShipping() {
		return requireShipping;
	}
	public String getVariantTax() {
		return variantTax;
	}
	public String getMedia_attribute_id() {
		return media_attribute_id;
	}
	public void setMedia_attribute_id(String media_attribute_id) {
		this.media_attribute_id = media_attribute_id;
	}
	public String get_root_category() {
		return _root_category;
	}
	public void set_root_category(String _root_category) {
		this._root_category = _root_category;
	}
	public String getRequired_options() {
		return required_options;
	}
	public void setRequired_options(String required_options) {
		this.required_options = required_options;
	}
	public String getWoocommerce() {
		return woocommerce;
	}
	public void setWoocommerce(String woocommerce) {
		this.woocommerce = woocommerce;
	}
	
}
