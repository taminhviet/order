package shoes.sup.model;

public class OfferReturn {

	private String note;
	private ReturnShippingAddress return_shipping_address;
	String offer_type;
//	private OfferAmount offer_amount;
//	private Money money;
//	private Refund_Amount refund_amount;
	
	public OfferReturn() {
		super();
	}

	
//	public Refund_Amount getRefund_amount() {
//		return refund_amount;
//	}
//
//
//
//	public void setRefund_amount(Refund_Amount refund_amount) {
//		this.refund_amount = refund_amount;
//	}



//	public Money getMoney() {
//		return money;
//	}
//
//
//	public void setMoney(Money money) {
//		this.money = money;
//	}


//	public OfferAmount getOffer_amount() {
//		return offer_amount;
//	}
//
//
//	public void setOffer_amount(OfferAmount offer_amount) {
//		this.offer_amount = offer_amount;
//	}


	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public ReturnShippingAddress getReturn_shipping_address() {
		return return_shipping_address;
	}
	public void setReturn_shipping_address(
			ReturnShippingAddress return_shipping_address) {
		this.return_shipping_address = return_shipping_address;
	}
	public String getOffer_type() {
		return offer_type;
	}
	public void setOffer_type(String offer_type) {
		this.offer_type = offer_type;
	}
	
	
}
