package shoes.sup.model;

import java.util.List;

public class ProductWoo {
	int id;
	String name;
	String slug;
	String type;
	String description;
	String sku;
	String price;
	String regular_price;
	String sale_price;
	int stock_quantity;
	List<CategoriesWoo> categories;
	List<ImageWoo> images;
	String stock_status;
	String status;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSlug() {
		return slug;
	}
	public void setSlug(String slug) {
		this.slug = slug;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getRegular_price() {
		return regular_price;
	}
	public void setRegular_price(String regular_price) {
		this.regular_price = regular_price;
	}
	public String getSale_price() {
		return sale_price;
	}
	public void setSale_price(String sale_price) {
		this.sale_price = sale_price;
	}
	public int getStock_quantity() {
		return stock_quantity;
	}
	public void setStock_quantity(int stock_quantity) {
		this.stock_quantity = stock_quantity;
	}
	public List<ImageWoo> getImages() {
		return images;
	}
	public void setImages(List<ImageWoo> images) {
		this.images = images;
	}
	public List<CategoriesWoo> getCategories() {
		return categories;
	}
	public void setCategories(List<CategoriesWoo> categories) {
		this.categories = categories;
	}
	public String getStock_status() {
		return stock_status;
	}
	public void setStock_status(String stock_status) {
		this.stock_status = stock_status;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}

