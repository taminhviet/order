package shoes.sup.model;

public class Transaction {
	
	long id;
	long order_id;
	String gateway;
	ReceiptAPI receipt;
	
	public Transaction() {
		super();
	}
	public Transaction(long id, long order_id, String gateway,
			ReceiptAPI receipt) {
		super();
		this.id = id;
		this.order_id = order_id;
		this.gateway = gateway;
		this.receipt = receipt;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getOrder_id() {
		return order_id;
	}
	public void setOrder_id(long order_id) {
		this.order_id = order_id;
	}
	public String getGateway() {
		return gateway;
	}
	public void setGateway(String gateway) {
		this.gateway = gateway;
	}
	public ReceiptAPI getReceipt() {
		return receipt;
	}
	public void setReceipt(ReceiptAPI receipt) {
		this.receipt = receipt;
	}
}
