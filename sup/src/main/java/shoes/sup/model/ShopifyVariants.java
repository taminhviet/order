package shoes.sup.model;

import java.math.BigInteger;

public class ShopifyVariants {
	private BigInteger product_id;
	private BigInteger id;
	private String title;
	private String price;
	private String sku;
	private int position;
	private String inventory_management;
	private String option1;
	private String option2;
	private String option3;
	private String barcode;
	private int inventory_quantity;
	private String admin_graphql_api_id;
	
	public ShopifyVariants() {
		super();
	}
	public BigInteger getProduct_id() {
		return product_id;
	}
	public void setProduct_id(BigInteger product_id) {
		this.product_id = product_id;
	}
	public BigInteger getId() {
		return id;
	}
	
	public String getAdmin_graphql_api_id() {
		return admin_graphql_api_id;
	}
	public void setAdmin_graphql_api_id(String admin_graphql_api_id) {
		this.admin_graphql_api_id = admin_graphql_api_id;
	}
	public void setId(BigInteger id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public String getOption1() {
		return option1;
	}
	public void setOption1(String option1) {
		this.option1 = option1;
	}
	public String getOption2() {
		return option2;
	}
	public void setOption2(String option2) {
		this.option2 = option2;
	}
	public String getOption3() {
		return option3;
	}
	public void setOption3(String option3) {
		this.option3 = option3;
	}
	public String getBarcode() {
		return barcode;
	}
	
	public String getInventory_management() {
		return inventory_management;
	}
	public void setInventory_management(String inventory_management) {
		this.inventory_management = inventory_management;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public int getInventory_quantity() {
		return inventory_quantity;
	}
	public void setInventory_quantity(int inventory_quantity) {
		this.inventory_quantity = inventory_quantity;
	}
	
	
}
