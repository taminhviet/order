package shoes.sup.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "store_table")
public class Store {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
	
	@Column(name="domain")
    private String domain;
	
	@Column(name="apikey")
    private String apikey;
	
	@Column(name="password")
    private String password;
	
	@Column(name="accesstoken")
    private String accesstoken;
	
	@Column(name = "type")
	private int type;
	
	@Column(name = "product")
	private String product;
	
	@Column(name = "state")
	private String state;
	
	@Column(name = "ppacc")
	private String ppacc;
	
	@Column(name = "thresholdpp")
	private String thresholdpp;
	
	@Column(name = "prefix")
	private String prefix;
	
	@Column(name="subdomainshopify")
    private String subdomainshopify;
	
	@Column(name = "w")
	private String w;
	

	public String getSubdomainshopify() {
		return subdomainshopify;
	}

	public void setSubdomainshopify(String subdomainshopify) {
		this.subdomainshopify = subdomainshopify;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Store() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getApikey() {
		return apikey;
	}

	public void setApikey(String apikey) {
		this.apikey = apikey;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPpacc() {
		return ppacc;
	}

	public void setPpacc(String ppacc) {
		this.ppacc = ppacc;
	}

	public String getThresholdpp() {
		return thresholdpp;
	}

	public void setThresholdpp(String thresholdpp) {
		this.thresholdpp = thresholdpp;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getW() {
		return w;
	}

	public void setW(String w) {
		this.w = w;
	}

	public String getAccesstoken() {
		return accesstoken;
	}

	public void setAccesstoken(String accesstoken) {
		this.accesstoken = accesstoken;
	}

}
