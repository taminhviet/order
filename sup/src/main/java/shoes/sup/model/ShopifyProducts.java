package shoes.sup.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ShopifyProducts")
public class ShopifyProducts {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
	
	@Column(name="handle")
	private String handle;
	
	@Column(name="title", length = 1000,columnDefinition="LONGTEXT")
	private String title;
	
	@Column(name="store", length = 1000,columnDefinition="LONGTEXT")
	private String store;
	
	@Column(name="description", length = 5000,columnDefinition="LONGTEXT")
	private String description;
	
	@Column(name="vendor")
	private String vendor;
	
	@Column(name="category")
	private String category;
	
	@Column(name="label")
	private String label;
	
	@Column(name="type")
	private String type;
	
	@Column(name="tags")
	private String tags;
	
	@Column(name="option1name")
	private String option1name;
	
	@Column(name="option1value")
	private String option1value;
	
	@Column(name="option2name")
	private String option2name;
	
	@Column(name="option2value")
	private String option2value;
	
	@Column(name="variantsku")
	private String variantsku;
	
	@Column(name="qty")
	private int qty;
	
	@Column(name="price")
	private double price;
	
	@Column(name="pricecompare")
	private double pricecompare;
	
	@Column(name="barcode")
	private String barcode;
	
	@Column(name="image")
	private String image;
	
	@Column(name="position")
	private String position;
	
	

	public ShopifyProducts(String handle, String title, String store, String description, String vendor,
			String category, String label, String type, String tags, String option1name, String option1value,String option2name, String option2value,
			String variantsku, int qty, double price, double pricecompare, String barcode, String image, String position) {
		super();
		this.handle = handle;
		this.title = title;
		this.store = store;
		this.description = description;
		this.vendor = vendor;
		this.category = category;
		this.label = label;
		this.type = type;
		this.tags = tags;
		this.option1name = option1name;
		this.option1value = option1value;
		this.option2name = option2name;
		this.option2value = option2value;
		this.variantsku = variantsku;
		this.qty = qty;
		this.price = price;
		this.pricecompare = pricecompare;
		this.barcode = barcode;
		this.image = image;
		this.position = position;
	}

	public ShopifyProducts() {
		super();
	}
	

	

	public ShopifyProducts(int id, String handle, String title, String store, String description, String vendor,
			String category, String type, String tags, String option1name, String option1value, String variantsku,
			int qty, double price, double pricecompare, String barcode, String image) {
		super();
		this.id = id;
		this.handle = handle;
		this.title = title;
		this.store = store;
		this.description = description;
		this.vendor = vendor;
		this.category = category;
		this.type = type;
		this.tags = tags;
		this.option1name = option1name;
		this.option1value = option1value;
		this.variantsku = variantsku;
		this.qty = qty;
		this.price = price;
		this.pricecompare = pricecompare;
		this.barcode = barcode;
		this.image = image;
	}
	
	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getHandle() {
		return handle;
	}

	public void setHandle(String handle) {
		this.handle = handle;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getOption1name() {
		return option1name;
	}

	public void setOption1name(String option1name) {
		this.option1name = option1name;
	}

	public String getOption1value() {
		return option1value;
	}

	public void setOption1value(String option1value) {
		this.option1value = option1value;
	}

	public String getVariantsku() {
		return variantsku;
	}

	public void setVariantsku(String variantsku) {
		this.variantsku = variantsku;
	}

	public int getQty() {
		return qty;
	}
	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}


	public void setQty(int qty) {
		this.qty = qty;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getPricecompare() {
		return pricecompare;
	}

	public void setPricecompare(double pricecompare) {
		this.pricecompare = pricecompare;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getOption2name() {
		return option2name;
	}

	public void setOption2name(String option2name) {
		this.option2name = option2name;
	}

	public String getOption2value() {
		return option2value;
	}

	public void setOption2value(String option2value) {
		this.option2value = option2value;
	}
	
}
