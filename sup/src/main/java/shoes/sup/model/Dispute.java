package shoes.sup.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dispute")
public class Dispute {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
	
	@Column(name="disputeid")
    private String disputeid;
	
	@Column(name = "reason")
	private String reason;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "dispute_state")
	private String dispute_state;
	
	@Column(name = "seller_response_due_date")
	private String seller_response_due_date;
	
	@Column(name = "seller_transaction_id")
	private String seller_transaction_id;
	
	@Column(name = "outcome_code")
	private String outcome_code;
	
	@Column(name = "currency")
	private String currency;
	
	@Column(name = "value")
	private String value;

	public Dispute() {
		super();
	}
	
	public String getCurrency() {
		return currency;
	}


	public void setCurrency(String currency) {
		this.currency = currency;
	}


	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDisputeid() {
		return disputeid;
	}

	public void setDisputeid(String disputeid) {
		this.disputeid = disputeid;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDispute_state() {
		return dispute_state;
	}

	public void setDispute_state(String dispute_state) {
		this.dispute_state = dispute_state;
	}

	public String getSeller_response_due_date() {
		return seller_response_due_date;
	}

	public void setSeller_response_due_date(String seller_response_due_date) {
		this.seller_response_due_date = seller_response_due_date;
	}

	public String getSeller_transaction_id() {
		return seller_transaction_id;
	}

	public void setSeller_transaction_id(String seller_transaction_id) {
		this.seller_transaction_id = seller_transaction_id;
	}

	public String getOutcome_code() {
		return outcome_code;
	}

	public void setOutcome_code(String outcome_code) {
		this.outcome_code = outcome_code;
	}
	
	
}
