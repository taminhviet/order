package shoes.sup.model;

public class CaptureFailDetail {

	String issue;
	String description;
	
	public CaptureFailDetail() {
		super();
	}
	public CaptureFailDetail(String issue, String description) {
		super();
		this.issue = issue;
		this.description = description;
	}
	public String getIssue() {
		return issue;
	}
	public void setIssue(String issue) {
		this.issue = issue;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
