package shoes.sup.model;

public class SaleAmount {

	String total;
	String currency;
	SaleAmountDetail sadetails;
	
	public SaleAmount() {
		super();
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public SaleAmountDetail getSadetails() {
		return sadetails;
	}
	public void setSadetails(SaleAmountDetail sadetails) {
		this.sadetails = sadetails;
	}
	
}