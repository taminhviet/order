package shoes.sup.model;

public class SaleAmountRefund {
	String total;
	String currency;
	
	public SaleAmountRefund(String total, String currency) {
		super();
		this.total = total;
		this.currency = currency;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
}