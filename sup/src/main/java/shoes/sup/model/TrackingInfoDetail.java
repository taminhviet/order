package shoes.sup.model;

public class TrackingInfoDetail {

	String carrier_name;
	String tracking_number;
	
	public TrackingInfoDetail(String carrier_name, String tracking_number) {
		super();
		this.carrier_name = carrier_name;
		this.tracking_number = tracking_number;
	}
	public String getCarrier_name() {
		return carrier_name;
	}
	public void setCarrier_name(String carrier_name) {
		this.carrier_name = carrier_name;
	}
	public String getTracking_number() {
		return tracking_number;
	}
	public void setTracking_number(String tracking_number) {
		this.tracking_number = tracking_number;
	}
	
}
