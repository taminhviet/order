package shoes.sup.model;

public class InputNotePP {

	String id;
	String notetext;
	
	
	public InputNotePP(String id, String notetext) {
		super();
		this.id = id;
		this.notetext = notetext;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNotetext() {
		return notetext;
	}
	public void setNotetext(String notetext) {
		this.notetext = notetext;
	}
	
	
}
