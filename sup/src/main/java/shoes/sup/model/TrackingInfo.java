package shoes.sup.model;

import java.util.List;

public class TrackingInfo {
	List<TrackingInfoDetail> tracking_info;
	
	public TrackingInfo() {
		super();
	}

	public List<TrackingInfoDetail> getTracking_info() {
		return tracking_info;
	}

	public void setTracking_info(List<TrackingInfoDetail> tracking_info) {
		this.tracking_info = tracking_info;
	}

	
}
