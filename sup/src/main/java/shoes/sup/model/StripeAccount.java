package shoes.sup.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "stripe")
public class StripeAccount {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
	
	@Column(name="key")
    private String key;
	
	@Column(name="secret")
    private String secret;
	
	@Column(name = "capturetotal")
	private double capturetotal;
	
	
	@Column(name="status")
    private String status;
	
	
	

	public StripeAccount() {
		super();
	}

	public double getCapturetotal() {
		return capturetotal;
	}

	public void setCapturetotal(double capturetotal) {
		this.capturetotal = capturetotal;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public StripeAccount(int id, String key, String secret, double capturetotal) {
		super();
		this.id = id;
		this.key = key;
		this.secret = secret;
		this.capturetotal = capturetotal;
	}

	public StripeAccount(int id, String key, String secret) {
		super();
		this.id = id;
		this.key = key;
		this.secret = secret;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}
	
	
}
