package shoes.sup.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "proxycheck")
public class ProxyCheck {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
	
	@Column(name="license")
    private String license;
	
	@Column(name="round")
    private int round;
	
	@Temporal(TemporalType.TIMESTAMP)
    Date createDate;
	

	public ProxyCheck() {
		super();
	}

	public ProxyCheck(String license, int round, Date createDate) {
		super();
		this.license = license;
		this.round = round;
		this.createDate = createDate;
	}


	public ProxyCheck(int id, String license, int round, Date createDate) {
		super();
		this.id = id;
		this.license = license;
		this.round = round;
		this.createDate = createDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}

	public int getRound() {
		return round;
	}

	public void setRound(int round) {
		this.round = round;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	
}
