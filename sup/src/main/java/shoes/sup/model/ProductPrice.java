package shoes.sup.model;

public class ProductPrice {

	private int id;
	private String regular_price;
	private int type;
	private String parent;
	
	public ProductPrice(int id, String regular_price) {
		super();
		this.id = id;
		this.regular_price = regular_price;
	}
	
	public ProductPrice(int id, String regular_price, int type) {
		super();
		this.id = id;
		this.regular_price = regular_price;
		this.type = type;
	}

	public ProductPrice(int id, String regular_price, int type, String parent) {
		super();
		this.id = id;
		this.regular_price = regular_price;
		this.type = type;
		this.parent = parent;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getRegular_price() {
		return regular_price;
	}
	public void setRegular_price(String regular_price) {
		this.regular_price = regular_price;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	} 
	
	
}
