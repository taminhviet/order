package shoes.sup.model;

import java.util.List;

public class LineItemWoo {

	int id;
	String name;
	int product_id;
	int variation_id;
	int quantity;
	List<MetaWoo> meta_data;
	double price;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getProduct_id() {
		return product_id;
	}
	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}
	public int getVariation_id() {
		return variation_id;
	}
	public void setVariation_id(int variation_id) {
		this.variation_id = variation_id;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public LineItemWoo() {
		super();
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public List<MetaWoo> getMeta_data() {
		return meta_data;
	}
	public void setMeta_data(List<MetaWoo> meta_data) {
		this.meta_data = meta_data;
	}
	
}
