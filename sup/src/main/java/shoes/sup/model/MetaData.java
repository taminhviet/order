package shoes.sup.model;

import java.util.List;

public class MetaData {
	List<MetaWoo> meta_data;

	public List<MetaWoo> getMeta_data() {
		return meta_data;
	}

	public void setMeta_data(List<MetaWoo> meta_data) {
		this.meta_data = meta_data;
	}
	
}
