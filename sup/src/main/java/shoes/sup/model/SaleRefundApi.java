package shoes.sup.model;

public class SaleRefundApi {
	SaleAmountRefund amount;
	String invoice_number;
	
	public SaleRefundApi(SaleAmountRefund amount, String invoice_number) {
		super();
		this.amount = amount;
		this.invoice_number = invoice_number;
	}
	public SaleAmountRefund getAmount() {
		return amount;
	}
	public void setAmount(SaleAmountRefund amount) {
		this.amount = amount;
	}
	public String getInvoice_number() {
		return invoice_number;
	}
	public void setInvoice_number(String invoice_number) {
		this.invoice_number = invoice_number;
	}
	

}