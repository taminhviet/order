package shoes.sup.model;

import java.util.List;

public class DisputeDetailAPI {

	String dispute_id;
	List<DisputeTransaction> disputed_transactions;
	String reason;
	String status;
	String seller_response_due_date;
	DisputeAPIOutcome dispute_outcome;
	DisputeAmount dispute_amount;
	List<Link> links;
	
	public DisputeDetailAPI() {
		super();
	}
	
	public DisputeAmount getDispute_amount() {
		return dispute_amount;
	}

	public void setDispute_amount(DisputeAmount dispute_amount) {
		this.dispute_amount = dispute_amount;
	}

	public String getDispute_id() {
		return dispute_id;
	}
	public void setDispute_id(String dispute_id) {
		this.dispute_id = dispute_id;
	}
	public List<DisputeTransaction> getDisputed_transactions() {
		return disputed_transactions;
	}
	public void setDisputed_transactions(
			List<DisputeTransaction> disputed_transactions) {
		this.disputed_transactions = disputed_transactions;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSeller_response_due_date() {
		return seller_response_due_date;
	}
	public void setSeller_response_due_date(String seller_response_due_date) {
		this.seller_response_due_date = seller_response_due_date;
	}
	public DisputeAPIOutcome getDispute_outcome() {
		return dispute_outcome;
	}
	public void setDispute_outcome(DisputeAPIOutcome dispute_outcome) {
		this.dispute_outcome = dispute_outcome;
	}
	public List<Link> getLinks() {
		return links;
	}
	public void setLinks(List<Link> links) {
		this.links = links;
	}
	
	
}
