package shoes.sup.model;

import java.util.List;

public class OrderWoo {
	int id;
	String date_created;
	String status;
	String currency;
	Double total;
	ShippingWoo shipping;
	BillingWoo billing;
	String transaction_id;
	String payment_method;
	List<MetaWoo> meta_data;
	List<LineItemWoo> line_items;
	String customer_note;
	
	public List<MetaWoo> getMeta_data() {
		return meta_data;
	}
	public void setMeta_data(List<MetaWoo> meta_data) {
		this.meta_data = meta_data;
	}
	public String getPayment_method() {
		return payment_method;
	}
	public void setPayment_method(String payment_method) {
		this.payment_method = payment_method;
	}
	public BillingWoo getBilling() {
		return billing;
	}
	public void setBilling(BillingWoo billing) {
		this.billing = billing;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public ShippingWoo getShipping() {
		return shipping;
	}
	public void setShipping(ShippingWoo shipping) {
		this.shipping = shipping;
	}
	public String getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}
	public List<LineItemWoo> getLine_items() {
		return line_items;
	}
	public void setLine_items(List<LineItemWoo> line_items) {
		this.line_items = line_items;
	}
	public OrderWoo() {
		super();
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public String getDate_created() {
		return date_created;
	}
	public void setDate_created(String date_created) {
		this.date_created = date_created;
	}
	public String getCustomer_note() {
		return customer_note;
	}
	public void setCustomer_note(String customer_note) {
		this.customer_note = customer_note;
	}
	
	
	
}
