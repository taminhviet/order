package shoes.sup.model;

public class WooPPCheckout {
	String id;
	String title;
	String method_title;
	WooPPSetting settings;
	
	public WooPPCheckout() {
		super();
	}
	
	public WooPPCheckout(String id, String title, String method_title,
			WooPPSetting settings) {
		super();
		this.id = id;
		this.title = title;
		this.method_title = method_title;
		this.settings = settings;
	}



	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMethod_title() {
		return method_title;
	}
	public void setMethod_title(String method_title) {
		this.method_title = method_title;
	}
	public WooPPSetting getSettings() {
		return settings;
	}
	public void setSettings(WooPPSetting settings) {
		this.settings = settings;
	}
	
}