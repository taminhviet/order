package shoes.sup.model;

public class DisputeAPIOutcome {
	String outcome_code;
	
	public DisputeAPIOutcome() {
		super();
	}

	public DisputeAPIOutcome(String outcome_code) {
		super();
		this.outcome_code = outcome_code;
	}

	public String getOutcome_code() {
		return outcome_code;
	}

	public void setOutcome_code(String outcome_code) {
		this.outcome_code = outcome_code;
	}
	
	
}
