package shoes.sup.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "proxy")
public class Proxy {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
	
	@Column(name="license")
    private String license;
	
	@Column(name="server")
    private String server;
	
	@Column(name="port")
    private int port;
	
	@Column(name="username")
    private String username;
	
	@Column(name="password")
    private String password;
	

	public Proxy(int id, String license, String server, int port, String username, String password) {
		super();
		this.id = id;
		this.license = license;
		this.server = server;
		this.port = port;
		this.username = username;
		this.password = password;
	}



	public Proxy(String license, String server, int port, String username, String password) {
		super();
		this.license = license;
		this.server = server;
		this.port = port;
		this.username = username;
		this.password = password;
	}
	
	

	public Proxy() {
		super();
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
