package shoes.sup.model;

public class WooLink {
	private String id;
	private String slug;
	private String permalink;
	private String name;
	private String spsID;
	public WooLink(String id, String slug, String permalink, String name) {
		super();
		this.id = id;
		this.slug = slug;
		this.permalink = permalink;
		this.name = name;
	}
	
	public WooLink(String id, String slug, String permalink, String name, String spsID) {
		super();
		this.id = id;
		this.slug = slug;
		this.permalink = permalink;
		this.name = name;
		this.spsID = spsID;
	}

	public String getSpsID() {
		return spsID;
	}

	public void setSpsID(String spsID) {
		this.spsID = spsID;
	}

	public WooLink() {
		super();
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSlug() {
		return slug;
	}
	public void setSlug(String slug) {
		this.slug = slug;
	}
	public String getPermalink() {
		return permalink;
	}
	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
