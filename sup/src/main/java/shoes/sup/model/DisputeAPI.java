package shoes.sup.model;

public class DisputeAPI {

	private String dispute_id;
	private String reason;
	private String status;
	private String dispute_state;
	public String getDispute_id() {
		return dispute_id;
	}
	public void setDispute_id(String dispute_id) {
		this.dispute_id = dispute_id;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDispute_state() {
		return dispute_state;
	}
	public void setDispute_state(String dispute_state) {
		this.dispute_state = dispute_state;
	}
	
}
