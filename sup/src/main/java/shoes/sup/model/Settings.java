package shoes.sup.model;

public class Settings {

	private PublishableKey publishable_key;
	
	private SecretKey secret_key;
	
	public Settings() {
		super();
	}

	public Settings(PublishableKey publishable_key, SecretKey secret_key) {
		super();
		this.publishable_key = publishable_key;
		this.secret_key = secret_key;
	}

	public PublishableKey getPublishable_key() {
		return publishable_key;
	}

	public void setPublishable_key(PublishableKey publishable_key) {
		this.publishable_key = publishable_key;
	}

	public SecretKey getSecret_key() {
		return secret_key;
	}

	public void setSecret_key(SecretKey secret_key) {
		this.secret_key = secret_key;
	}
	
	
}
