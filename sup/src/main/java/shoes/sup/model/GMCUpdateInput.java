package shoes.sup.model;

public class GMCUpdateInput {
	private String merchantid;
	private String feedid;
	private String gmccountry;
	
	
	public GMCUpdateInput() {
		super();
	}
	public GMCUpdateInput(String merchantid, String feedid, String gmccountry) {
		super();
		this.merchantid = merchantid;
		this.feedid = feedid;
		this.gmccountry = gmccountry;
	}
	public String getMerchantid() {
		return merchantid;
	}
	public void setMerchantid(String merchantid) {
		this.merchantid = merchantid;
	}
	public String getFeedid() {
		return feedid;
	}
	public void setFeedid(String feedid) {
		this.feedid = feedid;
	}
	public String getGmccountry() {
		return gmccountry;
	}
	public void setGmccountry(String gmccountry) {
		this.gmccountry = gmccountry;
	}
	
	
}
