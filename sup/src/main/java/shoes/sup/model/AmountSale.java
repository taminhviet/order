package shoes.sup.model;

public class AmountSale {
	String currency_code;
	String value;
	
	
	
	public AmountSale() {
		super();
	}
	public AmountSale(String currency_code, String value) {
		super();
		this.currency_code = currency_code;
		this.value = value;
	}
	public String getCurrency_code() {
		return currency_code;
	}
	public void setCurrency_code(String currency_code) {
		this.currency_code = currency_code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	
	
}
