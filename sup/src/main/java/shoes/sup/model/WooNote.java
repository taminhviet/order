package shoes.sup.model;

public class WooNote {
	int id;
	String note;
	boolean customer_note;
	public WooNote(int id, String note, boolean customer_note) {
		super();
		this.id = id;
		this.note = note;
		this.customer_note = customer_note;
	}
	public WooNote() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public boolean isCustomer_note() {
		return customer_note;
	}
	public void setCustomer_note(boolean customer_note) {
		this.customer_note = customer_note;
	}
	
	
}
