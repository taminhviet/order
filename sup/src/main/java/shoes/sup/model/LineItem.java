package shoes.sup.model;

public class LineItem {
	private String product_id;
	private String name;
	private String variant_title;
	private long quantity;
	private String price;
	
	
	
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getVariant_title() {
		return variant_title;
	}
	public void setVariant_title(String variant_title) {
		this.variant_title = variant_title;
	}
	public String getProduct_id() {
		return product_id;
	}
	public LineItem() {
		super();
	}
	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getQuantity() {
		return quantity;
	}
	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}
}
