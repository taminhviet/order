package shoes.sup.model;

public class ColumbiaVariant {

	private int color;
	private String size;
	private int dimension;
	private boolean bisnflag;
	private double inventory;
	
	public ColumbiaVariant(int color, String size, int dimension, boolean bisnflag, double inventory) {
		super();
		this.color = color;
		this.size = size;
		this.dimension = dimension;
		this.bisnflag = bisnflag;
		this.inventory = inventory;
	}
	
	public ColumbiaVariant() {
		super();
	}

	public int getColor() {
		return color;
	}
	public void setColor(int color) {
		this.color = color;
	}
	
	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public int getDimension() {
		return dimension;
	}
	public void setDimension(int dimension) {
		this.dimension = dimension;
	}
	public boolean isBisnflag() {
		return bisnflag;
	}
	public void setBisnflag(boolean bisnflag) {
		this.bisnflag = bisnflag;
	}
	public double getInventory() {
		return inventory;
	}
	public void setInventory(double inventory) {
		this.inventory = inventory;
	}
	
	
	
}
