package shoes.sup.model;

import java.util.List;

public class Trackers {

	List<Tracker> trackers;
	
	public Trackers(List<Tracker> trackers) {
		super();
		this.trackers = trackers;
	}

	public List<Tracker> getTrackers() {
		return trackers;
	}

	public void setTrackers(List<Tracker> trackers) {
		this.trackers = trackers;
	}
	
	
}
