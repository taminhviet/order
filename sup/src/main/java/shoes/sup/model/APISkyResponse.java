package shoes.sup.model;

import java.util.List;

public class APISkyResponse {
	private int draw;
    private int total;
    private int recordsFiltered;
    private List<TrackingData> data;
    private int balance;
    private String error;
	public int getDraw() {
		return draw;
	}
	public void setDraw(int draw) {
		this.draw = draw;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public int getRecordsFiltered() {
		return recordsFiltered;
	}
	public void setRecordsFiltered(int recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}
	public List<TrackingData> getData() {
		return data;
	}
	public void setData(List<TrackingData> data) {
		this.data = data;
	}
	public int getBalance() {
		return balance;
	}
	public void setBalance(int balance) {
		this.balance = balance;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
    
    
}
