package shoes.sup.model;

public class TrackingWoo {

	private String tracking_provider;
	private String tracking_number;
	private String date_shipped;
	private int status_shipped;
	
	
	public TrackingWoo(String tracking_provider, String tracking_number,
			String date_shipped, int status_shipped) {
		super();
		this.tracking_provider = tracking_provider;
		this.tracking_number = tracking_number;
		this.date_shipped = date_shipped;
		this.status_shipped = status_shipped;
	}
	public String getTracking_provider() {
		return tracking_provider;
	}
	public void setTracking_provider(String tracking_provider) {
		this.tracking_provider = tracking_provider;
	}
	public String getTracking_number() {
		return tracking_number;
	}
	public void setTracking_number(String tracking_number) {
		this.tracking_number = tracking_number;
	}
	public String getDate_shipped() {
		return date_shipped;
	}
	public void setDate_shipped(String date_shipped) {
		this.date_shipped = date_shipped;
	}
	public int getStatus_shipped() {
		return status_shipped;
	}
	public void setStatus_shipped(int status_shipped) {
		this.status_shipped = status_shipped;
	}
	
	
}
