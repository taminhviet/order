package shoes.sup.model;

import java.util.List;

public class Notes {
	List<WooNote> note;

	public List<WooNote> getNote() {
		return note;
	}

	public void setNote(List<WooNote> note) {
		this.note = note;
	}

	public Notes(List<WooNote> note) {
		super();
		this.note = note;
	}

	public Notes() {
		super();
	}
	
	
}
