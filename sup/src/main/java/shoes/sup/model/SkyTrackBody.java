package shoes.sup.model;

public class SkyTrackBody {
	private int country;
	private String zipcode;
	private int tracking_type;
	private int start;
	private int city;
	private String date_range;
	private int limit;
	
	
	
	public SkyTrackBody() {
		super();
	}
	public SkyTrackBody(int country, String zipcode, int tracking_type, int start, int city, String date_range,
			int limit) {
		super();
		this.country = country;
		this.zipcode = zipcode;
		this.tracking_type = tracking_type;
		this.start = start;
		this.city = city;
		this.date_range = date_range;
		this.limit = limit;
	}
	public int getCountry() {
		return country;
	}
	public void setCountry(int country) {
		this.country = country;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public int getTracking_type() {
		return tracking_type;
	}
	public void setTracking_type(int tracking_type) {
		this.tracking_type = tracking_type;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getCity() {
		return city;
	}
	public void setCity(int city) {
		this.city = city;
	}
	public String getDate_range() {
		return date_range;
	}
	public void setDate_range(String date_range) {
		this.date_range = date_range;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	
	
}
