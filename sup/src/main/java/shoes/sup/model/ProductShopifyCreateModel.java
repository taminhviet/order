package shoes.sup.model;

public class ProductShopifyCreateModel {
	private ProductShopifyCreate product;
	
	public ProductShopifyCreateModel() {
		super();
	}

	public ProductShopifyCreate getProduct() {
		return product;
	}

	public void setProduct(ProductShopifyCreate product) {
		this.product = product;
	}
	
}
