package shoes.sup.model;

import java.util.List;

public class TransactionAPI {
	
	List<Transaction> transactions;
	
	public TransactionAPI() {
		super();
	}

	public List<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<Transaction> transactions) {
		this.transactions = transactions;
	}
	
}
