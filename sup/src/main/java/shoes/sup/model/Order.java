package shoes.sup.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "order_table")
public class Order {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
	
	@Column(name="oname")
    private String oname;
	
	@Column(name="orderidapi")
    private String orderidapi;
	
	@Column(name ="address")
	private String address;
	
	@Column(name ="addressname")
	private String addressname;
	
	@Column(name ="address1")
	private String address1;
	
	@Column(name ="address2")
	private String address2;
	
	@Column(name ="city")
	private String city;
	
	@Column(name ="zip")
	private String zip;
	
	@Column(name ="storedomain")
	private String storedomain;
	
	@Column(name ="province")
	private String province;
	
	@Column(name = "notecarrier")
	private String notecarrier;
	
	@Column(name = "phone")
	private String phone;
	
	@Column(name = "email")
	private String email;

	@Column(name ="country")
	private String country;
	
	@Column(name="imageurl")
    private String imageurl;
	
	@Column(name="productname", length = 1000)
    private String productname;
	
	@Column(name = "quantity")
	private String quantity;
	
	@Column(name="variant")
    private String variant;
	
	@Column(name="style")
    private String style;
	
	@Column(name="status")
    private String status;
	
	@Column(name="trackingnumber")
    private String trackingnumber;
	
	@Column(name="trackingreturn")
    private String trackingreturn;
	
	@Column(name="supplier")
    private String supplier;
	
	@Column(name="note")
    private String note;
	
	@Column(name = "adminnote")
	private String adminnote;
	
	@Column(name = "lastsupplier")
	private String lastsupplier;
	
	@Column(name = "price")
	private String price;
	
	@Column(name = "type")
	private int type;
	
	@Temporal(TemporalType.TIMESTAMP)
    Date createDate;
	
	@Column(name = "apistatus")
	private String apistatus;

	@Column(name = "pay_pal_account_id")
	private String paypalaccountid;
	
	@Column(name = "paypalstatus")
	private String paypalstatus;
	
	@Column(name = "stripe_sk")
	private String stripe_sk;
	
	@Column(name = "stripe_intent_id")
	private String stripe_intent_id;
	
	@Column(name = "transaction_id")
	private String transaction_id;
	
	@Column(name = "disputed")
	private String disputed;
	
	@Column(name = "dispute_id")
	private String dispute_id;
	
	@Column(name = "dispute_reason")
	private String dispute_reason;
	
	@Column(name = "dispute_status")
	private String dispute_status;
	
	@Column(name = "dispute_state")
	private String dispute_state;
	
	@Column(name = "seller_response_due_date")
	private String seller_response_due_date;
	
	@Column(name = "outcome_code")
	private String outcome_code;
	
	@Column(name = "returned_by_customer")
	private String returned_by_customer;
	
	@Column(name = "payment_status")
	private String payment_status;
	
	@Column(name = "product")
	private String product;
	
	@Column(name = "carrier")
	private String carrier;
	
	@Column(name = "capture_status")
	private String capture_status;
	
	@Column(name = "capture_issue")
	private String capture_issue;
	
	@Column(name = "cost")
	private String cost;
	
	@Column(name = "siteb")
	private String siteb;
	
	
	
	public String getPaypalaccountid() {
		return paypalaccountid;
	}

	public void setPaypalaccountid(String paypalaccountid) {
		this.paypalaccountid = paypalaccountid;
	}

	public String getPaypalstatus() {
		return paypalstatus;
	}

	public void setPaypalstatus(String paypalstatus) {
		this.paypalstatus = paypalstatus;
	}

	public String getPay_pal_account_id() {
		return paypalaccountid;
	}

	public void setPay_pal_account_id(String pay_pal_account_id) {
		this.paypalaccountid = pay_pal_account_id;
	}

	public String getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}

	public String getApistatus() {
		return apistatus;
	}

	public void setApistatus(String apistatus) {
		this.apistatus = apistatus;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCapture_issue() {
		return capture_issue;
	}

	public void setCapture_issue(String capture_issue) {
		this.capture_issue = capture_issue;
	}

	public String getLastsupplier() {
		return lastsupplier;
	}

	public void setLastsupplier(String lastsupplier) {
		this.lastsupplier = lastsupplier;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getOrderidapi() {
		return orderidapi;
	}

	public void setOrderidapi(String orderidapi) {
		this.orderidapi = orderidapi;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAdminnote() {
		return adminnote;
	}

	public void setAdminnote(String adminnote) {
		this.adminnote = adminnote;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOname() {
		return oname;
	}

	public void setOname(String oname) {
		this.oname = oname;
	}

	public String getImageurl() {
		return imageurl;
	}

	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}

	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}

	public String getVariant() {
		return variant;
	}

	public void setVariant(String variant) {
		this.variant = variant;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTrackingnumber() {
		return trackingnumber;
	}

	public void setTrackingnumber(String trackingnumber) {
		this.trackingnumber = trackingnumber;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
	public String getStoredomain() {
		return storedomain;
	}

	public void setStoredomain(String storedomain) {
		this.storedomain = storedomain;
	}


	public Order() {
		super();
	}
	
	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getAddressname() {
		return addressname;
	}

	public void setAddressname(String addressname) {
		this.addressname = addressname;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getTrackingreturn() {
		return trackingreturn;
	}

	public void setTrackingreturn(String trackingreturn) {
		this.trackingreturn = trackingreturn;
	}


	public String getDisputed() {
		return disputed;
	}

	public void setDisputed(String disputed) {
		this.disputed = disputed;
	}

	public String getDispute_reason() {
		return dispute_reason;
	}

	public void setDispute_reason(String dispute_reason) {
		this.dispute_reason = dispute_reason;
	}

	public String getDispute_status() {
		return dispute_status;
	}

	public void setDispute_status(String dispute_status) {
		this.dispute_status = dispute_status;
	}

	public String getDispute_state() {
		return dispute_state;
	}

	public void setDispute_state(String dispute_state) {
		this.dispute_state = dispute_state;
	}

	public String getSeller_response_due_date() {
		return seller_response_due_date;
	}

	public void setSeller_response_due_date(String seller_response_due_date) {
		this.seller_response_due_date = seller_response_due_date;
	}

	public String getDispute_id() {
		return dispute_id;
	}

	public void setDispute_id(String dispute_id) {
		this.dispute_id = dispute_id;
	}

	public String getOutcome_code() {
		return outcome_code;
	}

	public void setOutcome_code(String outcome_code) {
		this.outcome_code = outcome_code;
	}

	public String getReturned_by_customer() {
		return returned_by_customer;
	}

	public void setReturned_by_customer(String returned_by_customer) {
		this.returned_by_customer = returned_by_customer;
	}

	public String getPayment_status() {
		return payment_status;
	}

	public void setPayment_status(String payment_status) {
		this.payment_status = payment_status;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCapture_status() {
		return capture_status;
	}

	public void setCapture_status(String capture_status) {
		this.capture_status = capture_status;
	}

	public String getNotecarrier() {
		return notecarrier;
	}

	public void setNotecarrier(String notecarrier) {
		this.notecarrier = notecarrier;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getStripe_sk() {
		return stripe_sk;
	}

	public void setStripe_sk(String stripe_sk) {
		this.stripe_sk = stripe_sk;
	}

	public String getStripe_intent_id() {
		return stripe_intent_id;
	}

	public void setStripe_intent_id(String stripe_intent_id) {
		this.stripe_intent_id = stripe_intent_id;
	}

	public String getSiteb() {
		return siteb;
	}

	public void setSiteb(String siteb) {
		this.siteb = siteb;
	}
	
	
}
