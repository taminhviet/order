package shoes.sup.model;

public class DisputeTransaction {

	String buyer_transaction_id;
	String seller_transaction_id;
	String transaction_status;
	public String getBuyer_transaction_id() {
		return buyer_transaction_id;
	}
	public void setBuyer_transaction_id(String buyer_transaction_id) {
		this.buyer_transaction_id = buyer_transaction_id;
	}
	public String getSeller_transaction_id() {
		return seller_transaction_id;
	}
	public void setSeller_transaction_id(String seller_transaction_id) {
		this.seller_transaction_id = seller_transaction_id;
	}
	public String getTransaction_status() {
		return transaction_status;
	}
	public void setTransaction_status(String transaction_status) {
		this.transaction_status = transaction_status;
	}
	
}
