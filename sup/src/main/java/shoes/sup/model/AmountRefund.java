package shoes.sup.model;

public class AmountRefund {
	AmountCapture amountCapture;
	String invoice_id;
	String note_to_payer;
	
	
	public AmountRefund(AmountCapture amountCapture, String invoice_id, String note_to_payer) {
		super();
		this.amountCapture = amountCapture;
		this.invoice_id = invoice_id;
		this.note_to_payer = note_to_payer;
	}
	
	
	
	public AmountRefund() {
		super();
	}



	public AmountCapture getAmountCapture() {
		return amountCapture;
	}
	public void setAmountCapture(AmountCapture amountCapture) {
		this.amountCapture = amountCapture;
	}
	public String getInvoice_id() {
		return invoice_id;
	}
	public void setInvoice_id(String invoice_id) {
		this.invoice_id = invoice_id;
	}
	public String getNote_to_payer() {
		return note_to_payer;
	}
	public void setNote_to_payer(String note_to_payer) {
		this.note_to_payer = note_to_payer;
	}
	
	
}
