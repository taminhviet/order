package shoes.sup.model;

import java.math.BigInteger;
import java.util.List;

public class ShopifyOptions {
	BigInteger product_id;
	BigInteger id;
	String name;
	int position;
	List<String> values;
	
	public ShopifyOptions() {
		super();
	}
	public BigInteger getProduct_id() {
		return product_id;
	}
	public void setProduct_id(BigInteger product_id) {
		this.product_id = product_id;
	}
	public BigInteger getId() {
		return id;
	}
	public void setId(BigInteger id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public List<String> getValues() {
		return values;
	}
	public void setValues(List<String> values) {
		this.values = values;
	}
	
	
}
