package shoes.sup.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "paypal_account")
public class PaypalAccount {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
	
	@Column(name="email")
    private String email;
	
	@Column(name = "clientid")
	private String clientid;
	
	@Column(name = "clientsecret")
	private String clientsecret;
	
	@Column(name = "active")
	private String active;
	
	@Column(name = "username")
	private String username;
	
	@Column(name = "password")
	private String password;
	
	@Column(name = "signature")
	private String signature;
	
	@Column(name = "dayorder")
	private String dayorder;
	
	@Column(name = "total")
	private String total;
	
	@Column(name = "store")
	private String store;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "round")
	private String round;
	
	@Column(name = "sumprice")
	private String sumprice;
	
	@Column(name = "capturetotal")
	private String capturetotal;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "withdraw")
	private String withdraw;
	
	@Column(name = "queue")
	private String queue;
	
	@Column(name = "lastdomain")
	private String lastdomain;

	
	@Column(name = "note")
	private String note;
	
	public PaypalAccount() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getClientid() {
		return clientid;
	}

	public void setClientid(String clientid) {
		this.clientid = clientid;
	}

	public String getClientsecret() {
		return clientsecret;
	}

	public void setClientsecret(String clientsecret) {
		this.clientsecret = clientsecret;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}
	
	public String getLastdomain() {
		return lastdomain;
	}

	public void setLastdomain(String lastdomain) {
		this.lastdomain = lastdomain;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getDayorder() {
		return dayorder;
	}

	public void setDayorder(String dayorder) {
		this.dayorder = dayorder;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	public String getRound() {
		return round;
	}

	public void setRound(String round) {
		this.round = round;
	}

	public String getSumprice() {
		return sumprice;
	}

	public void setSumprice(String sumprice) {
		this.sumprice = sumprice;
	}

	public String getCapturetotal() {
		return capturetotal;
	}

	public void setCapturetotal(String capturetotal) {
		this.capturetotal = capturetotal;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getWithdraw() {
		return withdraw;
	}

	public void setWithdraw(String withdraw) {
		this.withdraw = withdraw;
	}

	public String getQueue() {
		return queue;
	}

	public void setQueue(String queue) {
		this.queue = queue;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
	
	
}
