package shoes.sup.model;

public class DataFeedGMC {
	private long feedid;
	private String name;
	private String link;
	private String country;
	private String language;
	
	public DataFeedGMC() {
		super();
	}
	public DataFeedGMC(long feedid, String name, String link, String country, String language) {
		super();
		this.feedid = feedid;
		this.name = name;
		this.link = link;
		this.country = country;
		this.language = language;
	}
	public long getFeedid() {
		return feedid;
	}
	public void setFeedid(long feedid) {
		this.feedid = feedid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	
	
}
