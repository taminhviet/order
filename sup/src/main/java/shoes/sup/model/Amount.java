package shoes.sup.model;

public class Amount {

	private String amount;
	
	public Amount(String amount) {
		super();
		this.amount = amount;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
}
