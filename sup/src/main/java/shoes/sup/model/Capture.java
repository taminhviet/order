package shoes.sup.model;

public class Capture {
	AmountCapture amountCapture;
	String invoice_id;
	boolean final_capture;
	
	
	
	public Capture(AmountCapture amountCapture, String invoice_id,
			boolean final_capture) {
		super();
		this.amountCapture = amountCapture;
		this.invoice_id = invoice_id;
		this.final_capture = final_capture;
	}
	public AmountCapture getAmountCapture() {
		return amountCapture;
	}
	public void setAmountCapture(AmountCapture amountCapture) {
		this.amountCapture = amountCapture;
	}
	public Capture() {
		super();
	}
	public String getInvoice_id() {
		return invoice_id;
	}
	public void setInvoice_id(String invoice_id) {
		this.invoice_id = invoice_id;
	}
	public boolean isFinal_capture() {
		return final_capture;
	}
	public void setFinal_capture(boolean final_capture) {
		this.final_capture = final_capture;
	}
	
}
