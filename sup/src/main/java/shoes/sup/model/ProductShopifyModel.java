package shoes.sup.model;

import java.util.List;

public class ProductShopifyModel {
	List<ShopifyModel> products;

	public List<ShopifyModel> getProducts() {
		return products;
	}

	public void setProducts(List<ShopifyModel> products) {
		this.products = products;
	}

	public ProductShopifyModel() {
		super();
	}
	
}
