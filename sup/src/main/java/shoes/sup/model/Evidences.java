package shoes.sup.model;

import java.util.List;

public class Evidences {

	List<Evidence> evidences;
	

	public Evidences() {
		super();
	}

	public List<Evidence> getEvidences() {
		return evidences;
	}

	public void setEvidences(List<Evidence> evidences) {
		this.evidences = evidences;
	}
	
}
