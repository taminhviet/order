package shoes.sup.model;

public class XMLProduct {
	private String id;
	private String title;
	private String description;
	private String link;
	private String price;
	private String image_link;
	private String item_group_id;
	private String gtin;
	private String size;
	private String gender;
	private String age_group;
	private String color;
	
	public XMLProduct() {
		super();
	}
	
	public String getGtin() {
		return gtin;
	}

	public void setGtin(String gtin) {
		this.gtin = gtin;
	}
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
	public XMLProduct(String id, String title, String description, String link, String price, String image_link,
			String item_group_id, String gtin) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.link = link;
		this.price = price;
		this.image_link = image_link;
		this.item_group_id = item_group_id;
		this.gtin = gtin;
	}
	


	public XMLProduct(String id, String title, String description, String link, String price, String image_link,
			String item_group_id, String gtin, String size, String gender, String age_group, String color) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.link = link;
		this.price = price;
		this.image_link = image_link;
		this.item_group_id = item_group_id;
		this.gtin = gtin;
		this.size = size;
		this.gender = gender;
		this.age_group = age_group;
		this.color = color;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAge_group() {
		return age_group;
	}

	public void setAge_group(String age_group) {
		this.age_group = age_group;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getImage_link() {
		return image_link;
	}
	public void setImage_link(String image_link) {
		this.image_link = image_link;
	}
	public String getItem_group_id() {
		return item_group_id;
	}
	public void setItem_group_id(String item_group_id) {
		this.item_group_id = item_group_id;
	}
	
	
}
