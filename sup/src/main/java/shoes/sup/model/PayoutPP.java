package shoes.sup.model;

import java.util.Date;

public class PayoutPP {
	private String order;
	private String currency;
	private double gross;
	private Date date;
	private String type;
	
	public PayoutPP(String order, String currency, double gross, Date date) {
		super();
		this.order = order;
		this.currency = currency;
		this.gross = gross;
		this.date = date;
	}
	public PayoutPP() {
		super();
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public double getGross() {
		return gross;
	}
	public void setGross(double gross) {
		this.gross = gross;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	
}
