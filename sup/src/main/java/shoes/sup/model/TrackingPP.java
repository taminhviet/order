package shoes.sup.model;

public class TrackingPP {
	String id;
	String trackingnumber;
	String carrier;
	
	public TrackingPP() {
		super();
	}
	public TrackingPP(String id, String trackingnumber, String carrier) {
		super();
		this.id = id;
		this.trackingnumber = trackingnumber;
		this.carrier = carrier;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTrackingnumber() {
		return trackingnumber;
	}
	public void setTrackingnumber(String trackingnumber) {
		this.trackingnumber = trackingnumber;
	}
	public String getCarrier() {
		return carrier;
	}
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	
	
}
