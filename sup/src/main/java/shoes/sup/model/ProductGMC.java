package shoes.sup.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ProductGMC")
public class ProductGMC {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
	
	@Column(name="domain")
    private String domain;
	
	@Column(name="gmcid")
    private String gmcid;
	
	@Column(name="productid")
    private String productid;
	
	@Column(name="offerid")
    private String offerid;
	
	@Column(name="country")
    private String country;
	
	@Column(name="feedlabel")
    private String feedlabel;
	
	@Column(name="category")
    private String category;
	
	@Column(name="title", length = 1000,columnDefinition="LONGTEXT")
    private String title;
	
	@Column(name="description", length = 5000,columnDefinition="LONGTEXT")
    private String description;
	
	@Column(name="additonalimage", length = 2000,columnDefinition="LONGTEXT")
    private String additonalimage;
	
	@Column(name="price")
    private String price;
	
	@Column(name="currency")
    private String currency;
	
	@Column(name="parentid")
    private String parentid;
	
	@Column(name="image", length = 1000,columnDefinition="LONGTEXT")
    private String image;
	
	@Column(name="limk", length = 1000,columnDefinition="LONGTEXT")
    private String limk;
	
	@Column(name="supfeedtm")
    private String supfeedtm;
	
	@Column(name="producttmid")
    private String producttmid;
	
	@Column(name="producttmtitle")
    private String producttmtitle;
	
	@Column(name="gtin")
    private String gtin;
	
	@Column(name = "vpsserver")
	private String vpsserver;
	
	@Column(name = "supfeedid")
	private String supfeedid;
	
	public String getAdditonalimage() {
		return additonalimage;
	}

	public void setAdditonalimage(String additonalimage) {
		this.additonalimage = additonalimage;
	}

	public String getSupfeedid() {
		return supfeedid;
	}

	public void setSupfeedid(String supfeedid) {
		this.supfeedid = supfeedid;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getVpsserver() {
		return vpsserver;
	}

	public void setVpsserver(String vpsserver) {
		this.vpsserver = vpsserver;
	}

	public String getGmcid() {
		return gmcid;
	}

	public void setGmcid(String gmcid) {
		this.gmcid = gmcid;
	}

	public String getProductid() {
		return productid;
	}

	public void setProductid(String productid) {
		this.productid = productid;
	}

	public String getOfferid() {
		return offerid;
	}

	public void setOfferid(String offerid) {
		this.offerid = offerid;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getFeedlabel() {
		return feedlabel;
	}

	public void setFeedlabel(String feedlabel) {
		this.feedlabel = feedlabel;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getParentid() {
		return parentid;
	}

	public void setParentid(String parentid) {
		this.parentid = parentid;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getLimk() {
		return limk;
	}

	public void setLimk(String limk) {
		this.limk = limk;
	}

	public ProductGMC(String domain, String gmcid, String productid, String offerid, String country, String feedlabel,
			String category, String title, String description, String price, String parentid, String image,
			String limk, String currency) {
		super();
		this.domain = domain;
		this.gmcid = gmcid;
		this.productid = productid;
		this.offerid = offerid;
		this.country = country;
		this.feedlabel = feedlabel;
		this.category = category;
		this.title = title;
		this.description = description;
		this.price = price;
		this.parentid = parentid;
		this.image = image;
		this.limk = limk;
		this.currency = currency;
	}

	
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public ProductGMC() {
		super();
	}

	public String getSupfeedtm() {
		return supfeedtm;
	}

	public void setSupfeedtm(String supfeedtm) {
		this.supfeedtm = supfeedtm;
	}

	public String getProducttmid() {
		return producttmid;
	}

	public void setProducttmid(String producttmid) {
		this.producttmid = producttmid;
	}

	public String getProducttmtitle() {
		return producttmtitle;
	}

	public void setProducttmtitle(String producttmtitle) {
		this.producttmtitle = producttmtitle;
	}

	public String getGtin() {
		return gtin;
	}

	public void setGtin(String gtin) {
		this.gtin = gtin;
	}
	
}
