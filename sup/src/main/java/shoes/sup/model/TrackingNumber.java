package shoes.sup.model;

public class TrackingNumber {
	private String tracking_number;

	public String getTracking_number() {
		return tracking_number;
	}

	public void setTracking_number(String tracking_number) {
		this.tracking_number = tracking_number;
	}

	public TrackingNumber(String tracking_number) {
		super();
		this.tracking_number = tracking_number;
	}
	
}
