package shoes.sup.model;

public class Offers {

	private String type;
	private String priceCurrency;
	private int price;
	private String availability;
	
	public Offers(String type, String priceCurrency, int price, String availability) {
		super();
		this.type = type;
		this.priceCurrency = priceCurrency;
		this.price = price;
		this.availability = availability;
	}
	
	public Offers() {
		super();
	}

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getPriceCurrency() {
		return priceCurrency;
	}
	public void setPriceCurrency(String priceCurrency) {
		this.priceCurrency = priceCurrency;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getAvailability() {
		return availability;
	}
	public void setAvailability(String availability) {
		this.availability = availability;
	}
	
	
}
