package shoes.sup.model;

public class WooStripe {
	private String id;
	private String title;
	private String description;
	private String method_title;
	private Settings settings;
	
	
	public WooStripe() {
		super();
	}
	public WooStripe(String id, String title, String description, String method_title, Settings settings) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.method_title = method_title;
		this.settings = settings;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getMethod_title() {
		return method_title;
	}
	public void setMethod_title(String method_title) {
		this.method_title = method_title;
	}
	public Settings getSettings() {
		return settings;
	}
	public void setSettings(Settings settings) {
		this.settings = settings;
	}
	
	
}
