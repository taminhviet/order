package shoes.sup.model;

public class Fulfillment {

	private String tracking_number;
	private boolean notify_customer;
	
	public Fulfillment() {
		super();
	}
	public String getTracking_number() {
		return tracking_number;
	}
	public void setTracking_number(String tracking_number) {
		this.tracking_number = tracking_number;
	}
	public boolean isNotify_customer() {
		return notify_customer;
	}
	public void setNotify_customer(boolean notify_customer) {
		this.notify_customer = notify_customer;
	}
	
}
