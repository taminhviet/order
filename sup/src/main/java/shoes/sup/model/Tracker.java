package shoes.sup.model;

public class Tracker {

	String transaction_id;
	String tracking_number;
	String status;
	String carrier;
	boolean notify_buyer;
	
	public Tracker(String transaction_id, String tracking_number,
			String status, String carrier, boolean notify_buyer) {
		super();
		this.transaction_id = transaction_id;
		this.tracking_number = tracking_number;
		this.status = status;
		this.carrier = carrier;
		this.notify_buyer = notify_buyer;
	}
	public String getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}
	public String getTracking_number() {
		return tracking_number;
	}
	public void setTracking_number(String tracking_number) {
		this.tracking_number = tracking_number;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCarrier() {
		return carrier;
	}
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	public boolean isNotify_buyer() {
		return notify_buyer;
	}
	public void setNotify_buyer(boolean notify_buyer) {
		this.notify_buyer = notify_buyer;
	}
	
	
}
