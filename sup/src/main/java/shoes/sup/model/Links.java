package shoes.sup.model;

import java.util.List;

public class Links {

	List<Link> links;

	public Links() {
		super();
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}
	
	
}
