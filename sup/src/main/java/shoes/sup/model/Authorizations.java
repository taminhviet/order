package shoes.sup.model;

public class Authorizations {
	
	String id;
	String status;
	AmountCapture amount;
	String invoice_id;
	
	public Authorizations(String id, String status, AmountCapture amount,
			String invoice_id) {
		super();
		this.id = id;
		this.status = status;
		this.amount = amount;
		this.invoice_id = invoice_id;
	}
	public Authorizations() {
		super();
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public AmountCapture getAmount() {
		return amount;
	}
	public void setAmount(AmountCapture amount) {
		this.amount = amount;
	}
	public String getInvoice_id() {
		return invoice_id;
	}
	public void setInvoice_id(String invoice_id) {
		this.invoice_id = invoice_id;
	}
	
	
}
