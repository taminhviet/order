package shoes.sup.model;

public class WooPPSetting {
	WooPPUserName api_username;
	
	public WooPPSetting() {
		super();
	}

	public WooPPSetting(WooPPUserName api_username) {
		super();
		this.api_username = api_username;
	}

	public WooPPUserName getApi_username() {
		return api_username;
	}

	public void setApi_username(WooPPUserName api_username) {
		this.api_username = api_username;
	}
	
	
}