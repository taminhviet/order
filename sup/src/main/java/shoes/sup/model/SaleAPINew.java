package shoes.sup.model;

public class SaleAPINew {
	String id;
	AmountSale amount;
	String invoice_id;
	
	public SaleAPINew() {
		super();
	}
	public SaleAPINew(String id, AmountSale amount, String invoice_id) {
		super();
		this.id = id;
		this.amount = amount;
		this.invoice_id = invoice_id;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public AmountSale getAmount() {
		return amount;
	}
	public void setAmount(AmountSale amount) {
		this.amount = amount;
	}
	public String getInvoice_id() {
		return invoice_id;
	}
	public void setInvoice_id(String invoice_id) {
		this.invoice_id = invoice_id;
	}
	
	
}
