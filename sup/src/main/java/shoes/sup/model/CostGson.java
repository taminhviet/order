package shoes.sup.model;

public class CostGson {

	private int id;
	private String ordercost;
	
	public CostGson() {
		super();
	}
	public CostGson(int id, String ordercost) {
		super();
		this.id = id;
		this.ordercost = ordercost;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getOrdercost() {
		return ordercost;
	}
	public void setOrdercost(String ordercost) {
		this.ordercost = ordercost;
	}
	
	
}
