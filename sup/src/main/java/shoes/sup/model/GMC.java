package shoes.sup.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "gmc")
public class GMC {

	@Id
	@Column(name="merchantId")
    private String merchantId;
	
	@Column(name="accountSampleUser")
    private String accountSampleUser;
	
	@Column(name="type")
    private String type;
	
	@Column(name="project_id")
    private String projectid;
	
	@Column(name="private_key_id")
    private String private_key_id;
	
	@Column(name="private_key", length = 5000,columnDefinition="LONGTEXT")
    private String private_key;
	
	@Column(name="client_email",length = 5000,columnDefinition="LONGTEXT")
    private String client_email;
	
	@Column(name="client_id")
    private String client_id;
	
	@Column(name="auth_uri")
    private String auth_uri;
	
	@Column(name="token_uri")
    private String token_uri;
	
	@Column(name="auth_provider_x509_cert_url",length = 5000,columnDefinition="LONGTEXT")
    private String auth_provider_x509_cert_url;
	
	@Column(name="client_x509_cert_url")
    private String client_x509_cert_url;
	
	@Column(name="universe_domain")
    private String universe_domain;
	
	@Column(name="status")
    private String status;
	
	@Column(name="eligibility")
    private String eligibility;
	
	@Column(name="domain")
    private String domain;
	
	@Column(name="country")
    private String country;
	
	@Column(name="supfeedid")
    private String supfeedid;
	
	@Column(name="domaintm")
    private String domaintm;
	
	@Column(name="revieweligibility")
    private String revieweligibility;
	
	@Column(name="suspendedtype")
    private String suspendedtype;
	
	@Column(name = "createddate")
	private String createddate;
	
	@Column(name = "note")
	private String note;
	
	@Column(name = "suspenddate")
	private String suspenddate;
	
	@Column(name = "vpsserver")
	private String vpsserver;
	
	@Column(name = "businessname")
	private String businessname;

	public GMC() {
		super();
	}
	

	public String getBusinessname() {
		return businessname;
	}



	public void setBusinessname(String businessname) {
		this.businessname = businessname;
	}



	public String getEligibility() {
		return eligibility;
	}



	public void setEligibility(String eligibility) {
		this.eligibility = eligibility;
	}



	public String getDomain() {
		return domain;
	}

	

	public String getCreateddate() {
		return createddate;
	}


	public String getProjectid() {
		return projectid;
	}



	public void setProjectid(String projectid) {
		this.projectid = projectid;
	}



	public String getVpsserver() {
		return vpsserver;
	}



	public void setVpsserver(String vpsserver) {
		this.vpsserver = vpsserver;
	}



	public void setCreateddate(String createddate) {
		this.createddate = createddate;
	}



	public void setDomain(String domain) {
		this.domain = domain;
	}



	public String getCountry() {
		return country;
	}

	

	public String getSuspenddate() {
		return suspenddate;
	}



	public void setSuspenddate(String suspenddate) {
		this.suspenddate = suspenddate;
	}



	public void setCountry(String country) {
		this.country = country;
	}



	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getAccountSampleUser() {
		return accountSampleUser;
	}

	public void setAccountSampleUser(String accountSampleUser) {
		this.accountSampleUser = accountSampleUser;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getProject_id() {
		return projectid;
	}

	public void setProject_id(String project_id) {
		this.projectid = project_id;
	}

	public String getPrivate_key_id() {
		return private_key_id;
	}

	public void setPrivate_key_id(String private_key_id) {
		this.private_key_id = private_key_id;
	}

	public String getPrivate_key() {
		return private_key;
	}

	public void setPrivate_key(String private_key) {
		this.private_key = private_key;
	}

	public String getClient_email() {
		return client_email;
	}

	public void setClient_email(String client_email) {
		this.client_email = client_email;
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public String getAuth_uri() {
		return auth_uri;
	}

	public void setAuth_uri(String auth_uri) {
		this.auth_uri = auth_uri;
	}

	public String getToken_uri() {
		return token_uri;
	}

	public void setToken_uri(String token_uri) {
		this.token_uri = token_uri;
	}

	public String getAuth_provider_x509_cert_url() {
		return auth_provider_x509_cert_url;
	}

	public void setAuth_provider_x509_cert_url(String auth_provider_x509_cert_url) {
		this.auth_provider_x509_cert_url = auth_provider_x509_cert_url;
	}

	public String getClient_x509_cert_url() {
		return client_x509_cert_url;
	}

	public void setClient_x509_cert_url(String client_x509_cert_url) {
		this.client_x509_cert_url = client_x509_cert_url;
	}

	public String getUniverse_domain() {
		return universe_domain;
	}

	public void setUniverse_domain(String universe_domain) {
		this.universe_domain = universe_domain;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public GMC(String merchantId, String accountSampleUser, String type, String project_id, String private_key_id,
			String private_key, String client_email, String client_id, String auth_uri, String token_uri,
			String auth_provider_x509_cert_url, String client_x509_cert_url, String universe_domain, String status) {
		super();
		this.merchantId = merchantId;
		this.accountSampleUser = accountSampleUser;
		this.type = type;
		this.projectid = project_id;
		this.private_key_id = private_key_id;
		this.private_key = private_key;
		this.client_email = client_email;
		this.client_id = client_id;
		this.auth_uri = auth_uri;
		this.token_uri = token_uri;
		this.auth_provider_x509_cert_url = auth_provider_x509_cert_url;
		this.client_x509_cert_url = client_x509_cert_url;
		this.universe_domain = universe_domain;
		this.status = status;
	}



	public GMC(String merchantId, String accountSampleUser, String type, String project_id, String private_key_id,
			String private_key, String client_email, String client_id, String auth_uri, String token_uri,
			String auth_provider_x509_cert_url, String client_x509_cert_url, String universe_domain, String status,
			String eligibility, String domain, String country) {
		super();
		this.merchantId = merchantId;
		this.accountSampleUser = accountSampleUser;
		this.type = type;
		this.projectid = project_id;
		this.private_key_id = private_key_id;
		this.private_key = private_key;
		this.client_email = client_email;
		this.client_id = client_id;
		this.auth_uri = auth_uri;
		this.token_uri = token_uri;
		this.auth_provider_x509_cert_url = auth_provider_x509_cert_url;
		this.client_x509_cert_url = client_x509_cert_url;
		this.universe_domain = universe_domain;
		this.status = status;
		this.eligibility = eligibility;
		this.domain = domain;
		this.country = country;
	}



	public GMC(String merchantId, String accountSampleUser, String type, String project_id, String private_key_id,
			String private_key, String client_email, String client_id, String auth_uri, String token_uri,
			String auth_provider_x509_cert_url, String client_x509_cert_url, String universe_domain, String status,
			String eligibility, String domain, String country, String supfeedid, String domaintm) {
		super();
		this.merchantId = merchantId;
		this.accountSampleUser = accountSampleUser;
		this.type = type;
		this.projectid = project_id;
		this.private_key_id = private_key_id;
		this.private_key = private_key;
		this.client_email = client_email;
		this.client_id = client_id;
		this.auth_uri = auth_uri;
		this.token_uri = token_uri;
		this.auth_provider_x509_cert_url = auth_provider_x509_cert_url;
		this.client_x509_cert_url = client_x509_cert_url;
		this.universe_domain = universe_domain;
		this.status = status;
		this.eligibility = eligibility;
		this.domain = domain;
		this.country = country;
		this.supfeedid = supfeedid;
		this.domaintm = domaintm;
	}



	public String getSupfeedid() {
		return supfeedid;
	}



	public void setSupfeedid(String supfeedid) {
		this.supfeedid = supfeedid;
	}



	public String getDomaintm() {
		return domaintm;
	}



	public void setDomaintm(String domaintm) {
		this.domaintm = domaintm;
	}

	

	public String getRevieweligibility() {
		return revieweligibility;
	}



	public void setRevieweligibility(String revieweligibility) {
		this.revieweligibility = revieweligibility;
	}



	public String getSuspendedtype() {
		return suspendedtype;
	}



	public void setSuspendedtype(String suspendedtype) {
		this.suspendedtype = suspendedtype;
	}



	public String getNote() {
		return note;
	}



	public void setNote(String note) {
		this.note = note;
	}
	
	
	
}
