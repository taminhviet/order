package shoes.sup.model;

import java.util.List;

public class EvidenceInfo {
	
	List<TrackingInfoDetail> tracking_info;
	
	
	
	public EvidenceInfo() {
		super();
	}

	public EvidenceInfo(List<TrackingInfoDetail> tracking_info) {
		super();
		this.tracking_info = tracking_info;
	}

	public List<TrackingInfoDetail> getTracking_info() {
		return tracking_info;
	}

	public void setTracking_info(List<TrackingInfoDetail> tracking_info) {
		this.tracking_info = tracking_info;
	}
	

}
