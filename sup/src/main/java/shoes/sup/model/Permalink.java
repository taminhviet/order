package shoes.sup.model;

public class Permalink {

	private String slug;
	private String permanlink;
	
	

	public Permalink() {
		super();
	}

	public Permalink(String permanlink) {
		super();
		this.permanlink = permanlink;
	}

	public String getPermanlink() {
		return permanlink;
	}

	public void setPermanlink(String permanlink) {
		this.permanlink = permanlink;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public Permalink(String slug, String permanlink) {
		super();
		this.slug = slug;
		this.permanlink = permanlink;
	}
	
	
}
