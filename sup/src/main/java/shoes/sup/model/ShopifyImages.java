package shoes.sup.model;

import java.math.BigInteger;

public class ShopifyImages {

	BigInteger product_id;
	BigInteger id;
	int position;
	String src;
	
	
	public ShopifyImages() {
		super();
	}
	public BigInteger getProduct_id() {
		return product_id;
	}
	public void setProduct_id(BigInteger product_id) {
		this.product_id = product_id;
	}
	public BigInteger getId() {
		return id;
	}
	public void setId(BigInteger id) {
		this.id = id;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public String getSrc() {
		return src;
	}
	public void setSrc(String src) {
		this.src = src;
	}
	
	
}
