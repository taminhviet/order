package shoes.sup.model;

public class MessageDispute {

	String message;
	

	public MessageDispute(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
