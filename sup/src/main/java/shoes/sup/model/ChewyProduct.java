package shoes.sup.model;

public class ChewyProduct {
	private String title;
	private String imagelink;
	private String description;
	private String price;
	private String category;
	
	public ChewyProduct() {
		super();
	}
	public ChewyProduct(String title, String imagelink, String description, String price) {
		super();
		this.title = title;
		this.imagelink = imagelink;
		this.description = description;
		this.price = price;
	}
	
	public ChewyProduct(String title, String imagelink, String description, String price, String category) {
		super();
		this.title = title;
		this.imagelink = imagelink;
		this.description = description;
		this.price = price;
		this.category = category;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getImagelink() {
		return imagelink;
	}
	public void setImagelink(String imagelink) {
		this.imagelink = imagelink;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	
	
}
