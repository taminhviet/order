package shoes.sup.model;

import java.util.List;

public class ListDisputeAPI {

	List<DisputeAPI> items;
	

	public ListDisputeAPI() {
		super();
	}

	public List<DisputeAPI> getItems() {
		return items;
	}

	public void setItems(List<DisputeAPI> items) {
		this.items = items;
	}
	
}
