package shoes.sup.model;

public class Refund_Amount {

	String currency_code;
	String value;
	
	public Refund_Amount() {
		super();
	}
	public String getCurrency_code() {
		return currency_code;
	}
	public void setCurrency_code(String currency_code) {
		this.currency_code = currency_code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	
}
