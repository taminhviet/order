package shoes.sup.model;

public class ReceiptAPI {

	String protection_eligibility;
	String pay_pal_account_id;
	String transaction_id;
	
	public ReceiptAPI() {
		super();
	}
	public String getProtection_eligibility() {
		return protection_eligibility;
	}
	public void setProtection_eligibility(String protection_eligibility) {
		this.protection_eligibility = protection_eligibility;
	}
	public String getPay_pal_account_id() {
		return pay_pal_account_id;
	}
	public void setPay_pal_account_id(String pay_pal_account_id) {
		this.pay_pal_account_id = pay_pal_account_id;
	}
	public String getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}
	
}
