package shoes.sup.model;

public class CapturePaypal {
	String id;
	String partial;
	
	public CapturePaypal() {
		super();
	}
	public CapturePaypal(String id, String partial) {
		super();
		this.id = id;
		this.partial = partial;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPartial() {
		return partial;
	}
	public void setPartial(String partial) {
		this.partial = partial;
	}
	
	
}
