package shoes.sup.model;


public class Evidence {
	
	String evidence_type;
	EvidenceInfo evidence_info;
	String notes;
	

	public Evidence() {
		super();
	}
	public String getEvidence_type() {
		return evidence_type;
	}
	public void setEvidence_type(String evidence_type) {
		this.evidence_type = evidence_type;
	}
	public EvidenceInfo getEvidence_info() {
		return evidence_info;
	}
	public void setEvidence_info(EvidenceInfo evidence_info) {
		this.evidence_info = evidence_info;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	
}
