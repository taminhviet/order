package shoes.sup.model;

import java.util.List;

public class Variants {

	private List<ColumbiaVariant> variants;

	public List<ColumbiaVariant> getVariants() {
		return variants;
	}

	public void setVariants(List<ColumbiaVariant> variants) {
		this.variants = variants;
	}
	
}
