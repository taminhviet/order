package shoes.sup.model;

public class TrackingSup {

	int id;
	String trackingnumber;
	String carrier;
	String notecarrier;
	String partial;
	
	public TrackingSup() {
		super();
	}


	public TrackingSup(int id, String trackingnumber, String carrier,
			String notecarrier) {
		super();
		this.id = id;
		this.trackingnumber = trackingnumber;
		this.carrier = carrier;
		this.notecarrier = notecarrier;
	}
	


	public TrackingSup(int id, String trackingnumber, String carrier,
			String note3, String partial) {
		super();
		this.id = id;
		this.trackingnumber = trackingnumber;
		this.carrier = carrier;
		this.notecarrier = note3;
		this.partial = partial;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTrackingnumber() {
		return trackingnumber;
	}
	public void setTrackingnumber(String trackingnumber) {
		this.trackingnumber = trackingnumber;
	}
	public String getCarrier() {
		return carrier;
	}
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}




	public String getNotecarrier() {
		return notecarrier;
	}


	public void setNotecarrier(String notecarrier) {
		this.notecarrier = notecarrier;
	}


	public String getPartial() {
		return partial;
	}


	public void setPartial(String partial) {
		this.partial = partial;
	}
}
