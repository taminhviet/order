package shoes.sup.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "daypp")
public class DayPP {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
	
	@Column(name="accountpp")
	private String accountpp;
	
	@Column(name="day")
	String day;
	
	@Column(name="daytotal")
	double daytotal;
	
	@Column(name = "round")
	int round;
	
	
	public DayPP() {
		super();
	}
	
	public String getAccountpp() {
		return accountpp;
	}

	public void setAccountpp(String accountpp) {
		this.accountpp = accountpp;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public double getDaytotal() {
		return daytotal;
	}
	public void setDaytotal(double daytotal) {
		this.daytotal = daytotal;
	}

	public DayPP(int id, String accountpp, String day, double daytotal) {
		super();
		this.id = id;
		this.accountpp = accountpp;
		this.day = day;
		this.daytotal = daytotal;
	}

	public int getRound() {
		return round;
	}

	public void setRound(int round) {
		this.round = round;
	}
	
}
