package shoes.sup.model;

public class AdidasModel {
	private String context;
	private String type;
	private String sku;
	private String brand;
	private String[] image;
	private String name;
	private String color;
	private String category;
	private String description;
	private Offers offers;
	
	
	public Offers getOffers() {
		return offers;
	}
	

	public AdidasModel(String context, String type, String sku, String brand, String[] image, String name, String color,
			String category, String description, Offers offers) {
		super();
		this.context = context;
		this.type = type;
		this.sku = sku;
		this.brand = brand;
		this.image = image;
		this.name = name;
		this.color = color;
		this.category = category;
		this.description = description;
		this.offers = offers;
	}


	public void setOffers(Offers offers) {
		this.offers = offers;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public AdidasModel() {
		super();
	}
	
	public String getContext() {
		return context;
	}
	public void setContext(String context) {
		this.context = context;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String[] getImage() {
		return image;
	}
	public void setImage(String[] image) {
		this.image = image;
	}
	
	
	
}
