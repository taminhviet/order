package shoes.sup.model;

public class SaleAmountDetail {

	private String subtotal;
	

	public SaleAmountDetail(String subtotal) {
		super();
		this.subtotal = subtotal;
	}

	public String getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(String subtotal) {
		this.subtotal = subtotal;
	}
	
}