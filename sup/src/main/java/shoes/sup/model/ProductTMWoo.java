package shoes.sup.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ProductTMWoo")
public class ProductTMWoo {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
	
	@Column(name="slug")
    private String slug;
	
	@Column(name="domain")
    private String domain;

	@Column(name="productid")
    private String productid;
	
	@Column(name="category")
    private String category;
	
	@Column(name="title", length = 1000,columnDefinition="LONGTEXT")
    private String title;
	
	@Column(name="description", length = 5000,columnDefinition="LONGTEXT")
    private String description;
	
	@Column(name="price")
    private String price;
	
	@Column(name="variant")
    private String variant;
	
	@Column(name="parentid")
    private String parentid;
	
	@Column(name="image", length = 1000,columnDefinition="LONGTEXT")
    private String image;

	@Column(name="color")
    private String color;
	
	@Column(name="weight")
    private String weight;
	
	@Column(name="gtin")
    private String gtin;
	
	@Column(name="feedstatus")
    private String feedstatus;
	
	@Column(name="feedgmcid")
    private String feedgmcid;
	
	@Column(name="listgmc")
    private String listgmc;
	
	@Column(name="permalink", length = 1000,columnDefinition="LONGTEXT")
    private String permalink;
	
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	
	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public ProductTMWoo(String slug, String domain, String productid, String category, String title,
			String description, String price, String variant, String parentid, String image, String color, String weight, String gtin, String permalink) {
		super();
		this.slug = slug;
		this.domain = domain;
		this.productid = productid;
		this.category = category;
		this.title = title;
		this.description = description;
		this.price = price;
		this.variant = variant;
		this.parentid = parentid;
		this.image = image;
		this.color = color;
		this.weight = weight;
		this.gtin = gtin;
		this.permalink = permalink;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSlug() {
		return slug;
	}



	public void setSlug(String slug) {
		this.slug = slug;
	}



	public String getDomain() {
		return domain;
	}



	public void setDomain(String domain) {
		this.domain = domain;
	}



	public String getProductid() {
		return productid;
	}



	public void setProductid(String productid) {
		this.productid = productid;
	}



	public String getCategory() {
		return category;
	}



	public void setCategory(String category) {
		this.category = category;
	}



	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public String getPrice() {
		return price;
	}



	public void setPrice(String price) {
		this.price = price;
	}



	public String getVariant() {
		return variant;
	}



	public void setVariant(String variant) {
		this.variant = variant;
	}



	public String getParentid() {
		return parentid;
	}



	public void setParentid(String parentid) {
		this.parentid = parentid;
	}



	public String getImage() {
		return image;
	}



	public void setImage(String image) {
		this.image = image;
	}

	public String getFeedstatus() {
		return feedstatus;
	}

	public void setFeedstatus(String feedstatus) {
		this.feedstatus = feedstatus;
	}

	public String getFeedgmcid() {
		return feedgmcid;
	}

	public void setFeedgmcid(String feedgmcid) {
		this.feedgmcid = feedgmcid;
	}

	public String getGtin() {
		return gtin;
	}

	public void setGtin(String gtin) {
		this.gtin = gtin;
	}

	public ProductTMWoo() {
		super();
	}


	public ProductTMWoo (String slug, String productid, String category, String title, String description,
			String price, String variant, String parentid, String image) {
		super();
		this.slug = slug;
		this.productid = productid;
		this.category = category;
		this.title = title;
		this.description = description;
		this.price = price;
		this.variant = variant;
		this.parentid = parentid;
		this.image = image;
	}



	public ProductTMWoo(int id, String slug, String productid, String category, String title,
			String description, String price, String variant, String parentid, String image) {
		super();
		this.id = id;
		this.slug = slug;
		this.productid = productid;
		this.category = category;
		this.title = title;
		this.description = description;
		this.price = price;
		this.variant = variant;
		this.parentid = parentid;
		this.image = image;
	}

	public String getListgmc() {
		return listgmc;
	}

	public void setListgmc(String listgmc) {
		this.listgmc = listgmc;
	}

	public String getPermalink() {
		return permalink;
	}

	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}
}

