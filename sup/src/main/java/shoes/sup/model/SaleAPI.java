package shoes.sup.model;

public class SaleAPI {
	String id;
	SaleAmount amount;
	String invoice_number;
	
	public SaleAPI() {
		super();
	}
	public SaleAPI(String id, SaleAmount amount, String invoice_number) {
		super();
		this.id = id;
		this.amount = amount;
		this.invoice_number = invoice_number;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public SaleAmount getAmount() {
		return amount;
	}
	public void setAmount(SaleAmount amount) {
		this.amount = amount;
	}
	public String getInvoice_number() {
		return invoice_number;
	}
	public void setInvoice_number(String invoice_number) {
		this.invoice_number = invoice_number;
	}
	
	
}

