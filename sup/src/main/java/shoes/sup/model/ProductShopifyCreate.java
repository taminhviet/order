package shoes.sup.model;

import java.util.List;

public class ProductShopifyCreate {
	private String title;
	private String body_html;
	private String vendor;
	private String product_type;
	private String handle;
	private String tags;
	private String status;
	private List<ShopifyVariants> variants;
	private List<ShopifyOptions> options;
	private List<ShopifyImages> images;
	private ShopifyImages image;
	
	public ProductShopifyCreate() {
		super();
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBody_html() {
		return body_html;
	}
	public void setBody_html(String body_html) {
		this.body_html = body_html;
	}
	public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	public String getProduct_type() {
		return product_type;
	}
	public void setProduct_type(String product_type) {
		this.product_type = product_type;
	}
	public String getHandle() {
		return handle;
	}
	public void setHandle(String handle) {
		this.handle = handle;
	}
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	public List<ShopifyVariants> getVariants() {
		return variants;
	}
	public void setVariants(List<ShopifyVariants> variants) {
		this.variants = variants;
	}
	public List<ShopifyOptions> getOptions() {
		return options;
	}
	public void setOptions(List<ShopifyOptions> options) {
		this.options = options;
	}
	public List<ShopifyImages> getImages() {
		return images;
	}
	public void setImages(List<ShopifyImages> images) {
		this.images = images;
	}
	public ShopifyImages getImage() {
		return image;
	}
	public void setImage(ShopifyImages image) {
		this.image = image;
	}
	
	
}
