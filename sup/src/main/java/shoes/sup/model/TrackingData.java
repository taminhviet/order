package shoes.sup.model;

import com.google.gson.annotations.SerializedName;

public class TrackingData {
	private int id;
    @SerializedName("tracking_number")
    private String trackingNumber;
    @SerializedName("tracking_type")
    private String trackingType;
    @SerializedName("scheduled_delivery")
    private String scheduledDelivery;
    @SerializedName("packed_at")
    private String packedAt;
    @SerializedName("pickup_at")
    private String pickupAt;
    @SerializedName("shipping_address")
    private String shippingAddress;
    private String zipcode;
    private String country;
    private String state;
    private double weight;
    private String status;
    @SerializedName("scheduled_vol")
    private String scheduledVol;
    private String url;
    private int prpt;
    private int ed;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTrackingNumber() {
		return trackingNumber;
	}
	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}
	public String getTrackingType() {
		return trackingType;
	}
	public void setTrackingType(String trackingType) {
		this.trackingType = trackingType;
	}
	public String getScheduledDelivery() {
		return scheduledDelivery;
	}
	public void setScheduledDelivery(String scheduledDelivery) {
		this.scheduledDelivery = scheduledDelivery;
	}
	public String getPackedAt() {
		return packedAt;
	}
	public void setPackedAt(String packedAt) {
		this.packedAt = packedAt;
	}
	public String getPickupAt() {
		return pickupAt;
	}
	public void setPickupAt(String pickupAt) {
		this.pickupAt = pickupAt;
	}
	public String getShippingAddress() {
		return shippingAddress;
	}
	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getScheduledVol() {
		return scheduledVol;
	}
	public void setScheduledVol(String scheduledVol) {
		this.scheduledVol = scheduledVol;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getPrpt() {
		return prpt;
	}
	public void setPrpt(int prpt) {
		this.prpt = prpt;
	}
	public int getEd() {
		return ed;
	}
	public void setEd(int ed) {
		this.ed = ed;
	}
    
    
}
