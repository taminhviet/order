package shoes.sup.model;

import java.util.List;

public class CaptureFail {

	String name;
	String message;
	String debug_id;
	List<CaptureFailDetail> details;
	
	public CaptureFail(String name, String message, String debug_id,
			List<CaptureFailDetail> details) {
		super();
		this.name = name;
		this.message = message;
		this.debug_id = debug_id;
		this.details = details;
	}
	public CaptureFail() {
		super();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDebug_id() {
		return debug_id;
	}
	public void setDebug_id(String debug_id) {
		this.debug_id = debug_id;
	}
	public List<CaptureFailDetail> getDetails() {
		return details;
	}
	public void setDetails(List<CaptureFailDetail> details) {
		this.details = details;
	}
	
}
