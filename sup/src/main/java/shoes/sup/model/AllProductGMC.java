package shoes.sup.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
public class AllProductGMC {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
	
    private String domain;
	
    private String gmcid;
	
    private String productid;
	
    private String offerid;
	
    private String country;
	
    private String feedlabel;
	
    private String category;
	
    private String title;
	
    private String description;
	
    private String price;
	
    private String currency;
	
    private String parentid;
	
    private String image;
	
    private String limk;
	
    private String supfeedtm;
	
    private String producttmid;
	
    private String producttmtitle;
	
    private String gtin;
    
    private String gmcstatus;
    
    private String gmcnote;
    
    private String suspendeddate;
    
	private String vpsserver;

	private String accountSampleUser;
	
	private String additonalimage;
   
	public String getVpsserver() {
		return vpsserver;
	}

	public void setVpsserver(String vpsserver) {
		this.vpsserver = vpsserver;
	}

	public String getSuspendeddate() {
		return suspendeddate;
	}

	public void setSuspendeddate(String suspendeddate) {
		this.suspendeddate = suspendeddate;
	}

	public int getId() {
		return id;
	}

	public String getAdditonalimage() {
		return additonalimage;
	}

	public void setAdditonalimage(String additonalimage) {
		this.additonalimage = additonalimage;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAccountSampleUser() {
		return accountSampleUser;
	}

	public void setAccountSampleUser(String accountSampleUser) {
		this.accountSampleUser = accountSampleUser;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getGmcid() {
		return gmcid;
	}

	public void setGmcid(String gmcid) {
		this.gmcid = gmcid;
	}

	public String getProductid() {
		return productid;
	}

	public void setProductid(String productid) {
		this.productid = productid;
	}

	public String getOfferid() {
		return offerid;
	}

	
	public String getGmcnote() {
		return gmcnote;
	}

	public void setGmcnote(String gmcnote) {
		this.gmcnote = gmcnote;
	}

	public void setOfferid(String offerid) {
		this.offerid = offerid;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getFeedlabel() {
		return feedlabel;
	}

	public void setFeedlabel(String feedlabel) {
		this.feedlabel = feedlabel;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getParentid() {
		return parentid;
	}

	public void setParentid(String parentid) {
		this.parentid = parentid;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getLimk() {
		return limk;
	}

	public void setLimk(String limk) {
		this.limk = limk;
	}
	
	public String getGmcstatus() {
		return gmcstatus;
	}

	public void setGmcstatus(String gmcstatus) {
		this.gmcstatus = gmcstatus;
	}

	public AllProductGMC(String domain, String gmcid, String productid, String offerid, String country, String feedlabel,
			String category, String title, String description, String price, String parentid, String image,
			String limk, String currency) {
		super();
		this.domain = domain;
		this.gmcid = gmcid;
		this.productid = productid;
		this.offerid = offerid;
		this.country = country;
		this.feedlabel = feedlabel;
		this.category = category;
		this.title = title;
		this.description = description;
		this.price = price;
		this.parentid = parentid;
		this.image = image;
		this.limk = limk;
		this.currency = currency;
	}

	
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public AllProductGMC() {
		super();
	}

	public String getSupfeedtm() {
		return supfeedtm;
	}

	public void setSupfeedtm(String supfeedtm) {
		this.supfeedtm = supfeedtm;
	}

	public String getProducttmid() {
		return producttmid;
	}

	public void setProducttmid(String producttmid) {
		this.producttmid = producttmid;
	}

	public String getProducttmtitle() {
		return producttmtitle;
	}

	public void setProducttmtitle(String producttmtitle) {
		this.producttmtitle = producttmtitle;
	}

	public String getGtin() {
		return gtin;
	}

	public void setGtin(String gtin) {
		this.gtin = gtin;
	}
	
}
