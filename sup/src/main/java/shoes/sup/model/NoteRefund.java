package shoes.sup.model;

public class NoteRefund {

	String note;
	
	public NoteRefund(String note) {
		super();
		this.note = note;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
	
}
