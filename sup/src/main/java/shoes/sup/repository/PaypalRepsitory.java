package shoes.sup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import shoes.sup.model.PaypalAccount;

@Repository
public interface PaypalRepsitory extends JpaRepository<PaypalAccount, Integer>{

	PaypalAccount findByEmail(String email);
	PaypalAccount findByUsername(String username);
	List<PaypalAccount> findByActive(String active);
	List<PaypalAccount> findByActiveAndQueue(String active, String queue);
	PaypalAccount findByClientid(String clientid);
}
