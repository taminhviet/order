package shoes.sup.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import shoes.sup.model.ProxyCheck;

@Repository
public interface ProxyCheckRepository extends JpaRepository<ProxyCheck, Integer>{

	ProxyCheck findByLicenseAndCreateDate(String license, Date createDate);
}
