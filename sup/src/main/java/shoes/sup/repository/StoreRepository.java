package shoes.sup.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import shoes.sup.model.Store;

@Repository
public interface StoreRepository extends JpaRepository<Store, Integer>{

	Store findByDomain(String domain);
	List<Store> findByType(int type);
	

	@Transactional
	@Query("SELECT s FROM Store s WHERE s.type = ?1 AND s.state = ?2")
	List<Store> searchbyType(int type, String state);
}
