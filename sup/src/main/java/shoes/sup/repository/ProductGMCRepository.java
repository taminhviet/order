package shoes.sup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import shoes.sup.model.ProductGMC;

@Repository
public interface ProductGMCRepository extends JpaRepository<ProductGMC, Integer>{
	public List<ProductGMC> findByGmcidAndOfferid(String gmcid, String offerid);
	public List<ProductGMC> findByGmcid(String gmcid);
	public List<ProductGMC> findByGmcidAndParentid(String gmcid,String parentid);
	public List<ProductGMC> findByParentid(String parentid);
	public List<ProductGMC> findByProducttmtitleNotNull();
	public List<ProductGMC> findByGmcidAndOfferidAndCountry(String gmcid, String offerid, String country);
	public List<ProductGMC> findByGmcidAndProductid(String gmcid, String productid);
}
