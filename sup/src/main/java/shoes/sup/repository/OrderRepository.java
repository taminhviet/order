package shoes.sup.repository;


import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import shoes.sup.model.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Integer>{

	List<Order> findTop1000BySupplier(String supplier, Pageable pageable);
	
	List<Order> findTop2000BySupplier(String supplier, Pageable pageable);
	
	List<Order> findByType(int type);
	
	List<Order> findByOname(String oname);
	
	List<Order> findByStatus(String status);
	
	List<Order> findBySupplier(String supplier);
	
	List<Order> findByStoredomain(String storedomain);
	
	List<Order> findByLastsupplierContaining(String lastsupplier);
	
	List<Order> findByDisputed(String disputed);
	
	List<Order> findByProduct(String product);
	
	List<Order> findByPaypalaccountid(String paypalaccountid);
	
	@Transactional
	@Query("SELECT o FROM Order o WHERE o.supplier = ?1 AND o.product = ?2")
	List<Order> findBySupplierProduct(String supplier, String product);
	
	@Transactional
	@Query("SELECT o FROM Order o WHERE o.oname = ?1 AND o.supplier = ?2")
	List<Order> searchOrder(String oname, String supplier);
	
	@Transactional
	@Query("SELECT o FROM Order o WHERE o.type = ?1 AND o.status = ?2 AND o.payment_status = ?3 AND o.product = ?4")
	List<Order> findCancelWoo(int type, String status, String payment_status, String product);
	
	@Transactional
	@Query("SELECT o FROM Order o WHERE o.payment_status = ?1")
	List<Order> findByPaymentStatus(String payment_status);
	
	
	@Transactional
	@Query("SELECT o FROM Order o WHERE o.capture_status = ?1 AND o.capture_issue = ?2")
	List<Order> findPayerFail(String capture_status, String capture_issue);
	
	@Transactional
	@Query("SELECT o FROM Order o WHERE o.transaction_id = ?1")
	List<Order> findByTransaction_id(String transaction_id);
	
	@Transactional
	@Query("SELECT o FROM Order o WHERE o.status = ?1 AND o.supplier = ?2")
	List<Order> searchOrderByStatusSupplier(String status, String supplier);
	
	@Transactional
	@Query("SELECT o FROM Order o WHERE o.status = ?1 AND o.storedomain = ?2")
	List<Order> searchByStatusStore(String status, String storedomain);
	
	@Transactional
	@Query("SELECT o FROM Order o WHERE o.supplier = ?1 AND o.lastsupplier != ?2")
	List<Order> searchBySupplier(String supplier,String lastsupplier);
	
	@Transactional
	@Query("SELECT o FROM Order o WHERE o.oname LIKE CONCAT('%',:storecode,'%')")
	List<Order> findAllOrderWithCode(@Param("storecode") String storecode);
}
