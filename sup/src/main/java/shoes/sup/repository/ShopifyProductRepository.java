package shoes.sup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import shoes.sup.model.ShopifyProducts;

@Repository
public interface ShopifyProductRepository extends JpaRepository<ShopifyProducts, Integer>{
	List<ShopifyProducts> findByHandleAndOption1value(String handle, String option1value);
	List<ShopifyProducts> findByHandle(String handle);
	List<ShopifyProducts> findByTitle(String title);
}
