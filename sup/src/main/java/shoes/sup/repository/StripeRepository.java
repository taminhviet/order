package shoes.sup.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import shoes.sup.model.StripeAccount;

@Repository
public interface StripeRepository extends JpaRepository<StripeAccount, Integer>{

	public StripeAccount findByKey(String key);
}
