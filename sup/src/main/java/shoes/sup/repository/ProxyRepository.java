package shoes.sup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import shoes.sup.model.Proxy;


@Repository
public interface ProxyRepository extends JpaRepository<Proxy, Integer>{
	List<Proxy> findByLicense(String license);
	Proxy findByLicenseAndServer(String license, String server);
	Proxy findByServer(String server);
}
