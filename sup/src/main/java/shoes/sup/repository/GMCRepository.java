package shoes.sup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import shoes.sup.model.GMC;

@Repository
public interface GMCRepository extends JpaRepository<GMC, String>{

	public GMC findByMerchantId(String merchantId);
	public GMC findByDomain(String domain);
	public GMC findByDomaintm(String domaintm);
	public List<GMC> findByStatusNot(String status);
	public List<GMC> findTop2ByOrderByCreateddateDescProjectidDesc();
}
