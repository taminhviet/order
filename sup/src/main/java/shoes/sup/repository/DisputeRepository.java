package shoes.sup.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import shoes.sup.model.Dispute;

@Repository
public interface DisputeRepository extends JpaRepository<Dispute, Integer>{

	Dispute findByDisputeid(String disputeid);
}
