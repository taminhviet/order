package shoes.sup.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import shoes.sup.model.DayPP;

@Repository
public interface DayPPRepository extends JpaRepository<DayPP, Integer>{
	
	DayPP findByDay(String day);
	
	@Transactional
	@Query("SELECT d FROM DayPP d WHERE d.accountpp = ?1 AND d.day = ?2")
	DayPP findByDayByPP(String accountpp, String day);
}
