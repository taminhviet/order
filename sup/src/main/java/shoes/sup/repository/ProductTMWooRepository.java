package shoes.sup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import shoes.sup.model.ProductTMWoo;

@Repository
public interface ProductTMWooRepository extends JpaRepository<ProductTMWoo, Integer>{
	List<ProductTMWoo> findByDomain(String domain);
	ProductTMWoo findByDomainAndProductid(String domain, String productid);
	List<ProductTMWoo> findByDomainAndParentid(String domain, String parentid);
	List<ProductTMWoo> removeByDomain(String domain);
	List<ProductTMWoo> findByParentid(String parentid);
}
