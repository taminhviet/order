package shoes.sup;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import shoes.sup.model.Images;
import shoes.sup.model.LineItem;
import shoes.sup.model.Order;
import shoes.sup.model.OrderaAPI;
import shoes.sup.model.Product;
import shoes.sup.model.ShippingAddress;
import shoes.sup.service.OrderService;
import shoes.sup.util.Constants;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class ShopifyRest {
	public static String APIKEY = "984adbca9f2045f23810b833a5c43281";
	public static String password = "ac38540d0476208369aa1251b1bf94b1";
	public static String shopURL = "maclevy.myshopify.com";
	public static String apiVersion = "/admin/api/2019-04/orders.json";
	@Autowired
	static OrderService orderService;
	
	public static void main(String[] args) {
		
		
		
		StringBuilder apiUrl = new StringBuilder();
		apiUrl.append("https://");
		apiUrl.append(APIKEY);
		apiUrl.append(":");
		apiUrl.append(password);
		apiUrl.append("@");
		apiUrl.append("shopURL");
		apiUrl.append(apiVersion);
		List<Order> listOrder = new ArrayList<Order>();
		try {
			CredentialsProvider provider = new BasicCredentialsProvider();
			UsernamePasswordCredentials credentials
			 = new UsernamePasswordCredentials(APIKEY, password);
			provider.setCredentials(AuthScope.ANY, credentials);
			  
			HttpClient client = HttpClientBuilder.create()
			  .setDefaultCredentialsProvider(provider)
			  .build();
			
			HttpResponse response = client.execute(
					  new HttpGet("https://maclevy.myshopify.com/admin/api/2019-04/orders.json"));

			if (response.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
				   + response.getStatusLine().getStatusCode());
			}

			BufferedReader br = new BufferedReader(
	                         new InputStreamReader((response.getEntity().getContent())));

			String output;
			String orderList = null;
			while ((output = br.readLine()) != null) {
				orderList = output;
			}
			orderList = orderList.substring(10, orderList.length()-1);
			client.getConnectionManager().shutdown();
			Gson gson=new Gson();
			Type collectionType = new TypeToken<Collection<OrderaAPI>>(){}.getType();
			List<OrderaAPI> lsOrder = gson.fromJson(orderList, collectionType);
			int i = 1;
			for (OrderaAPI orderaAPI : lsOrder) {
				Order order = new Order();
				String oname = orderaAPI.getName();
				order.setOname(oname);
				//set shipping address
				if(orderaAPI.getShipping_address() != null){
					ShippingAddress shippingAddress = orderaAPI.getShipping_address();
					StringBuilder sb = new StringBuilder();
					sb.append("name: ");
					sb.append(shippingAddress.getName());
					sb.append(Constants.BREAK_LINE);
					sb.append("address 1: ");
					sb.append(shippingAddress.getAddress1());
					sb.append(Constants.BREAK_LINE);
					sb.append("address 2: ");
					sb.append(shippingAddress.getAddress2());
					sb.append(Constants.BREAK_LINE);
					sb.append("city: ");
					sb.append(shippingAddress.getCity());
					sb.append(Constants.BREAK_LINE);
					sb.append("zip: ");
					sb.append(shippingAddress.getZip());
					sb.append(Constants.BREAK_LINE);
					sb.append("province:");
					sb.append(shippingAddress.getProvince());
					sb.append(Constants.BREAK_LINE);
					sb.append("country: ");
					sb.append(shippingAddress.getCountry());
					sb.append(Constants.BREAK_LINE);
					order.setAddress(sb.toString());
				}
				if(!orderaAPI.getLine_items().isEmpty()){
					LineItem lineItem = orderaAPI.getLine_items().get(0);
					String product_id = lineItem.getProduct_id();
					String product_name = lineItem.getName();
					order.setProductname(product_name);
					long quantity = lineItem.getQuantity();
					order.setQuantity(String.valueOf(quantity));
					String variant = lineItem.getVariant_title();
					order.setVariant(variant);
					String productUrl = "https://maclevy.myshopify.com/admin/api/2019-04/products/" + product_id + ".json";
					HttpClient client1 = HttpClientBuilder.create()
							  .setDefaultCredentialsProvider(provider)
							  .build();
					HttpResponse response1 = client1.execute(
							  new HttpGet(productUrl));
					if (response1.getStatusLine().getStatusCode() != 200) {
						throw new RuntimeException("Failed : HTTP error code : "
						   + response1.getStatusLine().getStatusCode());
					}

					BufferedReader br1 = new BufferedReader(
			                         new InputStreamReader((response1.getEntity().getContent())));

					String output1;
					String productlist = null;
					while ((output1 = br1.readLine()) != null) {
						productlist = output1;
					}
					client1.getConnectionManager().shutdown();
					productlist = productlist.substring(11, productlist.length()-1);
					Type collectionType1 = new TypeToken<Product>(){}.getType();
					Product product = gson.fromJson(productlist, collectionType1);
 					if(product != null){
						String style = product.getProduct_type();
						order.setStyle(style);
						Images image = product.getImages().get(0);
						String imageUrl = image.getSrc();
						order.setImageurl(imageUrl);
					}
				}
				listOrder.add(order);
				i++;
				if( i >=3 ){
					break;
				}
			}
			for (Order order : listOrder) {
				order.setCreateDate(new Date());
				order.setStatus(Constants.NEW_ORDER);
				orderService.saveOrder(order);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
