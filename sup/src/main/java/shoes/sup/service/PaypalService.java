package shoes.sup.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import shoes.sup.model.PaypalAccount;
import shoes.sup.repository.PaypalRepsitory;

@Service
public class PaypalService {

	@Autowired
	PaypalRepsitory paypalRepsitory;
	
	
	public List<PaypalAccount> listAll(){
		return paypalRepsitory.findAll();
	}
	
	public List<PaypalAccount> findByActive(String active){
		return paypalRepsitory.findByActive(active);
	}
	public List<PaypalAccount> findByActiveAndQueue(String active,String queue){
		return paypalRepsitory.findByActiveAndQueue(active,queue);
	}
	public PaypalAccount findByEmail(String email){
		PaypalAccount paypalAccount = paypalRepsitory.findByEmail(email);
		if(paypalAccount == null && email != null) {
			email =  email.replace("@", "_api1.");
			paypalAccount = paypalRepsitory.findByUsername(email);
		}
		return paypalAccount;
	}
	
	public PaypalAccount findByUsername(String username){
		return paypalRepsitory.findByUsername(username);
	}
	
	public PaypalAccount findByClientID(String clientid) {
		return paypalRepsitory.findByClientid(clientid);
	}
	
	public void savePaypalAccount(PaypalAccount paypalAccount){
		paypalRepsitory.save(paypalAccount);
	}
	
	public void deletePaypalAccount(PaypalAccount paypalAccount){
		paypalRepsitory.delete(paypalAccount);
	}
	
	public PaypalAccount findById(int id){
		return paypalRepsitory.findById(id).get();
	}
}
