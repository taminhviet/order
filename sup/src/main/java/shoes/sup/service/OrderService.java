package shoes.sup.service;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import shoes.sup.model.Order;
import shoes.sup.repository.OrderRepository;

@Service
public class OrderService {

	@Autowired
	OrderRepository orderRepository;
	
	public List<Order> findBySupplier(String supplier){
		Pageable page1000 = PageRequest.of(0, 500,Sort.by("id").descending());
		return orderRepository.findTop1000BySupplier(supplier,page1000);
	}
	
	public Order findById(int id){
		Order order =  orderRepository.findById(id).get();
		return order;
	}
	
	public List<Order> findAll(){
		return orderRepository.findAll();
	}
	
	public List<Order> findOrder50(){
		Pageable page50 = PageRequest.of(0, 50,Sort.by("id").descending());
		return orderRepository.findAll(page50).getContent();
	}
	public List<Order> findOrder200(){
		Pageable page200 = PageRequest.of(0, 200,Sort.by("id").descending());
		return orderRepository.findAll(page200).getContent();
	}
	
	public List<Order> findOrder2000(){
		Pageable page2000 = PageRequest.of(0, 1000,Sort.by("id").descending());
		return orderRepository.findAll(page2000).getContent();
	}
	
	public List<Order> findByType(int type){
		return orderRepository.findByType(type);
	}
	
	public List<Order> findCancelWoo(int type, String status, String payment_status, String product){
		return orderRepository.findCancelWoo(type, status, payment_status, product);
	}
	
	public List<Order> findByName(String oname, String supplier){
		List<Order> lsOrder = new ArrayList<Order>();
		lsOrder = orderRepository.searchOrder(oname, supplier);
		return lsOrder;
	}
	
	public List<Order> findPayerFail(String capture_status, String capture_issue){
		return orderRepository.findPayerFail(capture_status, capture_issue);
	}
	
	public List<Order> findOnHold(String payment_status){
		return orderRepository.findByPaymentStatus(payment_status);
	}
	
	public List<Order> findByStatusSupplier(String status, String supplier){
		List<Order> lsOrder = new ArrayList<Order>();
		lsOrder = orderRepository.searchOrderByStatusSupplier(status, supplier);
		return lsOrder;
	}
	
	public Order findByTransaction_id(String transaction_id){
		List<Order> orders = orderRepository.findByTransaction_id(transaction_id);
		if(orders != null && !orders.isEmpty()){
			return orders.get(0);
		}else {
			return null;
		}
	}
	
	public List<Order> findByDisputed(String disputed){
		return orderRepository.findByDisputed(disputed);
	}
	
	public List<Order> findByStatus(String status){
		List<Order> lsOrder = new ArrayList<Order>();
		lsOrder = orderRepository.findByStatus(status);
		return lsOrder;
	}
	
	public List<Order> findByStatusStore(String status, String storedomain){
		List<Order> lsOrder = orderRepository.searchByStatusStore(status, storedomain);
		return lsOrder;
	}
	
	public List<Order> findByStoreDomain(String storedomain){
		return orderRepository.findByStoredomain(storedomain);
	}
	
	public List<Order> findByPaypalAccountid(String paypalid){
		return orderRepository.findByPaypalaccountid(paypalid);
	}
	
	public List<Order> findBySupplier(String supplier, String lastSupplier, String product){
		List<Order> lsAllOrder = orderRepository.findBySupplier(null);
		//find cancel order by this supplier
		List<Order> lsCancelOrder = orderRepository.findByLastsupplierContaining(lastSupplier);
		//remove cancel order by username
		for (Order order : lsCancelOrder) {
			if(lsAllOrder.contains(order)){
				lsAllOrder.remove(order);
			}
		}
		for (Iterator iterator = lsAllOrder.iterator(); iterator.hasNext();) {
			Order order = (Order) iterator.next();
			if(!product.equalsIgnoreCase(order.getProduct())){
				iterator.remove();
			}
		}
		return lsAllOrder;
	}
	
	public List<Order> findAdminByName(String oname){
		List<Order> lsOrder = new ArrayList<Order>();
		lsOrder = orderRepository.findByOname(oname);
		return lsOrder;
	}
	public List<Order> findAllOrderWithCode(String storecode){
		List<Order> lsOrder = new ArrayList<Order>();
		lsOrder = orderRepository.findAllOrderWithCode(storecode);
		return lsOrder;
	}
	
	public void saveOrder(Order order){
		orderRepository.save(order);
	}
	
	public void deleteOrder (Order order){
		orderRepository.delete(order);
	}
	
	public void clearOrder(){
		orderRepository.deleteAll();
	}
}
