package shoes.sup.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import shoes.sup.model.ProductTMWoo;
import shoes.sup.repository.ProductTMWooRepository;
import shoes.sup.util.Constants;

@Service
@org.springframework.transaction.annotation.Transactional
public class ProductTMWooService {
	
	@Autowired
	ProductTMWooRepository productTMWooRepo;
	
	public List<ProductTMWoo> listAll(){
		return productTMWooRepo.findAll();
	}
	public void saveProductTM(ProductTMWoo product) {
		productTMWooRepo.save(product);
	}
	public ProductTMWoo findByDomainAndProductid(String domain, String productid) {
		return productTMWooRepo.findByDomainAndProductid(domain, productid);
	}
	public List<ProductTMWoo> findByDomainAndParentid(String domain, String parentid) {
		return productTMWooRepo.findByDomainAndParentid(domain, parentid);
	}
	public void deleteProductTM(ProductTMWoo product) {
		productTMWooRepo.delete(product);
	}
	public List<ProductTMWoo> findByDomain(String domain){
		return productTMWooRepo.findByDomain(domain);
	}
	public List<ProductTMWoo> deleteByDomain(String domain){
		return productTMWooRepo.removeByDomain(domain);
	}
	public ProductTMWoo findById(int id) {
		return productTMWooRepo.findById(id).get();
	}
	public List<ProductTMWoo> findByParentid(String parentid){
		return productTMWooRepo.findByParentid(parentid);
	}
}
