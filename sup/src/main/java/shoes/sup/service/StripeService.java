package shoes.sup.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import shoes.sup.model.StripeAccount;
import shoes.sup.repository.StripeRepository;

@Service
public class StripeService {

	@Autowired
	StripeRepository stripeReposotiry;
	
	public List<StripeAccount> findAll(){
		return stripeReposotiry.findAll();
	}
	
	public void saveStripe(StripeAccount stripe) {
		stripeReposotiry.save(stripe);
	}
	
	public StripeAccount findByKey(String key) {
		return stripeReposotiry.findByKey(key);
	}
}
