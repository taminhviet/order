package shoes.sup.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import shoes.sup.model.ShopifyProducts;
import shoes.sup.repository.ShopifyProductRepository;

@Service
public class ShopifyProductService {
	
	@Autowired
	ShopifyProductRepository shopifyProductRepository;
	
	public List<ShopifyProducts> findAll(){
		return shopifyProductRepository.findAll();
	}
	
	public void saveShopifyProduct(ShopifyProducts shopifyProduct) {
		shopifyProductRepository.save(shopifyProduct);
	}
	
	public void deleteShopifyProduct(ShopifyProducts shopifyProduct) {
		shopifyProductRepository.delete(shopifyProduct);
	}
	
	public ShopifyProducts findById(int id) {
		return shopifyProductRepository.findById(id).get();
	}
	
	public List<ShopifyProducts> findByHandle(String handle){
		return shopifyProductRepository.findByHandle(handle);
	}
	
	public List<ShopifyProducts> findByTitle(String title){
		return shopifyProductRepository.findByTitle(title);
	}
	
	public List<ShopifyProducts> findByHandleAndSize(String hanle, String size){
		return shopifyProductRepository.findByHandleAndOption1value(hanle, size);
	}
}
