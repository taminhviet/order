package shoes.sup.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import shoes.sup.model.Proxy;
import shoes.sup.repository.ProxyRepository;
import org.springframework.stereotype.Service;

@Service
public class ProxyService {

	@Autowired
	ProxyRepository proxyRepository;
	
	public List<Proxy> findByLicense(String license){
		return proxyRepository.findByLicense(license);
	}
	
	public Proxy findByLicenseAndServer(String license, String server) {
		return proxyRepository.findByLicenseAndServer(license, server);
	}
	
	public Proxy findByServer(String server) {
		return proxyRepository.findByServer(server);
	}
	
	
	public void saveProxy(Proxy proxy) {
		proxyRepository.save(proxy);
	}
	
	public void deleteProxy(Proxy proxy) {
		proxyRepository.delete(proxy);
	}
}
