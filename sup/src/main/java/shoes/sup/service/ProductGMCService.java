package shoes.sup.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import shoes.sup.model.ProductGMC;
import shoes.sup.repository.ProductGMCRepository;

@Service
@Transactional
public class ProductGMCService {

	@Autowired
	ProductGMCRepository productGMCRepo;
	
	public List<ProductGMC> findAll(){
		return productGMCRepo.findAll();
	}
	
	public List<ProductGMC> findByGmcid(String gmcid){
		return productGMCRepo.findByGmcid(gmcid);
	}
	
	public List<ProductGMC> findByGmcidAndOfferid(String gmcid, String offderid) {
		return productGMCRepo.findByGmcidAndOfferid(gmcid, offderid);
	}
	public List<ProductGMC> findByGmcidAndOfferidAndCountry(String gmcid, String offderid, String country) {
		return productGMCRepo.findByGmcidAndOfferidAndCountry(gmcid, offderid,country);
	}
	public List<ProductGMC> findByGmcidAndProductid(String gmcid, String productid) {
		return productGMCRepo.findByGmcidAndProductid(gmcid, productid);
	}
	
	public void saveProductGMC(ProductGMC productgmc) {
		productGMCRepo.save(productgmc);
	}
	
	public ProductGMC findById(int id) {
		return productGMCRepo.findById(id).get();
	}
	
	public List<ProductGMC> findByParentid(String parentid){
		return productGMCRepo.findByParentid(parentid);
	}
	public List<ProductGMC> findByGMCandParentid(String gmcid , String parentid){
		return productGMCRepo.findByGmcidAndParentid(gmcid,parentid);
	}
	public void deleteProductGMC(ProductGMC productgmc) {
		productGMCRepo.delete(productgmc);
	}
	public List<ProductGMC> getAllProductFeeded() {
		return productGMCRepo.findByProducttmtitleNotNull();
	}
}
