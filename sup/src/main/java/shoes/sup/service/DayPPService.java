package shoes.sup.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import shoes.sup.model.DayPP;
import shoes.sup.repository.DayPPRepository;

@Service
public class DayPPService {

	@Autowired
	DayPPRepository dayPPRepository;
	
	public void saveDayPP(DayPP dayPP){
		dayPPRepository.save(dayPP);
	}
	
	public DayPP findByDayByPP(String ppUsername,String day){
		return dayPPRepository.findByDayByPP(ppUsername, day);
	}
}
