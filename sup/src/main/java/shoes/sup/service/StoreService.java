package shoes.sup.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import shoes.sup.model.Store;
import shoes.sup.repository.StoreRepository;

@Service
public class StoreService {

	@Autowired
	StoreRepository storeRepository;
	
	public void saveStore(Store store){
		storeRepository.save(store);
	}
	
	public List<Store> getAllStore(){
		return storeRepository.findAll();
	}
	
	public List<Store> getByType(int type){
		return storeRepository.findByType(type);
	}
	
	public List<Store> searchByType(int type, String state){
		return storeRepository.searchbyType(type, state);
	}
	
	public Store getStoreByDomain(String domain){
		return storeRepository.findByDomain(domain);
	}
	
	public Store getStore(int id){
		return storeRepository.findById(id).get();
	}
	
	public void deleteStore(Store store){
		storeRepository.delete(store);
	}
}
