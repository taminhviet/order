package shoes.sup.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import shoes.sup.model.User;
import shoes.sup.repository.UserRepository;
import shoes.sup.util.Constants;

@Service
public class UserService {
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	PasswordEncoder bCryptPasswordEncoder;
	
	public User findbyUsername(String username){
		return userRepository.findByUsername(username);
	}
	public void saveUser(User user){
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		userRepository.save(user);
	}
	
	public void deleteUser(User user){
		userRepository.delete(user);
	}
	
	public List<User> getAllUser(){
		return userRepository.findAll();
	}
	public List<User> findSupplier(){
		return userRepository.findByIsAdmin(Constants.ROLE_SUPPLIER);
	}
}
