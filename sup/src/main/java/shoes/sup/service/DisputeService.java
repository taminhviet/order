package shoes.sup.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import shoes.sup.model.Dispute;
import shoes.sup.repository.DisputeRepository;

@Service
public class DisputeService {

	@Autowired
	DisputeRepository disputeRepository;
	
	public void saveDispute(Dispute dispute){
		disputeRepository.save(dispute);
	}
	
	public void deleteDispute(){
		disputeRepository.deleteAll();
	}
	
	public List<Dispute> listAll(){
		return disputeRepository.findAll();
	}
	
	public Dispute findByDisputeId(String disputeid){
		return disputeRepository.findByDisputeid(disputeid);
	}
}
