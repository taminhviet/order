package shoes.sup.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import shoes.sup.model.ProxyCheck;
import shoes.sup.repository.ProxyCheckRepository;

@Service
public class ProxycheckService {

	@Autowired
	ProxyCheckRepository proxyCheckRepository;
	
	public List<ProxyCheck> findAll(){
		return proxyCheckRepository.findAll();
	}
	public void saveProxyCheck(ProxyCheck proxyCheck) {
		proxyCheckRepository.save(proxyCheck);
	}
	public ProxyCheck findByLicenseAndDate(String license, Date date) {
		return proxyCheckRepository.findByLicenseAndCreateDate(license, date);
	}
}
