package shoes.sup.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import shoes.sup.model.GMC;
import shoes.sup.repository.GMCRepository;
import shoes.sup.util.Constants;

@Service
public class GMCService {

	@Autowired
	GMCRepository gmcRepository;
	
	public GMC findByID(String merchantId) {
		return gmcRepository.findByMerchantId(merchantId);
	}
	public GMC findByDomain(String domain) {
		return gmcRepository.findByDomain(domain);
	}
//	public GMC findByDomaintm(String domaintm) {
//		return gmcRepository.findByDomaintm(domaintm);
//	}
	public List<GMC> findAll(){
		return gmcRepository.findAll();
	}
	public List<GMC> findLast100(){
		Pageable page100 = PageRequest.of(0, 100,Sort.by("projectid").descending());
		return gmcRepository.findAll(page100).getContent();
	}
	public void saveGMC(GMC gmc) {
		gmcRepository.save(gmc);
	}
	public void deleteGMC(GMC gmc) {
		gmcRepository.delete(gmc);
	}
	public List<GMC> findNotDisable(){
		return gmcRepository.findByStatusNot(Constants.DISABLE);
	}
	public GMC findLastSecondGMC(){
		List<GMC> allGMC = gmcRepository.findAll();
		GMC last = allGMC.get(allGMC.size() - 2);
		return last;
	}
	public GMC findLastGMC(){
		List<GMC> allGMC = gmcRepository.findAll();
		GMC last = allGMC.get(allGMC.size() - 1);
		return last;
	}
}
