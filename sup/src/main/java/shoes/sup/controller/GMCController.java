package shoes.sup.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.google.api.services.content.ShoppingContent;
import com.google.api.services.content.model.Account;
import com.google.api.services.content.model.AccountStatus;
import com.google.api.services.content.model.AccountStatusAccountLevelIssue;
import com.google.api.services.content.model.BusinessDayConfig;
import com.google.api.services.content.model.Datafeed;
import com.google.api.services.content.model.DatafeedFetchSchedule;
import com.google.api.services.content.model.DatafeedFormat;
import com.google.api.services.content.model.DatafeedTarget;
import com.google.api.services.content.model.DatafeedsFetchNowResponse;
import com.google.api.services.content.model.DatafeedsListResponse;
import com.google.api.services.content.model.DeliveryTime;
import com.google.api.services.content.model.FreeListingsProgramStatus;
import com.google.api.services.content.model.FreeListingsProgramStatusRegionStatus;
import com.google.api.services.content.model.Headers;
import com.google.api.services.content.model.Price;
import com.google.api.services.content.model.Product;
import com.google.api.services.content.model.ProductsCustomBatchRequest;
import com.google.api.services.content.model.ProductsCustomBatchRequestEntry;
import com.google.api.services.content.model.ProductsCustomBatchResponse;
import com.google.api.services.content.model.ProductsCustomBatchResponseEntry;
import com.google.api.services.content.model.ProductsListResponse;
import com.google.api.services.content.model.RateGroup;
import com.google.api.services.content.model.RequestReviewFreeListingsRequest;
import com.google.api.services.content.model.Row;
import com.google.api.services.content.model.Service;
import com.google.api.services.content.model.ShippingSettings;
import com.google.api.services.content.model.ShoppingAdsProgramStatus;
import com.google.api.services.content.model.ShoppingAdsProgramStatusRegionStatus;
import com.google.api.services.content.model.Table;
import com.google.api.services.content.model.Value;
import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;

import shoes.sup.model.AllProductGMC;
import shoes.sup.model.CostGson;
import shoes.sup.model.DataFeedGMC;
import shoes.sup.model.GMC;
import shoes.sup.model.GMCNote;
import shoes.sup.model.GMCUpdateInput;
import shoes.sup.model.IdInput;
import shoes.sup.model.InputNotePP;
import shoes.sup.model.ProductGMC;
import shoes.sup.model.ProductShopifyCreate;
import shoes.sup.model.ProductShopifyCreateModel;
import shoes.sup.model.ProductTMWoo;
import shoes.sup.model.ShopifyImages;
import shoes.sup.model.ShopifyOptions;
import shoes.sup.model.ShopifyProducts;
import shoes.sup.model.ShopifyVariants;
import shoes.sup.model.Store;
import shoes.sup.service.GMCService;
import shoes.sup.service.ProductGMCService;
import shoes.sup.service.ProductTMWooService;
import shoes.sup.service.ShopifyProductService;
import shoes.sup.service.StoreService;
import shoes.sup.util.Constants;
import shopping.common.AuthenticatorGMC;
import shopping.common.model.FetchInput;
import shopping.common.model.MerchantsInfo;

@Controller
public class GMCController {

	@Autowired
	GMCService gmcService;

	@Autowired
	ProductTMWooService productTMWooService;

	@Autowired
	ProductGMCService productGMCService;
	
	@Autowired
	StoreService storeService;
	
	@Autowired
	ShopifyProductService shopifyProductService;

	@RequestMapping(value = "/admin/addgmc", method = RequestMethod.POST)
	public String addGMC(Model model, @RequestParam("file") MultipartFile file, HttpServletRequest request,
			@RequestParam("merchantId") String merchantId, @RequestParam("accountSampleUser") String accountSampleUser,
			@RequestParam("domain") String domain, @RequestParam("country") String country,
			@RequestParam(name ="domaintm", required = false) String domaintm,
			@RequestParam(name = "supfeedid", required = false) String supfeedid) {
		// Define the desired date format
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        // Get today's date
        LocalDate today = LocalDate.now();

        // Format the date as a string
        String formattedDate = today.format(formatter);

		Gson gson = new Gson();
		try {
			GMC gmcDomainExist = gmcService.findByDomain(domain.trim());
//			GMC gmcDomainTMExist = gmcService.findByDomaintm(domaintm.trim());
			if (gmcDomainExist == null) {
				InputStream xmlfile = file.getInputStream();
				Reader reader = new InputStreamReader(xmlfile, "UTF-8");
				MerchantsInfo merchantsInfo = gson.fromJson(reader, MerchantsInfo.class);
				String type = merchantsInfo.getType().trim();
				String project_id = merchantsInfo.getProject_id().trim();
				String private_key_id = merchantsInfo.getPrivate_key_id().trim();
				String private_key = merchantsInfo.getPrivate_key().trim();
				String client_email = merchantsInfo.getClient_email().trim();
				String client_id = merchantsInfo.getClient_id().trim();
				String auth_uri = merchantsInfo.getAuth_uri().trim();
				String token_uri = merchantsInfo.getToken_uri().trim();
				String auth_provider_x509_cert_url = merchantsInfo.getAuth_provider_x509_cert_url();
				String client_x509_cert_url = merchantsInfo.getClient_x509_cert_url();
				String universe_domain = merchantsInfo.getUniverse_domain();
				GMC gmcnew = new GMC(merchantId.trim(), accountSampleUser.trim(), type, project_id, private_key_id,
						private_key, client_email, client_id, auth_uri, token_uri, auth_provider_x509_cert_url,
						client_x509_cert_url, universe_domain, Constants.ENABLE, null, domain.trim(), country.trim(),
						supfeedid.trim(), domaintm.trim());
				gmcnew.setCreateddate(formattedDate);
				gmcService.saveGMC(gmcnew);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		List<GMC> lsgmc = gmcService.findLast100();
		if (lsgmc != null && !lsgmc.isEmpty()) {
			for (GMC gmc : lsgmc) {
				String gmcDate = gmc.getCreateddate();
				if(gmcDate != null && !gmcDate.isEmpty()) {
					String gmcDatenew = gmcDate.substring(6,10) + "-" + gmcDate.substring(3,5) + "-" + gmcDate.substring(0,2);
					gmc.setCreateddate(gmcDatenew);
				}
			}
			model.addAttribute("lsgmc", lsgmc);
		}
		GMC lastGMC = gmcService.findLastGMC();
		String countryLastGMC = lastGMC.getCountry();
		model.addAttribute("countryLastGMC", countryLastGMC);
		return "redirect:/admin/livegmc";
	}

	@RequestMapping(value = "/admin/editgmc", method = RequestMethod.POST)
	public String editGMC(Model model, @RequestParam("idedit") String idedit,
			@RequestParam("emailedit") String emailedit, @RequestParam("countryedit") String countryedit,
			@RequestParam(name = "domaintmedit", required = false) String domaintmedit,
			@RequestParam(name = "supfeedapiedit", required = false) String supfeedapiedit) {
		GMC updateGMC = gmcService.findByID(idedit);
		updateGMC.setAccountSampleUser(emailedit);
		updateGMC.setCountry(countryedit);
		updateGMC.setDomaintm(domaintmedit);
		updateGMC.setSupfeedid(supfeedapiedit);
		gmcService.saveGMC(updateGMC);
		return "redirect:/admin/livegmc";
	}
	@RequestMapping(value = "/admin/ajaxsuppfeedgmc",method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ajaxsuppfeedgmc(@RequestBody String json){
		String result = Constants.FAILED;
		Gson gson = new Gson();
		GMCNote noteGMC = gson.fromJson(json, GMCNote.class);
		String id = noteGMC.getId();
		GMC gmc = gmcService.findByID(id);
		String supfeedid = noteGMC.getNote();
		gmc.setSupfeedid(supfeedid);
		gmcService.saveGMC(gmc);
		result = Constants.SUCCESS;
		return ResponseEntity.ok(result);
	}
	
	@RequestMapping(value = "/admin/ajaxchangegmcbusiness",method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ajaxchangegmcbusiness(@RequestBody String json){
		String result = Constants.FAILED;
		Gson gson = new Gson();
		GMCNote noteGMC = gson.fromJson(json, GMCNote.class);
		String id = noteGMC.getId();
		GMC gmc = gmcService.findByID(id);
		String businessname = noteGMC.getNote();
		boolean isChange = updateBusinessName(gmc, businessname);
		if(isChange) {
			result = Constants.SUCCESS;
		}
		return ResponseEntity.ok(result);
	}
	
	@RequestMapping(value = "/admin/ajaxnotegmc",method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ajaxnotegmc(@RequestBody String json){
		String result = Constants.FAILED;
		Gson gson = new Gson();
		GMCNote noteGMC = gson.fromJson(json, GMCNote.class);
		String id = noteGMC.getId();
		GMC gmc = gmcService.findByID(id);
		String noteSet = noteGMC.getNote();
		gmc.setNote(noteSet);
		gmcService.saveGMC(gmc);
		result = Constants.SUCCESS;
		return ResponseEntity.ok(result);
	}

	@RequestMapping(value = "/admin/gmc", method = RequestMethod.GET)
	public String listGMC(Model model) {
		List<GMC> lsgmc = gmcService.findAll();
		if (lsgmc != null && !lsgmc.isEmpty()) {
			for (GMC gmc : lsgmc) {
				String gmcDate = gmc.getCreateddate();
				if(gmcDate != null && !gmcDate.isEmpty()) {
					String gmcDatenew = gmcDate.substring(6,10) + "-" + gmcDate.substring(3,5) + "-" + gmcDate.substring(0,2);
					gmc.setCreateddate(gmcDatenew);
				}
			}
			Collections.reverse(lsgmc);
			model.addAttribute("lsgmc", lsgmc);
			
		}
		GMC lastGMC = gmcService.findLastGMC();
		String countryLastGMC = lastGMC.getCountry();
		model.addAttribute("countryLastGMC", countryLastGMC);
		//checkDomainCyber(lsgmc);
		return "gmc";
	}
	
	private void checkDomainCyber(List<GMC> lsgmc) {
		List<String> domainCyber = lsDomainCyber();
		List<String> lsSuspend = new ArrayList<>();
		for (String domain : domainCyber) {
			Optional<GMC> gmcexist = lsgmc.stream()
	                .filter(p -> p.getDomain().equals(domain))
	                .findFirst();

	        // Check if a person is found and print the result
	        if (gmcexist.isPresent()) {
	        	String status = gmcexist.get().getStatus();
	        	String review = gmcexist.get().getEligibility();
	            if(status.equals(Constants.DISABLE) || (status.equals(Constants.SUSPENDED) && Constants.DISAPPROVED.equals(review))) {
	            	lsSuspend.add(domain);
	            }
	        }
		}
		String csvDeleteDomain = String.join(",", lsSuspend);
		System.out.println(csvDeleteDomain);
	}
	
	@RequestMapping(value = "/admin/livegmc", method = RequestMethod.GET)
	public String listliveGMC(Model model) {
		List<GMC> lsgmc = gmcService.findAll();
		List<GMC> activeGMC = new ArrayList<>();
		if (lsgmc != null && !lsgmc.isEmpty()) {
			for (GMC gmc : lsgmc) {
				String gmcstatus = gmc.getStatus();
				String gmcDate = gmc.getCreateddate();
				if(Constants.ENABLE.equalsIgnoreCase(gmcstatus) || Constants.REACTIVE.equalsIgnoreCase(gmcstatus)) {
					String gmcDatenew = gmcDate.substring(6,10) + "-" + gmcDate.substring(3,5) + "-" + gmcDate.substring(0,2);
					gmc.setCreateddate(gmcDatenew);
					activeGMC.add(gmc);
				}
			}
			Collections.reverse(activeGMC);
			model.addAttribute("lsgmc", activeGMC);
		}
		return "gmc";
	}

	@RequestMapping(value = "/admin/deletegmc", method = RequestMethod.POST)
	public String deleteGMC(Model model, @RequestParam("merchantId") String imerchantIdd) {
		GMC gmc = gmcService.findByID(imerchantIdd);
		if (gmc != null) {
			gmcService.deleteGMC(gmc);
		}
		return "redirect:/admin/gmc";
	}

	@RequestMapping(value = "/admin/ajaxcheckgmc", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ajaxcheckgmc(@RequestBody String json) {
		
		if (json.contains("\"")) {
			json = json.replace("\"", "");
		}
		Gson gson = new Gson();
		InputNotePP idInputGMC = gson.fromJson(json, InputNotePP.class);
		String id = idInputGMC.getId();
		GMC gmc = gmcService.findByID(id);
		String country = gmc.getCountry();
		String status = checkAccountStatus(gmc);
		String vps_server = gmc.getVpsserver();
		if("VPS_FR".equals(vps_server)){
			country = "FR";
		}else if("VPS_DE".equals(vps_server)){
			country = "DE";
		}
		if (Constants.SUSPENDED.equals(status) || Constants.REACTIVE.equals(status)) {
			gmc.setStatus(status);
		}
		if (Constants.SUSPENDED.equals(status)) {
			try {
				List<Datafeed> lsFeed = listDatafeedsForMerchant(gmc);
				for (Datafeed datafeed : lsFeed) {
					long feedID = datafeed.getId();
					List<DatafeedTarget> targets = datafeed.getTargets();
					for (DatafeedTarget target : targets) {
						List<String> targetCountry = target.getTargetCountries();
						if(targetCountry.size() >= 2) {
							String message = updatefeedGMC(gmc, feedID, country);
							if(Constants.SUCCESSED.equals(message)) {
								fetchGMC(gmc, feedID);
							}
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		GMC updatestatus = checkGoogleProgram(gmc, country);
		gmcService.saveGMC(updatestatus);
		return ResponseEntity
				.ok(status + ":" + updatestatus.getEligibility() + " : " + updatestatus.getRevieweligibility());
	}

	@RequestMapping(value = "/admin/ajaxreviewgmc", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ajaxreviewgmc(@RequestBody String json) {
		String result = Constants.FAILED;
		if (json.contains("\"")) {
			json = json.replace("\"", "");
		}
		Gson gson = new Gson();
		InputNotePP idInputGMC = gson.fromJson(json, InputNotePP.class);
		String id = idInputGMC.getId();
		String country = idInputGMC.getNotetext();
		GMC gmc = gmcService.findByID(id);
		result = requestReviewGMC(gmc, country);
		return ResponseEntity.ok(result);
	}
	
	@RequestMapping(value = "/admin/ajaxdisablegmc", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ajaxdisablegmc(@RequestBody String json) {
		String result = Constants.FAILED;
		if (json.contains("\"")) {
			json = json.replace("\"", "");
		}
		Gson gson = new Gson();
		IdInput idInputGMC = gson.fromJson(json, IdInput.class);
		String id = idInputGMC.getId();
		GMC gmc = gmcService.findByID(id);
		gmc.setStatus(Constants.DISABLE);
		gmcService.saveGMC(gmc);
		result = Constants.SUCCESS;
		return ResponseEntity.ok(result);
	}
	@RequestMapping(value = "/admin/ajaxdeletegmcproduct", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ajaxdeletegmcproduct(@RequestBody String json) {
		String result = Constants.FAILED;
		if (json.contains("\"")) {
			json = json.replace("\"", "");
		}
		Gson gson = new Gson();
		IdInput idInputGMC = gson.fromJson(json, IdInput.class);
		String gmcid = idInputGMC.getId();
		List<ProductGMC> lsDelete = productGMCService.findByGmcid(gmcid);
		for (ProductGMC productGMC : lsDelete) {
			productGMCService.deleteProductGMC(productGMC);
		}
		result = Constants.SUCCESS;
		return ResponseEntity.ok(result);
	}
	
	@RequestMapping(value = "/admin/ajaxenablegmc", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ajaxenablegmc(@RequestBody String json) {
		String result = Constants.FAILED;
		if (json.contains("\"")) {
			json = json.replace("\"", "");
		}
		Gson gson = new Gson();
		IdInput idInputGMC = gson.fromJson(json, IdInput.class);
		String id = idInputGMC.getId();
		GMC gmc = gmcService.findByID(id);
		gmc.setStatus(Constants.ENABLE);
		gmcService.saveGMC(gmc);
		result = Constants.SUCCESS;
		return ResponseEntity.ok(result);
	}

	private String requestReviewGMC(GMC gmc, String country) {
		String status = Constants.FAILED;
		try {
			AuthenticatorGMC gmcAuth = new AuthenticatorGMC(gmc);
			ShoppingContent content = gmcAuth.initBuilder();
			Long gmcID = Long.valueOf(gmc.getMerchantId());
			String suspendedType = gmc.getSuspendedtype();
//			if(suspendedType != null && Constants.GOOGLE_ADS.equals(suspendedType)) {
//				RequestReviewShoppingAdsRequest request = new RequestReviewShoppingAdsRequest();
//				request.setRegionCode(gmc.getCountry());
//				content.shoppingadsprogram().requestreview(gmcID, request).execute();
//				status = Constants.REVIEWED;
//			}else {
			RequestReviewFreeListingsRequest request = new RequestReviewFreeListingsRequest();
			request.setRegionCode(gmc.getCountry());
			content.freelistingsprogram().requestreview(gmcID, request).execute();
			status = Constants.REVIEWED;
//			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	@RequestMapping(path = "/admin/gmc/{merchantId}", method = RequestMethod.GET)
	public String gmcfeed(Model model, @PathVariable String merchantId) {
		GMC gmc = gmcService.findByID(merchantId);
		String domain = gmc.getDomain();
		String domainTM = gmc.getDomaintm();
		String email = gmc.getAccountSampleUser();
		String country = gmc.getCountry();
		model.addAttribute("domain", domain);
		model.addAttribute("domainTM", domainTM);
		model.addAttribute("email", email);
		model.addAttribute("merchantId", merchantId);
		Set<String> parentIDGMC = new HashSet<>();
		List<ProductTMWoo> lsproducttm = productTMWooService.findByDomain(domainTM);
		if (!lsproducttm.isEmpty()) {
			Set<String> parentIDSet = new HashSet<>();
			List<ProductTMWoo> lsproducttmByParent = lsproducttm.stream().filter(e -> parentIDSet.add(e.getParentid()))
					.collect(Collectors.toList());
			model.addAttribute("lsproducttm", lsproducttmByParent);
		}
		List<ProductGMC> lsProductGMC = productGMCService.findByGmcid(merchantId);
		if (!lsProductGMC.isEmpty()) {
			List<ProductGMC> lsproductGMCByParent = lsProductGMC.stream().filter(e -> parentIDGMC.add(e.getParentid()))
					.collect(Collectors.toList());
			model.addAttribute("lsProductGMC", lsProductGMC);
		}
		String gmcsupfeed = gmc.getSupfeedid();
		List<String> lsSupfeed = new ArrayList<>();
		if(!gmcsupfeed.isEmpty()) {
			if(!gmcsupfeed.contains(Constants.COMMA)) {
				lsSupfeed.add(gmcsupfeed);
			}else {
				String[] supfeedCountry = gmcsupfeed.split(Constants.COMMA);
				for (String supCountry : supfeedCountry) {
					lsSupfeed.add(supCountry);
				}
			}
		}
		model.addAttribute("message", Constants.EMPTY);
		model.addAttribute("lsSupfeed", lsSupfeed);
		int minvalue = 90;
		if(country.equals("PL")) {
			minvalue = 500;
		}
		model.addAttribute("minvalue", minvalue);
		return "gmcfeed";
	}
	@RequestMapping(path = "/admin/allproductgmcfeed", method = RequestMethod.GET)
	public String allproductgmcfeed(Model model) {
		List<ProductGMC> lsProductGMC = productGMCService.getAllProductFeeded();
		List<AllProductGMC> lsAllProductGMC = new ArrayList<>();
		if (!lsProductGMC.isEmpty()) {
			for (ProductGMC productGMC : lsProductGMC) {
				GMC gmc = gmcService.findByID(productGMC.getGmcid());
				AllProductGMC allGMC = new AllProductGMC();
				if(gmc != null) {
					allGMC.setGmcstatus(gmc.getStatus());
					allGMC.setGmcid(gmc.getMerchantId());
					allGMC.setGmcnote(gmc.getNote());
					allGMC.setSuspendeddate(gmc.getSuspenddate());
					allGMC.setDomain(productGMC.getDomain());
					allGMC.setProducttmtitle(productGMC.getProducttmtitle());
					allGMC.setSupfeedtm(productGMC.getSupfeedtm());
					allGMC.setPrice(productGMC.getPrice());
					allGMC.setImage(productGMC.getImage());
					allGMC.setCurrency(productGMC.getCurrency());
					allGMC.setTitle(productGMC.getTitle());
					allGMC.setParentid(productGMC.getParentid());
					allGMC.setCountry(productGMC.getCountry());
					allGMC.setAccountSampleUser(gmc.getAccountSampleUser());
					allGMC.setGtin(productGMC.getGtin());
					allGMC.setVpsserver(gmc.getVpsserver());
					allGMC.setAdditonalimage(productGMC.getAdditonalimage());
					lsAllProductGMC.add(allGMC);
				}
			}
			Set<String> parentIDSet = new HashSet<>();
			Set<String> imageLinkSet = new HashSet<>();
			List<AllProductGMC> lsproductByParent = lsAllProductGMC.stream().filter(e -> parentIDSet.add(e.getParentid()))
					.sorted(Comparator.comparing(AllProductGMC::getGmcid).reversed()).collect(Collectors.toList());
			List<AllProductGMC> lsproductfinal = lsproductByParent.stream().filter(e -> imageLinkSet.add(e.getGmcstatus() +"-" + e.getCurrency()+  "-" + e.getImage()))
					.sorted(Comparator.comparing(AllProductGMC::getGmcid).reversed()).collect(Collectors.toList());
			model.addAttribute("lsAllProductGMC", lsproductfinal);
		}
		model.addAttribute("message", Constants.EMPTY);
		return "allproductgmcfeed";
	}
	@RequestMapping(path = "/admin/searchproductgmc", method = RequestMethod.GET)
	public String searchproductgmc(Model model,@RequestParam("currency") String currency,@RequestParam("country") String country) {
		List<ProductGMC> lsProductGMC = productGMCService.findAll();
		List<AllProductGMC> lsAllProductGMC = new ArrayList<>();
		if (!lsProductGMC.isEmpty()) {
			for (ProductGMC productGMC : lsProductGMC) {
				GMC gmc = gmcService.findByID(productGMC.getGmcid());
				AllProductGMC allGMC = new AllProductGMC();
				if(gmc != null) {
					allGMC.setGmcstatus(gmc.getStatus());
					allGMC.setGmcid(gmc.getMerchantId());
					allGMC.setDomain(productGMC.getDomain());
					allGMC.setProducttmtitle(productGMC.getProducttmtitle());
					allGMC.setSupfeedtm(productGMC.getSupfeedtm());
					allGMC.setPrice(productGMC.getPrice());
					allGMC.setImage(productGMC.getImage());
					allGMC.setCurrency(productGMC.getCurrency());
					allGMC.setGmcnote(gmc.getNote());
					allGMC.setTitle(productGMC.getTitle());
					allGMC.setParentid(productGMC.getParentid());
					allGMC.setSuspendeddate(gmc.getSuspenddate());
					allGMC.setCountry(productGMC.getCountry());
					allGMC.setAccountSampleUser(gmc.getAccountSampleUser());
					allGMC.setVpsserver(gmc.getVpsserver());
					allGMC.setAdditonalimage(productGMC.getAdditonalimage());
					allGMC.setGtin(productGMC.getGtin());
					if(currency.trim().equalsIgnoreCase(productGMC.getCurrency()) && country.trim().equalsIgnoreCase(productGMC.getCountry()))
					{
						lsAllProductGMC.add(allGMC);
					}
				}
			}
			Set<String> parentIDSet = new HashSet<>();
			Set<String> imageLinkSet = new HashSet<>();
			List<AllProductGMC> lsproductByParent = lsAllProductGMC.stream().filter(e -> parentIDSet.add(e.getParentid()))
					.sorted(Comparator.comparing(AllProductGMC::getGmcid).reversed()).collect(Collectors.toList());
			List<AllProductGMC> lsproductfinal = lsproductByParent.stream().filter(e -> imageLinkSet.add(e.getGmcstatus()+  "-" + e.getImage()))
					.sorted(Comparator.comparing(AllProductGMC::getGmcid).reversed()).collect(Collectors.toList());
			model.addAttribute("lsAllProductGMC", lsproductfinal);
		}
		model.addAttribute("message", Constants.EMPTY);
		return "allproductgmcfeed";
	}
	@RequestMapping(path = "/admin/searchproductgmccountry", method = RequestMethod.GET)
	public String searchproductgmccountry(Model model,@RequestParam("country") String country) {
		List<ProductGMC> lsProductGMC = productGMCService.getAllProductFeeded();
		List<AllProductGMC> lsAllProductGMC = new ArrayList<>();
		if (!lsProductGMC.isEmpty()) {
			for (ProductGMC productGMC : lsProductGMC) {
				GMC gmc = gmcService.findByID(productGMC.getGmcid());
				AllProductGMC allGMC = new AllProductGMC();
				if(gmc != null) {
					allGMC.setGmcstatus(gmc.getStatus());
					allGMC.setGmcid(gmc.getMerchantId());
					allGMC.setDomain(productGMC.getDomain());
					allGMC.setProducttmtitle(productGMC.getProducttmtitle());
					allGMC.setSupfeedtm(productGMC.getSupfeedtm());
					allGMC.setPrice(productGMC.getPrice());
					allGMC.setImage(productGMC.getImage());
					allGMC.setCurrency(productGMC.getCurrency());
					allGMC.setGmcnote(gmc.getNote());
					allGMC.setTitle(productGMC.getTitle());
					allGMC.setParentid(productGMC.getParentid());
					allGMC.setSuspendeddate(gmc.getSuspenddate());
					allGMC.setCountry(productGMC.getCountry());
					allGMC.setGtin(productGMC.getGtin());
					allGMC.setGtin(productGMC.getGtin());
					if(country.trim().equalsIgnoreCase(productGMC.getCountry()))
					{
						lsAllProductGMC.add(allGMC);
					}
				}
			}
			Set<String> parentIDSet = new HashSet<>();
			Set<String> imageLinkSet = new HashSet<>();
			List<AllProductGMC> lsproductByParent = lsAllProductGMC.stream().filter(e -> parentIDSet.add(e.getParentid()))
					.sorted(Comparator.comparing(AllProductGMC::getGmcid).reversed()).collect(Collectors.toList());
			List<AllProductGMC> lsproductfinal = lsproductByParent.stream().filter(e -> imageLinkSet.add(e.getGmcstatus()+  "-" + e.getImage()))
					.sorted(Comparator.comparing(AllProductGMC::getGmcid).reversed()).collect(Collectors.toList());
			model.addAttribute("lsAllProductGMC", lsproductfinal);
		}
		model.addAttribute("message", Constants.EMPTY);
		return "allproductgmcfeed";
	}

	@RequestMapping(value = "/admin/addmanualfeed", method = RequestMethod.POST)
	public String addSupFeed(Model model, @RequestParam("merchantId") String merchantId,
			@RequestParam("name") String name, @RequestParam("language") String language,
			@RequestParam("country") String country, @RequestParam("link") String link,@RequestParam("includeads") String includeads) {
		GMC gmc = gmcService.findByID(merchantId);
		String domainTM = gmc.getDomaintm();
		String email = gmc.getAccountSampleUser();
		model.addAttribute("domain", gmc.getDomain());
		model.addAttribute("domainTM", domainTM);
		model.addAttribute("email", email);
		model.addAttribute("merchantId", gmc.getMerchantId());
		List<ProductTMWoo> lsproductMain = productTMWooService.findByDomain(gmc.getDomain());
		Set<String> parentIDSet = new HashSet<>();
		List<ProductTMWoo> lsproductByParent = lsproductMain.stream().filter(e -> parentIDSet.add(e.getParentid()))
				.collect(Collectors.toList());
		if (!lsproductMain.isEmpty()) {
			model.addAttribute("lsproductmain", lsproductByParent);
		}
		// start add feed API
		String message = "Feed data failed!";
		long feedId = addfeedData(gmc, name, language, country, link,includeads);
		if (feedId != 0) {
			message = "Feeded successfully with ID: " + feedId;
		}
		if(link.contains(".uk")) {
			gmc.setVpsserver("VPS_UK");
		}else if (link.contains(".com.au")){
			gmc.setVpsserver("VPS_AU");
		}else if (link.contains(".ie")){
			gmc.setVpsserver("VPS_IE");
		}else if (link.contains(".fr")){
			gmc.setVpsserver("VPS_FR");
		}else if (link.contains(".de")){
			gmc.setVpsserver("VPS_DE");
		}else if (link.contains(".ca")){
			gmc.setVpsserver("VPS_CA");
		}else if (link.contains(".pl")){
			gmc.setVpsserver("VPS_PL");
		}else {
			gmc.setVpsserver("VPS_US");
		}
		gmcService.saveGMC(gmc);
		model.addAttribute("message", message);
		List<DataFeedGMC> lsdataFeedGMC = fetchDataFeedGMC(gmc);
		if (!lsdataFeedGMC.isEmpty()) {
			model.addAttribute("lsdataFeedGMC", lsdataFeedGMC);
		}
		return "gmcdomain";
	}

	@RequestMapping(value = "/admin/fetchfeed", method = RequestMethod.POST)
	public ResponseEntity<?> fetchfeed(Model model, @RequestBody String json) {
		if (json.contains("\"")) {
			json = json.replace("\"", "");
		}
		Gson gson = new Gson();
		FetchInput fetchInput = gson.fromJson(json, FetchInput.class);
		String merchantid = fetchInput.getMerchantid();
		GMC gmc = gmcService.findByID(merchantid);
		Long feedid = Long.valueOf(fetchInput.getFeedid());
		String message = fetchGMC(gmc, feedid);
		return ResponseEntity.ok(message);
	}
	@RequestMapping(value = "/admin/updatefetchfeed", method = RequestMethod.POST)
	public ResponseEntity<?> updatefetchfeed(Model model, @RequestBody String json) {
		if (json.contains("\"")) {
			json = json.replace("\"", "");
		}
		Gson gson = new Gson();
		GMCUpdateInput fetchInput = gson.fromJson(json, GMCUpdateInput.class);
		String merchantid = fetchInput.getMerchantid();
		GMC gmc = gmcService.findByID(merchantid);
		Long feedid = Long.valueOf(fetchInput.getFeedid());
		String country = fetchInput.getGmccountry().trim();
		String message = updatefeedGMC(gmc, feedid, country);
		return ResponseEntity.ok(message);
	}

	@RequestMapping(value = "/admin/deletefeedgmc", method = RequestMethod.POST)
	public ResponseEntity<?> deletefeedgmc(Model model, @RequestBody String json) {
		if (json.contains("\"")) {
			json = json.replace("\"", "");
		}
		Gson gson = new Gson();
		FetchInput fetchInput = gson.fromJson(json, FetchInput.class);
		String merchantid = fetchInput.getMerchantid();
		GMC gmc = gmcService.findByID(merchantid);
		Long feedid = Long.valueOf(fetchInput.getFeedid());
		String message = deleteFeedGMC(gmc, feedid);
		return ResponseEntity.ok(message);
	}

	@RequestMapping(value = "/admin/ajaxupdatepricegmc", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ajaxupdatepricegmc(@RequestBody String json) {
		String result = Constants.FAILED;
		Gson gson = new Gson();
		CostGson cost = gson.fromJson(json, CostGson.class);
		int id = cost.getId();
		ProductGMC gmcProduct = productGMCService.findById(id);
		String parentid = gmcProduct.getParentid();
		List<ProductGMC> lsGmcs = productGMCService.findByGMCandParentid(gmcProduct.getGmcid(), parentid);
		for (ProductGMC productGMC : lsGmcs) {
			productGMC.setPrice(cost.getOrdercost());
			productGMCService.saveProductGMC(productGMC);
		}
		result = Constants.SUCCESS;
		return ResponseEntity.ok(result);
	}

	@RequestMapping(value = "/admin/updategmcproductmain", method = RequestMethod.POST)
	public String updategmcproductmain(Model model,
			@RequestParam(name = "hiddenupdategmcproduct", required = false) String hiddenupdategmcproduct,
			@RequestParam(name = "merchantid", required = false) String merchantid) {
		try {
			GMC gmc = gmcService.findByID(merchantid);
			String domain = gmc.getDomain();
			String domainTM = gmc.getDomaintm();
			String email = gmc.getAccountSampleUser();
			model.addAttribute("domain", domain);
			model.addAttribute("domainTM", domainTM);
			model.addAttribute("email", email);
			model.addAttribute("merchantId", merchantid);
			List<Product> lsProduct = new ArrayList<>();
			AuthenticatorGMC gmcAuth = new AuthenticatorGMC(gmc);
			ShoppingContent content = gmcAuth.initBuilder();
			Long merchantInt = Long.valueOf(gmc.getMerchantId());
			int countupdate = 0;
			if (hiddenupdategmcproduct.length() > 0) {
				if (!hiddenupdategmcproduct.contains(Constants.COMMA)) {
					ProductGMC productGMCUpdate = productGMCService.findById(Integer.valueOf(hiddenupdategmcproduct));
					String parentID = productGMCUpdate.getParentid();
					List<ProductGMC> lsUpdate = productGMCService.findByGMCandParentid(merchantid, parentID);
					for (ProductGMC productGMC : lsUpdate) {
						Product updateProduct = new Product();
						Price price = new Price().setValue(productGMC.getPrice()).setCurrency(productGMC.getCurrency());
						updateProduct.setPrice(price);
						Product update =  content.products().update(BigInteger.valueOf(merchantInt), productGMC.getProductid(), updateProduct).execute();
						if(update != null) {
							countupdate ++;
						}
					}
				}else {
					String[] arrayId = hiddenupdategmcproduct.split(Constants.COMMA);
					for (String id : arrayId) {
						ProductGMC productGMCUpdate = productGMCService.findById(Integer.valueOf(id));
						String parentID = productGMCUpdate.getParentid();
						List<ProductGMC> lsUpdate = productGMCService.findByGMCandParentid(merchantid, parentID);
						for (ProductGMC productGMC : lsUpdate) {
							Product updateProduct = new Product();
							Price price = new Price().setValue(productGMC.getPrice()).setCurrency(productGMC.getCurrency());
							updateProduct.setPrice(price);
							Product update =  content.products().update(BigInteger.valueOf(merchantInt), productGMC.getProductid(), updateProduct).execute();
							if(update != null) {
								countupdate ++;
							}
						}
					}
				}
			}
			Set<String> parentIDGMC = new HashSet<>();
			List<ProductGMC> lsProductGMC = productGMCService.findByGmcid(merchantid);
			if (!lsProductGMC.isEmpty()) {
				List<ProductGMC> lsproductGMCByParent = lsProductGMC.stream().filter(e -> parentIDGMC.add(e.getParentid()))
						.collect(Collectors.toList());
				model.addAttribute("lsProductGMC", lsproductGMCByParent);
			}
			List<ProductTMWoo> lsproducttm = productTMWooService.findByDomain(domainTM);
			if (!lsproducttm.isEmpty()) {
				Set<String> parentIDSet = new HashSet<>();
				List<ProductTMWoo> lsproducttmByParent = lsproducttm.stream().filter(e -> parentIDSet.add(e.getParentid()))
						.collect(Collectors.toList());
				model.addAttribute("lsproducttm", lsproducttmByParent);
			}
			String message = "Total products has been updated: " + countupdate;
			model.addAttribute("message", message);
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		
		return "gmcfeed";

	}

	@RequestMapping(value = "/admin/deletegmcproductmain", method = RequestMethod.POST)
	public String deletegmcproductmain(Model model,
			@RequestParam(name = "hiddenproductgmcdeletemain", required = false) String hiddenproductgmcdeletemain,
			@RequestParam(name = "merchantid", required = false) String merchantid) {
		GMC gmc = gmcService.findByID(merchantid);
		String domain = gmc.getDomain();
		String domainTM = gmc.getDomaintm();
		String email = gmc.getAccountSampleUser();
		model.addAttribute("domain", domain);
		model.addAttribute("domainTM", domainTM);
		model.addAttribute("email", email);
		model.addAttribute("merchantId", merchantid);
		int countdelete = 0;
		if (hiddenproductgmcdeletemain.length() > 0) {
			if (!hiddenproductgmcdeletemain.contains(Constants.COMMA)) {
				ProductGMC productGMCDelete = productGMCService.findById(Integer.valueOf(hiddenproductgmcdeletemain));
				String parentID = productGMCDelete.getParentid();
				List<ProductGMC> lsDelete = productGMCService.findByGMCandParentid(merchantid, parentID);
				for (ProductGMC productGMC : lsDelete) {
					String gmcid = productGMC.getProductid();
					boolean isDeleteSuccessed = deleteproductsGMC(gmc, gmcid);
					if (isDeleteSuccessed) {
						countdelete++;
						productGMCService.deleteProductGMC(productGMC);
					}
				}
			} else {
				String[] arrayDelete = hiddenproductgmcdeletemain.split(Constants.COMMA);
				for (String deleteGMCId : arrayDelete) {
					ProductGMC productGMCDelete = productGMCService.findById(Integer.valueOf(deleteGMCId));
					String parentID = productGMCDelete.getParentid();
					List<ProductGMC> lsDelete = productGMCService.findByGMCandParentid(merchantid, parentID);
					for (ProductGMC productGMC : lsDelete) {
						String gmcid = productGMC.getProductid();
						boolean isDeleteSuccessed = deleteproductsGMC(gmc, gmcid);
						if (isDeleteSuccessed) {
							countdelete++;
							productGMCService.deleteProductGMC(productGMC);
						}
					}
				}
			}
		}
		Set<String> parentIDGMC = new HashSet<>();
		List<ProductGMC> lsProductGMC = productGMCService.findByGmcid(merchantid);
		if (!lsProductGMC.isEmpty()) {
			List<ProductGMC> lsproductGMCByParent = lsProductGMC.stream().filter(e -> parentIDGMC.add(e.getParentid()))
					.collect(Collectors.toList());
			model.addAttribute("lsProductGMC", lsproductGMCByParent);
		}
		List<ProductTMWoo> lsproducttm = productTMWooService.findByDomain(domainTM);
		if (!lsproducttm.isEmpty()) {
			Set<String> parentIDSet = new HashSet<>();
			List<ProductTMWoo> lsproducttmByParent = lsproducttm.stream().filter(e -> parentIDSet.add(e.getParentid()))
					.collect(Collectors.toList());
			model.addAttribute("lsproducttm", lsproducttmByParent);
		}
		String message = "Total products has been deleted: " + countdelete;
		model.addAttribute("message", message);
		return "gmcfeed";
	}

	@RequestMapping(path = "/admin/gmcdomain/{domain}", method = RequestMethod.GET)
	public String gmcdomain(Model model, @PathVariable String domain) {
		GMC gmc = gmcService.findByDomain(domain);
		String country = gmc.getCountry();
		String domainTM = gmc.getDomaintm();
		String email = gmc.getAccountSampleUser();
		model.addAttribute("domain", domain);
		model.addAttribute("domainTM", domainTM);
		model.addAttribute("email", email);
		model.addAttribute("merchantId", gmc.getMerchantId());
		GMC gMCPrevious = gmcService.findLastSecondGMC();
//		List<ProductTMWoo> lsproductMain = productTMWooService.findByDomain(domain);
//		Set<String> parentIDSet = new HashSet<>();
//		List<ProductTMWoo> lsproductByParent = lsproductMain.stream().filter(e -> parentIDSet.add(e.getParentid()))
//				.collect(Collectors.toList());
//		if (!lsproductMain.isEmpty()) {
//			model.addAttribute("lsproductmain", lsproductByParent);
//		}
		model.addAttribute("message", Constants.EMPTY);
		String countryfeed = "UK";
		List<DataFeedGMC> lsdataFeedGMC = fetchDataFeedGMC(gmc);
		String vpsPrevious = gMCPrevious.getVpsserver();
		//List<DataFeedGMC> lsdataPrevious = fetchDataFeedGMC(gMCPrevious);
		//for (DataFeedGMC dataFeedGMC : lsdataPrevious) {
			//urlPrevious = dataFeedGMC.getLink();
		//}
		if(vpsPrevious.equals("VPS_US")) {
			countryfeed = "US";
		}else if(vpsPrevious.equals("VPS_AU")) {
			countryfeed = "AU";
		}else if(vpsPrevious.equals("VPS_IE")) {
			countryfeed = "EU";
		}else if(vpsPrevious.equals("VPS_FR")) {
			countryfeed = "EU";
		}else if(vpsPrevious.equals("VPS_DE")) {
			countryfeed = "EU";
		}else if(vpsPrevious.equals("VPS_PL")) {
			countryfeed = "PL";
		}else if (vpsPrevious.equals("VPS_CA")) {
			countryfeed = "CA";
		}
		if (!lsdataFeedGMC.isEmpty()) {
			model.addAttribute("lsdataFeedGMC", lsdataFeedGMC);
		}
		String language = country.toLowerCase();
		if(country.equals("JP")) {
			language = "en";
		}
		model.addAttribute("countryfeed",countryfeed);
		model.addAttribute("country",country);
		model.addAttribute("language",language);
		return "gmcdomain";
	}

	@RequestMapping(value = "/admin/getgmcproduct", params = "getproduct", method = RequestMethod.GET)
	public String getgmcproduct(Model model, @RequestParam(name = "merchantId") String merchantId,
			@RequestParam(name = "minprice") int minprice) {
		GMC gmc = gmcService.findByID(merchantId);
		List<Product> lsProduct = getGMCProduct(gmc);
		if (!lsProduct.isEmpty()) {
			for (Product product : lsProduct) {
				String offerID = product.getOfferId().trim();
				String title = product.getTitle();
				List<ProductGMC> productGMC = productGMCService.findByGmcidAndProductid(merchantId, product.getId().trim());
				if (productGMC == null || productGMC.isEmpty()) {
					ProductGMC productGMCnew = new ProductGMC(gmc.getDomain(), merchantId, product.getId(),
							offerID, product.getTargetCountry(), product.getFeedLabel(),
							product.getGoogleProductCategory(), title, product.getDescription(),
							product.getPrice() == null ? "" : product.getPrice().getValue(), product.getItemGroupId(),
							product.getImageLink(), product.getLink(),product.getPrice() == null ? "" : product.getPrice().getCurrency());
					String priceP = productGMCnew.getPrice();
					if(!priceP.isEmpty()) {
						Double priceGMC = Double.valueOf(priceP);
						if(priceGMC >= minprice) {
							productGMCService.saveProductGMC(productGMCnew);
						}
					}
				}
			}
		}
		return "redirect:/admin/gmc/" + merchantId;
	}

	@RequestMapping(path = "/admin/getgmcproduct", params = "deleteproduct", method = RequestMethod.GET)
	public String deleleteproductgmc(Model model, @RequestParam(name = "merchantId") String merchantId) {
		List<ProductGMC> lsProduct = productGMCService.findByGmcid(merchantId);
		for (ProductGMC product : lsProduct) {
			productGMCService.deleteProductGMC(product);
		}
		return "redirect:/admin/gmc/" + merchantId;
	}
	
	@RequestMapping(path = "/admin/getshippinginfo",method = RequestMethod.GET)
	public String getshippinginfo(Model model, @RequestParam(name = "merchantId") String merchantId) throws IOException {
		GMC gmc = gmcService.findByID(merchantId);
		AuthenticatorGMC gmcAuth = new AuthenticatorGMC(gmc);
		ShoppingContent content = gmcAuth.initBuilder();
		Long merchantInt = Long.valueOf(gmc.getMerchantId());
		ShippingSettings settings =
		          content.shippingsettings().get(BigInteger.valueOf(merchantInt), BigInteger.valueOf(merchantInt)).execute();
		for (Service service : settings.getServices()) {
			for (RateGroup group : service.getRateGroups()) {
				System.out.println("main: " + group.getMainTable());
				Table main = group.getMainTable();
				for (Row row : main.getRows()) {
//					row.getCells().get(0).getFlatRate().get
				}
			}
		}
		return "gmcdomain";
	}
	@RequestMapping(path = "/admin/setvpsserver",method = RequestMethod.POST)
	public String setvpsserver(Model model, @RequestParam(name = "merchantId") String merchantId,
			@RequestParam(name = "vpsserver") String vpsserver) throws IOException {
		GMC gmc = gmcService.findByID(merchantId);
		gmc.setVpsserver(vpsserver.trim());
		gmcService.saveGMC(gmc);
		return "redirect:/admin/livegmc";
	}
	@RequestMapping(path = "/admin/setshippinginfo",method = RequestMethod.POST)
	public String setshippinginfo(Model model, @RequestParam(name = "merchantId") String merchantId,
			@RequestParam(name = "fromus") long fromus,@RequestParam(name = "tous") long tous,
			@RequestParam(name = "fromjp") long fromjp,@RequestParam(name = "tojp") long tojp,
			@RequestParam(name = "isus", required = false) String isus) throws IOException {
		GMC gmc = gmcService.findByID(merchantId);
		String domainTM = gmc.getDomaintm();
		String email = gmc.getAccountSampleUser();
		model.addAttribute("domain", gmc.getDomain());
		model.addAttribute("domainTM", domainTM);
		model.addAttribute("email", email);
		model.addAttribute("merchantId", gmc.getMerchantId());
		AuthenticatorGMC gmcAuth = new AuthenticatorGMC(gmc);
		ShoppingContent content = gmcAuth.initBuilder();
		Long merchantInt = Long.valueOf(gmc.getMerchantId());
		String country = gmc.getCountry();
//		if(country.equalsIgnoreCase("JP") || country.equalsIgnoreCase("US")) {
		if(isus.equals("GBP")) {
			ShippingSettings shippingSetting = buildShippingSettingsUKJP(fromus, tous, fromjp, tojp);
			content.shippingsettings()
			.update(BigInteger.valueOf(merchantInt), BigInteger.valueOf(merchantInt), shippingSetting)
			.execute();
		}else if(isus.equals("USD")) {
			ShippingSettings shippingSetting = buildShippingSettings(fromus, tous, fromjp, tojp);
			content.shippingsettings()
			.update(BigInteger.valueOf(merchantInt), BigInteger.valueOf(merchantInt), shippingSetting)
			.execute();
		}else if(isus.equals("AUD")) {
			ShippingSettings shippingSetting = buildShippingSettingsAUJP(fromus, tous, fromjp, tojp);
			content.shippingsettings()
			.update(BigInteger.valueOf(merchantInt), BigInteger.valueOf(merchantInt), shippingSetting)
			.execute();
		}else if(isus.equals("EUR")) {
			ShippingSettings shippingSetting = buildShippingSettingsIEJP(fromus, tous, fromjp, tojp);
			content.shippingsettings()
			.update(BigInteger.valueOf(merchantInt), BigInteger.valueOf(merchantInt), shippingSetting)
			.execute();
		}else if(isus.equals("PLN")) {
			ShippingSettings shippingSetting = buildShippingSettingsPL(fromus, tous);
			content.shippingsettings()
			.update(BigInteger.valueOf(merchantInt), BigInteger.valueOf(merchantInt), shippingSetting)
			.execute();
		}else if(isus.equals("CAD")) {
			ShippingSettings shippingSetting = buildShippingSettingsJPCA(fromus, tous, fromjp, tojp);
			content.shippingsettings()
			.update(BigInteger.valueOf(merchantInt), BigInteger.valueOf(merchantInt), shippingSetting)
			.execute();
		}
//		}else if (country.equalsIgnoreCase("VN") || country.equalsIgnoreCase("GB") || country.equalsIgnoreCase("DE")) {
//			if(isus == null) {
//				ShippingSettings shippingSetting = buildShippingSettingsUK(fromus, tous, fromjp, tojp);
//				content.shippingsettings()
//				.update(BigInteger.valueOf(merchantInt), BigInteger.valueOf(merchantInt), shippingSetting)
//				.execute();
//			}else {
//				ShippingSettings shippingSetting = buildShippingSettingsDE(fromus, tous, fromjp, tojp);
//				content.shippingsettings()
//				.update(BigInteger.valueOf(merchantInt), BigInteger.valueOf(merchantInt), shippingSetting)
//				.execute();
//			}
//		}
		List<DataFeedGMC> lsdataFeedGMC = fetchDataFeedGMC(gmc);
		if (!lsdataFeedGMC.isEmpty()) {
			model.addAttribute("lsdataFeedGMC", lsdataFeedGMC);
		}
		String language = country.toLowerCase();
		if(country.equals("JP")) {
			language = "en";
		}
		model.addAttribute("country",country);
		model.addAttribute("language",language);
		return "gmcdomain";
	}
	private ShippingSettings buildShippingSettingsAUJP(long fromUS, long toUS, long fromJP, long toJP) {
		//AU
		RateGroup rategroupUS = new RateGroup();
		Table mainTableUS = new Table();
		Headers headersUS = new Headers();
		headersUS.setPrices(ImmutableList.of(
				new Price().setCurrency("AUD").setValue("9.98"),
                new Price().setCurrency("AUD").setValue("24.98"),
                new Price().setCurrency("AUD").setValue("49.98"),
                new Price().setCurrency("AUD").setValue("99.98"),
                new Price().setCurrency("AUD").setValue("119.98"),
                new Price().setCurrency("AUD").setValue("infinity")
				));
		mainTableUS.setRowHeaders(headersUS);
		mainTableUS.setRows(ImmutableList.of(
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("AUD").setValue("5.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("AUD").setValue("9.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("AUD").setValue("14.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("AUD").setValue("22.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("AUD").setValue("29.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("AUD").setValue("9.99"))))
				));
		rategroupUS.setMainTable(mainTableUS);
		Service usService = new Service()
				.setName("AU")
	            .setCurrency("AUD")
	            .setDeliveryCountry("AU")
	            .setDeliveryTime(new DeliveryTime()
	                    .setHandlingBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                    .setMaxHandlingTimeInDays(2L)
	                    .setMaxTransitTimeInDays(toUS-2L)
	                    .setMinHandlingTimeInDays(1L)
	                    .setMinTransitTimeInDays(fromUS-1L)
	                    .setTransitBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                )
	                .setEligibility("All scenarios")
	                .setRateGroups(ImmutableList.of(rategroupUS))
	                .setActive(true)
	                .setShipmentType("delivery");
		
		//JP
		RateGroup rategroupJP = new RateGroup();
		Table mainTableJP = new Table();
		Headers headersJP = new Headers();
		headersJP.setPrices(ImmutableList.of(
				new Price().setCurrency("AUD").setValue("9.98"),
                new Price().setCurrency("AUD").setValue("24.98"),
                new Price().setCurrency("AUD").setValue("49.98"),
                new Price().setCurrency("AUD").setValue("99.98"),
                new Price().setCurrency("AUD").setValue("119.98"),
                new Price().setCurrency("AUD").setValue("infinity")
				));
		mainTableJP.setRowHeaders(headersJP);
		mainTableJP.setRows(ImmutableList.of(
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("AUD").setValue("5.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("AUD").setValue("9.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("AUD").setValue("14.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("AUD").setValue("22.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("AUD").setValue("29.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("AUD").setValue("36.99"))))
				));
		rategroupJP.setMainTable(mainTableJP);
		Service JPService = new Service()
				.setName("JP")
	            .setCurrency("AUD")
	            .setDeliveryCountry("JP")
	            .setDeliveryTime(new DeliveryTime()
	                    .setHandlingBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                    .setMaxHandlingTimeInDays(4L)
	                    .setMaxTransitTimeInDays(toJP-4L)
	                    .setMinHandlingTimeInDays(2L)
	                    .setMinTransitTimeInDays(fromJP-2L)
	                    .setTransitBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                )
	                .setEligibility("All scenarios")
	                .setRateGroups(ImmutableList.of(rategroupJP))
	                .setActive(true)
	                .setShipmentType("delivery");
		return new ShippingSettings().setServices(ImmutableList.of(usService, JPService));
	}
	private ShippingSettings buildShippingSettingsPL(long fromUS, long toUS) {
		//PL
		RateGroup rategroupPL = new RateGroup();
		Table mainTablePL = new Table();
		Headers headersPL = new Headers();
		headersPL.setPrices(ImmutableList.of(
				new Price().setCurrency("PLN").setValue("49.98"),
                new Price().setCurrency("PLN").setValue("107.45"),
                new Price().setCurrency("PLN").setValue("257.97"),
                new Price().setCurrency("PLN").setValue("infinity")
				));
		mainTablePL.setRowHeaders(headersPL);
		mainTablePL.setRows(ImmutableList.of(
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("PLN").setValue("11.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("PLN").setValue("21.50")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("PLN").setValue("30.00")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("PLN").setValue("38.50"))))
				));
		rategroupPL.setMainTable(mainTablePL);
		Service plService = new Service()
				.setName("PL")
	            .setCurrency("PLN")
	            .setDeliveryCountry("PL")
	            .setDeliveryTime(new DeliveryTime()
	                    .setHandlingBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                    .setMaxHandlingTimeInDays(2L)
	                    .setMaxTransitTimeInDays(toUS-2L)
	                    .setMinHandlingTimeInDays(1L)
	                    .setMinTransitTimeInDays(fromUS-1L)
	                    .setTransitBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                )
	                .setEligibility("All scenarios")
	                .setRateGroups(ImmutableList.of(rategroupPL))
	                .setActive(true)
	                .setShipmentType("delivery");
		return new ShippingSettings().setServices(ImmutableList.of(plService));
	}
	private ShippingSettings buildShippingSettingsIEJP(long fromUS, long toUS, long fromJP, long toJP) {
		//IE
		RateGroup rategroupUS = new RateGroup();
		Table mainTableUS = new Table();
		Headers headersUS = new Headers();
		headersUS.setPrices(ImmutableList.of(
				new Price().setCurrency("EUR").setValue("9.98"),
                new Price().setCurrency("EUR").setValue("24.98"),
                new Price().setCurrency("EUR").setValue("49.98"),
                new Price().setCurrency("EUR").setValue("69.98"),
                new Price().setCurrency("EUR").setValue("99.98"),
                new Price().setCurrency("EUR").setValue("infinity")
				));
		mainTableUS.setRowHeaders(headersUS);
		mainTableUS.setRows(ImmutableList.of(
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("2.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("4.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("6.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("8.49")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("9.49")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("9.99"))))
				));
		rategroupUS.setMainTable(mainTableUS);
		Service usService = new Service()
				.setName("IE")
	            .setCurrency("EUR")
	            .setDeliveryCountry("IE")
	            .setDeliveryTime(new DeliveryTime()
	                    .setHandlingBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                    .setMaxHandlingTimeInDays(2L)
	                    .setMaxTransitTimeInDays(toUS-2L)
	                    .setMinHandlingTimeInDays(1L)
	                    .setMinTransitTimeInDays(fromUS-1L)
	                    .setTransitBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                )
	                .setEligibility("All scenarios")
	                .setRateGroups(ImmutableList.of(rategroupUS))
	                .setActive(true)
	                .setShipmentType("delivery");
		
		//JP
		RateGroup rategroupJP = new RateGroup();
		Table mainTableJP = new Table();
		Headers headersJP = new Headers();
		headersJP.setPrices(ImmutableList.of(
				new Price().setCurrency("EUR").setValue("9.98"),
                new Price().setCurrency("EUR").setValue("24.98"),
                new Price().setCurrency("EUR").setValue("59.98"),
                new Price().setCurrency("EUR").setValue("infinity")
				));
		mainTableJP.setRowHeaders(headersJP);
		mainTableJP.setRows(ImmutableList.of(
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("7.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("9.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("16.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("24.49"))))
				));
		rategroupJP.setMainTable(mainTableJP);
		Service JPService = new Service()
				.setName("JP")
	            .setCurrency("EUR")
	            .setDeliveryCountry("JP")
	            .setDeliveryTime(new DeliveryTime()
	                    .setHandlingBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                    .setMaxHandlingTimeInDays(4L)
	                    .setMaxTransitTimeInDays(toJP-4L)
	                    .setMinHandlingTimeInDays(2L)
	                    .setMinTransitTimeInDays(fromJP-2L)
	                    .setTransitBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                )
	                .setEligibility("All scenarios")
	                .setRateGroups(ImmutableList.of(rategroupJP))
	                .setActive(true)
	                .setShipmentType("delivery");
		RateGroup rategroupDE = new RateGroup();
		Table mainTableDE = new Table();
		Headers headersDE = new Headers();
		headersDE.setPrices(ImmutableList.of(
				new Price().setCurrency("EUR").setValue("10.98"),
                new Price().setCurrency("EUR").setValue("24.98"),
                new Price().setCurrency("EUR").setValue("49.98"),
                new Price().setCurrency("EUR").setValue("69.98"),
                new Price().setCurrency("EUR").setValue("99.98"),
                new Price().setCurrency("EUR").setValue("infinity")
				));
		mainTableDE.setRowHeaders(headersDE);
		mainTableDE.setRows(ImmutableList.of(
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("2.69")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("4.49")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("5.49")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("5.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("6.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("0.00"))))
				));
		rategroupDE.setMainTable(mainTableDE);
		Service deService = new Service()
				.setName("DE")
	            .setCurrency("EUR")
	            .setDeliveryCountry("DE")
	            .setDeliveryTime(new DeliveryTime()
	                    .setHandlingBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                    .setMaxHandlingTimeInDays(2L)
	                    .setMaxTransitTimeInDays(toUS-2L)
	                    .setMinHandlingTimeInDays(1L)
	                    .setMinTransitTimeInDays(fromUS-1L)
	                    .setTransitBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                )
	                .setEligibility("All scenarios")
	                .setRateGroups(ImmutableList.of(rategroupDE))
	                .setActive(true)
	                .setShipmentType("delivery");
		//FR
		Service frService = settingCountryService("FR", fromUS, toUS);
		Service spainService = settingCountryService("ES", fromUS, toUS);
		Service portugalService = settingCountryService("PT", fromUS, toUS);
		//IE
		Service italyService = settingCountryService("IT", fromUS, toUS);
		Service netherlandService = settingCountryService("NL", fromUS, toUS);
		Service belgiumService = settingCountryService("BE", fromUS, toUS);
		//PL
		Service polandService = settingCountryService("PL", fromUS, toUS);
		Service switzelandService = settingCountryService("CH", fromUS, toUS);
		Service swedenService = settingCountryService("SE", fromUS, toUS);
		
		
		return new ShippingSettings().setServices(ImmutableList.of(usService, JPService,deService, frService,spainService,switzelandService,netherlandService,polandService,portugalService,belgiumService,italyService,swedenService));
	}
	
	private Service settingCountryService(String country,long fromUS, long toUS) {
		RateGroup rategroup = new RateGroup();
		Table mainTable = new Table();
		Headers headers = new Headers();
		headers.setPrices(ImmutableList.of(
				new Price().setCurrency("EUR").setValue("9.98"),
                new Price().setCurrency("EUR").setValue("24.98"),
                new Price().setCurrency("EUR").setValue("49.98"),
                new Price().setCurrency("EUR").setValue("69.98"),
                new Price().setCurrency("EUR").setValue("99.98"),
                new Price().setCurrency("EUR").setValue("infinity")
				));
		mainTable.setRowHeaders(headers);
		mainTable.setRows(ImmutableList.of(
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("2.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("4.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("6.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("8.49")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("9.49")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("9.99"))))
				));
		rategroup.setMainTable(mainTable);
		Service euroService = new Service()
				.setName(country)
	            .setCurrency("EUR")
	            .setDeliveryCountry(country)
	            .setDeliveryTime(new DeliveryTime()
	                    .setHandlingBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                    .setMaxHandlingTimeInDays(2L)
	                    .setMaxTransitTimeInDays(toUS-2L)
	                    .setMinHandlingTimeInDays(1L)
	                    .setMinTransitTimeInDays(fromUS-1L)
	                    .setTransitBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                )
	                .setEligibility("All scenarios")
	                .setRateGroups(ImmutableList.of(rategroup))
	                .setActive(true)
	                .setShipmentType("delivery");
		return euroService;
	}
	
	private ShippingSettings buildShippingSettingsDEJP(long fromUS, long toUS, long fromJP, long toJP) {
		//DE
		RateGroup rategroupUS = new RateGroup();
		Table mainTableUS = new Table();
		Headers headersUS = new Headers();
		headersUS.setPrices(ImmutableList.of(
				new Price().setCurrency("EUR").setValue("10.98"),
                new Price().setCurrency("EUR").setValue("24.98"),
                new Price().setCurrency("EUR").setValue("49.98"),
                new Price().setCurrency("EUR").setValue("69.98"),
                new Price().setCurrency("EUR").setValue("99.98"),
                new Price().setCurrency("EUR").setValue("infinity")
				));
		mainTableUS.setRowHeaders(headersUS);
		mainTableUS.setRows(ImmutableList.of(
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("2.69")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("4.49")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("5.49")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("5.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("6.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("0.00"))))
				));
		rategroupUS.setMainTable(mainTableUS);
		Service usService = new Service()
				.setName("DE")
	            .setCurrency("EUR")
	            .setDeliveryCountry("DE")
	            .setDeliveryTime(new DeliveryTime()
	                    .setHandlingBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                    .setMaxHandlingTimeInDays(2L)
	                    .setMaxTransitTimeInDays(toUS-2L)
	                    .setMinHandlingTimeInDays(1L)
	                    .setMinTransitTimeInDays(fromUS-1L)
	                    .setTransitBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                )
	                .setEligibility("All scenarios")
	                .setRateGroups(ImmutableList.of(rategroupUS))
	                .setActive(true)
	                .setShipmentType("delivery");
		
		//JP
		RateGroup rategroupJP = new RateGroup();
		Table mainTableJP = new Table();
		Headers headersJP = new Headers();
		headersJP.setPrices(ImmutableList.of(
				new Price().setCurrency("EUR").setValue("9.98"),
                new Price().setCurrency("EUR").setValue("24.98"),
                new Price().setCurrency("EUR").setValue("59.98"),
                new Price().setCurrency("EUR").setValue("infinity")
				));
		mainTableJP.setRowHeaders(headersJP);
		mainTableJP.setRows(ImmutableList.of(
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("7.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("9.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("16.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("24.49"))))
				));
		rategroupJP.setMainTable(mainTableJP);
		Service JPService = new Service()
				.setName("JP")
	            .setCurrency("EUR")
	            .setDeliveryCountry("JP")
	            .setDeliveryTime(new DeliveryTime()
	                    .setHandlingBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                    .setMaxHandlingTimeInDays(4L)
	                    .setMaxTransitTimeInDays(toJP-4L)
	                    .setMinHandlingTimeInDays(2L)
	                    .setMinTransitTimeInDays(fromJP-2L)
	                    .setTransitBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                )
	                .setEligibility("All scenarios")
	                .setRateGroups(ImmutableList.of(rategroupJP))
	                .setActive(true)
	                .setShipmentType("delivery");
		return new ShippingSettings().setServices(ImmutableList.of(usService, JPService));
	}
	
	private ShippingSettings buildShippingSettingsDE(long fromUS, long toUS, long fromJP, long toJP) {
		//DE
		RateGroup rategroupUS = new RateGroup();
		Table mainTableUS = new Table();
		Headers headersUS = new Headers();
		headersUS.setPrices(ImmutableList.of(
				new Price().setCurrency("EUR").setValue("10.98"),
                new Price().setCurrency("EUR").setValue("24.98"),
                new Price().setCurrency("EUR").setValue("49.98"),
                new Price().setCurrency("EUR").setValue("69.98"),
                new Price().setCurrency("EUR").setValue("99.98"),
                new Price().setCurrency("EUR").setValue("infinity")
				));
		mainTableUS.setRowHeaders(headersUS);
		mainTableUS.setRows(ImmutableList.of(
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("2.69")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("4.49")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("5.49")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("5.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("6.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("0.00"))))
				));
		rategroupUS.setMainTable(mainTableUS);
		Service usService = new Service()
				.setName("DE")
	            .setCurrency("EUR")
	            .setDeliveryCountry("DE")
	            .setDeliveryTime(new DeliveryTime()
	                    .setHandlingBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                    .setMaxHandlingTimeInDays(2L)
	                    .setMaxTransitTimeInDays(toUS-2L)
	                    .setMinHandlingTimeInDays(1L)
	                    .setMinTransitTimeInDays(fromUS-1L)
	                    .setTransitBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                )
	                .setEligibility("All scenarios")
	                .setRateGroups(ImmutableList.of(rategroupUS))
	                .setActive(true)
	                .setShipmentType("delivery");
		
		//VN
		RateGroup rategroupJP = new RateGroup();
		Table mainTableJP = new Table();
		Headers headersJP = new Headers();
		headersJP.setPrices(ImmutableList.of(
				new Price().setCurrency("EUR").setValue("9.98"),
                new Price().setCurrency("EUR").setValue("24.98"),
                new Price().setCurrency("EUR").setValue("59.98"),
                new Price().setCurrency("EUR").setValue("infinity")
				));
		mainTableJP.setRowHeaders(headersJP);
		mainTableJP.setRows(ImmutableList.of(
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("7.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("9.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("16.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("EUR").setValue("24.49"))))
				));
		rategroupJP.setMainTable(mainTableJP);
		Service JPService = new Service()
				.setName("VN")
	            .setCurrency("EUR")
	            .setDeliveryCountry("VN")
	            .setDeliveryTime(new DeliveryTime()
	                    .setHandlingBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                    .setMaxHandlingTimeInDays(4L)
	                    .setMaxTransitTimeInDays(toJP-4L)
	                    .setMinHandlingTimeInDays(2L)
	                    .setMinTransitTimeInDays(fromJP-2L)
	                    .setTransitBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                )
	                .setEligibility("All scenarios")
	                .setRateGroups(ImmutableList.of(rategroupJP))
	                .setActive(true)
	                .setShipmentType("delivery");
		return new ShippingSettings().setServices(ImmutableList.of(usService, JPService));
	}
	
	private ShippingSettings buildShippingSettingsUK(long fromUS, long toUS, long fromJP, long toJP) {
		//UK
		RateGroup rategroupUS = new RateGroup();
		Table mainTableUS = new Table();
		Headers headersUS = new Headers();
		headersUS.setPrices(ImmutableList.of(
				new Price().setCurrency("GBP").setValue("10.98"),
                new Price().setCurrency("GBP").setValue("19.98"),
                new Price().setCurrency("GBP").setValue("47.98"),
                new Price().setCurrency("GBP").setValue("infinity")
				));
		mainTableUS.setRowHeaders(headersUS);
		mainTableUS.setRows(ImmutableList.of(
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("GBP").setValue("2.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("GBP").setValue("5.49")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("GBP").setValue("6.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("GBP").setValue("8.99"))))
				));
		rategroupUS.setMainTable(mainTableUS);
		Service usService = new Service()
				.setName("UK")
	            .setCurrency("GBP")
	            .setDeliveryCountry("GB")
	            .setDeliveryTime(new DeliveryTime()
	                    .setHandlingBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                    .setMaxHandlingTimeInDays(4L)
	                    .setMaxTransitTimeInDays(toUS-4L)
	                    .setMinHandlingTimeInDays(2L)
	                    .setMinTransitTimeInDays(fromUS-2L)
	                    .setTransitBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                )
	                .setEligibility("All scenarios")
	                .setRateGroups(ImmutableList.of(rategroupUS))
	                .setActive(true)
	                .setShipmentType("delivery");
		
		//VN
		RateGroup rategroupJP = new RateGroup();
		Table mainTableJP = new Table();
		Headers headersJP = new Headers();
		headersJP.setPrices(ImmutableList.of(
				new Price().setCurrency("GBP").setValue("10.98"),
                new Price().setCurrency("GBP").setValue("19.98"),
                new Price().setCurrency("GBP").setValue("47.98"),
                new Price().setCurrency("GBP").setValue("infinity")
				));
		mainTableJP.setRowHeaders(headersJP);
		mainTableJP.setRows(ImmutableList.of(
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("GBP").setValue("2.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("GBP").setValue("8.49")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("GBP").setValue("15.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("GBP").setValue("20.49"))))
				));
		rategroupJP.setMainTable(mainTableJP);
		Service JPService = new Service()
				.setName("VN")
	            .setCurrency("GBP")
	            .setDeliveryCountry("VN")
	            .setDeliveryTime(new DeliveryTime()
	                    .setHandlingBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                    .setMaxHandlingTimeInDays(4L)
	                    .setMaxTransitTimeInDays(toJP-4L)
	                    .setMinHandlingTimeInDays(2L)
	                    .setMinTransitTimeInDays(fromJP-2L)
	                    .setTransitBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                )
	                .setEligibility("All scenarios")
	                .setRateGroups(ImmutableList.of(rategroupJP))
	                .setActive(true)
	                .setShipmentType("delivery");
		return new ShippingSettings().setServices(ImmutableList.of(usService, JPService));
	}
	
	private ShippingSettings buildShippingSettingsUKJP(long fromUS, long toUS, long fromJP, long toJP) {
		//UK
		RateGroup rategroupUS = new RateGroup();
		Table mainTableUS = new Table();
		Headers headersUS = new Headers();
		headersUS.setPrices(ImmutableList.of(
				new Price().setCurrency("GBP").setValue("10.98"),
                new Price().setCurrency("GBP").setValue("19.98"),
                new Price().setCurrency("GBP").setValue("47.98"),
                new Price().setCurrency("GBP").setValue("infinity")
				));
		mainTableUS.setRowHeaders(headersUS);
		mainTableUS.setRows(ImmutableList.of(
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("GBP").setValue("2.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("GBP").setValue("5.49")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("GBP").setValue("6.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("GBP").setValue("8.99"))))
				));
		rategroupUS.setMainTable(mainTableUS);
		Service usService = new Service()
				.setName("UK")
	            .setCurrency("GBP")
	            .setDeliveryCountry("GB")
	            .setDeliveryTime(new DeliveryTime()
	                    .setHandlingBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                    .setMaxHandlingTimeInDays(4L)
	                    .setMaxTransitTimeInDays(toUS-4L)
	                    .setMinHandlingTimeInDays(2L)
	                    .setMinTransitTimeInDays(fromUS-2L)
	                    .setTransitBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                )
	                .setEligibility("All scenarios")
	                .setRateGroups(ImmutableList.of(rategroupUS))
	                .setActive(true)
	                .setShipmentType("delivery");
		
		//JP
		RateGroup rategroupJP = new RateGroup();
		Table mainTableJP = new Table();
		Headers headersJP = new Headers();
		headersJP.setPrices(ImmutableList.of(
				new Price().setCurrency("GBP").setValue("10.98"),
                new Price().setCurrency("GBP").setValue("19.98"),
                new Price().setCurrency("GBP").setValue("47.98"),
                new Price().setCurrency("GBP").setValue("infinity")
				));
		mainTableJP.setRowHeaders(headersJP);
		mainTableJP.setRows(ImmutableList.of(
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("GBP").setValue("2.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("GBP").setValue("8.49")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("GBP").setValue("15.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("GBP").setValue("20.49"))))
				));
		rategroupJP.setMainTable(mainTableJP);
		Service JPService = new Service()
				.setName("JP")
	            .setCurrency("GBP")
	            .setDeliveryCountry("JP")
	            .setDeliveryTime(new DeliveryTime()
	                    .setHandlingBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                    .setMaxHandlingTimeInDays(4L)
	                    .setMaxTransitTimeInDays(toJP-4L)
	                    .setMinHandlingTimeInDays(2L)
	                    .setMinTransitTimeInDays(fromJP-2L)
	                    .setTransitBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                )
	                .setEligibility("All scenarios")
	                .setRateGroups(ImmutableList.of(rategroupJP))
	                .setActive(true)
	                .setShipmentType("delivery");
		RateGroup rategroupVN = new RateGroup();
		Table mainTableVN = new Table();
		Headers headersVN = new Headers();
		headersVN.setPrices(ImmutableList.of(
				new Price().setCurrency("GBP").setValue("10.98"),
                new Price().setCurrency("GBP").setValue("19.98"),
                new Price().setCurrency("GBP").setValue("47.98"),
                new Price().setCurrency("GBP").setValue("infinity")
				));
		mainTableVN.setRowHeaders(headersVN);
		mainTableVN.setRows(ImmutableList.of(
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("GBP").setValue("2.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("GBP").setValue("8.49")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("GBP").setValue("15.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("GBP").setValue("20.49"))))
				));
		rategroupVN.setMainTable(mainTableVN);
		Service VNService = new Service()
				.setName("VN")
	            .setCurrency("GBP")
	            .setDeliveryCountry("VN")
	            .setDeliveryTime(new DeliveryTime()
	                    .setHandlingBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                    .setMaxHandlingTimeInDays(4L)
	                    .setMaxTransitTimeInDays(toJP-4L)
	                    .setMinHandlingTimeInDays(2L)
	                    .setMinTransitTimeInDays(fromJP-2L)
	                    .setTransitBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                )
	                .setEligibility("All scenarios")
	                .setRateGroups(ImmutableList.of(rategroupVN))
	                .setActive(true)
	                .setShipmentType("delivery");
		return new ShippingSettings().setServices(ImmutableList.of(usService, JPService, VNService));
	}
	
	private ShippingSettings buildShippingSettings(long fromUS, long toUS, long fromJP, long toJP) {
		//US
		RateGroup rategroupUS = new RateGroup();
		Table mainTableUS = new Table();
		Headers headersUS = new Headers();
		headersUS.setPrices(ImmutableList.of(
				new Price().setCurrency("USD").setValue("9.98"),
                new Price().setCurrency("USD").setValue("24.98"),
                new Price().setCurrency("USD").setValue("49.98"),
                new Price().setCurrency("USD").setValue("99.98"),
                new Price().setCurrency("USD").setValue("119.98"),
                new Price().setCurrency("USD").setValue("infinity")
				));
		mainTableUS.setRowHeaders(headersUS);
		mainTableUS.setRows(ImmutableList.of(
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("USD").setValue("3.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("USD").setValue("4.49")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("USD").setValue("6.49")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("USD").setValue("7.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("USD").setValue("9.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("USD").setValue("0"))))
				));
		rategroupUS.setMainTable(mainTableUS);
		Service usService = new Service()
				.setName("US")
	            .setCurrency("USD")
	            .setDeliveryCountry("US")
	            .setDeliveryTime(new DeliveryTime()
	                    .setHandlingBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                    .setMaxHandlingTimeInDays(4L)
	                    .setMaxTransitTimeInDays(toUS-4L)
	                    .setMinHandlingTimeInDays(2L)
	                    .setMinTransitTimeInDays(fromUS-2L)
	                    .setTransitBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                )
	                .setEligibility("All scenarios")
	                .setRateGroups(ImmutableList.of(rategroupUS))
	                .setActive(true)
	                .setShipmentType("delivery");
		
		//JP
		RateGroup rategroupJP = new RateGroup();
		Table mainTableJP = new Table();
		Headers headersJP = new Headers();
		headersJP.setPrices(ImmutableList.of(
				new Price().setCurrency("USD").setValue("9.98"),
                new Price().setCurrency("USD").setValue("24.98"),
                new Price().setCurrency("USD").setValue("49.98"),
                new Price().setCurrency("USD").setValue("99.98"),
                new Price().setCurrency("USD").setValue("119.98"),
                new Price().setCurrency("USD").setValue("infinity")
				));
		mainTableJP.setRowHeaders(headersJP);
		mainTableJP.setRows(ImmutableList.of(
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("USD").setValue("5.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("USD").setValue("9.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("USD").setValue("14.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("USD").setValue("22.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("USD").setValue("29.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("USD").setValue("36.99"))))
				));
		rategroupJP.setMainTable(mainTableJP);
		Service JPService = new Service()
				.setName("JP")
	            .setCurrency("USD")
	            .setDeliveryCountry("JP")
	            .setDeliveryTime(new DeliveryTime()
	                    .setHandlingBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                    .setMaxHandlingTimeInDays(4L)
	                    .setMaxTransitTimeInDays(toJP-4L)
	                    .setMinHandlingTimeInDays(2L)
	                    .setMinTransitTimeInDays(fromJP-2L)
	                    .setTransitBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                )
	                .setEligibility("All scenarios")
	                .setRateGroups(ImmutableList.of(rategroupJP))
	                .setActive(true)
	                .setShipmentType("delivery");
		return new ShippingSettings().setServices(ImmutableList.of(usService, JPService));
	}
	private ShippingSettings buildShippingSettingsJPCA(long fromUS, long toUS, long fromJP, long toJP) {
		//US
		RateGroup rategroupUS = new RateGroup();
		Table mainTableUS = new Table();
		Headers headersUS = new Headers();
		headersUS.setPrices(ImmutableList.of(
				new Price().setCurrency("CAD").setValue("9.98"),
                new Price().setCurrency("CAD").setValue("24.98"),
                new Price().setCurrency("CAD").setValue("49.98"),
                new Price().setCurrency("CAD").setValue("99.98"),
                new Price().setCurrency("CAD").setValue("119.98"),
                new Price().setCurrency("CAD").setValue("infinity")
				));
		mainTableUS.setRowHeaders(headersUS);
		mainTableUS.setRows(ImmutableList.of(
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("CAD").setValue("3.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("CAD").setValue("6.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("CAD").setValue("8.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("CAD").setValue("12.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("CAD").setValue("18.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("CAD").setValue("15.99"))))
				));
		rategroupUS.setMainTable(mainTableUS);
		Service usService = new Service()
				.setName("CA")
	            .setCurrency("CAD")
	            .setDeliveryCountry("CA")
	            .setDeliveryTime(new DeliveryTime()
	                    .setHandlingBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                    .setMaxHandlingTimeInDays(4L)
	                    .setMaxTransitTimeInDays(toUS-4L)
	                    .setMinHandlingTimeInDays(2L)
	                    .setMinTransitTimeInDays(fromUS-2L)
	                    .setTransitBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                )
	                .setEligibility("All scenarios")
	                .setRateGroups(ImmutableList.of(rategroupUS))
	                .setActive(true)
	                .setShipmentType("delivery");
		
		//JP
		RateGroup rategroupJP = new RateGroup();
		Table mainTableJP = new Table();
		Headers headersJP = new Headers();
		headersJP.setPrices(ImmutableList.of(
				new Price().setCurrency("CAD").setValue("9.98"),
                new Price().setCurrency("CAD").setValue("24.98"),
                new Price().setCurrency("CAD").setValue("49.98"),
                new Price().setCurrency("CAD").setValue("99.98"),
                new Price().setCurrency("CAD").setValue("119.98"),
                new Price().setCurrency("CAD").setValue("infinity")
				));
		mainTableJP.setRowHeaders(headersJP);
		mainTableJP.setRows(ImmutableList.of(
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("CAD").setValue("5.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("CAD").setValue("9.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("CAD").setValue("14.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("CAD").setValue("22.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("CAD").setValue("29.99")))),
				new Row().setCells(ImmutableList.of(
						new Value().setFlatRate(new Price().setCurrency("CAD").setValue("36.99"))))
				));
		rategroupJP.setMainTable(mainTableJP);
		Service JPService = new Service()
				.setName("JP")
	            .setCurrency("CAD")
	            .setDeliveryCountry("JP")
	            .setDeliveryTime(new DeliveryTime()
	                    .setHandlingBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                    .setMaxHandlingTimeInDays(4L)
	                    .setMaxTransitTimeInDays(toJP-4L)
	                    .setMinHandlingTimeInDays(2L)
	                    .setMinTransitTimeInDays(fromJP-2L)
	                    .setTransitBusinessDayConfig(new BusinessDayConfig().setBusinessDays(ImmutableList.of(
	                        "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	                    )))
	                )
	                .setEligibility("All scenarios")
	                .setRateGroups(ImmutableList.of(rategroupJP))
	                .setActive(true)
	                .setShipmentType("delivery");
		return new ShippingSettings().setServices(ImmutableList.of(usService, JPService));
	}
	
	@RequestMapping(value = "/admin/updategmcapi", method = RequestMethod.POST)
	public String updategmcapi(Model model,
			@RequestParam(name = "hiddenproductgmcmodal", required = false) String hiddenproductgmcmodal,
			@RequestParam(name = "merchantidmodal", required = false) String merchantidmodal,
			@RequestParam(name = "titlemodal", required = false) String titlemodal,
			@RequestParam(name = "descriptionmodal", required = false) String descriptionmodal,
			@RequestParam(name = "sizemodal", required = false) String sizemodal,
			@RequestParam(name = "gtinmodal", required = false) String gtinmodal,
			@RequestParam(name = "imagelink", required = false) String imagelink,
			@RequestParam(name = "brand", required = false) String brand,
			@RequestParam(name = "supfeedid", required = false) String supfeedid,
			@RequestParam(name = "additionalimage", required = false) String additionalimage) {
		String idsGMC = hiddenproductgmcmodal;
		GMC gmc = gmcService.findByID(merchantidmodal);
		String domain = gmc.getDomain();
		String domainTM = gmc.getDomaintm();
		String email = gmc.getAccountSampleUser();
		if(supfeedid.contains(":")) {
			supfeedid = supfeedid.substring(supfeedid.indexOf(":")+1, supfeedid.length());
		}
		model.addAttribute("domain", domain);
		model.addAttribute("domainTM", domainTM);
		model.addAttribute("email", email);
		model.addAttribute("merchantId", merchantidmodal);
		List<ProductGMC> lsProductGMC = productGMCService.findByGmcid(merchantidmodal);
		Set<String> parentIDGMC = new HashSet<>();
		List<ProductTMWoo> lsproducttm = productTMWooService.findByDomain(domainTM);
		Set<String> parentIDSet = new HashSet<>();
		List<ProductTMWoo> lsproducttmByParent = lsproducttm.stream().filter(e -> parentIDSet.add(e.getParentid()))
				.collect(Collectors.toList());
		if (!lsproducttm.isEmpty()) {
			model.addAttribute("lsproducttm", lsproducttmByParent);
		}
		List<ProductGMC> lsproductGMCByParent = lsProductGMC.stream().filter(e -> parentIDGMC.add(e.getParentid()))
				.collect(Collectors.toList());
		if (!lsProductGMC.isEmpty()) {
			model.addAttribute("lsProductGMC", lsProductGMC);
		}
		List<Product> lsProduct = new ArrayList<>();
		// only select 1 GMC Product
		if (!idsGMC.contains(Constants.COMMA)) {
			ProductGMC productGMC = productGMCService.findById(Integer.valueOf(idsGMC));
			if(imagelink.isEmpty()) {
				String titleUpdate = titlemodal.trim() + " " + sizemodal.trim();
				String productID = productGMC.getProductid();
				String language = productID.substring(7,9);		
				Product productUpdate = new Product().setId(productID).setOfferId(productGMC.getOfferid())
						.setTitle(titleUpdate).setDescription(descriptionmodal).setChannel(Constants.CHANNEL)
						.setContentLanguage(language).setGtin(gtinmodal.trim()).setIdentifierExists(true).setBrand(brand)
						.setFeedLabel(productGMC.getFeedlabel());
				if(!additionalimage.isEmpty()) {
					String[] arrayImages =  additionalimage.split(Constants.COMMA);
					List<String> addImages = Arrays.asList(arrayImages);
					productUpdate.setAdditionalImageLinks(addImages);
					productGMC.setAdditonalimage(additionalimage);
				}
				productGMC.setSupfeedtm(Constants.FEEDED1ST);
				productGMC.setProducttmtitle(titleUpdate);
				productGMC.setDescription(descriptionmodal);
				productGMC.setGtin(gtinmodal.trim());
				productGMC.setSupfeedid(supfeedid);
				productGMCService.saveProductGMC(productGMC);
				lsProduct.add(productUpdate);
			}else {
				String productID = productGMC.getProductid();
				String language = productID.substring(7,9);		
				Product productUpdate = new Product().setId(productID).setOfferId(productGMC.getOfferid())
						.setTitle(productGMC.getProducttmtitle()).setDescription(productGMC.getDescription()).setChannel(Constants.CHANNEL)
						.setContentLanguage(language).setGtin(productGMC.getGtin()).setIdentifierExists(true).setBrand(brand).setImageLink(imagelink)
						.setFeedLabel(productGMC.getFeedlabel());
				if(!additionalimage.isEmpty()) {
					String[] arrayImages =  additionalimage.split(Constants.COMMA);
					List<String> addImages = Arrays.asList(arrayImages);
					productUpdate.setAdditionalImageLinks(addImages);
					productGMC.setAdditonalimage(additionalimage);
				}
				productGMC.setSupfeedtm(Constants.FEEDEDTM);
				productGMC.setImage(imagelink);
				productGMC.setSupfeedid(supfeedid);
				productGMCService.saveProductGMC(productGMC);
				lsProduct.add(productUpdate);
			}
		} else {
			// multiple gmc product selected
			String[] multipleGMCs = idsGMC.split(Constants.COMMA);
			String[] multipleSize = sizemodal.split(Constants.COMMA);
			String[] multiGtin = gtinmodal.split(Constants.COMMA);
			for (int i = 0; i < multipleGMCs.length; i++) {
				ProductGMC productGMC = productGMCService.findById(Integer.valueOf(multipleGMCs[i]));
				if(imagelink.isEmpty()) {
					String titleUpdate = titlemodal + " " + multipleSize[i];
					String gtin = multiGtin[i];
					String productID = productGMC.getProductid();
					String language = productID.substring(7,9);		
					Product productUpdate = new Product().setId(productGMC.getProductid()).setOfferId(productGMC.getOfferid())
							.setTitle(titleUpdate).setDescription(descriptionmodal).setChannel(Constants.CHANNEL)
							.setContentLanguage(language).setGtin(gtin).setIdentifierExists(true).setBrand(brand)
							.setFeedLabel(productGMC.getFeedlabel());
					if(!additionalimage.isEmpty()) {
						String[] arrayImages =  additionalimage.split(Constants.COMMA);
						List<String> addImages = Arrays.asList(arrayImages);
						productUpdate.setAdditionalImageLinks(addImages);
						productGMC.setAdditonalimage(additionalimage);
					}
					productGMC.setSupfeedtm(Constants.FEEDED1ST);
					productGMC.setProducttmtitle(titleUpdate);
					productGMC.setDescription(descriptionmodal);
					productGMC.setGtin(gtin);
					productGMC.setSupfeedid(supfeedid);
					productGMCService.saveProductGMC(productGMC);
					lsProduct.add(productUpdate);
				}else {
					String productID = productGMC.getProductid();
					String language = productID.substring(7,9);	
					Product productUpdate = new Product().setId(productGMC.getProductid()).setOfferId(productGMC.getOfferid())
							.setTitle(productGMC.getProducttmtitle()).setDescription(productGMC.getDescription()).setChannel(Constants.CHANNEL)
							.setContentLanguage(language).setGtin(productGMC.getGtin()).setImageLink(imagelink).setIdentifierExists(true).setBrand(brand)
							.setFeedLabel(productGMC.getFeedlabel());
					if(!additionalimage.isEmpty()) {
						String[] arrayImages =  additionalimage.split(Constants.COMMA);
						List<String> addImages = Arrays.asList(arrayImages);
						productUpdate.setAdditionalImageLinks(addImages);
						productGMC.setAdditonalimage(additionalimage);
					}
					productGMC.setSupfeedtm(Constants.FEEDEDTM);
					productGMC.setImage(imagelink);
					productGMC.setSupfeedid(supfeedid);
					productGMCService.saveProductGMC(productGMC);
					lsProduct.add(productUpdate);
				}
			}
		}
		
		Long supfeed = Long.valueOf(supfeedid);
		// start FeedGMC
		int feedednumber = supfeedMulti(lsProduct, gmc, true,supfeed);
		String message = "Total products has been fetched: " + feedednumber;
		model.addAttribute("message", message);
		String gmcsupfeed = gmc.getSupfeedid();
		List<String> lsSupfeed = new ArrayList<>();
		if(!gmcsupfeed.isEmpty()) {
			if(!gmcsupfeed.contains(Constants.COMMA)) {
				lsSupfeed.add(gmcsupfeed);
			}else {
				String[] supfeedCountry = gmcsupfeed.split(Constants.COMMA);
				for (String supCountry : supfeedCountry) {
					lsSupfeed.add(supCountry);
				}
			}
		}
		model.addAttribute("lsSupfeed", lsSupfeed);
		return "gmcfeed";
	}

	@RequestMapping(value = "/admin/feedsuptm", method = RequestMethod.POST)
	public String feedsuptm(Model model, @RequestParam(name = "hiddentm", required = false) String hiddentm,
			@RequestParam(name = "hiddenproductgmc", required = false) String hiddenproductgmc,
			@RequestParam(name = "merchantid", required = false) String merchantid,
			@RequestParam(name = "cbtm", required = false) String cbtm) {
		String idsTM = hiddentm;
		String idsGMC = hiddenproductgmc;
		GMC gmc = gmcService.findByID(merchantid);
		String domain = gmc.getDomain();
		String domainTM = gmc.getDomaintm();
		String email = gmc.getAccountSampleUser();
		model.addAttribute("domain", domain);
		model.addAttribute("domainTM", domainTM);
		model.addAttribute("email", email);
		model.addAttribute("merchantId", merchantid);
		List<ProductGMC> lsProductGMC = productGMCService.findByGmcid(merchantid);
		Set<String> parentIDGMC = new HashSet<>();
		List<ProductTMWoo> lsproducttm = productTMWooService.findByDomain(domainTM);
		Set<String> parentIDSet = new HashSet<>();
		List<ProductTMWoo> lsproducttmByParent = lsproducttm.stream().filter(e -> parentIDSet.add(e.getParentid()))
				.collect(Collectors.toList());
		if (!lsproducttm.isEmpty()) {
			model.addAttribute("lsproducttm", lsproducttmByParent);
		}
		List<ProductGMC> lsproductGMCByParent = lsProductGMC.stream().filter(e -> parentIDGMC.add(e.getParentid()))
				.collect(Collectors.toList());
		if (!lsProductGMC.isEmpty()) {
			model.addAttribute("lsProductGMC", lsproductGMCByParent);
		}
		List<ProductGMC> lsFeededGMC = new ArrayList<>();
		List<ProductTMWoo> lsFeededTMWoo = new ArrayList<>();
		// start feed sup TM
		if (idsTM.length() > 0 && idsGMC.length() > 0 && gmc.getSupfeedid() != null
				&& gmc.getSupfeedid().length() > 0) {
			List<Product> lsProduct = new ArrayList<>();
			// only select 1 GMC Product
			if (!idsGMC.contains(Constants.COMMA)) {
				addTMProductFeed(lsProduct, gmc, cbtm, idsGMC, idsTM, lsFeededTMWoo, lsFeededGMC);
			} else {
				// multiple gmc product selected
				String[] multipleGMCs = idsGMC.split(Constants.COMMA);
				for (int i = 0; i < multipleGMCs.length; i++) {
					ProductGMC productGMC = productGMCService.findById(Integer.valueOf(multipleGMCs[i]));
					ProductTMWoo productTMWoo = null;
					String[] multipleTM = null;
					if (!idsTM.contains(Constants.COMMA)) {
						productTMWoo = productTMWooService.findById(Integer.valueOf(idsTM));
					} else {
						multipleTM = idsTM.split(Constants.COMMA);
						int idTMWoo = Integer.valueOf(multipleTM[i]);

						productTMWoo = productTMWooService.findById(idTMWoo);
					}
					String parentIdGMC = productGMC.getParentid();
					if (productTMWoo != null) {
						List<ProductGMC> allFeedGMC = productGMCService.findByGMCandParentid(gmc.getMerchantId(),
								parentIdGMC);
						String parentIDTMWoo = productTMWoo.getParentid();
						List<ProductTMWoo> allFeedTM = productTMWooService.findByParentid(parentIDTMWoo);
						for (int j = 0; j < allFeedGMC.size(); j++) {
							// start map GMC product and TM product
							ProductGMC mapGMC = allFeedGMC.get(j);
							ProductTMWoo mapTMWoo = allFeedTM.get(j);
							// start Mapping
							mapGMCandWOO(gmc, cbtm, mapTMWoo, mapGMC, lsProduct, lsFeededGMC, lsFeededTMWoo);
							// end in case gmc products < tm products
							if (j == allFeedTM.size() - 1) {
								break;
							}
						}
						if (multipleTM != null && i == multipleTM.length - 1) {
							break;
						}
					}
				}
			}

			// start FeedGMC
			int feedednumber = supfeedMulti(lsProduct, gmc, true, Long.valueOf(gmc.getSupfeedid()));
			for (ProductTMWoo product : lsFeededTMWoo) {
				productTMWooService.saveProductTM(product);
			}
			for (ProductGMC product : lsFeededGMC) {
				productGMCService.saveProductGMC(product);
			}
			String message = "Total products has been fetched: " + feedednumber;
			model.addAttribute("message", message);
		}
		if (gmc.getSupfeedid() == null || gmc.getSupfeedid().length() == 0) {
			String message = "Sup Feed API is not set!";
			model.addAttribute("message", message);
		}
		return "gmcfeed";
	}

	@RequestMapping(value = "/admin/feedsuptm2", method = RequestMethod.POST)
	public String feedsuptm2(Model model,
			@RequestParam(name = "hiddenproductgmc2", required = false) String hiddenproductgmc2,
			@RequestParam(name = "merchantid", required = false) String merchantid) {
		String idsGMC = hiddenproductgmc2;
		GMC gmc = gmcService.findByID(merchantid);
		String domain = gmc.getDomain();
		String domainTM = gmc.getDomaintm();
		String email = gmc.getAccountSampleUser();
		model.addAttribute("domain", domain);
		model.addAttribute("domainTM", domainTM);
		model.addAttribute("email", email);
		model.addAttribute("merchantId", merchantid);
		List<ProductGMC> lsProductGMC = productGMCService.findByGmcid(merchantid);
		Set<String> parentIDGMC = new HashSet<>();
		List<ProductTMWoo> lsproducttm = productTMWooService.findByDomain(domainTM);
		Set<String> parentIDSet = new HashSet<>();
		List<ProductTMWoo> lsproducttmByParent = lsproducttm.stream().filter(e -> parentIDSet.add(e.getParentid()))
				.collect(Collectors.toList());
		if (!lsproducttm.isEmpty()) {
			model.addAttribute("lsproducttm", lsproducttmByParent);
		}
		List<ProductGMC> lsproductGMCByParent = lsProductGMC.stream().filter(e -> parentIDGMC.add(e.getParentid()))
				.collect(Collectors.toList());
		if (!lsProductGMC.isEmpty()) {
			model.addAttribute("lsProductGMC", lsproductGMCByParent);
		}
		List<ProductGMC> lsFeededGMC = new ArrayList<>();
		List<ProductTMWoo> lsFeededTMWoo = new ArrayList<>();
		// start feed sup TM
		if (idsGMC.length() > 0 && gmc.getSupfeedid() != null && gmc.getSupfeedid().length() > 0) {
			List<Product> lsProduct = new ArrayList<>();
			if (!idsGMC.contains(Constants.COMMA)) {
				ProductGMC productGMC = productGMCService.findById(Integer.valueOf(idsGMC));
				String productidsTM = productGMC.getProducttmid();
				if (productidsTM != null) {
					ProductTMWoo tmWoo = productTMWooService.findByDomainAndProductid(domainTM, productidsTM);
					String idsTM = String.valueOf(tmWoo.getId());
					addTMProductFeed(lsProduct, gmc, "cbtm", idsGMC, idsTM, lsFeededTMWoo, lsFeededGMC);
				}
			} else {
				// multiple gmc product selected
				String[] multipleGMCs = idsGMC.split(Constants.COMMA);
				for (int i = 0; i < multipleGMCs.length; i++) {
					ProductGMC productGMC = productGMCService.findById(Integer.valueOf(multipleGMCs[i]));
					String productidsTM = productGMC.getProducttmid();
					if (productidsTM != null) {
						ProductTMWoo tmWoo = productTMWooService.findByDomainAndProductid(domainTM, productidsTM);
						String idsTM = String.valueOf(tmWoo.getId());
						addTMProductFeed(lsProduct, gmc, "cbtm", multipleGMCs[i], idsTM, lsFeededTMWoo, lsFeededGMC);
					}
				}
			}
			// start FeedGMC
			int feedednumber = supfeedMulti(lsProduct, gmc, true,Long.valueOf(gmc.getSupfeedid()));
			String message2 = "Total products has been fetched: " + feedednumber;
			model.addAttribute("message2", message2);
		}
		return "gmcfeed";
	}

	@RequestMapping(value = "/admin/feedgmcproduct", method = RequestMethod.POST)
	public String feedgmcproduct(Model model,
			@RequestParam(name = "hiddenproductstore", required = false) String hiddenproductstore,
			@RequestParam(name = "merchantid", required = false) String merchantid) {
		GMC gmc = gmcService.findByID(merchantid);
		String domain = gmc.getDomain();
		String domainTM = gmc.getDomaintm();
		String email = gmc.getAccountSampleUser();
		model.addAttribute("domain", domain);
		model.addAttribute("domainTM", domainTM);
		model.addAttribute("email", email);
		model.addAttribute("merchantId", gmc.getMerchantId());
		List<ProductTMWoo> lsproductMain = productTMWooService.findByDomain(domain);
		Set<String> parentIDSet = new HashSet<>();
		List<ProductTMWoo> lsproductByParent = lsproductMain.stream().filter(e -> parentIDSet.add(e.getParentid()))
				.collect(Collectors.toList());
		if (!lsproductMain.isEmpty()) {
			model.addAttribute("lsproductmain", lsproductByParent);
		}
		List<DataFeedGMC> lsdataFeedGMC = fetchDataFeedGMC(gmc);
		if (!lsdataFeedGMC.isEmpty()) {
			model.addAttribute("lsdataFeedGMC", lsdataFeedGMC);
		}
		if (hiddenproductstore.length() > 0) {
			List<Product> lsProduct = new ArrayList<>();
			if (!hiddenproductstore.contains(Constants.COMMA)) {
				// start feed only 1 product
				ProductTMWoo productTMWoo = productTMWooService.findById(Integer.valueOf(hiddenproductstore));
				String parentId = productTMWoo.getParentid();
				List<ProductTMWoo> lsTMWoo = productTMWooService.findByDomainAndParentid(domain, parentId);
				// start convert
				convertFeedProduct(lsTMWoo, gmc, lsProduct);
			} else {
				String[] productArray = hiddenproductstore.split(Constants.COMMA);
				for (String singleId : productArray) {
					ProductTMWoo productTMWoo = productTMWooService.findById(Integer.valueOf(singleId));
					String parentId = productTMWoo.getParentid();
					List<ProductTMWoo> lsTMWoo = productTMWooService.findByDomainAndParentid(domain, parentId);
					// start convert
					convertFeedProduct(lsTMWoo, gmc, lsProduct);
				}
			}
			// start FeedGMC
			int feedednumber = supfeedMulti(lsProduct, gmc, false,Long.valueOf(gmc.getSupfeedid()));
			String message = "Total products has been fetched: " + feedednumber;
			//start add random 10 products
			Store storeupload = storeService.getStoreByDomain(domain);
			if(Constants.SHOPIFY == storeupload.getType()) {
				Gson gson = new Gson();
				Random rand = new Random();
				List<ShopifyProducts> findAllSP = shopifyProductService.findAll();
				List<ShopifyProducts> randomSP = new ArrayList<>();
				int countrandom = 0;
				for (int i = 0; i < 20; i++) {
					int randomIndex = rand.nextInt(findAllSP.size());
					ShopifyProducts shopifyProducts = findAllSP.get(randomIndex);
					String storesp = shopifyProducts.getStore();
					if(storesp != null && !storesp.contains(domain)) {
						randomSP.add(shopifyProducts);
						countrandom++;
					}
					if (countrandom == 10) {
						break;
					}
				}
				for (ShopifyProducts ransp : randomSP) {
					int singleUpload  = uploadShopifyProducts(gson, ransp, storeupload, domain);
				}
			}
			model.addAttribute("message2", message);
		}
		return "gmcdomain";
	}

	@RequestMapping(value = "/admin/delelesupfeed", method = RequestMethod.POST)
	public String delelesupfeed(Model model,
			@RequestParam(name = "hiddenproductgmcdelete", required = false) String hiddenproductgmcdelete,
			@RequestParam(name = "merchantid", required = false) String merchantid,
			@RequestParam(name = "cball", required = false) String cball) {
		String idsGMC = hiddenproductgmcdelete;
		GMC gmc = gmcService.findByID(merchantid);
		String domain = gmc.getDomain();
		String domainTM = gmc.getDomaintm();
		String email = gmc.getAccountSampleUser();
		model.addAttribute("domain", domain);
		model.addAttribute("domainTM", domainTM);
		model.addAttribute("email", email);
		model.addAttribute("merchantId", merchantid);
		List<ProductGMC> lsProductGMC = productGMCService.findByGmcid(merchantid);
		Set<String> parentIDGMC = new HashSet<>();
		List<ProductTMWoo> lsproducttm = productTMWooService.findByDomain(domainTM);
		Set<String> parentIDSet = new HashSet<>();
		List<ProductTMWoo> lsproducttmByParent = lsproducttm.stream().filter(e -> parentIDSet.add(e.getParentid()))
				.collect(Collectors.toList());
		if (!lsproducttm.isEmpty()) {
			model.addAttribute("lsproducttm", lsproducttmByParent);
		}
		List<ProductGMC> lsproductGMCByParent = lsProductGMC.stream().filter(e -> parentIDGMC.add(e.getParentid()))
				.collect(Collectors.toList());
		if (!lsProductGMC.isEmpty()) {
			model.addAttribute("lsProductGMC", lsproductGMCByParent);
		}
		int countdelete = 0;
		List<ProductGMC> lsDeleted = new ArrayList<>();
		if(cball == null) {
			if (idsGMC.length() > 0 && gmc.getSupfeedid() != null && gmc.getSupfeedid().length() > 0) {
				if (!idsGMC.contains(Constants.COMMA)) {
					// start delete
					ProductGMC productGMC = productGMCService.findById(Integer.valueOf(idsGMC));
					String parentId = productGMC.getParentid();
					List<ProductGMC> lsProductGMCDelete = productGMCService.findByGMCandParentid(gmc.getMerchantId(),
							parentId);
					for (ProductGMC productGMCdelete : lsProductGMCDelete) {
						boolean isdelete = deleteSupfeedsGMC(gmc, productGMCdelete.getProductid(),productGMCdelete);
						if (isdelete) {
							lsDeleted.add(productGMC);
							countdelete++;
						}
					}
				} else {
					// multiple gmc product selected
					String[] multipleGMCs = idsGMC.split(Constants.COMMA);
					for (int i = 0; i < multipleGMCs.length; i++) {
						ProductGMC productGMC = productGMCService.findById(Integer.valueOf(multipleGMCs[i]));
						String parentId = productGMC.getParentid();
						List<ProductGMC> lsProductGMCDelete = productGMCService.findByGMCandParentid(gmc.getMerchantId(),
								parentId);
						for (ProductGMC productGMCdelete : lsProductGMCDelete) {
							boolean isdelete = deleteSupfeedsGMC(gmc, productGMCdelete.getProductid(),productGMCdelete);
							if (isdelete) {
								lsDeleted.add(productGMC);
								countdelete++;
							}
						}
					}
				}
			}
			// update data
			for (ProductGMC productGMC : lsDeleted) {
				productGMC.setSupfeedtm(null);
				productGMC.setProducttmid(null);
				productGMC.setProducttmtitle(null);
				String producttm = productGMC.getOfferid();
				if (producttm != null) {
					ProductTMWoo productTMWoo = productTMWooService.findByDomainAndProductid(domainTM, producttm);
					if (productTMWoo != null) {
						productTMWoo.setFeedstatus("FeedRemoved:" + merchantid);
						String listgmc = productTMWoo.getListgmc();
						if (listgmc.contains(merchantid + Constants.COMMA)) {
							listgmc.replace(merchantid + Constants.COMMA, Constants.EMPTY);
						} else {
							listgmc.replace(merchantid, Constants.EMPTY);
						}
						productTMWoo.setListgmc(listgmc);
						productTMWooService.saveProductTM(productTMWoo);
					}
				}
				productGMCService.saveProductGMC(productGMC);

			}
		}else {
			List<ProductGMC> lsProductGMCId = productGMCService.findByGmcid(merchantid);
			for (ProductGMC productGMC : lsProductGMCId) {
				String tmtitle = productGMC.getProducttmtitle();
				double productPrice = Double.valueOf(productGMC.getPrice());
				if(productPrice >= 99 || (tmtitle != null && !tmtitle.isEmpty())) {
					lsDeleted.add(productGMC);
				}
			}
			for (ProductGMC productGMCdelete : lsDeleted) {
				boolean isdelete = deleteSupfeedsGMC(gmc, productGMCdelete.getProductid(),productGMCdelete);
				if (isdelete) {
					countdelete++;
				}
			}
		}
		
		String message2 = "Total products has been deleted: " + countdelete;
		model.addAttribute("message2", message2);
		return "gmcfeed";
	}

	private List<Product> convertFeedProduct(List<ProductTMWoo> lsFeededTMWoo, GMC gmc, List<Product> lsFeeded) {
		String countryCode = gmc.getCountry();
		String currency = convertCurrency(countryCode);
		String brand = convertVendor(gmc.getDomain());
		ProductTMWoo firstWoo = lsFeededTMWoo.get(0);
		String title = firstWoo.getTitle();
		List<ShopifyProducts> lsProduct = shopifyProductService.findByTitle(title);
		if(!lsProduct.isEmpty()) {
			ShopifyProducts sp = lsProduct.get(0);
			brand = sp.getVendor();
		}
		for (ProductTMWoo productWoo : lsFeededTMWoo) {
			String description = productWoo.getDescription().trim();
			description = removeHtmlTags(description);
			Price price = new Price().setValue(productWoo.getPrice()).setCurrency(currency);
			Product productUpdate = new Product().setId(productWoo.getProductid()).setOfferId(productWoo.getProductid())
					.setTitle(productWoo.getTitle()).setDescription(description)
					.setChannel(Constants.CHANNEL).setContentLanguage(Constants.CONTENT_LANGUAGE)
					.setTargetCountry(countryCode).setItemGroupId(productWoo.getParentid()).setFeedLabel(countryCode)
					.setLink(productWoo.getPermalink()).setAvailability("in stock").setPrice(price);
			List<String> lsSize = new ArrayList<>();
			lsSize.add(productWoo.getVariant());
			productUpdate.setImageLink(productWoo.getImage());
			productUpdate.setColor(productWoo.getColor());
			productUpdate.setAgeGroup(Constants.ADULT);
			productUpdate.setGender(Constants.UNISEX);
			productUpdate.setGtin(productWoo.getGtin());
			productUpdate.setSizes(lsSize);
			productUpdate.setBrand(brand);
			lsFeeded.add(productUpdate);
		}

		return lsFeeded;
	}
	private String removeHtmlTags(String description) {
        // Use a regular expression to remove HTML tags
        String plainText = description.replaceAll("\\<.*?\\>", "");
        return plainText;
    }
	private String convertVendor(String domain) {
		int indexdot = domain.indexOf(".");
		String brand = domain.substring(0, indexdot);
		return brand;
	}
	private String convertCurrency(String countryCode) {
		String currency = "USD";
		switch (countryCode) {
		case Constants.GB:
			currency = Constants.GBP;
			break;
		case Constants.DE:
			currency = Constants.EUR;
			break;
		case Constants.FR:
			currency = Constants.EUR;
			break;
		case Constants.AU:
			currency = Constants.AUD;
			break;

		default:
			break;
		}
		return currency;
	}

	private void mapGMCandWOO(GMC gmc, String cbtm, ProductTMWoo mapTMWoo, ProductGMC mapGMC, List<Product> lsProduct,
			List<ProductGMC> lsFeededGMC, List<ProductTMWoo> lsFeededTMWoo) {
		Product productUpdate = new Product().setId(mapGMC.getProductid()).setOfferId(mapGMC.getOfferid())
				.setTitle(mapTMWoo.getTitle()).setDescription(mapTMWoo.getDescription()).setChannel(Constants.CHANNEL)
				.setContentLanguage(Constants.CONTENT_LANGUAGE).setTargetCountry(mapGMC.getCountry())
				.setFeedLabel(mapGMC.getFeedlabel());
		Price price = new Price().setValue(mapGMC.getPrice()).setCurrency(mapGMC.getCurrency());
		productUpdate.setPrice(price);
		if (cbtm != null) {
			List<String> lsSize = new ArrayList<>();
			lsSize.add(mapTMWoo.getVariant());
			productUpdate.setImageLink(mapTMWoo.getImage());
			productUpdate.setColor(mapTMWoo.getColor());
			productUpdate.setAgeGroup(Constants.ADULT);
			productUpdate.setGender(Constants.UNISEX);
			productUpdate.setGtin(mapTMWoo.getGtin());
			productUpdate.setSizes(lsSize);
			String adsRedirect = convertAdsRedirect(mapGMC, mapTMWoo);
			productUpdate.setAdsRedirect(adsRedirect);
		}
		lsProduct.add(productUpdate);
		// set check feeded or not
		if (cbtm != null) {
			mapTMWoo.setFeedstatus(Constants.FEEDEDTM);
		} else {
			mapTMWoo.setFeedstatus(Constants.FEEDED1ST);
		}

		mapTMWoo.setFeedgmcid(mapGMC.getOfferid());
		String listGMC = mapTMWoo.getListgmc();
		if (listGMC == null) {
			mapTMWoo.setListgmc(mapGMC.getGmcid());
		} else {
			listGMC = listGMC.concat(mapGMC.getGmcid()).concat(Constants.COMMA);
			mapTMWoo.setListgmc(listGMC);
		}
		if (cbtm != null) {
			mapGMC.setSupfeedtm(Constants.FEEDEDTM);
		} else {
			mapGMC.setSupfeedtm(Constants.FEEDED1ST);
		}
		mapGMC.setProducttmid(mapTMWoo.getProductid());
		mapGMC.setProducttmtitle(mapTMWoo.getTitle());
		lsFeededTMWoo.add(mapTMWoo);
		lsFeededGMC.add(mapGMC);
	}

	private List<Product> addTMProductFeed(List<Product> lsProducts, GMC gmc, String cbtm, String idsGMC, String idsTM,
			List<ProductTMWoo> lsFeededTMWoo, List<ProductGMC> lsFeededGMC) {
		ProductGMC productGMC = productGMCService.findById(Integer.valueOf(idsGMC));
		ProductTMWoo productTMWoo = null;
		// only select 1 TM Product
		if (!idsTM.contains(Constants.COMMA)) {
			productTMWoo = productTMWooService.findById(Integer.valueOf(idsTM));
		} else {
			// select multiple TM product but we only feed 1
			String[] multipleTM = idsTM.split(Constants.COMMA);
			int idTMWoo = Integer.valueOf(multipleTM[0]);
			productTMWoo = productTMWooService.findById(idTMWoo);
		}
		String parentIdGMC = productGMC.getParentid();
		if (productTMWoo != null) {
			List<ProductGMC> allFeedGMC = productGMCService.findByGMCandParentid(gmc.getMerchantId(), parentIdGMC);
			String parentIDTMWoo = productTMWoo.getParentid();
			List<ProductTMWoo> allFeedTM = productTMWooService.findByParentid(parentIDTMWoo);
			for (int i = 0; i < allFeedGMC.size(); i++) {
				// start map GMC product and TM product
				ProductGMC mapGMC = allFeedGMC.get(i);
				ProductTMWoo mapTMWoo = allFeedTM.get(i);
				// start Mapping
				mapGMCandWOO(gmc, cbtm, mapTMWoo, mapGMC, lsProducts, lsFeededGMC, lsFeededTMWoo);
				// end in case gmc products < tm products
				if (i == allFeedTM.size() - 1) {
					break;
				}
			}
		}
		return lsProducts;
	}

	private int feedOnebyone(List<Product> lsProducts, GMC gmc) {
		int i = 0;
		try {
			AuthenticatorGMC gmcAuth = new AuthenticatorGMC(gmc);
			ShoppingContent content = gmcAuth.initBuilder();
			Long merchantInt = Long.valueOf(gmc.getMerchantId());
			Long supfeedInt = Long.valueOf(gmc.getSupfeedid());
			for (Product product : lsProducts) {
				Product result = content.products().insert(BigInteger.valueOf(merchantInt), product)
						.set("feedId", BigInteger.valueOf(supfeedInt)).execute();
				if (result != null) {
					i++;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return i;
	}

	private String convertAdsRedirect(ProductGMC productGMC, ProductTMWoo productTMwoo) {
		String gmcdomain = productGMC.getDomain();
		Store storeGMC = storeService.getStoreByDomain(gmcdomain);
		int storeType = storeGMC.getType();
		StringBuilder sb = new StringBuilder();
		String permalinkTM = productTMwoo.getPermalink();
		String linkGMC = productGMC.getLimk();
		sb.append("https://");
		sb.append(productGMC.getDomain());
		int indexProduct = permalinkTM.indexOf("products/");
		if (indexProduct == -1) {
			indexProduct = permalinkTM.indexOf("product/");
		}
		String slugTM = permalinkTM.substring(indexProduct + 8, permalinkTM.length());
		if(Constants.WOO_COMMERCE == storeType) {
			//convert link woo commerce
			if (linkGMC.contains("#!/")) {
				sb.append("/check.php?linksupp2=");
				sb.append(slugTM);
				sb.append("&sps=/");
				int indexSPS = linkGMC.indexOf("#!/");
				String splugSPS = linkGMC.substring(indexSPS + 3, linkGMC.length());
				sb.append(splugSPS);
			} else {
				sb.append("/product/");
				sb.append(slugTM);
				sb.append("&sps=/");
				int indexProductshoes = linkGMC.indexOf("product/");
				String slugshoes = linkGMC.substring(indexProductshoes + 8, linkGMC.length());
				sb.append(slugshoes);
			}
		}else {
			sb.append("/product/");
			sb.append(slugTM);
			sb.append("&spf=/");
			int indexProductshoes = linkGMC.indexOf("products/");
			String slugshoes = linkGMC.substring(indexProductshoes + 9, linkGMC.length());
			sb.append(slugshoes);
		}
		return sb.toString();
	}

	private boolean deleteSupfeedsGMC(GMC gmc, String productid, ProductGMC productdelete) {
		try {
			AuthenticatorGMC gmcAuth = new AuthenticatorGMC(gmc);
			ShoppingContent content = gmcAuth.initBuilder();
			Long merchantInt = Long.valueOf(gmc.getMerchantId());
			String supfeedid = gmc.getSupfeedid();
			List<String> lssupfeed = new ArrayList<>();
			if(supfeedid.contains(Constants.COMMA)) {
				supfeedid = productdelete.getSupfeedid();
				if(supfeedid == null || supfeedid.isEmpty()) {
					supfeedid = gmc.getSupfeedid();
					String[] arraySupFeed = supfeedid.split(Constants.COMMA);
					for (String sup : arraySupFeed) {
						if(sup.contains(":")) {
							sup = sup.substring(supfeedid.indexOf(":")+1, sup.length());
							lssupfeed.add(sup);
						}
					}
				}else {
					lssupfeed.add(supfeedid);
				}
			}
			for (String supfeed : lssupfeed) {
				try {
					Long supfeedInt = Long.valueOf(supfeed);
					content.products().delete(BigInteger.valueOf(merchantInt), productid)
					.set("feedId", BigInteger.valueOf(supfeedInt)).execute();
				} catch (Exception e) {
					e.fillInStackTrace();
				}
			}
		} catch (Exception e) {
			e.fillInStackTrace();
			return false;
		}
		return true;
	}

	private boolean deleteproductsGMC(GMC gmc, String productid) {
		try {
			AuthenticatorGMC gmcAuth = new AuthenticatorGMC(gmc);
			ShoppingContent content = gmcAuth.initBuilder();
			Long merchantInt = Long.valueOf(gmc.getMerchantId());
			content.products().delete(BigInteger.valueOf(merchantInt), productid).execute();
		} catch (Exception e) {
			e.fillInStackTrace();
			return false;
		}
		return true;
	}

	private int updatemulti(List<Product> lsProducts, GMC gmc) {
		int i = 0;
		try {
			AuthenticatorGMC gmcAuth = new AuthenticatorGMC(gmc);
			ShoppingContent content = gmcAuth.initBuilder();
			Long merchantInt = Long.valueOf(gmc.getMerchantId());
			for (Product product : lsProducts) {
				Product update =  content.products().update(BigInteger.valueOf(merchantInt), product.getId(), product).execute();
				if(update != null) {
					i ++;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return i;
	}

	private int supfeedMulti(List<Product> lsProducts, GMC gmc, boolean isSupfeed, Long supfeedInt) {
		int i = 0;
		try {
			AuthenticatorGMC gmcAuth = new AuthenticatorGMC(gmc);
			ShoppingContent content = gmcAuth.initBuilder();
			Long merchantInt = Long.valueOf(gmc.getMerchantId());
			ProductsCustomBatchResponse batchResponse = null;
			if (isSupfeed) {
				batchResponse = content.products().custombatch(
						createBatch(lsProducts, BigInteger.valueOf(merchantInt), BigInteger.valueOf(supfeedInt)))
						.execute();
			} else {
				batchResponse = content.products()
						.custombatch(createBatchMain(lsProducts, BigInteger.valueOf(merchantInt))).execute();
			}
			if (batchResponse.getEntries() == null) {
				return i;
			}
			for (ProductsCustomBatchResponseEntry entry : batchResponse.getEntries()) {
				if (entry.getErrors() != null) {
				} else {
					if (entry.getProduct() != null) {
						i++;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return i;
	}


	private ProductsCustomBatchRequest createBatchMain(List<Product> lsProducts, BigInteger merchantId) {
		List<ProductsCustomBatchRequestEntry> productsBatchRequestEntries = new ArrayList<>();
		for (int i = 0; i < lsProducts.size(); i++) {
			Product product = lsProducts.get(i);
			ProductsCustomBatchRequestEntry updateEntry = new ProductsCustomBatchRequestEntry().setBatchId((long) i)
					.setMerchantId(merchantId).setMethod("insert").setProduct(product);
			productsBatchRequestEntries.add(updateEntry);
		}
		return new ProductsCustomBatchRequest().setEntries(productsBatchRequestEntries);
	}

	private ProductsCustomBatchRequest createBatch(List<Product> lsProducts, BigInteger merchantId,
			BigInteger supfeedInt) {
		List<ProductsCustomBatchRequestEntry> productsBatchRequestEntries = new ArrayList<>();
		for (int i = 0; i < lsProducts.size(); i++) {
			Product product = lsProducts.get(i);
			ProductsCustomBatchRequestEntry updateEntry = new ProductsCustomBatchRequestEntry().setBatchId((long) i)
					.setMerchantId(merchantId).setMethod("insert").setFeedId(supfeedInt).setProduct(product);
			productsBatchRequestEntries.add(updateEntry);
		}
		return new ProductsCustomBatchRequest().setEntries(productsBatchRequestEntries);
	}
	private boolean updateBusinessName(GMC gmc, String name) {
		try {
			AuthenticatorGMC gmcAuth = new AuthenticatorGMC(gmc);
			ShoppingContent content = gmcAuth.initBuilder();
			Long intvalue = Long.valueOf(gmc.getMerchantId());
			Account account =
			          content.accounts().get(BigInteger.valueOf(intvalue), BigInteger.valueOf(intvalue)).execute();
			account.setName(name);
			content.accounts().update(BigInteger.valueOf(intvalue), BigInteger.valueOf(intvalue), account).execute();
			gmc.setBusinessname(name);
			gmcService.saveGMC(gmc);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	private String checkAccountStatus(GMC gmc) {
		String status = Constants.ENABLE;
		try {
			AuthenticatorGMC gmcAuth = new AuthenticatorGMC(gmc);
			ShoppingContent content = gmcAuth.initBuilder();
			Long intvalue = Long.valueOf(gmc.getMerchantId());
			String gmcStatus = gmc.getStatus();
			AccountStatus accountStatus = content.accountstatuses()
					.get(BigInteger.valueOf(intvalue), BigInteger.valueOf(intvalue)).execute();
			List<AccountStatusAccountLevelIssue> lsIssue = accountStatus.getAccountLevelIssues();
			if (lsIssue != null && !lsIssue.isEmpty()) {
				for (AccountStatusAccountLevelIssue accountStatusAccountLevelIssue : lsIssue) {
					String levelTitle = accountStatusAccountLevelIssue.getTitle();
					if(!Constants.NO_ADS.equals(levelTitle)) {
						status = levelTitle;
					}
					if (status.contains(Constants.SUSPENDED) || status.contains(Constants.MISPRESENTATION)) {
						status = Constants.SUSPENDED;
						break;
					}
				}
			}
			if (Constants.SUSPENDED.equals(gmcStatus) && Constants.ENABLE.equals(status)) {
				status = Constants.REACTIVE;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	private long addfeedData(GMC gmc, String name, String language, String country, String link, String includeads) {
		List<String> includeProgram = new ArrayList<>();
		List<String> excludeProgram = new ArrayList<>();
		if(includeads.equals("Yes")) {
			includeProgram.add("Shopping");
			excludeProgram.add("SurfacesAcrossGoogle");
			gmc.setSuspendedtype(Constants.GOOGLE_ADS);
		}else if (includeads.equals("No")) {
			excludeProgram.add("Shopping");
			includeProgram.add("SurfacesAcrossGoogle");
			gmc.setSuspendedtype(Constants.FREE_LISTING);
		}else if (includeads.equals("All")) {
			includeProgram.add("SurfacesAcrossGoogle");
			includeProgram.add("Shopping");
			gmc.setSuspendedtype(Constants.GOOGLE_ADS);
		}
		long feedid = 0;
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 25);
		long dateFeed = c.get(Calendar.DAY_OF_MONTH);
		try {
			AuthenticatorGMC gmcAuth = new AuthenticatorGMC(gmc);
			Long gmcID = Long.valueOf(gmc.getMerchantId());
			ShoppingContent content = gmcAuth.initBuilder();
			List<String> targetCountry = new ArrayList<>();
			if (!country.contains(Constants.HYPHEN)) {
				targetCountry.add(country);
			} else {
				String[] countryList = country.split(Constants.HYPHEN);
				for (String stringCountry : countryList) {
					targetCountry.add(stringCountry);
				}
			}
			Datafeed newfeed = new Datafeed().setName(name).setContentType("products").setAttributeLanguage(language)
					.setFileName(gmc.getMerchantId() + "product.rss")
					.setTargets(ImmutableList.of(new DatafeedTarget().setLanguage(language).setCountry(country)
							.setTargetCountries(targetCountry).setIncludedDestinations(includeProgram).setExcludedDestinations(excludeProgram)))
					.setFetchSchedule(new DatafeedFetchSchedule().setDayOfMonth(dateFeed).setHour(6L)
							.setTimeZone("America/Los_Angeles").setFetchUrl(link))
					.setFormat(new DatafeedFormat().setFileEncoding("utf-8").setColumnDelimiter("tab")
							.setQuotingMode("value quoting"));
			Datafeed result = content.datafeeds().insert(BigInteger.valueOf(gmcID), newfeed).execute();
			feedid = result.getId();
			gmcService.saveGMC(gmc);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return feedid;
	}

	private String fetchGMC(GMC gmc, long feedid) {
		String message = Constants.FAILED;
		try {
			AuthenticatorGMC gmcAuth = new AuthenticatorGMC(gmc);
			Long gmcID = Long.valueOf(gmc.getMerchantId());
			ShoppingContent content = gmcAuth.initBuilder();
			DatafeedsFetchNowResponse fetch = content.datafeeds()
					.fetchnow(BigInteger.valueOf(gmcID), BigInteger.valueOf(feedid)).execute();
			message = Constants.SUCCESSED;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return message;
	}
	private String updatefeedGMC(GMC gmc, long feedid, String country) {
		String message = Constants.FAILED;
		try {
			AuthenticatorGMC gmcAuth = new AuthenticatorGMC(gmc);
			Long gmcID = Long.valueOf(gmc.getMerchantId());
			ShoppingContent content = gmcAuth.initBuilder();
			List<Datafeed> lsFeed = listDatafeedsForMerchant(gmc);
			List<String> targetCountry = new ArrayList<>();
			if(country.contains(Constants.HYPHEN)) {
				String[] arrayCountry = country.split(Constants.HYPHEN);
				for (String newcountry : arrayCountry) {
					targetCountry.add(newcountry);
				}
			}else {
				targetCountry.add(country);
			}
			for (Datafeed datafeed : lsFeed) {
				long feedIDExist = datafeed.getId();
				if(feedid == feedIDExist) {
					datafeed.getTargets().get(0).setTargetCountries(targetCountry);
					Datafeed result = content.datafeeds().update(BigInteger.valueOf(gmcID),BigInteger.valueOf(feedid), datafeed).execute();
				}
			}
			message = Constants.SUCCESSED;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return message;
	}

	private String deleteFeedGMC(GMC gmc, long feedid) {
		String message = Constants.FAILED;
		try {
			AuthenticatorGMC gmcAuth = new AuthenticatorGMC(gmc);
			Long gmcID = Long.valueOf(gmc.getMerchantId());
			ShoppingContent content = gmcAuth.initBuilder();
			content.datafeeds().delete(BigInteger.valueOf(gmcID), BigInteger.valueOf(feedid)).execute();
			message = Constants.SUCCESSED;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return message;
	}

	private List<DataFeedGMC> fetchDataFeedGMC(GMC gmc) {
		List<DataFeedGMC> lsdataFeedGMC = new ArrayList<>();
		try {
			List<Datafeed> lsFeed = listDatafeedsForMerchant(gmc);
			for (Datafeed datafeed : lsFeed) {
				long feedID = datafeed.getId();
				String name = datafeed.getName();
				String link = datafeed.getFetchSchedule().getFetchUrl();
				String country = Constants.EMPTY;
				String language = Constants.EMPTY;
				List<DatafeedTarget> targets = datafeed.getTargets();
				for (DatafeedTarget target : targets) {
					List<String> targetCountry = target.getTargetCountries();
					for (String tc : targetCountry) {
						if (Constants.EMPTY.equals(country)) {
							country = country.concat(tc);
						} else {
							country = country.concat(Constants.HYPHEN).concat(tc);
						}
					}
					if (Constants.EMPTY.equals(language)) {
						language = language.concat(target.getLanguage());
					} else {
						language = language.concat(Constants.HYPHEN).concat(target.getLanguage());

					}
				}
				DataFeedGMC feedGMC = new DataFeedGMC(feedID, name, link, country, language);
				lsdataFeedGMC.add(feedGMC);
			}

		} catch (Exception e2) {
			e2.printStackTrace();
		}
		return lsdataFeedGMC;
	}

	private List<Datafeed> listDatafeedsForMerchant(GMC gmc) throws IOException {
		List<Datafeed> lsFeed = new ArrayList<>();
		AuthenticatorGMC gmcAuth = new AuthenticatorGMC(gmc);
		Long gmcID = Long.valueOf(gmc.getMerchantId());
		ShoppingContent content = gmcAuth.initBuilder();
		com.google.api.services.content.ShoppingContent.Datafeeds.List datafeedsList = content.datafeeds()
				.list(BigInteger.valueOf(gmcID));
		DatafeedsListResponse page = null;

		do {
			if (page != null) {
				datafeedsList.setPageToken(page.getNextPageToken());
			}
			page = datafeedsList.execute();
			if (page.getResources() == null) {
			}else {
				for (Datafeed datafeed : page.getResources()) {
					lsFeed.add(datafeed);
				}
			}
			
			
		} while (page.getNextPageToken() != null);
		return lsFeed;
	}

	private List<Product> getGMCProduct(GMC gmc) {
		List<Product> lsProduct = new ArrayList<>();
		try {
			AuthenticatorGMC gmcAuth = new AuthenticatorGMC(gmc);
			ShoppingContent content = gmcAuth.initBuilder();
			Long gmcID = Long.valueOf(gmc.getMerchantId());
			ShoppingContent.Products.List productsList = content.products().list(BigInteger.valueOf(gmcID));
			ProductsListResponse page = null;
			do {
				if (page != null) {
					productsList.setPageToken(page.getNextPageToken());
				}
				page = productsList.execute();
				if (page.getResources() == null) {
					return lsProduct;
				} else {
					lsProduct.addAll(page.getResources());
				}
			} while (page.getNextPageToken() != null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lsProduct;
	}

	private GMC checkGoogleProgram(GMC gmc, String country) {
		String eligible = Constants.APPROVED;
		try {
			AuthenticatorGMC gmcAuth = new AuthenticatorGMC(gmc);
			ShoppingContent content = gmcAuth.initBuilder();
			Long gmcID = Long.valueOf(gmc.getMerchantId());
			FreeListingsProgramStatus programStatus = content.freelistingsprogram().get(gmcID).execute();
			if (programStatus != null && !programStatus.getGlobalState().equals("NO_OFFERS_UPLOADED")) {
				List<FreeListingsProgramStatusRegionStatus> lsStatus = programStatus.getRegionStatuses();
				for (FreeListingsProgramStatusRegionStatus freeListingProgramStatusRegionStatus : lsStatus) {
					List<String> countrycodes = freeListingProgramStatusRegionStatus.getRegionCodes();
					for (String countryCode : countrycodes) {
						if (country.equals(countryCode)) {
							String eligibilereview = freeListingProgramStatusRegionStatus.getReviewEligibilityStatus();
							if (eligibilereview == null) {
								eligibilereview = freeListingProgramStatusRegionStatus.getReviewIneligibilityReason();
							}
							eligible = freeListingProgramStatusRegionStatus.getEligibilityStatus();
							gmc.setEligibility(eligible);
							gmc.setRevieweligibility(eligibilereview);
							if(Constants.DISAPPROVED.equals(eligible)) {
								gmc.setSuspendedtype(Constants.FREE_LISTING);
							}
							break;
						}
					}

				}
			}
			//if(Constants.SUSPENDED.equals(gmc.getStatus()) && Constants.APPROVED.equals(eligible)) {
				//start check google ads
				ShoppingAdsProgramStatus adsStatus = content.shoppingadsprogram().get(gmcID).execute();
				if(adsStatus != null) {
					String globalState = adsStatus.getGlobalState();
					if(Constants.ENABLED.equals(globalState)) {
						List<ShoppingAdsProgramStatusRegionStatus> lsStatus = adsStatus.getRegionStatuses();
						for (ShoppingAdsProgramStatusRegionStatus shoppingAdsProgramStatusRegionStatus : lsStatus) {
							List<String> countrycodes = shoppingAdsProgramStatusRegionStatus.getRegionCodes();
							for (String countryCode : countrycodes) {
								if (country.equals(countryCode)) {
									String eligibilereview = shoppingAdsProgramStatusRegionStatus.getReviewEligibilityStatus();
									if (eligibilereview == null) {
										eligibilereview = shoppingAdsProgramStatusRegionStatus.getReviewIneligibilityReason();
									}
									eligible = shoppingAdsProgramStatusRegionStatus.getEligibilityStatus();
									gmc.setEligibility(eligible);
									gmc.setRevieweligibility(eligibilereview);
									if(Constants.DISAPPROVED.equals(eligible)) {
										gmc.setSuspendedtype(Constants.GOOGLE_ADS);
									}
									break;
								}
							}
						}
					}
				}
			//}
		} catch (Exception e) {
			eligible = Constants.FAILED;
			e.printStackTrace();
		}
		return gmc;
	}
	
	private int uploadShopifyProducts(Gson gson, ShopifyProducts shopifyProducts, Store storeupload, String storeselect) {
		int countupload = 0;
		String handle = shopifyProducts.getHandle();
		List<ShopifyProducts> lsHandlesProduct = shopifyProductService.findByHandle(handle);
		int sizeHandle = lsHandlesProduct.size();
		// check simple product
		if (sizeHandle == 1) {
			for (ShopifyProducts productupload : lsHandlesProduct) {
				ProductShopifyCreate productShopifyCreate = new ProductShopifyCreate();
//				String title = generateRandomContent(productupload.getTitle(), true);
				productShopifyCreate.setTitle(productupload.getTitle());
				//String description = generateRandomContent(productupload.getDescription(),false);
				productShopifyCreate.setBody_html(productupload.getDescription());
				productShopifyCreate.setVendor(productupload.getVendor());
				productShopifyCreate.setProduct_type(productupload.getCategory());
				productShopifyCreate.setHandle(productupload.getHandle());
				productShopifyCreate.setStatus(Constants.ACTIVE_PRODUCT);
				productShopifyCreate.setTags(productupload.getTags());
				//variant
				List<ShopifyVariants> variants = new ArrayList<>();
				ShopifyVariants variant = new ShopifyVariants();
				variant.setBarcode(productupload.getBarcode());
				variant.setTitle(productupload.getOption1value());
				variant.setPrice(String.valueOf(productupload.getPrice()));
				variant.setPosition(Integer.valueOf(productupload.getPosition() == null ? "1" : productupload.getPosition()));
				variant.setInventory_management("shopify");
				variant.setOption1(productupload.getOption1value());
				variant.setOption2(productupload.getOption2value());
				variant.setInventory_quantity(89);
				variants.add(variant);
				productShopifyCreate.setVariants(variants);
				//option
				List<ShopifyOptions> options = new ArrayList<>();
				ShopifyOptions option = new ShopifyOptions();
				option.setName(productupload.getOption1name());
				List<String> values = new ArrayList<>();
				values.add(productupload.getOption1value());
				option.setValues(values);
				options.add(option);
				productShopifyCreate.setOptions(options);
				//images
				List<ShopifyImages> lsImage = new ArrayList<>();
				ShopifyImages image = new ShopifyImages();
				image.setPosition(Integer.valueOf(productupload.getPosition()));
				image.setSrc(productupload.getImage());
				lsImage.add(image);
				productShopifyCreate.setImage(image);
				productShopifyCreate.setImages(lsImage);
				if(!StringUtils.isEmpty(storeupload.getSubdomainshopify())) {
					HttpResponse httpResponse = uploadShopify(gson, storeupload, productShopifyCreate);
					int statusCode = httpResponse.getStatusLine().getStatusCode();
					if (HttpURLConnection.HTTP_CREATED == statusCode) {
						countupload++;
						String existStore = shopifyProducts.getStore();
						if(existStore == null || existStore.isEmpty()) {
							//shopifyProducts.setStore(storeselect);
						}else {
							//shopifyProducts.setStore(Constants.COMMA.concat(storeselect));
						}
						shopifyProductService.saveShopifyProduct(shopifyProducts);
					}
				}
			}
		}else {
			ShopifyProducts firstProduct = null;
			//variant
			List<ShopifyVariants> variants = new ArrayList<>();
			//option
			List<ShopifyOptions> options = new ArrayList<>();
			ShopifyOptions option = new ShopifyOptions();
			List<String> values = new ArrayList<>();
			//image
			List<ShopifyImages> lsImage = new ArrayList<>();
			for (ShopifyProducts productupload : lsHandlesProduct) {
				String position = productupload.getPosition();
				if("1".equals(position)) {
					firstProduct = productupload;
				}
				//Singe variant
				ShopifyVariants variant = new ShopifyVariants();
				variant.setBarcode(productupload.getBarcode());
				variant.setTitle(productupload.getOption1value());
				variant.setPrice(String.valueOf(productupload.getPrice()));
				if(!StringUtils.isEmpty(productupload.getPosition())) {
					variant.setPosition(Integer.valueOf(productupload.getPosition()));
				}
				variant.setInventory_management("shopify");
				variant.setOption1(productupload.getOption1value());
				variant.setOption2(productupload.getOption2value());
				variant.setInventory_quantity(89);
				variants.add(variant);
				
				//Single Option				
				values.add(productupload.getOption1value());
				
				if(!StringUtils.isEmpty(productupload.getPosition())) {
					ShopifyImages image = new ShopifyImages();
					image.setPosition(Integer.valueOf(productupload.getPosition()));
					image.setSrc(productupload.getImage());
					lsImage.add(image);
				}
			}
			if(firstProduct != null) {
				ProductShopifyCreate productShopifyCreate = new ProductShopifyCreate();
//				String title = generateRandomContent(firstProduct.getTitle(),true);
				productShopifyCreate.setTitle(firstProduct.getTitle());
				//String description = generateRandomContent(firstProduct.getDescription(),false);
				productShopifyCreate.setBody_html(firstProduct.getDescription());
				productShopifyCreate.setVendor(firstProduct.getVendor());
				productShopifyCreate.setProduct_type(firstProduct.getCategory());
				productShopifyCreate.setHandle(firstProduct.getHandle());
				productShopifyCreate.setStatus(Constants.ACTIVE_PRODUCT);
				productShopifyCreate.setTags(firstProduct.getTags());
				productShopifyCreate.setVariants(variants);
				option.setName(firstProduct.getOption1name());
				option.setValues(values);
				options.add(option);
				if(!lsImage.isEmpty()) {
					productShopifyCreate.setImages(lsImage);
					productShopifyCreate.setImage(lsImage.get(0));
				}
				if(!StringUtils.isEmpty(storeupload.getSubdomainshopify())) {
					HttpResponse httpResponse = uploadShopify(gson, storeupload, productShopifyCreate);
					int statusCode = httpResponse.getStatusLine().getStatusCode();
					if (HttpURLConnection.HTTP_CREATED == statusCode) {
						countupload++;
						String existStore = shopifyProducts.getStore();
						if(existStore == null || existStore.isEmpty()) {
							//shopifyProducts.setStore(storeselect);
						}else {
							//shopifyProducts.setStore(Constants.COMMA.concat(storeselect));
						}
						shopifyProductService.saveShopifyProduct(shopifyProducts);
					}
				}
			}
		}
		return countupload;
		
	}
	private HttpResponse uploadShopify(Gson gson, Store storeupload, ProductShopifyCreate product) {
		StringBuilder apiURL = new StringBuilder();
		apiURL.append("https://");
		apiURL.append(storeupload.getSubdomainshopify());
		apiURL.append("/admin/api/");
		apiURL.append(Constants.SHOPIFY_API_VERSION);
		apiURL.append("/products.json");
		ProductShopifyCreateModel newPsmodel = new ProductShopifyCreateModel();
		newPsmodel.setProduct(product);
		String jsonData = gson.toJson(newPsmodel);
		try {
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost httpPost = new HttpPost(apiURL.toString());
			StringEntity params = new StringEntity(jsonData);
			httpPost.setEntity(params);
			httpPost.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
			httpPost.setHeader(Constants.X_Shopify_Access_Token, storeupload.getAccesstoken());
			HttpResponse response = client.execute(httpPost);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	private List<String> lsDomainCyber(){
		List<String> cyberDomain =  new ArrayList<>();
		String htmldomain = "<select ng-change=\"fetchDetails()\" ng-model=\"websiteToBeDeleted\" class=\"form-control ng-pristine ng-valid ng-empty ng-touched\"><option value=\"? undefined:undefined ?\" selected=\"selected\"></option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"dublinafternoon.shop\">dublinafternoon.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"dublinangels.shop\">dublinangels.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"dublinbad.shop\">dublinbad.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"dublinblank.shop\">dublinblank.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"dublinboring.shop\">dublinboring.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"dublincamping.shop\">dublincamping.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"dublincats.shop\">dublincats.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"dublincoat.shop\">dublincoat.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"dublincoffee.shop\">dublincoffee.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"dublindrinks.shop\">dublindrinks.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"dublinend.shop\">dublinend.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"dublinheaven.shop\">dublinheaven.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"dublinlively.shop\">dublinlively.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"dublinlonely.shop\">dublinlonely.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"dublinlost.shop\">dublinlost.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"dublinmorning.shop\">dublinmorning.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"dublinperfume.shop\">dublinperfume.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"dublinpoor.shop\">dublinpoor.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"dublinshadow.shop\">dublinshadow.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"dublinspirit.shop\">dublinspirit.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"dublinstop.shop\">dublinstop.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"dublintear.shop\">dublintear.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"dublintint.shop\">dublintint.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"dublinwhite.shop\">dublinwhite.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"dublinworst.shop\">dublinworst.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"dublinyellow.shop\">dublinyellow.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"localhost.localdomain\">localhost.localdomain</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"sneakernew.shop\">sneakernew.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"warsawsecond.shop\">warsawsecond.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"warsawseldom.shop\">warsawseldom.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"warsawshape.shop\">warsawshape.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"warsawshine.shop\">warsawshine.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"warsawshirt.shop\">warsawshirt.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"warsawslay.shop\">warsawslay.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"warsawslow.shop\">warsawslow.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"warsawsneaker.shop\">warsawsneaker.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"warsawspring.shop\">warsawspring.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"warsawstick.shop\">warsawstick.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"warsawstreet.shop\">warsawstreet.shop</option>\r\n"
				+ "                            \r\n"
				+ "                                <option value=\"warsawsun.shop\">warsawsun.shop</option>\r\n"
				+ "                            \r\n"
				+ "                        </select>";
		// Regex pattern to match the value attributes inside option tags
        Pattern pattern = Pattern.compile("<option\\s+value=\"(.*?)\">");
        Matcher matcher = pattern.matcher(htmldomain);
     // Find all matches and add to the list
        while (matcher.find()) {
        	cyberDomain.add(matcher.group(1));
        }
		return cyberDomain;
	}
	
}
