package shoes.sup.controller;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.http.entity.StringEntity;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.PaymentIntent;

import shoes.sup.model.IdInput;
import shoes.sup.model.Order;
import shoes.sup.model.Status;
import shoes.sup.model.Store;
import shoes.sup.model.StripeAccount;
import shoes.sup.model.TrackingSup;
import shoes.sup.service.OrderService;
import shoes.sup.service.StoreService;
import shoes.sup.service.StripeService;
import shoes.sup.util.Constants;

@Controller
public class StripeController {
	
	@Autowired
	StripeService stripeService;
	
	@Autowired 
	OrderService orderService;
	
	@Autowired
	StoreService storeService;
	
	@RequestMapping(value = "/admin/stripe", method = RequestMethod.GET)
	public String paypalAccount(Model model) {
		List<StripeAccount> lsStripe = stripeService.findAll();
		if (lsStripe != null && !lsStripe.isEmpty()) {
			model.addAttribute("lsStripe", lsStripe);
		}
		return "stripeaccount";
	}
	
	@RequestMapping(value = "/admin/addstripe", method = RequestMethod.POST)
	public String addAccountPP(Model model,
			@RequestParam("key") String key,
			@RequestParam("secret") String secret) {
		StripeAccount existed = stripeService.findByKey(key);
		if(existed == null) {
			StripeAccount newStripe = new StripeAccount();
			newStripe.setKey(key);
			newStripe.setSecret(secret);
			newStripe.setStatus(Constants.ACTIVE);
			stripeService.saveStripe(newStripe);
		}
		return "redirect:/admin/stripe";
	}
	
	@RequestMapping(value = "/admin/ajaxcapturestripe",method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ajaxcaptureStripe(@RequestBody String json){
		String result = Constants.FAILED;
		if(json.contains("\"")){
			json = json.replace("\"", "");
		}
		Gson gson = new Gson();
		IdInput idInput = gson.fromJson(json, IdInput.class);
		String id = idInput.getId();
		Order order = orderService.findById(Integer.valueOf(id));
		boolean isCapture = captureStripe(order);
		if(isCapture) {
			result =  Constants.SUCCESSED;
		}
		return ResponseEntity.ok(result);
	}
	@RequestMapping(value = "/admin/deliverstripe",method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ajaxdeliver(@RequestBody String json){
		String result = Constants.FAILED;
		Gson gson = new Gson();
		TrackingSup trackingSup = gson.fromJson(json, TrackingSup.class);
		int id = trackingSup.getId();
		String trackingnumber = trackingSup.getTrackingnumber();
		String carrier = trackingSup.getCarrier();
		String notecarrier = trackingSup.getNotecarrier();
		String partial = trackingSup.getPartial();
		Order order = orderService.findById(id);
		String captureStatus = order.getPayment_status();
		//capture order
		if(!Constants.CAPTURED.equalsIgnoreCase(captureStatus)) {
			boolean isCapture = captureStripe(order);
			try {
				if(isCapture){
					String storeDomain = order.getStoredomain();
					Store store = storeService.getStoreByDomain(storeDomain);
					if(store.getState().equals(Constants.ACTIVE)){
						if(store.getType() == Constants.WOO_COMMERCE) {
							String ck = store.getApikey();
							String cs = store.getPassword();
							Status status = new Status(Constants.WOO_PROCESSING);
							String idwoo = order.getOname().substring(4, order.getOname().length());
							String jsonDataPut = gson.toJson(status);
							StringEntity paramsPut =new StringEntity(jsonDataPut);
							HttpClient clientPut = HttpClientBuilder.create().build();
							String urlPut = "https://www." + order.getStoredomain() + "/wp-json/wc/v3/orders/"+idwoo+"?consumer_key="+ck +"&consumer_secret="+cs;
							HttpPut httpPut = new HttpPut(urlPut);
							httpPut.setEntity(paramsPut);
							httpPut.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
							HttpResponse responsePut = clientPut.execute(httpPut);
							int statusCodePut = responsePut.getStatusLine().getStatusCode();
							if(HttpURLConnection.HTTP_CREATED == statusCodePut || HttpURLConnection.HTTP_OK == statusCodePut){
								order.setPayment_status(Constants.CAPTURED);
								order.setStatus(Constants.WOO_COMPLETED);
								orderService.saveOrder(order);
								result = Constants.SUCCESS;
							}
						}
					}
					order.setPayment_status(Constants.CAPTURED);
					order.setStatus(Constants.WOO_COMPLETED);
					orderService.saveOrder(order);
					result = Constants.SUCCESS;
				}else {
					order.setPayment_status(Constants.CAPTURED_FAILED);
					orderService.saveOrder(order);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		//update track
		boolean updateTrack = updateTracking(order,trackingnumber, carrier);
		if(updateTrack) {
			result += Constants.SUCCESSED;
		}
		
		return ResponseEntity.ok(result);
	}
	
	private boolean updateTracking(Order order, String tracking_number, String carrier) {
		String pi = order.getStripe_intent_id();
		try {
			String stripSK = order.getStripe_sk();
			StripeAccount stripeAccount = stripeService.findByKey(stripSK); 
			Stripe.apiKey = stripeAccount.getSecret();
			PaymentIntent paymentIntent = PaymentIntent.retrieve(pi);
			Map<String, Object> address = new HashMap<String, Object>();
			address.put("city", order.getCity());
			address.put("country", order.getCountry());
			address.put("line1", order.getAddress1());
			address.put("line2", order.getAddress2());
			address.put("postal_code", order.getZip());
			address.put("state", order.getProvince());
			Map<String, Object> tracking = new HashMap<String, Object>();
			tracking.put("address", address);
			tracking.put("name", order.getAddressname());
			tracking.put("tracking_number", tracking_number);
			tracking.put("carrier", carrier);
			Map<String, Object> shipping = new HashMap<String, Object>();
			shipping.put("shipping", tracking);
			PaymentIntent updatedPaymentIntent =
					  paymentIntent.update(shipping);
			order.setTrackingnumber(tracking_number);
			order.setStatus(Constants.DELIVERED);
			orderService.saveOrder(order);
			return true;
		} catch (Exception e) {
			e.getMessage();
			return false;
		}
	}
	
	private boolean captureStripe(Order order) {
		String pi = order.getStripe_intent_id();
		try {
			String stripSK = order.getStripe_sk();
			StripeAccount stripeAccount = stripeService.findByKey(stripSK); 
			Stripe.apiKey = stripeAccount.getSecret();
			PaymentIntent paymentIntent = PaymentIntent.retrieve(pi);
			PaymentIntent updatedPaymentIntent =
					paymentIntent.capture();
			order.setPayment_status(Constants.CAPTURED);
			order.setStatus(Constants.WOO_COMPLETED);
			orderService.saveOrder(order);
			return true;
		} catch (StripeException e) {
			order.setPayment_status(Constants.CAPTURED_FAILED);
			String errorMessage = e.getMessage();
			if(errorMessage.contains(";")) {
				errorMessage = errorMessage.substring(0, errorMessage.indexOf(";"));
			}
			order.setCapture_issue(errorMessage);
			orderService.saveOrder(order);
			return false;
		}
	}
	
}
