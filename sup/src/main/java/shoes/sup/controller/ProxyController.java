package shoes.sup.controller;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import shoes.sup.model.Proxy;
import shoes.sup.model.ProxyCheck;
import shoes.sup.service.ProxyService;
import shoes.sup.service.ProxycheckService;

@Controller
public class ProxyController {
	
	@Autowired
	ProxyService proxyService;
	
	
	@Autowired
	ProxycheckService proxyCheckService;

	
	@RequestMapping(path = "/proxy/{license}/{usedproxy}", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> proxy(@PathVariable String license,@PathVariable String usedproxy)
    {
         //Get data from service layer into entityList.
        Proxy used = proxyService.findByLicenseAndServer(license, usedproxy);
        //save round search
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        String formatted = format1.format(cal.getTime());
        try {
        	Date today = format1.parse(formatted);
			ProxyCheck proxyCheck = proxyCheckService.findByLicenseAndDate(license, today);
			if(proxyCheck != null) {
				int oldRound = proxyCheck.getRound();
				proxyCheck.setRound(oldRound+1);
				proxyCheckService.saveProxyCheck(proxyCheck);
			}else {
				ProxyCheck newProxyCheck = new ProxyCheck(license, 1, today);
				proxyCheckService.saveProxyCheck(newProxyCheck);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
        return new ResponseEntity<Object>(used, HttpStatus.OK);
    }
	
	@RequestMapping(path = "/proxy/{usedproxy}", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> proxyReqeust(@PathVariable String usedproxy)
    {
		Proxy used = proxyService.findByServer(usedproxy);
        //save round search
        return new ResponseEntity<Object>(used, HttpStatus.OK);
    }
	@RequestMapping(path = "/proxyfail/{license}", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> proxyFail(@PathVariable String license)
    {
        //save round search
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        String formatted = format1.format(cal.getTime());
        try {
        	Date today = format1.parse(formatted);
			ProxyCheck proxyCheck = proxyCheckService.findByLicenseAndDate(license, today);
			if(proxyCheck != null) {
				int oldRound = proxyCheck.getRound();
				proxyCheck.setRound(oldRound+1);
				proxyCheckService.saveProxyCheck(proxyCheck);
			}else {
				ProxyCheck newProxyCheck = new ProxyCheck(license, 1, today);
				proxyCheckService.saveProxyCheck(newProxyCheck);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
        return new ResponseEntity<Object>(null, HttpStatus.OK);
    }
	@RequestMapping(value = "/admin/uploadproxy", method = RequestMethod.POST)
	public String uploadproxy(@RequestParam("fileProxy") MultipartFile file,@RequestParam("license") String license,
			HttpServletRequest request) {
		try {
			InputStream proxyData = file.getInputStream();
			String stringProxy = IOUtils.toString(proxyData, StandardCharsets.UTF_8);
			String proxieslines[] = stringProxy.split("\\r?\\n");
			List<Proxy> existProxies = proxyService.findByLicense(license);
			if(existProxies != null && !existProxies.isEmpty()) {
				for (Proxy proxy : existProxies) {
					proxyService.deleteProxy(proxy);
				}
			}
			for (int i = 0; i < proxieslines.length; i++) {
				String singleProxy = proxieslines[i];
        		String singleProxyDetail[] = singleProxy.split(",");
        		String host = singleProxyDetail[0];
        		String port = singleProxyDetail[1];
        		String username = singleProxyDetail[2];
        		String password = singleProxyDetail[3];
				Proxy newProxy = new Proxy(license, host, Integer.valueOf(port), username, password);
				proxyService.saveProxy(newProxy);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "proxy";
	}
	
	@RequestMapping(value = "/admin/uploadproxy", method = RequestMethod.GET)
	public String proxy(Model model) {
		return "proxy";
	}
	
	@RequestMapping(value = "/admin/proxycheck", method = RequestMethod.GET)
	public String proxycheck(Model model) {
		List<ProxyCheck> proxyCheck = proxyCheckService.findAll();
		model.addAttribute("proxyCheck", proxyCheck);
		return "proxycheck";
	}
}
