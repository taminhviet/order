package shoes.sup.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.icoderman.woocommerce.ApiVersionType;
import com.icoderman.woocommerce.EndpointBaseType;
import com.icoderman.woocommerce.WooCommerce;
import com.icoderman.woocommerce.WooCommerceAPI;
import com.icoderman.woocommerce.oauth.OAuthConfig;

import shoes.sup.model.DisputeAPI;
import shoes.sup.model.IdInput;
import shoes.sup.model.Images;
import shoes.sup.model.LineItem;
import shoes.sup.model.ListDisputeAPI;
import shoes.sup.model.MetaWoo;
import shoes.sup.model.Order;
import shoes.sup.model.OrderaAPI;
import shoes.sup.model.PaypalAccount;
import shoes.sup.model.Product;
import shoes.sup.model.ProductShopifyModel;
import shoes.sup.model.ProductTMWoo;
import shoes.sup.model.ShippingAddress;
import shoes.sup.model.ShopifyImages;
import shoes.sup.model.ShopifyModel;
import shoes.sup.model.ShopifyProducts;
import shoes.sup.model.ShopifyVariants;
import shoes.sup.model.Store;
import shoes.sup.service.OrderService;
import shoes.sup.service.PaypalService;
import shoes.sup.service.ProductTMWooService;
import shoes.sup.service.StoreService;
import shoes.sup.util.Constants;
import shopping.common.model.Attribute;
import shopping.common.model.CategoriesWoo;
import shopping.common.model.ProductsWoo;

@Controller
public class StoreController {
	
	@Autowired
	StoreService storeService;
	
	@Autowired
	OrderService orderService;
	
	@Autowired
	PaypalService paypalService;
	
	@Autowired
	ProductTMWooService productTMWooService;

	@RequestMapping(value = "/admin/addstore", method = RequestMethod.POST)
	public String addStore(Model model, @RequestParam("domain") String domain,
			@RequestParam("apikey") String apikey,
			@RequestParam("password") String password,
			@RequestParam(name = "accesstoken", required = false) String accesstoken,
			@RequestParam(name = "shopifysubdomain", required = false) String shopifysubdomain,
			@RequestParam(name = "prefix", required = false) String prefix,
			@RequestParam("www") String www,
			@RequestParam("type") int type,
			@RequestParam("product") String product) {
		Store store = new Store();
		store.setDomain(domain);
		store.setApikey(apikey);
		store.setPassword(password);
		store.setAccesstoken(accesstoken);
		store.setSubdomainshopify(shopifysubdomain);
		store.setType(type);
		if(prefix != null && !prefix.isEmpty()){
			store.setPrefix(prefix);
		}
		store.setProduct(product);
		store.setState(Constants.ACTIVE);
		store.setW(www);
		storeService.saveStore(store);
		return "redirect:/admin/store";
	}

	@RequestMapping(value = "/admin/store", method = RequestMethod.GET)
	public String store(Model model) {
		List<Store> lsStore = storeService.getAllStore();
		if(!lsStore.isEmpty()){
			model.addAttribute("lsStore", lsStore);
		}
		List<PaypalAccount> lsPP = paypalService.findByActive(Constants.ACTIVE);
		if(!lsPP.isEmpty()){
			for (Iterator iterator = lsPP.iterator(); iterator.hasNext();) {
				PaypalAccount paypalAccount = (PaypalAccount) iterator.next();
				if(paypalAccount.getUsername()== null || paypalAccount.getUsername().isEmpty() || Constants.PP_LIMIT.equalsIgnoreCase(paypalAccount.getStatus())){
					iterator.remove();
				}
			}
			model.addAttribute("lsPP", lsPP);
		}
		return "store";
	}
	@RequestMapping(path = "/admin/store/{domain}", method =RequestMethod.GET)
    public String productstore(Model model,@PathVariable String domain)
    {
		Store store = storeService.getStoreByDomain(domain);
		if(store != null) {
			String domainStore = store.getDomain();
			model.addAttribute("domain", domainStore);
			List<ProductTMWoo> lsproducttm = productTMWooService.findByDomain(domainStore);
			if(!lsproducttm.isEmpty()) {
				model.addAttribute("lsproducttm", lsproducttm);
			}
		}else {
			model.addAttribute("domain", "Store not added at store manager!");
		}
		return "storeproduct";
    }
	@RequestMapping(path = "/admin/store/{domain}/{subdomain}", method =RequestMethod.GET)
    public String productstore(Model model,@PathVariable String domain,@PathVariable String subdomain)
    {
		String stagesite = domain + "/" +  subdomain;
		Store store = storeService.getStoreByDomain(stagesite);
		if(store != null) {
			String domainStore = store.getDomain();
			model.addAttribute("domain", domainStore);
			List<ProductTMWoo> lsproducttm = productTMWooService.findByDomain(domainStore);
			if(!lsproducttm.isEmpty()) {
				model.addAttribute("lsproducttm", lsproducttm);
			}
		}else {
			model.addAttribute("domain", "Store not added at store manager!");
		}
		return "storeproduct";
    }
	@RequestMapping(path = "/admin/getmainproduct", method =RequestMethod.GET)
    public String getmainproduct(Model model,@RequestParam("domain") String domain)
    {
		Store store = storeService.getStoreByDomain(domain);
		String domainStore = store.getDomain();
		int storeType = store.getType();
		if(Constants.WOO_COMMERCE == storeType) {
			getAllWooProducts(store);
		}else {
			getAllShopifyProducts(store);
		}
		List<ProductTMWoo> lsproducttm = productTMWooService.findByDomain(domainStore);
		model.addAttribute("domain", domainStore);
		if(!lsproducttm.isEmpty()) {
			model.addAttribute("lsproducttm", lsproducttm);
		}
		return "redirect:/admin/gmcdomain/"+ domainStore;
    }
	@RequestMapping(path = "/admin/getstoreproduct",params = "getproduct", method =RequestMethod.GET)
    public String getproductstore(Model model,@RequestParam("domain") String domain)
    {
		Store store = storeService.getStoreByDomain(domain);
		String domainStore = store.getDomain();
		int storeType = store.getType();
		if(Constants.WOO_COMMERCE == storeType) {
			getAllWooProducts(store);
		}else {
			getAllShopifyProducts(store);
		}
		List<ProductTMWoo> lsproducttm = productTMWooService.findByDomain(domainStore);
		model.addAttribute("domain", domainStore);
		if(!lsproducttm.isEmpty()) {
			model.addAttribute("lsproducttm", lsproducttm);
		}
		return "redirect:/admin/store/"+ domainStore;
    }
	@RequestMapping(path = "/admin/getstoreproduct",params = "deleteproduct", method =RequestMethod.GET)
    public String deleleteproductstore(Model model,@RequestParam("domain") String domain)
    {
		productTMWooService.deleteByDomain(domain);
		Store store = storeService.getStoreByDomain(domain);
		String domainStore = store.getDomain();
		return "redirect:/admin/store/"+ domainStore;
    }
	
	@RequestMapping(value = "/admin/ajaxdeletestore",method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ajaxdeletestore(@RequestBody String json){
		String result = Constants.FAILED;
		if(json.contains("\"")){
			json = json.replace("\"", "");
		}
		Gson gson = new Gson();
		IdInput idInput = gson.fromJson(json, IdInput.class);
		String id = idInput.getId();
		Store deleteStore = storeService.getStore(Integer.valueOf(id));
		storeService.deleteStore(deleteStore);
		result = Constants.SUCCESS;
		return ResponseEntity.ok(result);
	}
	
	
	@RequestMapping(value = "/admin/deletestore", method =RequestMethod.POST)
	public String deleteStore(Model model, @RequestParam("id") int id, @RequestParam(name = "cborder", required= false) String cborder){
		Store deleteStore = storeService.getStore(id);
		if(cborder!= null){
			String storedomain = deleteStore.getDomain();
			int numberPrefix = Constants.PREFIX;
			String storePrefix = deleteStore.getPrefix();
			if(storePrefix != null && !storePrefix.isEmpty()){
				numberPrefix = Integer.valueOf(storePrefix);
			}
			String storeCode = storedomain.substring(0, numberPrefix);
			List<Order> lsDeleteOrder= orderService.findAllOrderWithCode(storeCode);
			for (Order order : lsDeleteOrder) {
				orderService.deleteOrder(order);
			}
		}
		storeService.deleteStore(deleteStore);
		String ppAcc = deleteStore.getPpacc();
		if(ppAcc != null && !ppAcc.isEmpty()){
			if(ppAcc.contains(Constants.COMMA)){
				String[] ppArray = ppAcc.split(Constants.COMMA);
				if(ppArray != null && ppArray.length > 0){
					for (String pp : ppArray) {
						PaypalAccount paypalAccount = paypalService.findByUsername(pp);
						paypalAccount.setStore(null);
						paypalService.savePaypalAccount(paypalAccount);
					}
				}
			}else {
				PaypalAccount paypalAccount = paypalService.findByUsername(ppAcc);
				paypalAccount.setStore(null);
				paypalService.savePaypalAccount(paypalAccount);
			}
		}
		return "redirect:/admin/store";
	}
	
	@RequestMapping(value = "/admin/deactivestore", method = RequestMethod.POST)
	public String deactiveStore(Model model, @RequestParam("id") int id){
		Store deactive = storeService.getStore(id);
		deactive.setState(Constants.DEACTIVE);
		storeService.saveStore(deactive);
		return "redirect:/admin/store";
	}
	@RequestMapping(value = "/admin/activestore", method = RequestMethod.POST)
	public String activeStore(Model model, @RequestParam("id") int id){
		Store active = storeService.getStore(id);
		active.setState(Constants.ACTIVE);
		storeService.saveStore(active);
		return "redirect:/admin/store";
	}
	
	@RequestMapping(value = "/admin/thresholdstore", method = RequestMethod.POST)
	public String thresholdpp(Model model, @RequestParam("id") int id,@RequestParam("threshold") String threshold){
		Store store = storeService.getStore(id);
		store.setThresholdpp(threshold);
		storeService.saveStore(store);
		return "redirect:/admin/store";
	}
	
	@RequestMapping(value = "/admin/grantpp",params= "remove", method = RequestMethod.POST)
	public String removePP(Model model, @RequestParam("id") String id,
			@RequestParam("paypalaccount") String paypalaccount) {
		Store store = storeService.getStore(Integer.valueOf(id));
		if (store.getType()== Constants.WOO_COMMERCE && paypalaccount != null && !paypalaccount.isEmpty()) {
			String ppAcc = store.getPpacc();
			String commaUsername = Constants.COMMA.concat(paypalaccount);
			if(ppAcc.contains(commaUsername)){
				ppAcc = ppAcc.replace(commaUsername, Constants.EMPTY);
			}else {
				if(ppAcc.contains(paypalaccount)){
					ppAcc = ppAcc.replace(paypalaccount, Constants.EMPTY);
				}
			}
			if(ppAcc.startsWith(Constants.COMMA)){
				ppAcc = ppAcc.substring(1);
			}
			store.setPpacc(ppAcc);
			storeService.saveStore(store);
			PaypalAccount removePP = paypalService.findByUsername(paypalaccount);
			removePP.setStore(null);
			paypalService.savePaypalAccount(removePP);
		}if (store.getType()== Constants.WOO_COMMERCE && paypalaccount != null && !paypalaccount.isEmpty()) {
			String ppAcc = store.getPpacc();
			if(ppAcc != null && !ppAcc.isEmpty()) {
				store.setPpacc(null);
				storeService.saveStore(store);
			}
		}
		return "redirect:/admin/store";
	}
	
	@RequestMapping(value = "/admin/grantpp",params= "grant", method = RequestMethod.POST)
	public String grantPP(Model model, @RequestParam("id") String id,
			@RequestParam("paypalaccount") String paypalaccount,
			@RequestParam(name = "granto", required= false) String granto) {
		Store store = storeService.getStore(Integer.valueOf(id));
		if (store.getType()== Constants.WOO_COMMERCE && paypalaccount != null && !paypalaccount.isEmpty()) {
			PaypalAccount paypal = paypalService.findByUsername(paypalaccount);
			// check if assign in store
			String storeWoo = paypal.getStore();
			String accPP = store.getPpacc();
//			if(storeWoo == null || storeWoo.isEmpty()){
				paypal.setStore(store.getDomain());
				if(accPP == null || accPP.isEmpty()){
					store.setPpacc(paypal.getUsername());
					paypalService.savePaypalAccount(paypal);
				}else {
					if(!accPP.contains(paypal.getUsername())){
						//update round of new PP
						if(accPP.contains(Constants.COMMA)){
							String[] accPPArray = accPP.split(Constants.COMMA);
							for (String ppusername : accPPArray) {
								PaypalAccount existPaypalAccount = paypalService
										.findByUsername(ppusername);
								String round = existPaypalAccount.getRound();
								if(round != null){
									paypal.setRound(round);
									paypal.setSumprice(existPaypalAccount.getSumprice());
									break;
								}
							}
						}else {
							PaypalAccount existPaypalAccount = paypalService
									.findByUsername(accPP);
							String round = existPaypalAccount.getRound();
							if(round != null){
								paypal.setRound(round);
								paypal.setSumprice(existPaypalAccount.getSumprice());
							}
						}
						accPP = accPP.concat(Constants.COMMA).concat(paypal.getUsername());
						paypalService.savePaypalAccount(paypal);
					}
					store.setPpacc(accPP);
//				}
				storeService.saveStore(store);
			}
		}if (store.getType()== Constants.SHOPIFY && paypalaccount != null && !paypalaccount.isEmpty()) {
			store.setPpacc(paypalaccount);
			storeService.saveStore(store);
			PaypalAccount paypal = paypalService.findByUsername(paypalaccount);
			if(granto != null) {
				List<Order> lsOrderStore = orderService.findByStoreDomain(store.getDomain());
				for (Order order : lsOrderStore) {
					String ppEmail = paypal.getEmail();
						order.setPay_pal_account_id(ppEmail);
						orderService.saveOrder(order);
				}
			}
		}
		return "redirect:/admin/store";
	}
	@RequestMapping(value = "/admin/assignpp", method = RequestMethod.POST)
	public String assignPP(Model model, @RequestParam("id") String id,
			@RequestParam("paypalaccount") String paypalaccount) {
		Store store = storeService.getStore(Integer.valueOf(id));
		if (store.getType()== Constants.WOO_COMMERCE && paypalaccount != null && !paypalaccount.isEmpty()) {
			PaypalAccount paypal = paypalService.findByUsername(paypalaccount);
			// check if assign in store
			String accPP = store.getPpacc();
			if (accPP != null && accPP.contains(paypal.getUsername())) {
				boolean isAssign = changePPWoo(store, paypal);
			}
			store.setPpacc(paypalaccount);
			storeService.saveStore(store);
		}
		return "redirect:/admin/store";
	}
	
	
	@RequestMapping(value = "/admin/getstore", method = RequestMethod.GET)
	public String getstore(Model model) {
		List<Store> lsStore = storeService.getAllStore();
		List<Order> listOrder = new ArrayList<Order>();
		try {
			for (Store store : lsStore) {
				StringBuilder apiUrl = new StringBuilder();
				apiUrl.append("https://");
				apiUrl.append(store.getDomain());
				apiUrl.append("/admin/api/2019-04/orders.json");
				String url = apiUrl.toString();
				CredentialsProvider provider = new BasicCredentialsProvider();
				UsernamePasswordCredentials credentials
				= new UsernamePasswordCredentials(store.getApikey(), store.getPassword());
				provider.setCredentials(AuthScope.ANY, credentials);
				HttpClient client = HttpClientBuilder.create()
						.setDefaultCredentialsProvider(provider)
						.build();
				
				HttpResponse response = client.execute(
						new HttpGet(url));
				if (response.getStatusLine().getStatusCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : "
					   + response.getStatusLine().getStatusCode());
				}

				BufferedReader br = new BufferedReader(
		                         new InputStreamReader((response.getEntity().getContent())));

				String output;
				String orderList = null;
				while ((output = br.readLine()) != null) {
					orderList = output;
				}
				orderList = orderList.substring(10, orderList.length()-1);
				client.getConnectionManager().shutdown();
				Gson gson=new Gson();
				Type collectionType = new TypeToken<Collection<OrderaAPI>>(){}.getType();
				List<OrderaAPI> lsOrder = gson.fromJson(orderList, collectionType);
				int i = 1;
				for (OrderaAPI orderaAPI : lsOrder) {
					Order order = new Order();
					String oname = orderaAPI.getName();
					if(oname.contains("#")){
						oname = oname.replace("#", "");
					}
					order.setOname(oname);
					order.setStoredomain(store.getDomain());
					//set shipping address
					if(orderaAPI.getShipping_address() != null){
						ShippingAddress shippingAddress = orderaAPI.getShipping_address();
						StringBuilder sb = new StringBuilder();
						sb.append("Name: ");
						sb.append(shippingAddress.getName());
						order.setAddressname("Name: "+shippingAddress.getName());
						sb.append(", Address 1: ");
						sb.append(shippingAddress.getAddress1());
						order.setAddress1("Address 1: "+shippingAddress.getAddress1());
						sb.append(", Address 2: ");
						sb.append(shippingAddress.getAddress2());
						order.setAddress2("Address 2: "+shippingAddress.getAddress2());
						sb.append(", City: ");
						sb.append(shippingAddress.getCity());
						order.setCity("City: " + shippingAddress.getCity());
						sb.append(", Zip: ");
						sb.append(shippingAddress.getZip());
						order.setZip("Zip: "+ shippingAddress.getZip());
						sb.append(", Province:");
						sb.append(shippingAddress.getProvince());
						order.setProvince("Province: " + shippingAddress.getProvince());
						sb.append(", Country: ");
						sb.append(shippingAddress.getCountry());
						order.setCountry("Country: " + shippingAddress.getCountry());
						order.setAddress(sb.toString());
					}
					if(!orderaAPI.getLine_items().isEmpty()){
						LineItem lineItem = orderaAPI.getLine_items().get(0);
						String product_id = lineItem.getProduct_id();
						String product_name = lineItem.getName();
						order.setProductname(product_name);
						long quantity = lineItem.getQuantity();
						order.setQuantity(String.valueOf(quantity));
						String variant = lineItem.getVariant_title();
						order.setVariant(variant);
						String productUrl = "https://" + store.getDomain() + "/admin/api/2019-04/products/" + product_id + ".json";
						HttpClient client1 = HttpClientBuilder.create()
								  .setDefaultCredentialsProvider(provider)
								  .build();
						HttpResponse response1 = client1.execute(
								  new HttpGet(productUrl));
						if (response1.getStatusLine().getStatusCode() != 200) {
							throw new RuntimeException("Failed : HTTP error code : "
							   + response1.getStatusLine().getStatusCode());
						}

						BufferedReader br1 = new BufferedReader(
				                         new InputStreamReader((response1.getEntity().getContent())));

						String output1;
						String productlist = null;
						while ((output1 = br1.readLine()) != null) {
							productlist = output1;
						}
						client1.getConnectionManager().shutdown();
						productlist = productlist.substring(11, productlist.length()-1);
						Type collectionType1 = new TypeToken<Product>(){}.getType();
						Product product = gson.fromJson(productlist, collectionType1);
	 					if(product != null){
							String style = product.getProduct_type();
							order.setStyle(style);
							Images image = product.getImages().get(0);
							String imageUrl = image.getSrc();
							order.setImageurl(imageUrl);
						}
					}
					listOrder.add(order);
					i++;
				}
				Collections.reverse(listOrder);
				for (Order order : listOrder) {
					order.setCreateDate(new Date());
					order.setStatus(Constants.NEW_ORDER);
					orderService.saveOrder(order);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/admin";
	}

	private boolean changePPWoo(Store store, PaypalAccount paypalAccount){
		try {
			StringBuilder sb = new StringBuilder();
			String www = store.getW();
			if(www == null) {
				www = Constants.EMPTY;
			}
			if(www.equalsIgnoreCase(Constants.NO_WWW) || www.equalsIgnoreCase(Constants.FALSE_WWW)) {
				sb.append("https://");
			}else {
				sb.append("https://www.");
			}
			sb.append(store.getDomain());
			sb.append("/wp-json/custom-plugin/paypal_update?key=80943f212d5a4182a45fa86c02cc622d&api_username=");
			sb.append(paypalAccount.getUsername());
			sb.append("&api_password=");
			sb.append(paypalAccount.getPassword());
			sb.append("&api_signature=");
			sb.append(paypalAccount.getSignature());
			String url =sb.toString();
			HttpClient client = HttpClientBuilder.create().build();
			HttpGet httpGet = new HttpGet(url);
			HttpResponse response = client.execute(httpGet);
			int statusCode = response.getStatusLine().getStatusCode();
			if(HttpURLConnection.HTTP_CREATED == statusCode || HttpURLConnection.HTTP_OK == statusCode){
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	private void getAllShopifyProducts(Store store) {
		Gson gson  =  new Gson();
		StringBuilder apiURL = new StringBuilder();
		apiURL.append("https://");
		apiURL.append(store.getDomain());
		apiURL.append("/admin/api/");
		apiURL.append(Constants.SHOPIFY_API_VERSION);
		apiURL.append("/products.json?limit=250");
		try {
			String accesstoken = store.getAccesstoken();
			if(!StringUtils.isEmpty(accesstoken)) {
				HttpClient client = HttpClientBuilder.create().build();
				HttpGet httpGet =  new HttpGet(apiURL.toString());
				httpGet.setHeader(Constants.CONTENT_TYPE,
						Constants.APPLICATION_JSON);
				httpGet.setHeader(Constants.X_Shopify_Access_Token, accesstoken);
				HttpResponse response = client.execute(httpGet);
				int statusCode = response.getStatusLine().getStatusCode();
				if(HttpURLConnection.HTTP_OK == statusCode) {
					BufferedReader br = new BufferedReader(
							new InputStreamReader((response.getEntity().getContent())));

					String output;
					String listProduct = null;
					while ((output = br.readLine()) != null) {
						listProduct = output;
					}
					Type collectionTypeProduct = new TypeToken<ProductShopifyModel>() {
					}.getType();
					ProductShopifyModel psmodel = gson.fromJson(listProduct, collectionTypeProduct);
					if(psmodel != null) {
						List<ShopifyModel> lsProducts = psmodel.getProducts();
						if(!lsProducts.isEmpty()) {
							for (ShopifyModel shopifyModel : lsProducts) {
								BigInteger id = shopifyModel.getId();
								ProductTMWoo checkExist = productTMWooService.findByDomainAndProductid(store.getDomain(), String.valueOf(id));
								List<ProductTMWoo> checkExistparent = productTMWooService.findByDomainAndParentid(store.getDomain(), String.valueOf(id));
								if(checkExist == null && checkExistparent.isEmpty()) {
									List<ShopifyVariants> lsVariant = shopifyModel.getVariants();
									if(lsVariant != null && !lsVariant.isEmpty()) {
										int totalVariant = lsVariant.size();
										// only 1 variant
										if(totalVariant == 1) {
											ShopifyVariants variant = lsVariant.get(0);
											String barcode = variant.getBarcode();
											String permalink = "https://" + store.getDomain() + "/products/" + shopifyModel.getHandle();
											ProductTMWoo productTM = new ProductTMWoo(shopifyModel.getHandle(),store.getDomain(), String.valueOf(id), shopifyModel.getProduct_type(), shopifyModel.getTitle(), shopifyModel.getBody_html(), variant.getPrice(), Constants.EMPTY, String.valueOf(id), shopifyModel.getImages().get(0).getSrc(),variant.getOption2(), Constants.EMPTY,barcode, permalink);
											productTMWooService.saveProductTM(productTM);
										}else {
											//many variant
											int countvariant = 0;
											List<ShopifyImages> lsShopifyImage = shopifyModel.getImages();
											Map<Integer, String> mapImageVariant = new HashMap<>(); 
											for (ShopifyImages shopifyImg : lsShopifyImage) {
												mapImageVariant.put(shopifyImg.getPosition(), shopifyImg.getSrc());
											}
											for (ShopifyVariants variant : lsVariant) {
												ProductTMWoo checkExistVariant = productTMWooService.findByDomainAndProductid(store.getDomain(), String.valueOf(variant.getId()));
												if(checkExistVariant == null) {
													String barcode = variant.getBarcode();
													String permalink = "https://" + store.getDomain() + "/products/" + shopifyModel.getHandle();
													if(countvariant == 0) {
														ProductTMWoo productTM = new ProductTMWoo(shopifyModel.getHandle(),store.getDomain(), String.valueOf(variant.getId()), shopifyModel.getProduct_type(), shopifyModel.getTitle(), shopifyModel.getBody_html(), variant.getPrice(), variant.getOption1(), String.valueOf(variant.getProduct_id()), mapImageVariant.get(variant.getPosition()),variant.getOption2(),Constants.EMPTY, barcode, permalink);
														productTMWooService.saveProductTM(productTM);
													}else {
														
														ProductTMWoo productTM = new ProductTMWoo(shopifyModel.getHandle(),store.getDomain(), String.valueOf(variant.getId()), shopifyModel.getProduct_type(), shopifyModel.getTitle(), Constants.EMPTY, variant.getPrice(), variant.getOption1(), String.valueOf(variant.getProduct_id()), mapImageVariant.get(variant.getPosition()) != null ? mapImageVariant.get(variant.getPosition()) : Constants.EMPTY,variant.getOption2(),Constants.EMPTY, barcode, permalink);
														productTMWooService.saveProductTM(productTM);
													}
													countvariant++;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void getAllWooProducts(Store store) {
		Gson gson  =  new Gson();
		StringBuilder apiUrl = new StringBuilder();
		String www = store.getW();
		if(www == null) {
			www = Constants.EMPTY;
		}
		if(www.equalsIgnoreCase(Constants.NO_WWW) || www.equalsIgnoreCase(Constants.FALSE_WWW)) {
			apiUrl.append("https://");
		}else {
			apiUrl.append("https://www.");
		}
		apiUrl.append(store.getDomain());
		String url = apiUrl.toString();
		OAuthConfig config = new OAuthConfig(url,store.getApikey().trim(), store.getPassword().trim());
		WooCommerce wooCommerce = new WooCommerceAPI(config, ApiVersionType.V2);
		Map<String, String> params = new HashMap<>();
		params.put("per_page","100");
		params.put("offset","0");
		List products = wooCommerce.getAll(
				EndpointBaseType.PRODUCTS.getValue(), params);
		for(Object product: products) {
			String json = gson.toJson(product);
			ProductsWoo productWoo = gson.fromJson(json, ProductsWoo.class);
			int id = productWoo.getId();
			ProductTMWoo checkExist = productTMWooService.findByDomainAndProductid(store.getDomain(), String.valueOf(id));
			List<ProductTMWoo> checkExistparent = productTMWooService.findByDomainAndParentid(store.getDomain(), String.valueOf(id));
			if(checkExist == null && checkExistparent.isEmpty()) {
				String typeProduct = productWoo.getType();
				List<CategoriesWoo> woocat = productWoo.getCategories();
				String categoryTM = Constants.EMPTY;
				if(!woocat.isEmpty()) {
					categoryTM = woocat.get(0).getName();
				}
				if(Constants.SIMPLE.equals(typeProduct)) {
					String gtin = getGtin(productWoo);
					ProductTMWoo productTM = new ProductTMWoo(productWoo.getSlug(),store.getDomain(), String.valueOf(productWoo.getId()), categoryTM, productWoo.getName(), productWoo.getDescription(), productWoo.getPrice(), Constants.EMPTY, String.valueOf(productWoo.getId()), productWoo.getImages().get(0).getSrc(),Constants.WHITE_COLOR, productWoo.getWeight(),gtin, productWoo.getPermalink());
					productTMWooService.saveProductTM(productTM);
				}else if (Constants.VARIABLE.equals(typeProduct)) {
					String[] variants = productWoo.getVariations();
					int countvariant = 0;
					for (String va : variants) {
						ProductsWoo variant = getWooVariant(id, va, store);
						Attribute first = variant.getAttributes().get(0);
						String color = Constants.WHITE_COLOR;
						List<MetaWoo> lsMeta = variant.getMeta_data();
						for (MetaWoo meta : lsMeta) {
							if(meta.getKey().equals("color") || meta.getKey().equals("Color")) {
								color = meta.getValue().toString().trim();
								break;
							}
						}
						ProductTMWoo checkExistVariant = productTMWooService.findByDomainAndProductid(store.getDomain(), String.valueOf(variant.getId()));
						if(checkExistVariant == null) {
							String gtin = getGtin(variant);
							if(countvariant == 0) {
								ProductTMWoo productTM = new ProductTMWoo(productWoo.getSlug(),store.getDomain(), String.valueOf(variant.getId()), categoryTM, productWoo.getName(), productWoo.getDescription(), variant.getPrice(), first.getOption(), String.valueOf(productWoo.getId()), productWoo.getImages().get(0).getSrc(),color,productWoo.getWeight(), gtin, variant.getPermalink());
								productTMWooService.saveProductTM(productTM);
							}else {
								ProductTMWoo productTM = new ProductTMWoo(productWoo.getSlug(),store.getDomain(), String.valueOf(variant.getId()), categoryTM, productWoo.getName(), Constants.EMPTY, variant.getPrice(), first.getOption(), String.valueOf(productWoo.getId()), productWoo.getImages().get(0).getSrc(),color,productWoo.getWeight(), gtin, variant.getPermalink());
								productTMWooService.saveProductTM(productTM);
							}
							countvariant++;
						}
					}
				}
			}
		}
	}
	
	private ProductsWoo getWooVariant(int productID, String variantID, Store store) {
		Gson gson  = new Gson();
		StringBuilder sb = new StringBuilder();
		sb.append("https://");
		sb.append(store.getDomain());
		sb.append("/wp-json/wc/v3/products/");
		sb.append(productID);
		sb.append("/variations/");
		sb.append(variantID);
		sb.append("?consumer_key=");
		sb.append(store.getApikey().trim());
		sb.append("&consumer_secret=");
		sb.append(store.getPassword().trim());
		String urlProduct = sb.toString();
		String productDetail = getWooInfo(urlProduct);
		ProductsWoo product= gson.fromJson(productDetail, ProductsWoo.class);
		return product;
	}
	
	private String getGtin(ProductsWoo productWoo) {
		String gtin = Constants.EMPTY;
		List<MetaWoo> lsMetaWoo = productWoo.getMeta_data();
		for (MetaWoo meta : lsMetaWoo) {
			if(meta.getKey().equals("gtin")) {
				gtin = meta.getValue().toString().trim();
				break;
			}
		}
		if(Constants.EMPTY.equals(gtin)) {
			gtin = productWoo.getWeight();
		}
		return gtin;
	}
	
	 private  String getWooInfo(String urlWoo) {
			String responseString = Constants.EMPTY;
			try {
				URL url = new URL(urlWoo);
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.addRequestProperty("User-Agent", 
						"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
				conn.setRequestMethod("GET");

				BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String output;

				StringBuffer response = new StringBuffer();
				while ((output = in.readLine()) != null) {
					response.append(output);
				}

				in.close();
				// printing result from response
				responseString = response.toString();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return responseString;
		}
}
