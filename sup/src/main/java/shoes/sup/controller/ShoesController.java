package shoes.sup.controller;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.tomcat.util.bcel.classfile.Constant;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import shoes.sup.model.AdidasModel;
import shoes.sup.model.ColumbiaVariant;
import shoes.sup.model.Export;
import shoes.sup.util.Constants;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;

@Controller
public class ShoesController {

	public static final String portraitId = "portraitId";
	public static final String EMPTY = "";
	public static final String SPACE = " ";
	public static final String QUANTITY = "21";
	public static final String TRUE = "TRUE";
	public static final String pageName = "AdidasUltra.csv";
	// Delimiter used in CSV file
	
	@RequestMapping(value = "/shoes", method = RequestMethod.GET)
	public String home(Model model) {
		List<String> lsBrand = new ArrayList<String>();
		lsBrand.add("Adidas");
		lsBrand.add("Nike");
//		lsBrand.add("Kors");
		lsBrand.add("Walmart");
//		lsBrand.add("Crestado");
		lsBrand.add("Columbia");
		model.addAttribute("lsBrand", lsBrand);
		return "getdata";
	}
//	@RequestMapping(value = "/", method = RequestMethod.GET)
//	public String index(Model model) {
//		List<String> lsBrand = new ArrayList<String>();
//		lsBrand.add("Adidas");
//		lsBrand.add("Nike");
//		lsBrand.add("Kors");
//		lsBrand.add("Walmart");
//		model.addAttribute("lsBrand", lsBrand);
//		return "getdata";
//	}
	
	@RequestMapping(value = "/data/", method = RequestMethod.GET)
	public String data(Model model) {
		List<String> lsBrand = new ArrayList<String>();
		lsBrand.add("Adidas");
		lsBrand.add("Nike");
		lsBrand.add("Kors");
		model.addAttribute("lsBrand", lsBrand);
		return "getdata";
	}


	@RequestMapping(value = "/getdata", method = RequestMethod.POST)
	public ResponseEntity<InputStreamResource> getData(Model model,
			@RequestParam(name = "brand") String brand,
			@RequestParam(name = "link") String link,
			@RequestParam(name = "category", required = false) String category,
			@RequestParam(name = "percent", required = false) int percent) throws IOException {
		Set<String> linkList = new HashSet<String>();
		if (!link.contains(",")) {
			linkList.add(link);
		} else {
			String[] url = link.split(",");
			for (String string : url) {
				linkList.add(string);
			}
		}
		List<Export> lsExportAll = new ArrayList<Export>();
		for (String url : linkList) {
			List<Export> lsExport = new ArrayList<Export>();
			if(brand.equals("Adidas")){
				lsExport = getSpecificProductAdidas(url);
			}else if (brand.equals("Nike")) {
				lsExport = getSpecificProductNike(url);
			}else if (brand.equals("Kors")) {
				lsExport = getSpecificProductKors(url);
			}else if (brand.equals("Walmart")) {
				lsExport = getSpecificProductWalmart(url);
			}else if (brand.equals("Crestado")){
				lsExport = addExportCrestado(url, category);
			}else if (brand.equals("Columbia")) {
				lsExport = getSpecificProductColumbia(url, percent);
			}
			if(lsExport != null && !lsExport.isEmpty()) {
				lsExportAll.addAll(lsExport);
			}
		}
//		System.out.println(link);
		if(!lsExportAll.isEmpty()){
			ByteArrayInputStream in;
			if(!brand.equals("Walmart")){
				in = producttoExcel(lsExportAll, brand);
			}else {
				in = producttoExcelWalmart(lsExportAll, brand);
			}
			HttpHeaders headers = new HttpHeaders();
	        headers.add("Content-Disposition", "attachment; filename=Product.xlsx");
	        return ResponseEntity
	                .ok()
	                .headers(headers)
	                .body(new InputStreamResource(in));
		}
		return null;
	}
	
	private List<Export> getSpecificProductColumbia(String url, int percent){
		try {
			List<Export> lsExport = new ArrayList<Export>();
			Set<String> setDetail = new HashSet<String>();
			Document doc = Jsoup.connect(url).userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:66.0) Gecko/20100101 Firefox/66.0").data("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8").data("Accept-Language","en-US,en;q=0.5").data("Accept-Encoding", "gzip, deflate").timeout(20000).get();
			Elements srcLinkDetail = doc.getElementsByClass("product-tile__view-details");
			int i = 0;
			for (Element element : srcLinkDetail) {
				Elements detailLink = element.getElementsByTag("a");
				for (Element link : detailLink) {
					String href = "https://www.columbia.com" + link.attr("href");
					setDetail.add(href);
					i++;
					break;
				}
				if(i==1) {
					break;
				}
			}
			Set<String> colorString = new HashSet<String>();
			for (String detail : setDetail) {
				Document docDetail = Jsoup.connect(detail).userAgent("Mozilla").timeout(20000).get();
				Elements eCorlor = docDetail.getElementsByTag("a");
				for (Element element : eCorlor) {
					String href = element.attr("href");
					if(href.contains("color") && !href.contains("size")) {
						System.out.println("href: " + href);
						colorString.add(href);
					}
				}
			}
			for(String colorLink : colorString) {
				List<String> ImageSet = new ArrayList<String>();
				System.out.println("colorLink: " +  colorLink);
				Document docColor = Jsoup.connect(colorLink).userAgent("Mozilla").timeout(50000).get();
				Elements eImage = docColor.getElementsByTag("img");
				for (Element element : eImage) {
					String href = element.attr("src");
					if(href.contains("https")) {
						if(!href.contains(".jpg") && !(href.contains(".png"))) {
							href = href.concat(".jpg");
						}
						ImageSet.add(href);
					}
				}
				List<Export> exportColor = addExportProductColumbia(ImageSet, docColor,percent, colorLink);
				lsExport.addAll(exportColor);
			}
			return lsExport;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	private List<Export> getSpecificProductWalmart(String url){
		System.setProperty("java.net.preferIPv4Stack" , "true");
		List<Export> lsExport = new ArrayList<Export>();
		try {
			Document doc = Jsoup.connect(url).userAgent("Mozilla").timeout(5000).get();
			Set<String> sLink = new HashSet<String>();
			List<String> lsRaw = new ArrayList<String>();
//			Elements links = doc.select("a[href]");
//			for (Element link : links) {
//				String url1 = link.attr("href");
//				if(!url1.contains("walmart") && url1.startsWith("/") && Character.isDigit(url1.charAt(url1.length()-1)) && url1.length() > 30) {
//					sLink.add("https://www.walmart.com".concat(url1));
//				}
//			}
			String htmlData = doc.html();
			String guess = "productPageUrl";
			int index = htmlData.indexOf(guess);
			while (index >= 0) {
				lsRaw.add(htmlData.substring(index+17, index + 199));
			    index = htmlData.indexOf(guess, index + 1);
			}
			for (String raw : lsRaw) {
				int commaIndex = raw.indexOf("\"");
				if(commaIndex >= 0){
					sLink.add("https://www.walmart.com".concat(raw.substring(0, commaIndex)));
				}
//				System.out.println(raw);
			}
			for (String urlW : sLink) {
				List<Export> exports = getWalmart(urlW);
				lsExport.addAll(exports);
//				break;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return lsExport;
	}
	
	private List<Export> getSpecificProductKors(String url) {
		System.setProperty("java.net.preferIPv4Stack" , "true");
		List<Export> lsExport = new ArrayList<Export>();
		try {
			Document doc = Jsoup.connect(url).userAgent("Mozilla").timeout(5000).get();
			Elements links = doc.select("a[href]");
			Set<String> setLinkProduct = new HashSet<String>();
			for (Element link : links) {
				String url1 = link.attr("href");
				if(url1.contains("R-US")){
					setLinkProduct.add(url1);
				}
			}
			for (String urlKor : setLinkProduct) {
				List<Export> exports = getKors(urlKor);
				lsExport.addAll(exports);
//				break;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return lsExport;
	}
	
	private List<Export> getSpecificProductNike(String url) {
		List<Export> lsExport = new ArrayList<Export>();
		try {
			System.out.println(url);
			InputStream input = new URL(url).openStream();
			String source = getURLSource(input);
			Document doc = Jsoup.connect(url).userAgent("Mozilla").timeout(20000).get();
 //			boolean hasColorway = checkColorWay(doc);
//			if (!hasColorway) {
//				getProductOneColor(doc, source);
//			}else {
			lsExport = getProductMultiColor(doc, source);
//			}
//			List<String> listImageId = getListImageId(source);
//			for (String imageid : listImageId) {
//				System.out.println(imageid);
//			}
//			System.out.println(source);
		} catch (Exception e) {
			System.out.println("error connect: " + e.getMessage());
		}
		return lsExport;
	}
	private List<Export> getWalmart(String url){
		List<Export> lsExport = new ArrayList<Export>();
		try {
			System.out.println(url);
			Document doc = Jsoup.connect(url).userAgent("Mozilla").timeout(5000).get();

			String htmlData = doc.html();
			String guess = "https://i5.walmartimages.com/asr/";
			int index = htmlData.indexOf(guess);
			Set<String> linkImgs = new HashSet<String>();
			while (index >= 0) {
				linkImgs.add(htmlData.substring(index, index + 104).concat(".jpeg?odnHeight=450&odnWidth=450&odnBg=FFFFFF"));
			    index = htmlData.indexOf(guess, index + 1);
			}
			if(!linkImgs.isEmpty()){
				for (String string : linkImgs) {
					System.out.println(string);
				}
			}
			int indexUPC = htmlData.indexOf("wupc");
			String sUPC = EMPTY;
			if(indexUPC >= 0) {
				sUPC = htmlData.substring(indexUPC+7, indexUPC +20);
			}else {
				Elements elementsGtin = doc.getElementsByAttributeValue("itemprop", "gtin13");
				for (Element element : elementsGtin) {
					sUPC = element.attr("content");
					break;
				}
			}
			lsExport = addExportProductWalmart(linkImgs, doc, url, sUPC);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return lsExport;
	}
	
	private List<Export> getKors(String url){
		List<Export> lsExport = new ArrayList<Export>();
		try {
			Document doc = Jsoup.connect(url).userAgent("Mozilla").timeout(5000).get();
			Elements srcImg = doc.getElementsByTag("img");
			List<String> linkImgs = new ArrayList<String>();
			for (Element element : srcImg) {
				String linkimage = element.attr("src");
			}
			Elements e = doc.getElementsByAttribute("content");
			String content = null;
			String urlImage = null;
			for (Element element : e) {
				content  = element.attr("content");
				if(content.contains("/image")){
					if(!url.contains("color")){
						urlImage = content.substring(0, content.length()-2);
					}else {
						urlImage = content.substring(0, content.length()-2);
						int minus = urlImage.lastIndexOf("-");
						int equal = url.lastIndexOf("=");
						urlImage = urlImage.substring(0,minus+1);
						String colorCode = url.substring(equal+1, url.length());
						urlImage = urlImage.concat(colorCode).concat("_");
					}
					break;
				}
			}
			if (urlImage != null){
				String url1 = urlImage.concat("1");
				if(checkExistImage(url1)){
					linkImgs.add(url1);
				}
				String url2 = urlImage.concat("2");
				if(checkExistImage(url2)){
					linkImgs.add(url2);
				}
				String url3 = urlImage.concat("3");
				if(checkExistImage(url3)){
					linkImgs.add(url3);
				}
				String url4 = urlImage.concat("4");
				if(checkExistImage(url4)){
					linkImgs.add(url4);
				}
				String url5 = urlImage.concat("5");
				if(checkExistImage(url5)){
					linkImgs.add(url5);
				}
				
				String url6 = urlImage.concat("6");
				if(checkExistImage(url6)){
					linkImgs.add(url6);
				}
			}
			
			lsExport = addExportProductKors(linkImgs, doc, url);
//			Elements e1 = doc.getElementsByClass("gallery-images-item");
//			for (Element element : e1) {
//				title = element.attr("src");
//				break;
//			}
//			System.out.println(title);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return lsExport;
	}
	
	
	
	private List<Export> getSpecificProductAdidas(String url){
		try {
			Document doc = Jsoup.connect(url).userAgent("Mozilla").timeout(20000).get();
			Elements srcImg = doc.getElementsByTag("script");
			Gson gson = new Gson();
			List<String> linkImgs = new ArrayList<String>();
			for (Element element : srcImg) {
				String data = element.data();
				if(data.contains("@type")) {
					data = data.replace("@", "");
					AdidasModel adidasModel = gson.fromJson(data, AdidasModel.class);
					String[] images = adidasModel.getImage();
					for (int i = 0; i < images.length; i++) {
						String link = images[i];
						if(link.contains("standard")) {
							linkImgs.add(link);
						}
					}
					return addExportProductAdidas(linkImgs, doc, url,adidasModel);
				}
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new ArrayList<Export>();
	}
	
	private List<Export> addExportProductWalmart(Set<String> imageURLS, Document doc, String url, String UPC){
		List<Export> lsExport =new ArrayList<Export>();
		int i = 1;
		int k = 0;
		for (String imgURL : imageURLS) {
			Export export = new Export();
			String productName = EMPTY;
			Elements elements = doc.getElementsByClass("prod-ProductTitle");
			for (Element element : elements) {
				productName = element.text();
				break;
			}
			
			String price = EMPTY;
			Elements elementsPrice = doc.getElementsByAttributeValue("itemprop", "price");
			for (Element element : elementsPrice) {
				price = element.attr("content");
				break; 
			}
			String vendor = EMPTY;
			Elements elementVendor = doc.getElementsByAttributeValue("itemprop", "brand");
			for (Element element : elementVendor) {
				vendor = element.text();
				break;
			}
			String body = EMPTY;
			Elements elementBody = doc.getElementsByClass("prod-ProductHighlights-description");
			for (Element element : elementBody) {
				body = element.html();
				break;
			}
			String handle = productName.toLowerCase().replace(" ", "-");
			export.setStore(EMPTY);
			String imageName = handle.concat("_").concat(String.valueOf(i)).concat(".jpg");
			String imagePath = "/images/".concat(imageName);
			if(k == 0) {
				export.setWebsite("base");
				export.setAttSet("Default");
				export.setType_simple("simple");
				export.setCategoryId("Toys");
				export.set_root_category("Default Category");
				export.setHasOption("1");
				export.setTitle(productName);
				export.setImage(imagePath);
				export.setSmall_image(imagePath);
				export.setThumbnail(imagePath);
				export.setUrl_key(handle);
				export.setUrl_path(handle.concat(".html"));
				export.setPage_layout("No layout updates");
				export.setOptions_container("Product Info Column");
				export.setMsrp_enabled("Use config");
				export.setMsrp_display_actual_price_type("Use config");
				export.setGift_message_available("0");
				export.setPrice(price);
				export.setSpecial_price(price);
				export.setWeight("1");
				export.setRequired_options("0");
				export.setManufacturer(vendor);
				export.setMin_sale_qty("Enabled");
				export.setStatus("No");
				export.setIs_recurring("Catalog");
				export.setVisibility("4");
				export.setTax_class_id("None");
				export.setDescription(body.replace("\"", "\"\""));
				export.setQty("10");
				export.setMin_qty("0");
				export.setUse_config_min_qty("1");
				export.setIs_qty_decimal("0");
				export.setBackorders("0");
				export.setUse_config_backorders("1");
				export.setMin_sale_qty("1");
				export.setUse_config_min_sale_qty("1");
				export.setMax_sale_qty("0");
				export.setUse_config_max_sale_qty("1");
				export.setIs_in_stock("0");
				export.setUse_config_notify_stock_qty("1");
				export.setManage_stock("0");
				export.setUse_config_manage_stock("0");
				export.setStock_status_changed_auto("1");
				export.setUse_config_qty_increments("1");
				export.setQty_increments("0");
				export.setUse_config_enable_qty_inc("1");
				export.setEnable_qty_increments("0");
				export.setIs_decimal_divided("0");
				export.setStock_status_changed_automatically("1");
				export.setUse_config_enable_qty_increments("1");
				export.setProduct_name(productName);
				export.setStore_id("0");
			}else {
				export.setWebsite(EMPTY);
				export.setAttSet(EMPTY);
				export.setType_simple(EMPTY);
				export.setCategoryId(EMPTY);
				export.set_root_category(EMPTY);
				export.setRequired_options(EMPTY);
				export.setHasOption(EMPTY);
				export.setTitle(EMPTY);
				export.setImage(EMPTY);
				export.setSmall_image(EMPTY);
				export.setThumbnail(EMPTY);
				export.setUrl_key(EMPTY);
				export.setUrl_path(EMPTY);
				export.setPage_layout(EMPTY);
				export.setOptions_container(EMPTY);
				export.setMsrp_enabled(EMPTY);
				export.setMsrp_display_actual_price_type(EMPTY);
				export.setGift_message_available(EMPTY);
				export.setPrice(EMPTY);
				export.setSpecial_price(EMPTY);
				export.setWeight(EMPTY);
				export.setManufacturer(EMPTY);
				export.setMin_sale_qty(EMPTY);
				export.setStatus(EMPTY);
				export.setIs_recurring(EMPTY);
				export.setVisibility(EMPTY);
				export.setTax_class_id(EMPTY);
				export.setDescription(EMPTY);
				export.setQty(EMPTY);
				export.setMin_qty(EMPTY);
				export.setUse_config_min_qty(EMPTY);
				export.setIs_qty_decimal(EMPTY);
				export.setBackorders(EMPTY);
				export.setUse_config_backorders(EMPTY);
				export.setMin_sale_qty(EMPTY);
				export.setUse_config_min_sale_qty(EMPTY);
				export.setMax_sale_qty(EMPTY);
				export.setUse_config_max_sale_qty(EMPTY);
				export.setIs_in_stock(EMPTY);
				export.setUse_config_notify_stock_qty(EMPTY);
				export.setManage_stock(EMPTY);
				export.setUse_config_manage_stock(EMPTY);
				export.setStock_status_changed_auto(EMPTY);
				export.setUse_config_qty_increments(EMPTY);
				export.setQty_increments(EMPTY);
				export.setUse_config_enable_qty_inc(EMPTY);
				export.setEnable_qty_increments(EMPTY);
				export.setIs_decimal_divided(EMPTY);
				export.setStock_status_changed_automatically(EMPTY);
				export.setUse_config_enable_qty_increments(EMPTY);
				export.setProduct_name(EMPTY);
				export.setStore_id(EMPTY);
			}
			export.setVariantBarcode(UPC);
			export.setMetatitle(EMPTY);
			export.setMeta_description(EMPTY);
			export.setCustom_design(EMPTY);
			export.setImage_label(EMPTY);
			export.setSmall_image_label(EMPTY);
			export.setThumbnail_label(EMPTY);
			export.setCountry_of_manufacture(EMPTY);
			export.setGoogleshopping_category(EMPTY);
			export.setMsrp(EMPTY);
			export.setGoogleshopping_exclude(EMPTY);
			export.setGoogleshopping_condition(EMPTY);
			export.setShort_description(EMPTY);
			export.setMeta_keyword(EMPTY);
			export.setCustom_layout_update(EMPTY);
			export.setSpecial_from_date(EMPTY);
			export.setSpecial_to_date(EMPTY);
			export.setNews_from_date(EMPTY);
			export.setNews_to_date(EMPTY);
			export.setCustom_design_from(EMPTY);
			export.setCustom_design_to(EMPTY);
			export.setLow_stock_date(EMPTY);
			export.setNotify_stock_qty(EMPTY);
			export.setProduct_type_id("simple");
			export.setProduct_status_changed(EMPTY);
			export.setProduct_changed_websites(EMPTY);
			export.setMedia_attribute_id("90");
			export.set_media_image(imagePath);
			export.set_media_lable(EMPTY);
			export.set_media_position(String.valueOf(i));
			export.set_media_is_disabled("0");
			lsExport.add(export);
			i++;
			k++;
			downloadFile(imgURL, imageName);
		}
		return lsExport;
	}
	
	private void downloadFile(String urlShopify, String name){
		try {
			String folderPath = "D:\\Source\\shoes\\";
			URL url = new URL(urlShopify);
			InputStream in = new BufferedInputStream(url.openStream());
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			byte[] buf = new byte[1024];
			int n = 0;
			while (-1!=(n=in.read(buf)))
			{
				out.write(buf, 0, n);
			}
			out.close();
			in.close();
			byte[] response = out.toByteArray();
			FileOutputStream fos = new FileOutputStream(folderPath+"\\images\\" + name);
			fos.write(response);
			fos.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	private List<Export> addExportProductKors(List<String> ImageURLs, Document doc, String url) {
		System.out.println(url);
		List<Export> lsExport =new ArrayList<Export>();
		int i = 0;
		int k = 0;
		String data = doc.data().trim();
		int indexColor = data.indexOf("colors\":");
		int indexPrice = data.indexOf("highListPrice\":");
		String colorJson = data.substring(indexColor -1, indexColor + 200);
		String priceJson = data.substring(indexPrice -2, indexPrice + 200);
		int index1 = colorJson.indexOf("}");
		int index2 = priceJson.indexOf("}");
		colorJson = colorJson.substring(10, index1+1);
		priceJson = priceJson.substring(0, index2+1);
		Gson gson = new Gson();
		JsonObject oColor = gson.fromJson(colorJson, JsonObject.class);
		JsonElement jsonElement = oColor.get("displayName");
		String finalColor = jsonElement.getAsString();
		if(finalColor.contains("\"")){
			finalColor = finalColor.replace("\"", "");
		}
		JsonObject oPrice = gson.fromJson(priceJson, JsonObject.class);
		JsonElement jsonElement1 = oPrice.get("highListPrice");
		String finalPrice = jsonElement1.getAsString();
		if(finalPrice.contains("\"")){
			finalPrice = finalPrice.replace("\"", "");
		}
		for (String imageurl : ImageURLs) {
			Export export = new Export();
			String productName = EMPTY;
			Elements elements = doc.getElementsByAttributeValue("itemprop", "name");
			for (Element element : elements) {
				productName = element.text();
				break;
			}
			String handle = EMPTY;
			if(!productName.isEmpty()){
				handle = productName.toLowerCase().trim().replace(" ", "-");
			}
			String color = EMPTY;
			Elements eColor = doc.getElementsByClass("selected-color");
			for (Element element : eColor) {
				color = element.text();
				break;
			}
			String price = EMPTY;
			Elements ePrice = doc.getElementsByClass("Price");
			for (Element element2 : ePrice) {
				price = element2.text();
				break;
			}
			
			String design = EMPTY;
			Elements eDesgn = doc.getElementsByClass("design-section");
			for (Element element3 : eDesgn) {
				design = element3.html();
				break;
			}
			design = doc.select("meta[name=description]").get(0)
		              .attr("content");
			String detail = EMPTY;
			Elements eDetail = doc.getElementsByClass("detail-section");
			for (Element element4 : eDetail) {
				detail = element4.html();
				break;
			}
			String body = EMPTY;
			if(!design.isEmpty()){
				body = design.concat(detail).trim().replace("\"", "\"\"");
			}
			export.setHandle(handle);
			if(k == 0) {
				export.setTitle(productName);
				export.setBody("\"".concat(body).concat("\""));
				export.setOption1name("Color");
				export.setOption1value(finalColor);
				export.setVendor("Micheal Kors");
				export.setType("");
				export.setPublished(TRUE);
				export.setVariantSKU(EMPTY);
				export.setVariantTracker("shopify");
				export.setQuantity(QUANTITY);
				export.setVariantPolicy("deny");
				export.setVariantFulfillment("manual");
				export.setPrice(finalPrice);
				export.setCompare(finalPrice);
				export.setRequireShipping(TRUE);
				export.setVariantTax(TRUE);
				export.setVariantBarcode(EMPTY);
				export.setWeightUnit("lb");
			}else {
				export.setTitle(EMPTY);
				export.setBody(EMPTY);
				export.setOption1name(EMPTY);
				export.setOption1value(EMPTY);
				export.setVendor(EMPTY);
				export.setType(EMPTY);
				export.setPublished(EMPTY);
				export.setVariantSKU(EMPTY);
				export.setVariantTracker(EMPTY);
				export.setQuantity(EMPTY);
				export.setVariantPolicy(EMPTY);
				export.setVariantFulfillment(EMPTY);
				export.setPrice(EMPTY);
				export.setCompare(EMPTY);
				export.setRequireShipping(EMPTY);
				export.setVariantTax(EMPTY);
				export.setVariantBarcode(EMPTY);
				export.setWeightUnit(EMPTY);
			}
			k++;
//			export.setOption1value(finalColor);
			
			export.setImageURL(imageurl);
			
			export.setImagePosition(String.valueOf(++i));
			
			lsExport.add(export);
		}
		return lsExport;
	}
	
	private List<Export> addExportCrestado(String url, String category) throws IOException{
		List<Export> lsExport = new ArrayList<Export>();
		List<String> lsVariant = new ArrayList<String>(); 
		Document doc = Jsoup.connect(url).userAgent("Mozilla/5.0 (X11; Fedora; Lin� Gecko/20100101 Firefox/54.0").timeout(5000).get();
		Elements eSelect = doc.getElementsByTag("select");
		for (Element element2 : eSelect) {
			if(element2.attr("id").equalsIgnoreCase("productSelect")){
				Elements eOption =  element2.getElementsByTag("option");
				for (Element element : eOption) {
					String value = element.attr("value");
					String variant = element.text().substring(0, element.text().indexOf("-")).trim();
					lsVariant.add(value.concat(",").concat(variant));
				}
			}
		}
		int indexslash = url.lastIndexOf("/");
		String handle = url.substring(indexslash+1, url.length());
		Elements edes = doc.getElementsByClass("panel-body");
		String des = Constants.EMPTY;
		for (Element element : edes) {
			des = element.html().replace("\"", "\"\"");
		}
		int k = 0;
		String title = Constants.EMPTY;
		Elements pi = doc.getElementsByClass("product-images");
		StringBuilder wooImage = new StringBuilder();
		for (Element element : pi) {
			Elements eimg = element.getElementsByTag("img");
			for (Element element2 : eimg) {
				wooImage.append("https:" + element2.attr("src"));
				wooImage.append(";");
				title = element2.attr("alt");
			}
		}
		String option1Name = Constants.EMPTY;
		Elements eButton = doc.getElementsByClass("single-product-button-group");
		for (Element element : eButton) {
			Elements efor = element.getElementsByAttributeValue("for", "productSelect-option-0");
			for (Element element2 : efor) {
				option1Name = element2.text();
			}
		}
		String price =Constants.EMPTY;
		Elements eprice = doc.getElementsByClass("engoj_price_main");
		for (Element element : eprice) {
			Elements emoney = element.getElementsByClass("money");
			price = emoney.text().replace("$", "");
		}
		for (String variant : lsVariant) {
			String[] arrayV = variant.split(",");
			String value = arrayV[0];
			String option1Value = arrayV[1];
			Export export = new Export();
			export.setHandle(handle);
			if(k == 0){
				export.setBody(des);
				export.setTitle(title);
				export.setImageURL(wooImage.toString());
			}else {
				export.setBody(Constants.EMPTY);
				export.setTitle(Constants.EMPTY);
				export.setImageURL(Constants.EMPTY);
			}
			export.setVendor("Crestado");
			export.setType(category);
			export.setPublished(TRUE);
			export.setOption1name(option1Name);
			export.setOption1value(option1Value);
			export.setVariantSKU(value);
			export.setVariantTracker("shopify");
			export.setQuantity(QUANTITY);
			export.setVariantPolicy("deny");
			export.setVariantFulfillment("manual");
			export.setPrice(price);
			export.setCompare(Constants.EMPTY);
			export.setRequireShipping(TRUE);
			export.setVariantTax(TRUE);
			export.setVariantBarcode(value);
			export.setImagePosition(Constants.EMPTY);
			export.setWeightUnit("lb");
			lsExport.add(export);
			k++;
		}
		return lsExport;
	}
	
	private List<Export> addExportProductAdidas(List<String> ImageURLs, Document doc, String url, AdidasModel adidasModel) {
		List<Export> lsExport = new ArrayList<Export>();
		int i = 0;
		int k = 0;
		double j = 6;
		for (String imageUrl : ImageURLs) {
//			System.out.println(imageUrl);
			Export export = new Export();
			Elements elementTitle = doc.getElementsByAttributeValue("data-auto-id", "product-title");
			String title = null;
			for (Element element : elementTitle) {
				title = element.text();
				break;
			}
//			String subTitle = null;
//			Elements elementSubTitle = doc.getElementsByAttributeValueStarting("class", "subtitle");
//			for (Element element : elementSubTitle) {
//				subTitle = element.text();
//				break;
//			}
			String price =  String.valueOf(adidasModel.getOffers().getPrice());
			int indexHTML = url.lastIndexOf(".html");
			int indexSlash = url.lastIndexOf("/");
			String styleCode = url.substring(indexSlash+1, indexHTML);
			String handle = title.concat("-").concat(styleCode).replace(SPACE, "-").toLowerCase();
			handle = handle.replaceAll("[^a-zA-Z0-9-]","");
			export.setHandle(handle);
			String des = adidasModel.getDescription();
//			des = des.replace("\"", "\"\"");
//			String desBullet = getHTMLDesBulletAdidas(doc);
//			desBullet = desBullet.replace("\"", "\"\"");
//			des = des.concat(desBullet);
//			int colorIndex = des.indexOf("color: ");
//			int lastIndexSpace = des.lastIndexOf("/ ");
			String color = adidasModel.getColor();
//			if(colorIndex != -1 && lastIndexSpace != -1){
//				color = des.substring(colorIndex+ 6, lastIndexSpace);
//			}
			title = title.concat(SPACE).concat(SPACE)
					.concat(styleCode);
			if(k == 0) {
				export.setTitle(title);
				export.setBody("\"".concat(des).concat("\""));
			}else {
				export.setTitle(EMPTY);
				export.setBody(EMPTY);
			}
			k++;
			export.setVendor("Adidas");
			export.setType(adidasModel.getCategory());
			export.setPublished(TRUE);
			export.setOption1name("Size");
			String valueSize = "US " + j;
			j = j + 0.5;
			if(valueSize.contains(".0")) {
				valueSize = valueSize.replace(".0", "");
			}
			export.setOption1value(valueSize);
			export.setVariantSKU(EMPTY);
			export.setVariantTracker("shopify");
			export.setQuantity(QUANTITY);
			export.setVariantPolicy("deny");
			export.setVariantFulfillment("manual");
			export.setPrice(price);
			export.setCompare(price);
			export.setRequireShipping(TRUE);
			export.setVariantTax(TRUE);
			export.setVariantBarcode(EMPTY);
			export.setImageURL(imageUrl);
			
			export.setImagePosition(String.valueOf(++i));
			export.setWeightUnit("lb");
			
			lsExport.add(export);
		}
		if(ImageURLs.size() < 11) {
			int noImage = 11 - ImageURLs.size();
			for (int l = 0; l < noImage; l++) {
				Export export = new Export();
				Elements elementTitle = doc.getElementsByAttributeValue("data-auto-id", "product-title");
				String title = null;
				for (Element element : elementTitle) {
					title = element.text();
					break;
				}
//				String subTitle = null;
//				Elements elementSubTitle = doc.getElementsByAttributeValueStarting("class", "subtitle");
//				for (Element element : elementSubTitle) {
//					subTitle = element.text();
//					break;
//				}
				String price =  String.valueOf(adidasModel.getOffers().getPrice());
//				Elements elementPrice = doc.getElementsByClass("gl-price__value");
//				for (Element element : elementPrice) {
//					price = element.text();
//					break;
//				}
				int indexHTML = url.lastIndexOf(".html");
				int indexSlash = url.lastIndexOf("/");
				String styleCode = url.substring(indexSlash+1, indexHTML);
				if(title == null){
					title = styleCode;
				}
				if(title == null){
					int length = 10;
				    boolean useLetters = true;
				    boolean useNumbers = false;
				    title = RandomStringUtils.random(length, useLetters, useNumbers);
				}
				String handle = title.concat("-").concat(styleCode).toLowerCase();
				if(handle.contains(SPACE)){
					handle = handle.replace(SPACE, "-");
				}
//				if(subTitle == null){
//					int length = 10;
//				    boolean useLetters = true;
//				    boolean useNumbers = false;
//				    subTitle = RandomStringUtils.random(length, useLetters, useNumbers);
//				}
				title = title.concat(SPACE).concat(styleCode);
				handle = handle.replaceAll("[^a-zA-Z0-9-]","");
				export.setHandle(handle);
				String des = adidasModel.getDescription();
//				des = des.replace("\"", "\"\"");
//				String desBullet = getHTMLDesBulletAdidas(doc);
//				desBullet = desBullet.replace("\"", "\"\"");
//				des = des.concat(desBullet);
				if(k == 0) {
					export.setTitle(title);
					export.setBody("\"".concat(des).concat("\""));
				}else {
					export.setTitle(EMPTY);
					export.setBody(EMPTY);
				}
				k++;
				export.setVendor("Adidas");
				export.setType(adidasModel.getCategory());
				export.setPublished(TRUE);
				export.setOption1name("Size");
				String valueSize = "US " + j;
				j = j + 0.5;
				if(valueSize.contains(".0")) {
					valueSize = valueSize.replace(".0", "");
				}
				export.setOption1value(valueSize);
				export.setVariantSKU(EMPTY);
				export.setVariantTracker("shopify");
				export.setQuantity(QUANTITY);
				export.setVariantPolicy("deny");
				export.setVariantFulfillment("manual");
				export.setPrice(price);
				export.setCompare(price);
				export.setRequireShipping(TRUE);
				export.setVariantTax(TRUE);
				export.setVariantBarcode(EMPTY);
				export.setImageURL(EMPTY);
				export.setImagePosition(EMPTY);
				export.setWeightUnit("lb");
				
				lsExport.add(export);
			}
		}
		return lsExport;
	}
	
	private ByteArrayInputStream producttoExcelWalmart(List<Export> lsExport, String brand) throws IOException {
		String[] COLUMNs = {"sku","_store","_attribute_set","_type","_category","_root_category", "_product_websites","description","gift_message_available", "has_options", "image","msrp_display_actual_price_type","msrp_enabled","name",
				"options_container","price","required_options","short_description","small_image","special_from_date","special_price",
				"status","tax_class_id","thumbnail","updated_at","url_key",
				"url_path","visibility","weight","qty","min_qty","use_config_min_qty","is_qty_decimal","backorders","use_config_backorders",			
				"min_sale_qty", "use_config_min_sale_qty","max_sale_qty","use_config_max_sale_qty","is_in_stock","notify_stock_qty","use_config_notify_stock_qty",
				"manage_stock","use_config_manage_stock","stock_status_changed_auto","use_config_qty_increments","qty_increments","use_config_enable_qty_inc","enable_qty_increments",
				"is_decimal_divided","_media_attribute_id","_media_image","_media_lable","_media_position","_media_is_disabled"};
		try {
			Workbook workbook = new XSSFWorkbook();
	        ByteArrayOutputStream out = new ByteArrayOutputStream();
	        CreationHelper createHelper = workbook.getCreationHelper();
	        
	        Sheet sheet = workbook.createSheet(brand);
	        Font headerFont = workbook.createFont();
	        headerFont.setBold(true);
	        headerFont.setColor(IndexedColors.BLACK.getIndex());
	        CellStyle headerCellStyle = workbook.createCellStyle();
	        headerCellStyle.setFont(headerFont);
	        // Row for Header
	        Row headerRow = sheet.createRow(0);
	        // Header
	        for (int col = 0; col < COLUMNs.length; col++) {
	          Cell cell = headerRow.createCell(col);
	          cell.setCellValue(COLUMNs[col]);
	          cell.setCellStyle(headerCellStyle);
	        }
	        
	        
	        int rowIdx = 1;
	        for (Export export : lsExport) {
	          Row row = sheet.createRow(rowIdx++);
	          if(!export.getVariantBarcode().isEmpty()){
	        	  row.createCell(0).setCellValue(export.getVariantBarcode());
	          }else {
	        	  row.createCell(0).setCellValue(export.getVariantBarcode());
	          }
	          if(!export.getStore().isEmpty()){
	        	  row.createCell(1).setCellValue(export.getStore());
	          }else {
	        	  row.createCell(1).setCellValue(export.getStore());
	          }
	          if(!export.getAttSet().isEmpty()){
	        	  row.createCell(2).setCellValue(export.getAttSet());
	          }else {
	        	  row.createCell(2).setCellValue(export.getAttSet());
	          }
	          if(!export.getType_simple().isEmpty()){
	        	  row.createCell(3).setCellValue(export.getCategoryId());
	          }else {
	        	  row.createCell(3).setCellValue(export.getCategoryId());
	          }
	          if(export.getCategoryId().isEmpty()){
	        	  row.createCell(4).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(4).setCellValue(export.getCategoryId());
	          }
	          if(export.get_root_category().isEmpty()){
	        	  row.createCell(5).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(5).setCellValue(export.get_root_category());
	          }
	          if(export.getWebsite().isEmpty()){
	        	  row.createCell(6).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(6).setCellValue(export.getWebsite());
	          }
	          if(export.getDescription().isEmpty()){
	        	  row.createCell(7).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(7).setCellValue(export.getDescription());
	          }
	          if(export.getGift_message_available().isEmpty()){
	        	  row.createCell(8).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(8).setCellValue(export.getGift_message_available());
	          }
	          if(export.getHasOption().isEmpty()){
	        	  row.createCell(9).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(9).setCellValue(export.getHasOption());
	          }
	          if(export.getImage().isEmpty()){
	        	  row.createCell(10).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(10).setCellValue(export.getImage());
	          }
	          if(export.getMsrp_display_actual_price_type().isEmpty()){
	        	  row.createCell(11).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(11).setCellValue(export.getMsrp_display_actual_price_type());
	          }
	          if(export.getMsrp_enabled().isEmpty()){
	        	  row.createCell(12).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(12).setCellValue(export.getMsrp_enabled());
	          }
	          if(export.getTitle().isEmpty()){
	        	  row.createCell(13).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(13).setCellValue(export.getTitle());
	          }
	          if(export.getOptions_container().isEmpty()){
	        	  row.createCell(14).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(14).setCellValue(export.getOptions_container());
	          }
	          if(export.getPrice().isEmpty()){
	        	  row.createCell(15).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(15).setCellValue(export.getPrice());
	          }
	          if(export.getRequired_options().isEmpty()){
	        	  row.createCell(16).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(16).setCellValue(export.getRequired_options());
	          }
	          if(export.getShort_description().isEmpty()){
	        	  row.createCell(17).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(17).setCellValue(export.getShort_description());
	          }
	          if(export.getSmall_image().isEmpty()){
	        	  row.createCell(18).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(18).setCellValue(export.getSmall_image());
	          }
	          if(export.getSpecial_from_date().isEmpty()){
	        	  row.createCell(19).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(19).setCellValue(export.getSpecial_from_date());
	          }
	          if(export.getSpecial_price().isEmpty()){
	        	  row.createCell(20).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(20).setCellValue(export.getSpecial_price());
	          }
	          if(export.getStatus().isEmpty()){
	        	  row.createCell(21).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(21).setCellValue(export.getStatus());
	          }
	          if(export.getTax_class_id().isEmpty()){
	        	  row.createCell(22).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(22).setCellValue(export.getTax_class_id());
	          }
	          if(export.getThumbnail().isEmpty()){
	        	  row.createCell(23).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(23).setCellValue(export.getThumbnail());
	          }
	          if(export.getSpecial_from_date().isEmpty()){
	        	  row.createCell(24).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(24).setCellValue(export.getSpecial_from_date());
	          }
	          if(export.getUrl_key().isEmpty()){
	        	  row.createCell(25).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(25).setCellValue(export.getUrl_key());
	          }
	          if(export.getUrl_path().isEmpty()){
	        	  row.createCell(26).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(26).setCellValue(export.getUrl_path());
	          }
	          if(export.getVisibility().isEmpty()){
	        	  row.createCell(27).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(27).setCellValue(export.getVisibility());
	          }
	          if(export.getWeight().isEmpty()){
	        	  row.createCell(28).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(28).setCellValue(export.getWeight());
	          }
	          if(export.getQuantity().isEmpty()){
	        	  row.createCell(29).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(29).setCellValue(export.getQuantity());
	          }
	          if(export.getMin_qty().isEmpty()){
	        	  row.createCell(30).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(30).setCellValue(export.getMin_qty());
	          }
	          if(export.getUse_config_min_qty().isEmpty()){
	        	  row.createCell(31).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(31).setCellValue(export.getUse_config_min_qty());
	          }
	          if(export.getIs_qty_decimal().isEmpty()){
	        	  row.createCell(32).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(32).setCellValue(export.getIs_qty_decimal());
	          }
	          if(export.getBackorders().isEmpty()){
	        	  row.createCell(33).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(33).setCellValue(export.getBackorders());
	          }
	          if(export.getUse_config_backorders().isEmpty()){
	        	  row.createCell(34).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(34).setCellValue(export.getUse_config_backorders());
	          }
	          if(export.getMin_sale_qty().isEmpty()){
	        	  row.createCell(33).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(35).setCellValue(export.getMin_sale_qty());
	          }
	          if(export.getMin_sale_qty().isEmpty()){
	        	  row.createCell(36).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(36).setCellValue(export.getMin_sale_qty());
	          }
	          if(export.getMax_sale_qty().isEmpty()){
	        	  row.createCell(37).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(37).setCellValue(export.getMax_sale_qty());
	          }
	          if(export.getMax_sale_qty().isEmpty()){
	        	  row.createCell(38).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(38).setCellValue(export.getMax_sale_qty());
	          }
	          if(export.getIs_in_stock().isEmpty()){
	        	  row.createCell(39).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(39).setCellValue(export.getIs_in_stock());
	          }
	          if(export.getNotify_stock_qty().isEmpty()){
	        	  row.createCell(40).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(40).setCellValue(export.getNotify_stock_qty());
	          }
	          if(export.getUse_config_notify_stock_qty().isEmpty()){
	        	  row.createCell(41).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(41).setCellValue(export.getUse_config_notify_stock_qty());
	          }
	          if(export.getManage_stock().isEmpty()){
	        	  row.createCell(42).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(42).setCellValue(export.getManage_stock());
	          }
	          if(export.getUse_config_manage_stock().isEmpty()){
	        	  row.createCell(43).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(43).setCellValue(export.getUse_config_manage_stock());
	          }
	          if(export.getStock_status_changed_auto().isEmpty()){
	        	  row.createCell(44).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(44).setCellValue(export.getStock_status_changed_auto());
	          }
	          if(export.getUse_config_qty_increments().isEmpty()){
	        	  row.createCell(45).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(45).setCellValue(export.getUse_config_qty_increments());
	          }
	          if(export.getQty_increments().isEmpty()){
	        	  row.createCell(46).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(46).setCellValue(export.getQty_increments());
	          }
	          if(export.getUse_config_enable_qty_inc().isEmpty()){
	        	  row.createCell(47).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(47).setCellValue(export.getUse_config_enable_qty_inc());
	          }
	          if(export.getEnable_qty_increments().isEmpty()){
	        	  row.createCell(48).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(48).setCellValue(export.getEnable_qty_increments());
	          }
	          if(export.getIs_decimal_divided().isEmpty()){
	        	  row.createCell(49).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(49).setCellValue(export.getIs_decimal_divided());
	          }
	          if(export.getMedia_attribute_id().isEmpty()){
	        	  row.createCell(50).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(50).setCellValue(export.getMedia_attribute_id());
	          }
	          if(export.get_media_image().isEmpty()){
	        	  row.createCell(51).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(51).setCellValue(export.get_media_image());
	          }
	          if(export.get_media_lable().isEmpty()){
	        	  row.createCell(52).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(52).setCellValue(export.get_media_lable());
	          }
	          if(export.get_media_position().isEmpty()){
	        	  row.createCell(53).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(53).setCellValue(export.get_media_position());
	          }
	          if(export.get_media_is_disabled().isEmpty()){
	        	  row.createCell(54).setCellValue(EMPTY);
	          }else {
	        	  row.createCell(54).setCellValue(export.get_media_is_disabled());
	          }
	        }
	        workbook.write(out);
	        return new ByteArrayInputStream(out.toByteArray());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private ByteArrayInputStream producttoExcel(List<Export> lsExport, String brand) throws IOException {
		String[] COLUMNs = {"Handle","Title", "Body (HTML)","Vendor",  "Type", "Published", "Option1 Name", "Option1 Value","Variant SKU",
				"Variant Inventory Tracker","Variant Inventory Qty","Variant Inventory Policy","Variant Fulfillment Service","Variant Price",
				"Variant Compare At Price","Variant Requires Shipping","Variant Taxable","Variant Barcode","Image Src","Image Position","Variant Weight Unit","Woocommerce"};
		try {
			Workbook workbook = new XSSFWorkbook();
	        ByteArrayOutputStream out = new ByteArrayOutputStream();
	        CreationHelper createHelper = workbook.getCreationHelper();
	        
	        Sheet sheet = workbook.createSheet(brand);
	        Font headerFont = workbook.createFont();
	        headerFont.setBold(true);
	        headerFont.setColor(IndexedColors.BLACK.getIndex());
	        CellStyle headerCellStyle = workbook.createCellStyle();
	        headerCellStyle.setFont(headerFont);
	        // Row for Header
	        Row headerRow = sheet.createRow(0);
	        // Header
	        for (int col = 0; col < COLUMNs.length; col++) {
	          Cell cell = headerRow.createCell(col);
	          cell.setCellValue(COLUMNs[col]);
	          cell.setCellStyle(headerCellStyle);
	        }
	        
	        
	        int rowIdx = 1;
	        for (Export export : lsExport) {
	          Row row = sheet.createRow(rowIdx++);
	          row.createCell(0).setCellValue(export.getHandle());
	          row.createCell(1).setCellValue(export.getTitle());
	          if(export.getBody().length() > 0){
	        	  String bodyHTMl = export.getBody().substring(1,export.getBody().length()-1);
	        	  row.createCell(2).setCellValue(bodyHTMl);
	          }else {
	        	  row.createCell(2).setCellValue(export.getBody());
	          }
	          row.createCell(3).setCellValue(export.getVendor());
	          row.createCell(4).setCellValue(export.getType());
	          row.createCell(5).setCellValue(export.getPublished());
	          row.createCell(6).setCellValue(export.getOption1name());
	          row.createCell(7).setCellValue(export.getOption1value());
	          row.createCell(8).setCellValue(export.getVariantSKU());
	          row.createCell(9).setCellValue(export.getVariantTracker());
	          row.createCell(10).setCellValue(export.getQuantity());
	          row.createCell(11).setCellValue(export.getVariantPolicy());
	          row.createCell(12).setCellValue(export.getVariantFulfillment());
	          row.createCell(13).setCellValue(export.getPrice());
	          row.createCell(14).setCellValue(export.getCompare());
	          row.createCell(15).setCellValue(export.isRequireShipping());
	          row.createCell(16).setCellValue(export.isVariantTax());
	          row.createCell(17).setCellValue(export.getVariantBarcode());
	          row.createCell(18).setCellValue(export.getImageURL());
	          row.createCell(19).setCellValue(export.getImagePosition());
	          row.createCell(20).setCellValue(export.getWeightUnit());
	          row.createCell(21).setCellValue(export.getWoocommerce());
	        }
	        workbook.write(out);
	        return new ByteArrayInputStream(out.toByteArray());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private List<Export> getProductMultiColor(Document doc, String source) {
		Set<String> setLinkIMage = new HashSet<String>();
		List<Export> lsExport = new ArrayList<Export>();
		String firstLink = getFirstLinkImage(doc);
		if(!setLinkIMage.contains(firstLink)) {
			setLinkIMage.add(firstLink);
			List<String> ImageURLs = getAllLinkImageMulti(firstLink, source);
			lsExport = addExportProduct(ImageURLs, doc);
		}else {
			System.out.println("exist image url");
		}
		return lsExport;
		
	}
	
	private List<Export> addExportProduct(List<String> ImageURLs, Document doc) {
		List<Export> lsExport = new ArrayList<Export>();
		int i = 0;
		int k = 0;
		double j = 6;
		
		StringBuilder sb = new StringBuilder();
		for (int it = 0; it < ImageURLs.size(); it++) {
			if(it == 0) {
				sb.append(ImageURLs.get(it));
			}else {
				sb.append(";");
				sb.append(ImageURLs.get(it));
			}
			
		}
		
		for (String imageUrl : ImageURLs) {
			Export export = new Export();
			Element elementTitle = doc.getElementById("pdp_product_title");
			String title = elementTitle.text();
			String subTitle = EMPTY;

			Elements elementSubTitle = doc.getElementsByAttributeValue("data-test", "product-sub-title");
			for (Element element : elementSubTitle) {
				subTitle = element.text();
				break;
			}
			Elements elementPrice = doc.getElementsByAttributeValue("data-test", "product-price");
			String price = EMPTY;
			for (Element element : elementPrice) {
				price = element.text();
				break;
			}
			Elements estyleCode = doc.getElementsByClass("description-preview__style-color");
			String styleCode = EMPTY;
			for (Element element : estyleCode) {
				String fullStyleText = element.text();
				int lastIndex = fullStyleText.lastIndexOf(":");
				styleCode = (fullStyleText.substring(lastIndex + 1, fullStyleText.length()).trim());
			}
			String handle = title.concat("-").concat(styleCode).replace(SPACE, "-").toLowerCase();
			title = title.concat(SPACE).concat(subTitle).concat(SPACE).concat(styleCode);
			String des = getHTMLDesNike(doc);
			des = des.replace("\"", "\"\"");
			handle = handle.replaceAll("[^a-zA-Z0-9-]","");
			export.setHandle(handle);
			if(k == 0) {
				export.setTitle(title);
				export.setBody("\"".concat(des).concat("\""));
				export.setWoocommerce(sb.toString());
			}else {
				export.setTitle(EMPTY);
				export.setBody(EMPTY);
			}
			k++;
			
			export.setVendor("Nike");
			export.setType(subTitle);
			export.setPublished(TRUE);
			export.setOption1name("Size");
			String valueSize = "US " + j;
			j = j + 0.5;
			if(valueSize.contains(".0")) {
				valueSize = valueSize.replace(".0", "");
			}
			export.setOption1value(valueSize);
			export.setVariantSKU(EMPTY);
			export.setVariantTracker("shopify");
			export.setQuantity(QUANTITY);
			export.setVariantPolicy("deny");
			export.setVariantFulfillment("manual");
			export.setPrice(price);
			export.setCompare(price);
			export.setRequireShipping(TRUE);
			export.setVariantTax(TRUE);
			export.setVariantBarcode(EMPTY);
			export.setImageURL(imageUrl);
			
			export.setImagePosition(String.valueOf(++i));
			export.setWeightUnit("lb");
			
			lsExport.add(export);
		}
		if(ImageURLs.size() < 11) {
			int noImage = 11 - ImageURLs.size();
			for (int l = 0; l < noImage; l++) {
				Export export = new Export();
				Element elementTitle = doc.getElementById("pdp_product_title");
				String title = elementTitle.text();
				String subTitle = EMPTY;

				Elements elementSubTitle = doc.getElementsByAttributeValue("data-test", "product-sub-title");
				for (Element element : elementSubTitle) {
					subTitle = element.text();
					break;
				}
				Elements elementPrice = doc.getElementsByAttributeValue("data-test", "product-price");
				String price = EMPTY;
				for (Element element : elementPrice) {
					price = element.text();
					break;
				}
				Elements estyleCode = doc.getElementsByClass("description-preview__style-color");
				String styleCode = EMPTY;
				for (Element element : estyleCode) {
					String fullStyleText = element.text();
					int lastIndex = fullStyleText.lastIndexOf(":");
					styleCode = (fullStyleText.substring(lastIndex + 1, fullStyleText.length()).trim());
				}
				String handle = title.concat("-").concat(styleCode).replace(SPACE, "-").toLowerCase();
				title = title.concat(SPACE).concat(subTitle).concat(SPACE).concat(styleCode);
				String des = getHTMLDesNike(doc);
				des = des.replace("\"", "\"\"");
				handle = handle.replaceAll("[^a-zA-Z0-9-]","");
				export.setHandle(handle);
				if(k == 0) {
					export.setTitle(title);
					export.setBody("\"".concat(des).concat("\""));
					export.setWoocommerce(sb.toString());
				}else {
					export.setTitle(EMPTY);
					export.setBody(EMPTY);
				}
				k++;
				
				export.setVendor("Nike");
				export.setType(subTitle);
				export.setPublished(TRUE);
				export.setOption1name("Size");
				String valueSize = "US " + j;
				j = j + 0.5;
				if(valueSize.contains(".0")) {
					valueSize = valueSize.replace(".0", "");
				}
				export.setOption1value(valueSize);
				export.setVariantSKU(EMPTY);
				export.setVariantTracker("shopify");
				export.setQuantity(QUANTITY);
				export.setVariantPolicy("deny");
				export.setVariantFulfillment("manual");
				export.setPrice(price);
				export.setCompare(price);
				export.setRequireShipping(TRUE);
				export.setVariantTax(TRUE);
				export.setVariantBarcode(EMPTY);
				export.setImageURL(EMPTY);
				export.setImagePosition(EMPTY);
				export.setWeightUnit("lb");
				lsExport.add(export);
				
			}
		}
		return lsExport;
	}
	
	private List<Export> addExportProductColumbia(List<String> ImageURLs, Document doc, int percent, String colorLink) {
		Map<String, String> mapGtin = new HashedMap<String, String>();
		if(colorLink.contains("color=")) {
			if(colorLink.contains("dimension=")) {
				int firstIndex = colorLink.indexOf("&");
				int lastIndex = colorLink.lastIndexOf("&");
				int colorIndex = colorLink.indexOf("color=");
				int dimensionIndex = colorLink.indexOf("dimension=");
				String colorNo = colorLink.substring(colorIndex+6, firstIndex);
				String dimenNo = colorLink.substring(dimensionIndex+10, lastIndex);
				int colorNumber = Integer.valueOf(colorNo);
				int dimenNumber = Integer.valueOf(dimenNo);
				Elements scriptElements = doc.getElementsByTag("script");
				for (Element element : scriptElements) {
					if (element.data().contains("variantData")) {
						// find the line which contains 'infosite.token = <...>;'
						Pattern pattern = Pattern.compile(".*variantData = ([^;]*);");
						java.util.regex.Matcher matcher = pattern.matcher(element.data());
						// we only expect a single match here so there's no need to loop through the matcher's groups
						if (matcher.find()) {
							String variantData= matcher.group(1);
							Gson gson = new Gson();
							LinkedTreeMap<String, Object> treecolumbia = gson.fromJson(variantData, LinkedTreeMap.class);
							treecolumbia.entrySet().stream().forEach(e -> {
								String jsonString = e.getValue().toString();
								ColumbiaVariant cv = gson.fromJson(jsonString, ColumbiaVariant.class);
								int color = cv.getColor();
								int dimension = cv.getDimension();
								if(color == colorNumber && dimension == dimenNumber) {
									mapGtin.put(String.valueOf(cv.getSize()), e.getKey());
								}
							});
							
						} else {
							System.err.println("No match found!");
						}
						break;
					}
				}
			}else {
				int firstIndex = colorLink.indexOf("&");
				int colorIndex = colorLink.indexOf("color=");
				String colorNo = colorLink.substring(colorIndex+6, firstIndex);
				int colorNumber = Integer.valueOf(colorNo);
				Elements scriptElements = doc.getElementsByTag("script");
				for (Element element : scriptElements) {
					if (element.data().contains("variantData")) {
						// find the line which contains 'infosite.token = <...>;'
						Pattern pattern = Pattern.compile(".*variantData = ([^;]*);");
						java.util.regex.Matcher matcher = pattern.matcher(element.data());
						// we only expect a single match here so there's no need to loop through the matcher's groups
						if (matcher.find()) {
							String variantData= matcher.group(1);
							Gson gson = new Gson();
							LinkedTreeMap<String, Object> treecolumbia = gson.fromJson(variantData, LinkedTreeMap.class);
							treecolumbia.entrySet().stream().forEach(e -> {
								String jsonString = e.getValue().toString();
								ColumbiaVariant cv = gson.fromJson(jsonString, ColumbiaVariant.class);
								int color = cv.getColor();
								if(color == colorNumber) {
									mapGtin.put(String.valueOf(cv.getSize()), e.getKey());
								}
							});
							
						} else {
							System.err.println("No match found!");
						}
						break;
					}
				}
			}
				
		}
		List<Export> lsExport = new ArrayList<Export>();
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < ImageURLs.size(); i++) {
			if(i == 0) {
				sb.append(ImageURLs.get(i));
			}else {
				sb.append(";");
				sb.append(ImageURLs.get(i));
			}
			
		}
		String productname = "";
		String description = "";
		String category = "";
		String price = "";
		String id = "";
		Elements eName = doc.getElementsByClass("product-name");
		for (Element element : eName) {
			productname = element.html();
		}
		Elements eDes = doc.getElementsByClass("product__accordion__section__details");
		for (Element element : eDes) {
			description = element.html();
		}
		Elements eBreadCum = doc.getElementsByClass("breadcrumb-item");
		Element eLast = eBreadCum.last();
		Elements elastLink = eLast.getElementsByTag("a");
		for (Element element : elastLink) {
			category = element.html();
		}
		Elements ePrice = doc.getElementsByClass("price");
		for (Element element : ePrice) {
			Elements eValue = element.getElementsByClass("value");
			for (Element element2 : eValue) {
				price = element2.attr("content");
			}
			break;
		}
		if(price.equals("")) {
			for (Element element : ePrice) {
				Elements eDiscount = element.getElementsByClass("discounted");
				for (Element element2 : eDiscount) {
					price = element2.attr("content");
					break;
				}
				break;
			}
		}
		if(!price.isEmpty() &&  percent > 0) {
			double priced = Double.valueOf(price);
			priced = priced * (100 - percent) / 100;
			price = String.valueOf(priced);
		}
		Elements eid = doc.getElementsByClass("product-id");
		for (Element element : eid) {
			id = element.html();
		}
		String handle = productname.concat("-").concat(id).replace(" ", "-").toLowerCase();
		handle = handle.replaceAll("[^a-zA-Z0-9-]","");
		Elements eSizeAtt = doc.getElementsByClass("js-size-attribute");
		List<String> sizes = new ArrayList<String>();
		for (Element element : eSizeAtt) {
			Elements eLink = element.getElementsByTag("a");
			for (Element e : eLink) {
				String size  = e.attr("aria-label");
				if(size.contains("Selected")) {
					size = size.replace("Selected", "").trim();
				}
				sizes.add(size);
			}
		}
		int k = 0;
		int j = 0;
		int urlsize =  ImageURLs.size();
		
		for (int i = 0; i < sizes.size(); i++) {
		
			Export export = new Export();
			export.setHandle(handle);
			if(k == 0) {
				export.setTitle(productname);
				export.setBody("\"".concat(description).concat("\""));
				export.setWoocommerce(sb.toString());
			}else {
				export.setTitle("");
				export.setBody("");
			}
			k++;
			String sizee = sizes.get(i).trim();
			export.setVendor("Columbia");
			export.setType(category);
			export.setPublished("TRUE");
			export.setOption1name("Size");
			export.setOption1value(sizee);
			export.setVariantSKU("");
			export.setVariantTracker("shopify");
			export.setQuantity("21");
			export.setVariantPolicy("deny");
			export.setVariantFulfillment("manual");
			export.setPrice(price);
			export.setCompare(price);
			export.setRequireShipping("true");
			export.setVariantTax("true");
			String variantBarcode = mapGtin.get(sizee);
			if(variantBarcode == null) {
				variantBarcode = EMPTY;
			}
			export.setVariantBarcode(variantBarcode);
			if(i >= urlsize ) {
				export.setImageURL(ImageURLs.get(urlsize-1));
			}else {
				export.setImageURL(ImageURLs.get(i));
			}
			
			export.setImagePosition(String.valueOf(++j));
			export.setWeightUnit("lb");
			lsExport.add(export);
		}
		return lsExport;
	}
	
	private List<String> getAllLinkImageMulti(String firstLink, String source) {
		int lastIndex = firstLink.lastIndexOf("/");
		int secondLastSlashIndex = firstLink.lastIndexOf('/', lastIndex - 1);
		String firstImageID = firstLink.substring(secondLastSlashIndex+1, lastIndex);
		List<String> listImageURL = new ArrayList<String>();
		listImageURL.add(firstLink);
		int imageIdLength = firstImageID.length();
		List<String> listImageId = getListImageId(source,imageIdLength);
		int indexFirstImage = listImageId.indexOf(firstImageID);
		List<String> listImageIdMulti = new ArrayList<String>();
		listImageIdMulti.add(listImageId.get(indexFirstImage+1));
		listImageIdMulti.add(listImageId.get(indexFirstImage+2));
		listImageIdMulti.add(listImageId.get(indexFirstImage+3));
		listImageIdMulti.add(listImageId.get(indexFirstImage+4));
		for (String ImageId : listImageIdMulti) {
			if (!firstImageID.equalsIgnoreCase(ImageId)) {
				String newURL = firstLink.replace(firstImageID, ImageId);
				listImageURL.add(newURL);
			}
		}
		return listImageURL;
	}
	private String getFirstLinkImage(Document doc) {
		Elements srcImg = doc.getElementsByTag("img");
		String linkimage = null;
		for (Element element : srcImg) {
			if (element.attr("src").contains("1280") || element.attr("src").contains("864")) {
				linkimage = element.attr("src");
				break;
			}
		}
		System.out.println(linkimage);
		return linkimage;
	}
	private List<String> getListImageId(String source, int imageidlenth) {
		List<String> lsImagesId = new ArrayList<String>();
		for (int index = source.indexOf(portraitId); index >= 0; index = source.indexOf(portraitId, index + 1)) {
			String imageid = source.substring(index + 13, index + 13 + imageidlenth);
			if(!imageid.contains("squarishURL")) {
				lsImagesId.add(imageid);
			}
			
		}
		return lsImagesId;
	}

	
	private String getHTMLDes(Document doc) {
		Elements eDes = doc.getElementsByClass("wrapper___1HYKp");
		String des = EMPTY;
		for (Element element : eDes) {
			des = element.html();
			break;
		}
		return des;
	}
	
	public static String getHTMLDesNike(Document doc) {
		Elements eDes = doc.getElementsByClass("description-preview");
		String des = EMPTY;
		for (Element element : eDes) {
			des = element.html();
			break;
		}
		return des;
	}
	
	private String getHTMLDesBulletAdidas(Document doc) {
		Elements eDes = doc.getElementsByClass("bullet_list_wrapper___2O_YU");
		String des = EMPTY;
		for (Element element : eDes) {
			des = element.html();
			break;
		}
		return des;
	}
	
	private String getURLSource(InputStream inputStream) throws IOException {
		StringBuilder stringBuilder = new StringBuilder();
		try {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
			String inputLine;
			while ((inputLine = bufferedReader.readLine()) != null) {
				if (inputLine.contains(portraitId)) {
					stringBuilder.append(inputLine.trim());
				}
			}

		}catch(Exception e){
			
			e.printStackTrace();
		}
		return stringBuilder.toString();
	}
	
	private boolean checkExistImage(String urlImage){
		try {
			URL url = new URL(urlImage);
			InputStream in = new BufferedInputStream(url.openStream());
			in.close();
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
	}
}
