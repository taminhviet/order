package shoes.sup.controller;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.paypal.base.rest.APIContext;

import shoes.sup.model.Dispute;
import shoes.sup.model.DisputeAPI;
import shoes.sup.model.DisputeAPIOutcome;
import shoes.sup.model.DisputeAmount;
import shoes.sup.model.DisputeDetailAPI;
import shoes.sup.model.DisputeTransaction;
import shoes.sup.model.IdInput;
import shoes.sup.model.InputNotePP;
import shoes.sup.model.Link;
import shoes.sup.model.ListDisputeAPI;
import shoes.sup.model.Order;
import shoes.sup.model.PaypalAccount;
import shoes.sup.service.DisputeService;
import shoes.sup.service.OrderService;
import shoes.sup.service.PaypalService;
import shoes.sup.util.Constants;

@Controller
public class PaypalController {
	
	@Autowired
	PaypalService paypalServce;
	
	@Autowired
	DisputeService disputeService;
	
	@Autowired
	OrderService orderService;
	

	@RequestMapping(value = "/admin/addpaypal", method = RequestMethod.POST)
	public String addAccountPP(Model model,
			@RequestParam("email") String email,
			@RequestParam("clientid") String clientid,
			@RequestParam("clientsecret") String clientsecret,
			@RequestParam("username") String username,
			@RequestParam("password") String password,
			@RequestParam("address") String address,
			@RequestParam("signature") String signature) {
		PaypalAccount paypalAccount = new PaypalAccount();
		paypalAccount.setEmail(email);
		paypalAccount.setClientid(clientid);
		paypalAccount.setClientsecret(clientsecret);
		paypalAccount.setUsername(username);
		paypalAccount.setPassword(password);
		paypalAccount.setSignature(signature);
		paypalAccount.setAddress(address);
		paypalAccount.setQueue(Constants.READY_QUEUE);
		paypalAccount.setActive(Constants.ACTIVE);
		paypalServce.savePaypalAccount(paypalAccount);
		return "redirect:/admin/paypal";
	}
	@RequestMapping(value = "/admin/uploadpaypal", method = RequestMethod.POST)
	public String uploadPaypal(@RequestParam("file") MultipartFile file,
			HttpServletRequest request,Model model) {
		List<PaypalAccount> lsAccount = new ArrayList<PaypalAccount>();
		try {
			InputStream excelFile = file.getInputStream();
			Workbook workbook = new XSSFWorkbook(excelFile);
			Sheet datatypeSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = datatypeSheet.iterator();
			while (iterator.hasNext()) {
				Row currentRow = iterator.next();
				int rowindex = currentRow.getRowNum();
				if(rowindex == 0){
					continue;
				}
				PaypalAccount paypalAccount = new PaypalAccount();
				paypalAccount.setActive(Constants.ACTIVE);
				Iterator<Cell> cellIterator = currentRow.iterator();
				while (cellIterator.hasNext()) {
					Cell currentCell = cellIterator.next();
					int colindex = currentCell.getColumnIndex();
					switch (colindex) {
					case 0:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							paypalAccount.setEmail(currentCell.getStringCellValue().trim());
						}
						break;
					case 1:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							paypalAccount.setClientid(currentCell.getStringCellValue().trim());
						}
						break;
					case 2:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							paypalAccount.setClientsecret(currentCell.getStringCellValue().trim());
						}
						break;
					case 3:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							paypalAccount.setUsername(currentCell.getStringCellValue().trim());
						}
						break;
					case 4:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							paypalAccount.setPassword(currentCell.getStringCellValue().trim());
						}
						break;
					case 5:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							paypalAccount.setSignature(currentCell.getStringCellValue().trim());
						}
						break;
					case 6:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							paypalAccount.setAddress(currentCell.getStringCellValue().trim());
						}
						break;
					default:
						break;
					}
				}
				lsAccount.add(paypalAccount);
			}
			for (PaypalAccount paypalAccount : lsAccount) {
				String email = paypalAccount.getEmail();
				PaypalAccount checkExist = paypalServce.findByEmail(email);
				if(checkExist == null) {
					paypalAccount.setQueue(Constants.READY_QUEUE);
					paypalServce.savePaypalAccount(paypalAccount);
				}
			}
			
			List<PaypalAccount> lsPP = paypalServce.findByActiveAndQueue(Constants.ACTIVE, Constants.READY_QUEUE);
			if (!lsPP.isEmpty()) {
				for (Iterator iteratorpp = lsPP.iterator(); iterator.hasNext();) {
					PaypalAccount paypalAccount = (PaypalAccount) iteratorpp.next();
					if (paypalAccount.getUsername() == null || paypalAccount.getUsername().isEmpty()) {
						iteratorpp.remove();
					}
				}
			}
			for (PaypalAccount paypalAccount : lsPP) {
				StringBuilder sb = new StringBuilder();
				String userName = paypalAccount.getUsername().trim();
				sb.append("https://api-3t.paypal.com/nvp?&user=");
				sb.append(userName);
				sb.append("&pwd=");
				sb.append(paypalAccount.getPassword().trim());
				sb.append("&signature=");
				sb.append(paypalAccount.getSignature().trim());
				sb.append(
						"&version=70.0&METHOD=SetExpressCheckout&RETURNURL=http://www.paypal.com/test.php&CANCELURL=http://www.paypal.com/test.php&PAYMENTACTION=Sale&AMT=50&CURRENCYCODE=USD");
				String url = sb.toString();
				HttpClient client = HttpClientBuilder.create().build();
				HttpGet httpGet = new HttpGet(url);
					HttpResponse response = client.execute(httpGet);
					BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));

					String output;
					String statuscheck = null;
					while ((output = br.readLine()) != null) {
						statuscheck = output;
					}
					if (statuscheck.contains("Success")) {
						paypalAccount.setStatus(Constants.PP_LIVE);
						paypalServce.savePaypalAccount(paypalAccount);
					} else {
						paypalAccount.setStatus(Constants.PP_LIMIT);
						paypalServce.savePaypalAccount(paypalAccount);

					}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/admin/paypal";
	}

	@RequestMapping(value = "/admin/editpp", method = RequestMethod.POST)
	public String editAccountPP(Model model,
			@RequestParam("idedit") String idedit,
			@RequestParam("emailedit") String emailedit,
			@RequestParam("usernameedit") String usernameedit,
			@RequestParam("passwordedit") String passwordedit,
			@RequestParam("addressedit") String addressedit) {
		PaypalAccount editPP = paypalServce.findById(Integer.valueOf(idedit.trim()));
		if(editPP != null){
			editPP.setEmail(emailedit.trim());
			editPP.setUsername(usernameedit.trim());
			editPP.setPassword(passwordedit.trim());
			editPP.setAddress(addressedit.trim());
			paypalServce.savePaypalAccount(editPP);
		}
		return "redirect:/admin/paypal";
	}
	@RequestMapping(value = "/admin/deletepaypal", method = RequestMethod.POST)
	public String deleteAccountPP(Model model, @RequestParam("id") int id) {
		PaypalAccount paypalAccount = paypalServce.findById(id);
		if (paypalAccount != null) {
			paypalServce.deletePaypalAccount(paypalAccount);
		}
		return "redirect:/admin/paypal";
	}
	
	@RequestMapping(value = "/admin/paypal", method = RequestMethod.GET)
	public String paypalAccount(Model model) {
		List<PaypalAccount> lsPP = paypalServce.listAll();
		if (lsPP != null && !lsPP.isEmpty()) {
			model.addAttribute("lsPP", lsPP);
		}
		return "paypalaccount";
	}
	
	@RequestMapping(value = "/admin/activepaypal", method = RequestMethod.POST)
	public String activepaypal(Model model, @RequestParam("id") String id){
		PaypalAccount paypalAccount = paypalServce.findById(Integer.valueOf(id));
		paypalAccount.setActive(Constants.ACTIVE);
		paypalServce.savePaypalAccount(paypalAccount);
		return "redirect:/admin/paypal";
	}
	@RequestMapping(value = "/admin/deactivepaypal", method = RequestMethod.POST)
	public String deactivepaypal(Model model, @RequestParam("id") String id){
		PaypalAccount paypalAccount = paypalServce.findById(Integer.valueOf(id));
		paypalAccount.setActive(Constants.DEACTIVE);
		paypalServce.savePaypalAccount(paypalAccount);
		return "redirect:/admin/paypal";
	}
	
	@RequestMapping(value = "/admin/ajaxdeactivepaypal",method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ajaxdeactivepaypal(@RequestBody String json){
		String result = Constants.FAILED;
		if(json.contains("\"")){
			json = json.replace("\"", "");
		}
		Gson gson = new Gson();
		IdInput idInput = gson.fromJson(json, IdInput.class);
		String id = idInput.getId();
		PaypalAccount paypalAccount = paypalServce.findById(Integer.valueOf(id));
		paypalAccount.setActive(Constants.DEACTIVE);
		paypalAccount.setQueue(Constants.DAYS_180);
		paypalServce.savePaypalAccount(paypalAccount);
		result = Constants.SUCCESS;
		return ResponseEntity.ok(result);
	}
	
	@RequestMapping(value = "/admin/ajaxqueuepp",method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ajaxqueuepp(@RequestBody String json){
		String result = Constants.FAILED;
		if(json.contains("\"")){
			json = json.replace("\"", "");
		}
		Gson gson = new Gson();
		IdInput idInput = gson.fromJson(json, IdInput.class);
		String id = idInput.getId();
		PaypalAccount paypalAccount = paypalServce.findById(Integer.valueOf(id));
		paypalAccount.setQueue(Constants.READY_QUEUE);
		paypalServce.savePaypalAccount(paypalAccount);
		result = Constants.SUCCESS;
		return ResponseEntity.ok(result);
	}
	@RequestMapping(value = "/admin/ajaxnotepp",method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ajaxnotepp(@RequestBody String json){
		String result = Constants.FAILED;
//		if(json.contains("\"")){
//			json = json.replace("\"", "");
//		}
		Gson gson = new Gson();
		InputNotePP idInputPP = gson.fromJson(json, InputNotePP.class);
		String id = idInputPP.getId();
		PaypalAccount paypalAccount = paypalServce.findById(Integer.valueOf(id));
		paypalAccount.setNote(idInputPP.getNotetext());
		paypalServce.savePaypalAccount(paypalAccount);
		result = Constants.SUCCESS;
		return ResponseEntity.ok(result);
	}
	@RequestMapping(value = "/admin/ajaxcheckppstatus",method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ajaxcheckpp(@RequestBody String json){
		String result = Constants.PP_LIVE;
		Gson gson = new Gson();
		IdInput idInputPP = gson.fromJson(json, IdInput.class);
		String id = idInputPP.getId();
		PaypalAccount paypalAccount = paypalServce.findById(Integer.valueOf(id));
		try {
			StringBuilder sb = new StringBuilder();
			String userName = paypalAccount.getUsername().trim();
			sb.append("https://api-3t.paypal.com/nvp?&user=");
			sb.append(userName);
			sb.append("&pwd=");
			sb.append(paypalAccount.getPassword().trim());
			sb.append("&signature=");
			sb.append(paypalAccount.getSignature().trim());
			sb.append(
					"&version=70.0&METHOD=SetExpressCheckout&RETURNURL=http://www.paypal.com/test.php&CANCELURL=http://www.paypal.com/test.php&PAYMENTACTION=Sale&AMT=50&CURRENCYCODE=USD");
			String url = sb.toString();
			HttpClient client = HttpClientBuilder.create().build();
			HttpGet httpGet = new HttpGet(url);
			HttpResponse response = client.execute(httpGet);
			BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));

			String output;
			String statuscheck = null;
			while ((output = br.readLine()) != null) {
				statuscheck = output;
			}
			if (statuscheck.contains("Success")) {
				result = Constants.PP_LIVE;
				paypalAccount.setStatus(Constants.PP_LIVE);
				paypalAccount.setActive(Constants.ACTIVE);
				paypalServce.savePaypalAccount(paypalAccount);
			} else {
				result = Constants.PP_LIMIT;

			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return ResponseEntity.ok(result);
	}
	
	@RequestMapping(value = "/admin/disputecheck", method = RequestMethod.GET)
	public String disputeSchedule() {
		List<PaypalAccount> lsPaypal = paypalServce.findByActive(Constants.ACTIVE);
		try {
			if (lsPaypal != null && !lsPaypal.isEmpty()) {
				for (PaypalAccount paypalAccount : lsPaypal) {
					String clientID = paypalAccount.getClientid();
					String clientSecret = paypalAccount.getClientsecret();
					APIContext apiContext = new APIContext(clientID, clientSecret, Constants.EXECUTION_MODE);
					String fetch = apiContext.fetchAccessToken();
					List<DisputeAPI> lsDisputeAPI = new ArrayList<DisputeAPI>();
					Gson gson = new Gson();
					for (int i = 1; i < 2; i++) {
						HttpClient client = HttpClientBuilder.create().build();
						String url = Constants.LIST_DISPUTE + "?page_size=50&page=" + i;
						HttpGet httpGet = new HttpGet(url);
						httpGet.setHeader(Constants.AUTHOR, fetch);
						httpGet.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
						HttpResponse response = client.execute(httpGet);
						if (response.getStatusLine().getStatusCode() != 200) {
							continue;
						}
						BufferedReader br = new BufferedReader(
								new InputStreamReader((response.getEntity().getContent())));

						String output;
						String listDispute = null;
						while ((output = br.readLine()) != null) {
							listDispute = output;
						}
						Type collectionTypeDispute = new TypeToken<ListDisputeAPI>() {
						}.getType();
						ListDisputeAPI listDisputeAPI = gson.fromJson(listDispute, collectionTypeDispute);
						lsDisputeAPI.addAll(listDisputeAPI.getItems());
					}

					if (!lsDisputeAPI.isEmpty()) {
						if (lsDisputeAPI != null && !lsDisputeAPI.isEmpty()) {
							for (DisputeAPI disputeAPI : lsDisputeAPI) {
								String disputeId = disputeAPI.getDispute_id();
//								System.out.println(disputeId);
								String disputeState = disputeAPI.getDispute_state();
								String disputeReason = disputeAPI.getReason();
								String disputeStatus = disputeAPI.getStatus();

								APIContext apiContextNew = new APIContext(clientID, clientSecret,
										Constants.EXECUTION_MODE);
								fetch = apiContextNew.fetchAccessToken();
								URL urlDispute = new URL(Constants.LIST_DISPUTE + "/" + disputeId);
								HttpURLConnection connDispute = (HttpURLConnection) urlDispute.openConnection();
								connDispute.setRequestProperty(Constants.AUTHOR, fetch);

								connDispute.setRequestProperty(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
								connDispute.setRequestMethod(Constants.GET_METHOD);
								String outputDispute;
								StringBuffer responseDispute = new StringBuffer();
								try {
									BufferedReader inDispute = new BufferedReader(
											new InputStreamReader(connDispute.getInputStream()));
									while ((outputDispute = inDispute.readLine()) != null) {
										responseDispute.append(outputDispute);
									}
									inDispute.close();
								} catch (Exception e) {
//									System.out.println(paypalAccount.getEmail());
//									System.out.println(e.getMessage());
									continue;
								}


								// printing result from response
								String disputeDetail = responseDispute.toString();

								Type collectionTypeDisputeDetail = new TypeToken<DisputeDetailAPI>() {
								}.getType();
								DisputeDetailAPI disputeDetailAPI = gson.fromJson(disputeDetail,
										collectionTypeDisputeDetail);
								List<DisputeTransaction> lsTransaction = disputeDetailAPI.getDisputed_transactions();
								DisputeTransaction disputeTransaction = lsTransaction.get(0);
								// String buyer_transaction_id =
								// disputeTransaction.getBuyer_transaction_id();
								String seller_transaction_id = disputeTransaction.getSeller_transaction_id();
								String seller_response_due_date = disputeDetailAPI.getSeller_response_due_date();
								shoes.sup.model.Dispute checkExist = disputeService.findByDisputeId(disputeId);
								if (checkExist != null) {
									checkExist.setReason(disputeReason);
									checkExist.setStatus(disputeStatus);
									if (Constants.DISPUTE_RESOLVED.equalsIgnoreCase(disputeStatus)) {
										DisputeAPIOutcome disputeAPIOutcome = disputeDetailAPI.getDispute_outcome();
										if (disputeAPIOutcome != null) {
											checkExist.setOutcome_code(disputeAPIOutcome.getOutcome_code());
										}
									}
									checkExist.setDispute_state(disputeState);
									checkExist.setSeller_transaction_id(seller_transaction_id);
									checkExist.setSeller_response_due_date(seller_response_due_date);
									DisputeAmount disputeAmount = disputeDetailAPI.getDispute_amount();
									checkExist.setCurrency(disputeAmount.getCurrency_code());
									checkExist.setValue(disputeAmount.getValue());
									disputeService.saveDispute(checkExist);
								} else {
									if (!Constants.DISPUTE_RESOLVED.equalsIgnoreCase(disputeStatus)) {
										shoes.sup.model.Dispute dispute = new Dispute();
										dispute.setDisputeid(disputeId);
										dispute.setReason(disputeReason);
										dispute.setStatus(disputeStatus);
										dispute.setDispute_state(disputeState);
										dispute.setSeller_transaction_id(seller_transaction_id);
										dispute.setSeller_response_due_date(seller_response_due_date);
										DisputeAmount disputeAmount = disputeDetailAPI.getDispute_amount();
										dispute.setCurrency(disputeAmount.getCurrency_code());
										dispute.setValue(disputeAmount.getValue());
										disputeService.saveDispute(dispute);
									}
								}
								Order order = orderService.findByTransaction_id(seller_transaction_id);
								if (order != null && !Constants.DISPUTE_RESOLVED.equalsIgnoreCase(order.getDisputed())) {
									order.setDisputed(Constants.DISPUTED);
									order.setDispute_id(disputeId);
									order.setDispute_reason(disputeReason);
									order.setDispute_state(disputeState);
									order.setDispute_status(disputeStatus);
									if (Constants.MERCHANDISE_OR_SERVICE_NOT_AS_DESCRIBED
											.equalsIgnoreCase(disputeReason)) {
										List<Link> links = disputeDetailAPI.getLinks();
										for (Link link : links) {
											if (Constants.acknowledge_return_item.equalsIgnoreCase(link.getRel())) {
												order.setReturned_by_customer(Constants.CUSTOMER_RETURNED);
												order.setStatus(Constants.RETURNED);
											}
										}
									}
									DisputeAPIOutcome disputeAPIOutcome = disputeDetailAPI.getDispute_outcome();
									if(disputeAPIOutcome != null && Constants.RESOLVED_BUYER_FAVOUR.equalsIgnoreCase(disputeAPIOutcome.getOutcome_code()) && 
											!Constants.RESOLVED_BUYER_FAVOUR.equalsIgnoreCase(order.getOutcome_code())) {
										String capture_total = paypalAccount.getCapturetotal();
//										System.out.println(paypalAccount.getEmail() + " : " + capture_total);
										String price = order.getPrice();
										int open = price.indexOf("(");
										double priceRoot = Double.valueOf(price.substring(0, open));
										double newTotal = 0;
										if(capture_total != null) {
											newTotal = Double.valueOf(capture_total) - priceRoot;
										}
										if(newTotal <= 0 ) {
											paypalAccount.setStatus(Constants.DEACTIVE);
										}
										paypalAccount.setCapturetotal(String.valueOf(newTotal));
										paypalServce.savePaypalAccount(paypalAccount);
									}
									if (Constants.DISPUTE_RESOLVED.equalsIgnoreCase(disputeState)
											|| Constants.DISPUTE_APPEALABLE.equalsIgnoreCase(disputeState)) {
										if (disputeAPIOutcome != null) {
											order.setOutcome_code(disputeAPIOutcome.getOutcome_code());
										}
									}
									order.setSeller_response_due_date(seller_response_due_date);
									if (order.getPay_pal_account_id() == null) {
										order.setPay_pal_account_id(paypalAccount.getEmail());
									}
									orderService.saveOrder(order);
								}
							}
						}
					}
				}
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/admin/paypal";
	}
	
}
