package shoes.sup.controller;

import java.net.HttpURLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import shoes.sup.model.Fulfillment;
import shoes.sup.model.Order;
import shoes.sup.model.PaypalAccount;
import shoes.sup.model.Store;
import shoes.sup.model.TrackingWoo;
import shoes.sup.model.User;
import shoes.sup.service.OrderService;
import shoes.sup.service.PaypalService;
import shoes.sup.service.StoreService;
import shoes.sup.service.UserService;
import shoes.sup.util.Constants;

import com.google.gson.Gson;

@Controller
public class SupplierController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	OrderService orderService;
	
	@Autowired
	PaypalService paypalService;
	
	@Autowired
	StoreService storeService;

	@RequestMapping(value = "/supplier", method = RequestMethod.GET)
	public String supplier(Model model){
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName = authentication.getName();
		User user = userService.findbyUsername(currentPrincipalName);
		String name = user.getFullname();
		if(name.isEmpty() || name == null) {
			name = user.getUsername();
		}
		model.addAttribute("name", name);
		List<String> listSupplier = new ArrayList<String>();
		listSupplier.add(Constants.Fedex);
		listSupplier.add(Constants.FedEx_Sameday);
		listSupplier.add(Constants.UPS);
		listSupplier.add(Constants.USPS);
		listSupplier.add(Constants.DHL_US);
		listSupplier.add(Constants.DHL_Parcel);
		listSupplier.add(Constants.TNT);
		listSupplier.add(Constants.DHL_Inraship_DE);
		model.addAttribute("listSupplier", listSupplier);
		List<Order> lsOrder = orderService.findBySupplier(currentPrincipalName);
		model.addAttribute("lsOrder", lsOrder);
		List<Order> lsCancelOrder = orderService.findByStatusSupplier(Constants.CANCEL_SHOPIFY, currentPrincipalName);
		List<String> lsOrderIdCancelMessage = new ArrayList<String>();
		for (Order order : lsCancelOrder) {
			String warning = order.getOname() + " need cancel urgently! Please select VN_REQUEST and Cancel";
			lsOrderIdCancelMessage.add(warning);
		}
		model.addAttribute("message", lsOrderIdCancelMessage);
		List<String> lsReason = new ArrayList<String>();
//		lsReason.add(Constants.DO_NOT_HAVE);
		lsReason.add(Constants.OUT_OF_STOCK);
		lsReason.add(Constants.NO_SIZE);
		lsReason.add(Constants.LOW_QUALITY);
		lsReason.add(Constants.VN_REQUEST);
		lsReason.add(Constants.OTHER);
		model.addAttribute("lsReason", lsReason);
		return "supplier";
	}
	@RequestMapping(value = "/supplier/getone", method = RequestMethod.GET)
	public String findOrder(Model model, @RequestParam("orderid") String orderid){
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName = authentication.getName();
		User user = userService.findbyUsername(currentPrincipalName);
		List<Order> lsOrder = orderService.findByName(orderid, user.getUsername());
		model.addAttribute("lsOrder", lsOrder);
		return "supplier";
	}
	
	@RequestMapping(value = "/supplier/changepassword", method = RequestMethod.POST)
	public String changepassword(Model model, @RequestParam("newpassword") String newpassword){
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName = authentication.getName();
		User user = userService.findbyUsername(currentPrincipalName);
		user.setPassword(newpassword);
		userService.saveUser(user);
		return "redirect:/logout";
	}
	
	@RequestMapping(value = "/supplier/updateprofile", method = RequestMethod.POST)
	public String updateprofile(Model model, 
			@RequestParam(name = "fullname", required = false) String fullname,
			@RequestParam(name = "phone", required = false) String phone,
			@RequestParam(name = "address", required = false) String address,
			@RequestParam(name = "product", required = false) String product){
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName = authentication.getName();
		User user = userService.findbyUsername(currentPrincipalName);
		if(!fullname.isEmpty()){
			user.setFullname(fullname);
		}
		if(!address.isEmpty()){
			user.setAddress(address);
		}
		if(!phone.isEmpty()){
			user.setPhone(phone);
		}
		user.setProduct(product);
		userService.saveUser(user);
		model.addAttribute("user", user);
		return "redirect:/supplier/profile";
	}
	@RequestMapping(value = "/supplier/profile", method = RequestMethod.GET)
	public String profile(Model model){
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName = authentication.getName();
		User user = userService.findbyUsername(currentPrincipalName);
		model.addAttribute("user", user);
		String name = user.getFullname();
		if(name.isEmpty() || name == null) {
			name = user.getUsername();
		}
		model.addAttribute("name", name);
		return "profile";
	}
	
	@RequestMapping(value = "/supplier/deliver", method = RequestMethod.POST)
	public String welcome(Model model,@RequestParam("id") int id,@RequestParam("trackingnumber") String trackingnumber,
			@RequestParam(name = "carrier", required = false) String carrier,
			@RequestParam(name = "supplierwoo", required = false) String supplierwoo){
		Order order = orderService.findById(id);
		order.setTrackingnumber(trackingnumber);
		order.setCarrier(carrier);
		order.setStatus(Constants.DELIVERED);
		String paypalEmail = order.getPay_pal_account_id();
		if(paypalEmail != null){
			PaypalAccount paypalAccount = paypalService.findByEmail(paypalEmail);
			String clientID = paypalAccount.getClientid();
			String clientSecret = paypalAccount.getClientsecret();
			String transactionid = order.getTransaction_id();
		}
		String domain = order.getStoredomain();
		Gson gson = new Gson();
		if(domain != null && domain.length() > 0){
			Store store = storeService.getStoreByDomain(domain);
			//fulfill 
			if(Constants.SHOPIFY == store.getType()){
				try {
					StringBuilder apiUrl = new StringBuilder();
					apiUrl.append("https://");
					apiUrl.append(store.getApikey());
					apiUrl.append(":");
					apiUrl.append(store.getPassword());
					apiUrl.append("@");
					apiUrl.append(domain);
					apiUrl.append("/admin/api/2019-04/orders/");
					apiUrl.append(order.getOrderidapi());
					apiUrl.append("/fulfillments.json");
					HttpClient client = HttpClientBuilder.create().build();
					HttpPost httpPost = new HttpPost(apiUrl.toString());
					
					Fulfillment fulfillment = new Fulfillment();
					fulfillment.setTracking_number(trackingnumber);
					fulfillment.setNotify_customer(true);
					String jsonData = gson.toJson(fulfillment);
					jsonData = "{" + "\"" + "fulfillment" + "\"" + ":" +  jsonData + "}"; 
					StringEntity params =new StringEntity(jsonData);
					httpPost.setEntity(params);
					httpPost.setHeader("Accept", "application/json");
					httpPost.setHeader("Content-type", "application/json");	
					HttpResponse response = client.execute(httpPost);
					System.out.println("fulfill: " + order.getOname() + " "+ response.getStatusLine().getStatusCode());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}else {
				try {
					String ck = store.getApikey();
					String cs = store.getPassword();
					Date today = Calendar.getInstance().getTime();        
					String pattern = "yyyy-MM-dd";
					DateFormat df = new SimpleDateFormat(pattern);
					String todayAsString = df.format(today);
					String tracking_provider = supplierwoo;
					String tracking_number = trackingnumber;
					String date_shipped = todayAsString;
					TrackingWoo trackingWoo = new TrackingWoo(tracking_provider, tracking_number, date_shipped, 1);
					String jsonData = gson.toJson(trackingWoo);
					HttpClient client = HttpClientBuilder.create().build();
					String idwoo = order.getOname().substring(4, order.getOname().length());
					String url = "https://" + domain + "/wp-json/wc/v1/orders/"+idwoo+"/shipment-trackings?consumer_key="+ck +"&consumer_secret="+cs;
					HttpPost httpPost = new HttpPost(url);
					StringEntity params = new StringEntity(jsonData);
					httpPost.setEntity(params);
					httpPost.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
					HttpResponse response = client.execute(httpPost);
					int statusCode = response.getStatusLine().getStatusCode();
//					if(HttpURLConnection.HTTP_CREATED == statusCode){
//						orderService.saveOrder(order);
//					}
				} catch (Exception e) {
					// TODO: handle exception
				}
				
			}
			/** Paypal up track
			try {
				APIContext apiContext = new APIContext(clientID, clientSecret,
						Constants.EXECUTION_MODE);
				String fetch = apiContext.fetchAccessToken();
				HttpClient clientPPTrack = HttpClientBuilder.create().build();
				HttpPost httpPostPPTrack = new HttpPost(Constants.TRACK_PP);
				Tracker tracker = new Tracker(transactionid, trackingnumber, Constants.SHIPPED, carrier);
				List<Tracker> lstrackers = new ArrayList<Tracker>();
				lstrackers.add(tracker);
				Trackers trackers = new Trackers(lstrackers);
				String jsonDataPP = gson.toJson(trackers);
				StringEntity paramsPP = new StringEntity(jsonDataPP);
				httpPostPPTrack.setEntity(paramsPP);
				httpPostPPTrack.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);	
				httpPostPPTrack.setHeader(Constants.AUTHOR, fetch);
				HttpResponse responsePP = clientPPTrack.execute(httpPostPPTrack);
				System.out.println("fulfill PP: " + order.getOname() + " "+ responsePP.getStatusLine().getStatusCode());
			} catch (Exception e) {
				e.printStackTrace();
			}
			**/
		}
		orderService.saveOrder(order);
		return "redirect:/supplier";
	}
	@RequestMapping(value = "/supplier/neworder",method = RequestMethod.GET)
	public String neworder(Model model){
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName = authentication.getName();
		User user = userService.findbyUsername(currentPrincipalName);
		String name = user.getFullname();
		String product = user.getProduct();
		if(name.isEmpty() || name == null) {
			name = user.getUsername();
		}
		model.addAttribute("name", name);
		List<Order> lsOrder = orderService.findBySupplier(null, currentPrincipalName,product);
		if(!lsOrder.isEmpty()){
			model.addAttribute("lsOrder", lsOrder);
		}
		return "neworder";
	}
	@RequestMapping(value = "/supplier/pick",method = RequestMethod.GET)
	public String pickorder(Model model,@RequestParam("id") int id){
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName = authentication.getName();
		Order order = orderService.findById(id);
		order.setStatus(Constants.READY);
		order.setSupplier(currentPrincipalName);
		order.setNote(null);
		orderService.saveOrder(order);
		return "redirect:/supplier/neworder";
	}
	
	@RequestMapping(value = "/supplier/cancel", method = RequestMethod.POST)
	public String cancel(Model model,@RequestParam("id") int id,@RequestParam("reason") String reason){
		Order order = orderService.findById(id);
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName = authentication.getName();
		order.setNote(null);
		String adminNote = order.getAdminnote();
		if(adminNote == null || adminNote.isEmpty()){
			order.setAdminnote(currentPrincipalName + " : " + reason);
		}else {
			order.setAdminnote(adminNote + "," +  currentPrincipalName + " : " + reason);
		}
		if(reason.equalsIgnoreCase(Constants.VN_REQUEST)){
			order.setStatus(Constants.CANCEL_SHOPIFY);
		}else {
			order.setStatus(Constants.CANCEL_SUPPLIER);
		}
		order.setSupplier(null);
		String lastSupplier = order.getLastsupplier();
		if(lastSupplier == null ){
			order.setLastsupplier(currentPrincipalName);
		}else {
			order.setLastsupplier(lastSupplier + "," + currentPrincipalName);
		}
		orderService.saveOrder(order);
		return "redirect:/supplier";
	}
	@RequestMapping(value = "/supplier/note", method = RequestMethod.POST)
	public String note(Model model,@RequestParam("id") int id,@RequestParam("note") String note){
		Order order = orderService.findById(id);
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName = authentication.getName();
		String adminNote = order.getAdminnote();
		if(adminNote == null || adminNote.isEmpty()){
			order.setAdminnote(currentPrincipalName + " : " + note);
		}else {
			order.setAdminnote(adminNote + "," +  currentPrincipalName + " : " + note);
		}
		order.setNote(note);
		orderService.saveOrder(order);
		return "redirect:/supplier";
	}
}
