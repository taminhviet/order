package shoes.sup.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import shoes.sup.model.MetaWoo;
import shoes.sup.model.ProductShopifyCreate;
import shoes.sup.model.ProductShopifyCreateModel;
import shoes.sup.model.ProductShopifyModel;
import shoes.sup.model.ShopifyImages;
import shoes.sup.model.ShopifyOptions;
import shoes.sup.model.ShopifyProducts;
import shoes.sup.model.ShopifyVariants;
import shoes.sup.model.Store;
import shoes.sup.service.ShopifyProductService;
import shoes.sup.service.StoreService;
import shoes.sup.util.Constants;
import shopping.common.model.Attribute;
import shopping.common.model.AttributeAPI;
import shopping.common.model.ProductsWoo;
import shopping.common.model.ProductsWooAPI;
import shopping.common.model.ProductsWooAPIRest;

@Controller
public class ProductController {

	@Autowired
	ShopifyProductService shopifyProductService;

	@Autowired
	StoreService storeService;

	@RequestMapping(value = "/admin/products", method = RequestMethod.GET)
	public String manageProduct(Model model, String message) {
		List<ShopifyProducts> lsProduct = shopifyProductService.findAll();
		Set<String> sethandle = new HashSet<>();
		if (!lsProduct.isEmpty()) {
			List<ShopifyProducts> lsShopifyProducts = lsProduct.stream().filter(e -> !e.getDescription().isEmpty() && sethandle.add(e.getHandle()))
					.collect(Collectors.toList());
			model.addAttribute("lsProduct", lsShopifyProducts);
		}
		List<Store> lsstore = storeService.getAllStore();
		List<String> stores = new ArrayList<>();
		if (!lsstore.isEmpty()) {
			for (Store store : lsstore) {
				if(Constants.ACTIVE.equals(store.getState())) {
					stores.add(store.getDomain());
				}
			}
			Collections.reverse(stores);
			model.addAttribute("lsstore", stores);
		}
		model.addAttribute("message", message);
		return "productshopify";
	}

	@RequestMapping(value = "/admin/uploadwoo", method = RequestMethod.POST)
	public String uploadwoo(Model model, @RequestParam(name = "storeselect") String storeselect,
			@RequestParam(name = "hiddenproductupload", required = false) String hiddenproductupload,
			RedirectAttributes ra) {

		int countupload = 0;
		if (hiddenproductupload.length() > 0) {
			Store storeupload = storeService.getStoreByDomain(storeselect);
			// only 1 product selected
			Gson gson = new Gson();
			if (!hiddenproductupload.contains(Constants.COMMA)) {
				ShopifyProducts shopifyProducts = shopifyProductService.findById(Integer.valueOf(hiddenproductupload));
				if(Constants.WOO_COMMERCE == storeupload.getType()) {
					countupload = uploadWooProducts(gson, shopifyProducts, storeupload, storeselect);
				}else {
					countupload = uploadShopifyProducts(gson, shopifyProducts, storeupload, storeselect);
				}
			}else {
				//multi select
				String[] arrayid = hiddenproductupload.split(Constants.COMMA);
				for (String id : arrayid) {
					if(Constants.WOO_COMMERCE == storeupload.getType()) {
						ShopifyProducts shopifyProducts = shopifyProductService.findById(Integer.valueOf(id));
						int singleUpload  = uploadWooProducts(gson, shopifyProducts, storeupload, storeselect);
						countupload += singleUpload;
					}else {
						ShopifyProducts shopifyProducts = shopifyProductService.findById(Integer.valueOf(id));
						int singleUpload  = uploadShopifyProducts(gson, shopifyProducts, storeupload, storeselect);
						countupload += singleUpload;
					}
				}
			}
		}
		String message = "Total products uploaded: " + countupload;
		ra.addAttribute("message", message);
		return "redirect:/admin/products/";
	}
	
	private int uploadShopifyProducts(Gson gson, ShopifyProducts shopifyProducts, Store storeupload, String storeselect) {
		int countupload = 0;
		String handle = shopifyProducts.getHandle();
		List<ShopifyProducts> lsHandlesProduct = shopifyProductService.findByHandle(handle);
		int sizeHandle = lsHandlesProduct.size();
		// check simple product
		if (sizeHandle == 1) {
			for (ShopifyProducts productupload : lsHandlesProduct) {
				ProductShopifyCreate productShopifyCreate = new ProductShopifyCreate();
//				String title = generateRandomContent(productupload.getTitle(), true);
				productShopifyCreate.setTitle(productupload.getTitle());
				//String description = generateRandomContent(productupload.getDescription(),false);
				productShopifyCreate.setBody_html(productupload.getDescription());
				productShopifyCreate.setVendor(productupload.getVendor());
				productShopifyCreate.setProduct_type(productupload.getCategory());
				productShopifyCreate.setHandle(productupload.getHandle());
				productShopifyCreate.setStatus(Constants.ACTIVE_PRODUCT);
				productShopifyCreate.setTags(productupload.getTags());
				//variant
				List<ShopifyVariants> variants = new ArrayList<>();
				ShopifyVariants variant = new ShopifyVariants();
				variant.setBarcode(productupload.getBarcode());
				variant.setTitle(productupload.getOption1value());
				variant.setPrice(String.valueOf(productupload.getPrice()));
				variant.setPosition(Integer.valueOf(productupload.getPosition() == null ? "1" : productupload.getPosition()));
				variant.setInventory_management("shopify");
				variant.setOption1(productupload.getOption1value());
				variant.setOption2(productupload.getOption2value());
				variant.setInventory_quantity(89);
				variants.add(variant);
				productShopifyCreate.setVariants(variants);
				//option
				List<ShopifyOptions> options = new ArrayList<>();
				ShopifyOptions option = new ShopifyOptions();
				option.setName(productupload.getOption1name());
				List<String> values = new ArrayList<>();
				values.add(productupload.getOption1value());
				option.setValues(values);
				options.add(option);
				productShopifyCreate.setOptions(options);
				//images
				List<ShopifyImages> lsImage = new ArrayList<>();
				ShopifyImages image = new ShopifyImages();
				image.setPosition(Integer.valueOf(productupload.getPosition()));
				image.setSrc(productupload.getImage());
				lsImage.add(image);
				productShopifyCreate.setImage(image);
				productShopifyCreate.setImages(lsImage);
				if(!StringUtils.isEmpty(storeupload.getSubdomainshopify())) {
					HttpResponse httpResponse = uploadShopify(gson, storeupload, productShopifyCreate);
					int statusCode = httpResponse.getStatusLine().getStatusCode();
					if (HttpURLConnection.HTTP_CREATED == statusCode) {
						countupload++;
						String existStore = shopifyProducts.getStore();
						if(existStore == null || existStore.isEmpty()) {
							shopifyProducts.setStore(storeselect);
						}else {
							shopifyProducts.setStore(Constants.COMMA.concat(storeselect));
						}
						shopifyProductService.saveShopifyProduct(shopifyProducts);
					}
				}
			}
		}else {
			ShopifyProducts firstProduct = null;
			//variant
			List<ShopifyVariants> variants = new ArrayList<>();
			//option
			List<ShopifyOptions> options = new ArrayList<>();
			ShopifyOptions option = new ShopifyOptions();
			List<String> values = new ArrayList<>();
			//image
			List<ShopifyImages> lsImage = new ArrayList<>();
			for (ShopifyProducts productupload : lsHandlesProduct) {
				String position = productupload.getPosition();
				if("1".equals(position)) {
					firstProduct = productupload;
				}
				//Singe variant
				ShopifyVariants variant = new ShopifyVariants();
				variant.setBarcode(productupload.getBarcode());
				variant.setTitle(productupload.getOption1value());
				variant.setPrice(String.valueOf(productupload.getPrice()));
				if(!StringUtils.isEmpty(productupload.getPosition())) {
					variant.setPosition(Integer.valueOf(productupload.getPosition()));
				}
				variant.setInventory_management("shopify");
				variant.setOption1(productupload.getOption1value());
				variant.setOption2(productupload.getOption2value());
				variant.setInventory_quantity(89);
				variants.add(variant);
				
				//Single Option				
				values.add(productupload.getOption1value());
				
				if(!StringUtils.isEmpty(productupload.getPosition())) {
					ShopifyImages image = new ShopifyImages();
					image.setPosition(Integer.valueOf(productupload.getPosition()));
					image.setSrc(productupload.getImage());
					lsImage.add(image);
				}
			}
			if(firstProduct != null) {
				ProductShopifyCreate productShopifyCreate = new ProductShopifyCreate();
//				String title = generateRandomContent(firstProduct.getTitle(),true);
				productShopifyCreate.setTitle(firstProduct.getTitle());
				//String description = generateRandomContent(firstProduct.getDescription(),false);
				productShopifyCreate.setBody_html(firstProduct.getDescription());
				productShopifyCreate.setVendor(firstProduct.getVendor());
				productShopifyCreate.setProduct_type(firstProduct.getCategory());
				productShopifyCreate.setHandle(firstProduct.getHandle());
				productShopifyCreate.setStatus(Constants.ACTIVE_PRODUCT);
				productShopifyCreate.setTags(firstProduct.getTags());
				productShopifyCreate.setVariants(variants);
				option.setName(firstProduct.getOption1name());
				option.setValues(values);
				options.add(option);
				if(!lsImage.isEmpty()) {
					productShopifyCreate.setImages(lsImage);
					productShopifyCreate.setImage(lsImage.get(0));
				}
				if(!StringUtils.isEmpty(storeupload.getSubdomainshopify())) {
					HttpResponse httpResponse = uploadShopify(gson, storeupload, productShopifyCreate);
					int statusCode = httpResponse.getStatusLine().getStatusCode();
					if (HttpURLConnection.HTTP_CREATED == statusCode) {
						countupload++;
						String existStore = shopifyProducts.getStore();
						if(existStore == null || existStore.isEmpty()) {
							shopifyProducts.setStore(storeselect);
						}else {
							shopifyProducts.setStore(Constants.COMMA.concat(storeselect));
						}
						shopifyProductService.saveShopifyProduct(shopifyProducts);
					}
				}
			}
		}
		return countupload;
		
	}
	
	private int uploadWooProducts(Gson gson, ShopifyProducts shopifyProducts, Store storeupload, String storeselect) {
		int countupload = 0;
		String handle = shopifyProducts.getHandle();
		List<ShopifyProducts> lsHandlesProduct = shopifyProductService.findByHandle(handle);
		int sizeHandle = lsHandlesProduct.size();
		// check simple product
		if (sizeHandle == 1) {
			// start upload products
			for (ShopifyProducts productupload : lsHandlesProduct) {
				ProductsWooAPI productWoo = new ProductsWooAPI();
				productWoo.setName(productupload.getTitle());
				productWoo.setDescription(productupload.getDescription());
				productWoo.setType(Constants.SIMPLE);
				productWoo.setStatus(Constants.PUBLISH);
				productWoo.setStock_quantity(89);
				productWoo.setRegular_price(String.valueOf(productupload.getPrice()));
				productWoo.setPrice(String.valueOf(productupload.getPrice()));
				List<shopping.common.model.ImageWoo> lsImageWoo = new ArrayList<>();
				shopping.common.model.ImageWoo imageWoo = new shopping.common.model.ImageWoo();
				imageWoo.setSrc(productupload.getImage());
				lsImageWoo.add(imageWoo);
				productWoo.setImages(lsImageWoo);
				productWoo.setStock_status(Constants.INSTOCK);
				List<MetaWoo> lsMeta = new ArrayList<>();
				if (productupload.getOption2name() != null) {
					MetaWoo metaWooColor = new MetaWoo();
					metaWooColor.setKey(productupload.getOption2name());
					metaWooColor.setValue(productupload.getOption2value());
					lsMeta.add(metaWooColor);
				}
				if (productupload.getBarcode() != null) {
					MetaWoo metaWooGtin = new MetaWoo();
					metaWooGtin.setKey(Constants.GTIN);
					metaWooGtin.setValue(productupload.getBarcode());
					lsMeta.add(metaWooGtin);
				}
				productWoo.setMeta_data(lsMeta);
				HttpResponse httpResponse = uploadProduct(gson, storeupload, productWoo);
				int statusCode = httpResponse.getStatusLine().getStatusCode();
				if (HttpURLConnection.HTTP_CREATED == statusCode) {
					countupload++;
					String existStore = shopifyProducts.getStore();
					if(existStore == null || existStore.isEmpty()) {
						shopifyProducts.setStore(storeselect);
					}else {
						shopifyProducts.setStore(Constants.COMMA.concat(storeselect));
					}
					shopifyProductService.saveShopifyProduct(shopifyProducts);
				}
			}
		} else {
			// variable product
			ProductsWooAPIRest productWoo = new ProductsWooAPIRest();
			ShopifyProducts productFirst = null;
			//set size
			List<shopping.common.model.AttributeAPI> lsAttribute1 = new ArrayList<>();
			AttributeAPI sizeAtt1 = new AttributeAPI();
			ArrayList<String> options = new ArrayList<String>();
			List<shopping.common.model.ImageWoo> lsImageWoo1 = new ArrayList<>();
			int positionimage = 1;
			for (ShopifyProducts attributeAPI : lsHandlesProduct) {
				String size = attributeAPI.getOption1value();
				if(!size.isEmpty()) {
					options.add(size);
				}
				String description = attributeAPI.getDescription();
				if(description != null && !description.isEmpty()) {
					productFirst = attributeAPI;
				}
				String imagelink = attributeAPI.getImage();
				if(imagelink != null & !imagelink.isEmpty()) {
					shopping.common.model.ImageWoo imageWoo1 = new shopping.common.model.ImageWoo();
					imageWoo1.setSrc(attributeAPI.getImage());
					imageWoo1.setPosition(positionimage);
					positionimage++;
					lsImageWoo1.add(imageWoo1);
				}
			}
			if(options.size() == 1) {
				//simple
				ProductsWooAPI productWoonew = new ProductsWooAPI();
				productWoonew.setName(productFirst.getTitle());
				productWoonew.setDescription(productFirst.getDescription());
				productWoonew.setType(Constants.SIMPLE);
				productWoonew.setStatus(Constants.PUBLISH);
				productWoonew.setStock_quantity(89);
				productWoonew.setRegular_price(String.valueOf(productFirst.getPrice()));
				productWoonew.setPrice(String.valueOf(productFirst.getPrice()));
//				List<shopping.common.model.ImageWoo> lsImageWoo = new ArrayList<>();
//				shopping.common.model.ImageWoo imageWoo = new shopping.common.model.ImageWoo();
//				imageWoo.setSrc(productFirst.getImage());
//				lsImageWoo.add(imageWoo);
				productWoonew.setImages(lsImageWoo1);
				productWoonew.setStock_status(Constants.INSTOCK);
				List<MetaWoo> lsMeta = new ArrayList<>();
				if (productFirst.getOption2name() != null) {
					MetaWoo metaWooColor = new MetaWoo();
					metaWooColor.setKey(productFirst.getOption2name());
					metaWooColor.setValue(productFirst.getOption2value());
					lsMeta.add(metaWooColor);
				}
				if (productFirst.getBarcode() != null) {
					MetaWoo metaWooGtin = new MetaWoo();
					metaWooGtin.setKey(Constants.GTIN);
					metaWooGtin.setValue(productFirst.getBarcode());
					lsMeta.add(metaWooGtin);
				}
				productWoonew.setMeta_data(lsMeta);
				HttpResponse httpResponsenew = uploadProduct(gson, storeupload, productWoonew);
				int statusCode = httpResponsenew.getStatusLine().getStatusCode();
				if (HttpURLConnection.HTTP_CREATED == statusCode) {
					countupload++;
					String existStore = shopifyProducts.getStore();
					if(existStore == null || existStore.isEmpty()) {
						shopifyProducts.setStore(storeselect);
					}else {
						shopifyProducts.setStore(Constants.COMMA.concat(storeselect));
					}
					shopifyProductService.saveShopifyProduct(shopifyProducts);
				}
			}else {
				//variant
				productWoo.setImages(lsImageWoo1);
				sizeAtt1.setName(productFirst.getOption1name());
				sizeAtt1.setOption(options);
				sizeAtt1.setVariation(true);
				sizeAtt1.setVisible(true);
				productWoo.setDescription(productFirst.getDescription());
				productWoo.setType(Constants.VARIABLE);
				productWoo.setStatus(Constants.PUBLISH);
				productWoo.setStock_quantity(89);
				productWoo.setName(productFirst.getTitle());
				productWoo.setStock_status(Constants.INSTOCK);
				productWoo.setPrice(String.valueOf(productFirst.getPrice()));
				productWoo.setRegular_price(String.valueOf(productFirst.getPrice()));
				
				
				lsAttribute1.add(sizeAtt1);
				productWoo.setAttributes(lsAttribute1);
				List<shopping.common.model.Attribute> lsAttribute2 = new ArrayList<>();
				Attribute sizeAtt2 = new Attribute();
				sizeAtt2.setName(productFirst.getOption1name());
				sizeAtt2.setOption(productFirst.getOption1value());
				lsAttribute2.add(sizeAtt2);
				productWoo.setDefault_attributes(lsAttribute2);
				List<MetaWoo> lsMeta = new ArrayList<>();
				if (productFirst.getOption2name() != null) {
					MetaWoo metaWooColor = new MetaWoo();
					metaWooColor.setKey(productFirst.getOption2name());
					metaWooColor.setValue(productFirst.getOption2value());
					lsMeta.add(metaWooColor);
				}
				if (productFirst.getBarcode() != null) {
					MetaWoo metaWooGtin = new MetaWoo();
					metaWooGtin.setKey(Constants.GTIN);
					metaWooGtin.setValue(productFirst.getBarcode());
					lsMeta.add(metaWooGtin);
				}
				productWoo.setMeta_data(lsMeta);
				HttpResponse httpResponse = uploadProduct1(gson, storeupload, productWoo);
				int statusCode = httpResponse.getStatusLine().getStatusCode();
				try {
					if (HttpURLConnection.HTTP_CREATED == statusCode) {
						countupload++;
						String existStore = productFirst.getStore();
						if(existStore == null || existStore.isEmpty()) {
							productFirst.setStore(storeselect);
						}else {
							productFirst.setStore(Constants.COMMA.concat(storeselect));
						}
						shopifyProductService.saveShopifyProduct(productFirst);
						BufferedReader br = new BufferedReader(
								new InputStreamReader((httpResponse.getEntity().getContent())));

						String output;
						String newproduct = null;
						while ((output = br.readLine()) != null) {
							newproduct = output;
						}
						if(newproduct != null) {
							Type collectionTypeDispute = new TypeToken<ProductsWoo>() {
							}.getType();
							ProductsWoo apiProductsWoo = gson.fromJson(newproduct, collectionTypeDispute);
							int newid = apiProductsWoo.getId();
							String defaultOptionValue = Constants.EMPTY;
							for (ShopifyProducts productupload : lsHandlesProduct) {
								ProductsWooAPI productWooVariant = new ProductsWooAPI();
								productWooVariant.setPrice(String.valueOf(productupload.getPrice()));
								productWooVariant.setRegular_price(String.valueOf(productupload.getPrice()));
								productWooVariant.setStock_quantity(89);
								productWooVariant.setStock_status(Constants.INSTOCK);
								//set meta
								List<MetaWoo> lsMetaVariant = new ArrayList<>();
								if (productupload.getOption2name() != null) {
									MetaWoo metaWooColor = new MetaWoo();
									metaWooColor.setKey(productupload.getOption2name());
									metaWooColor.setValue(productupload.getOption2value());
									lsMetaVariant.add(metaWooColor);
								}
								if (productupload.getBarcode() != null) {
									MetaWoo metaWooGtin = new MetaWoo();
									metaWooGtin.setKey(Constants.GTIN);
									metaWooGtin.setValue(productupload.getBarcode());
									lsMetaVariant.add(metaWooGtin);
								}
								productWooVariant.setMeta_data(lsMeta);
								//set size
								List<shopping.common.model.Attribute> lsAttribute = new ArrayList<>();
								Attribute sizeAtt = new Attribute();
								String option1name = productupload.getOption1name();
								if(option1name != null && !option1name.isEmpty()) {
									defaultOptionValue = option1name;
								}
								sizeAtt.setName(defaultOptionValue);
								sizeAtt.setOption(productupload.getOption1value());
								lsAttribute.add(sizeAtt);
								productWooVariant.setAttributes(lsAttribute);
								//set image
								List<shopping.common.model.ImageWoo> lsImageWoo = new ArrayList<>();
								shopping.common.model.ImageWoo imageWoo = new shopping.common.model.ImageWoo();
								imageWoo.setSrc(productupload.getImage());
								lsImageWoo.add(imageWoo);
								productWooVariant.setImages(lsImageWoo);
								HttpResponse responeVariant =  uploadProductVariant(gson, storeupload, productWooVariant, newid);
								int codeVariant = responeVariant.getStatusLine().getStatusCode();
								if (HttpURLConnection.HTTP_CREATED == codeVariant) {
									countupload++;
								}
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
		}
		return countupload;
	}
	
	private HttpResponse uploadProductVariant(Gson gson, Store storeupload, ProductsWooAPI productWoo, int id) {
		StringBuilder sb = new StringBuilder();
		String www = storeupload.getW();
		if(www == null) {
			www = Constants.EMPTY;
		}
		if(www.equalsIgnoreCase(Constants.NO_WWW) || www.equalsIgnoreCase(Constants.FALSE_WWW)) {
			sb.append("https://");
		}else {
			sb.append("https://www.");
		}
		sb.append(storeupload.getDomain());
		sb.append("/wp-json/wc/v3/products/");
		sb.append(id);
		sb.append("/variations/?consumer_key=");
		sb.append(storeupload.getApikey());
		sb.append("&consumer_secret=");
		sb.append(storeupload.getPassword());
		String jsonData = gson.toJson(productWoo);
		try {
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost httpPost = new HttpPost(sb.toString());
			StringEntity params = new StringEntity(jsonData);
			httpPost.setEntity(params);
			httpPost.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
			HttpResponse response = client.execute(httpPost);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private HttpResponse uploadShopify(Gson gson, Store storeupload, ProductShopifyCreate product) {
		StringBuilder apiURL = new StringBuilder();
		apiURL.append("https://");
		apiURL.append(storeupload.getSubdomainshopify());
		apiURL.append("/admin/api/");
		apiURL.append(Constants.SHOPIFY_API_VERSION);
		apiURL.append("/products.json");
		ProductShopifyCreateModel newPsmodel = new ProductShopifyCreateModel();
		newPsmodel.setProduct(product);
		String jsonData = gson.toJson(newPsmodel);
		try {
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost httpPost = new HttpPost(apiURL.toString());
			StringEntity params = new StringEntity(jsonData);
			httpPost.setEntity(params);
			httpPost.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
			httpPost.setHeader(Constants.X_Shopify_Access_Token, storeupload.getAccesstoken());
			HttpResponse response = client.execute(httpPost);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private HttpResponse uploadProduct(Gson gson, Store storeupload, ProductsWooAPI productWoo) {
		StringBuilder sb = new StringBuilder();
		String www = storeupload.getW();
		if(www == null) {
			www = Constants.EMPTY;
		}
		if(www.equalsIgnoreCase(Constants.NO_WWW) || www.equalsIgnoreCase(Constants.FALSE_WWW)) {
			sb.append("https://");
		}else {
			sb.append("https://www.");
		}
		sb.append(storeupload.getDomain());
		sb.append("/wp-json/wc/v3/products/?consumer_key=");
		sb.append(storeupload.getApikey());
		sb.append("&consumer_secret=");
		sb.append(storeupload.getPassword());
		String jsonData = gson.toJson(productWoo);
		try {
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost httpPost = new HttpPost(sb.toString());
			StringEntity params = new StringEntity(jsonData);
			httpPost.setEntity(params);
			httpPost.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
			HttpResponse response = client.execute(httpPost);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private HttpResponse uploadProduct1(Gson gson, Store storeupload, ProductsWooAPIRest productWoo) {
		StringBuilder sb = new StringBuilder();
		String www = storeupload.getW();
		if(www == null) {
			www = Constants.EMPTY;
		}
		if(www.equalsIgnoreCase(Constants.NO_WWW) || www.equalsIgnoreCase(Constants.FALSE_WWW)) {
			sb.append("https://");
		}else {
			sb.append("https://www.");
		}
		sb.append(storeupload.getDomain());
		sb.append("/wp-json/wc/v3/products/?consumer_key=");
		sb.append(storeupload.getApikey());
		sb.append("&consumer_secret=");
		sb.append(storeupload.getPassword());
		String jsonData = gson.toJson(productWoo);
		try {
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost httpPost = new HttpPost(sb.toString());
			StringEntity params = new StringEntity(jsonData);
			httpPost.setEntity(params);
			httpPost.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
			HttpResponse response = client.execute(httpPost);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "/admin/deleteproductshopify", method = RequestMethod.POST)
	public String deleteproductshopify(Model model,
			@RequestParam(name = "hiddenproductshopify", required = false) String hiddenproductshopify,
			RedirectAttributes ra) {
		if (hiddenproductshopify.length() > 0) {
			List<ShopifyProducts> lsProduct = new ArrayList<>();
			int countDelete = 0;
			if (!hiddenproductshopify.contains(Constants.COMMA)) {
				// select only 1
				ShopifyProducts product = shopifyProductService.findById(Integer.valueOf(hiddenproductshopify));
				String handle = product.getHandle();
				List<ShopifyProducts> lsShopify = shopifyProductService.findByHandle(handle);
				for (ShopifyProducts shopifyProducts : lsShopify) {
					lsProduct.add(shopifyProducts);
				}
			} else {
				String[] arrayId = hiddenproductshopify.split(Constants.COMMA);
				for (String id : arrayId) {
					ShopifyProducts product = shopifyProductService.findById(Integer.valueOf(id));
					String handle = product.getHandle();
					List<ShopifyProducts> lsShopify = shopifyProductService.findByHandle(handle);
					for (ShopifyProducts shopifyProducts : lsShopify) {
						lsProduct.add(shopifyProducts);
					}
				}
			}
			for (ShopifyProducts shopifyProducts : lsProduct) {
				shopifyProductService.deleteShopifyProduct(shopifyProducts);
				countDelete++;
			}
			String message = "Total products deleted: " + countDelete;
			ra.addAttribute("message", message);
		}
		return "redirect:/admin/products";
	}

	

	@RequestMapping(value = "/admin/importproductshopify", method = RequestMethod.POST)
	public String uploadPaypal(@RequestParam("file") MultipartFile file,
			@RequestParam(name = "label", required = false) String label, HttpServletRequest request, Model model,
			RedirectAttributes ra) {
		try {
			BufferedReader fileReader = new BufferedReader(new InputStreamReader(file.getInputStream(), "UTF-8"));
			CSVParser parserr = new CSVParser(fileReader, CSVFormat.DEFAULT.withFirstRecordAsHeader().withTrim());
			Iterable<CSVRecord> csvRecords = parserr.getRecords();
			int countImport = 0;
			List<ShopifyProducts> lsShopifyProducts = new ArrayList<>();
			for (CSVRecord csvRecord : csvRecords) {
				String handle = csvRecord.get("Handle");
				String title = csvRecord.get("Title");
				String description = csvRecord.get("Body (HTML)");
				String vendor = csvRecord.get("Vendor");
//				String category = csvRecord.get("Product Category");
				String type = csvRecord.get("Type");
				String tags = csvRecord.get("Tags");
				String option1name = csvRecord.get("Option1 Name");
				String option1value = csvRecord.get("Option1 Value");
				String option2name = csvRecord.get("Option2 Name");
				String option2value = csvRecord.get("Option2 Value");
				String variantsku = checkcomma(csvRecord.get("Variant SKU"));
				String price = csvRecord.get("Variant Price");
				String compareprice = csvRecord.get("Variant Compare At Price");
				String barcode = checkcomma(csvRecord.get("Variant Barcode"));
				String image = csvRecord.get("Image Src");
				String position = csvRecord.get("Image Position");
				ShopifyProducts newProduct = new ShopifyProducts(handle, title, Constants.EMPTY, description, vendor,
						type, label, type, tags, option1name, option1value, option2name, option2value, variantsku,
						89, convertPrice(price), convertPrice(compareprice), barcode, image,position);
				lsShopifyProducts.add(newProduct);
			}
			for (ShopifyProducts shopifyProducts : lsShopifyProducts) {
				String option1Value = shopifyProducts.getOption1value();
				List<ShopifyProducts> checkExist = shopifyProductService
						.findByHandleAndSize(shopifyProducts.getHandle(), option1Value);
				
				if (checkExist.isEmpty() || StringUtils.isEmpty(option1Value)) {
					shopifyProductService.saveShopifyProduct(shopifyProducts);
					countImport++;
				}
			}
			String message = "Total products imported: " + countImport;
			ra.addAttribute("message", message);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "redirect:/admin/products";
	}

	private String checkcomma(String csvdata) {
		if (csvdata != null) {
			if(csvdata.contains("â€˜")) {
				csvdata = csvdata.replace("â€˜", Constants.EMPTY);
			}
			if(csvdata.contains("'")) {
				csvdata = csvdata.replace("'", Constants.EMPTY);
			}
		}
		return csvdata;
	}

	private Double convertPrice(String csvdata) {
		double value = 0;
		try {
			if (csvdata != null) {
				value = Double.valueOf(csvdata);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return value;
	}
	
//	private String generateRandomContent(String content, boolean istitle) {
//		StringBuilder sb= new StringBuilder();
//		if(content.contains(Constants.SPACE)) {
//			String[] arraynew = content.split(Constants.SPACE);
//			List<String> lsContent = Arrays.asList(arraynew);
//			Collections.shuffle(lsContent);
//			for (String data : lsContent) {
//				if(istitle && data.contains(Constants.COMMA)) {
//					data = data.replace(Constants.COMMA, Constants.EMPTY);
//				}
//				sb.append(data);
//				sb.append(Constants.SPACE);
//			}
//			return sb.toString().trim();
//		}else {
//			return content;
//		}
//	}
}
