package shoes.sup.controller;

import java.io.File;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import shoes.sup.model.Item;
import shoes.sup.model.Merchant;
import shoes.sup.model.Order;
import shoes.sup.model.Proxy;
import shoes.sup.model.Shopify;
import shoes.sup.model.User;
import shoes.sup.service.OrderService;
import shoes.sup.service.StoreService;
import shoes.sup.service.UserService;
import shoes.sup.util.Constants;

@Controller
public class HomeController {

	@Autowired
	UserService userService;

	@Autowired
	OrderService orderService;

	@Autowired
	StoreService storeService;

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String home(Model model) {
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		String currentPrincipalName = authentication.getName();
		if (currentPrincipalName.equalsIgnoreCase("anonymousUser")) {
			return "accessdenied";
		}
		User user = userService.findbyUsername(currentPrincipalName);
		if (user.getIsAdmin() == Constants.ROLE_ADMIN) {
			return "redirect:/admin/";
		} else {
			return "redirect:/supplier";
		}
	}

	

	@RequestMapping(value = "/startconvert", method = RequestMethod.POST)
	public String startConvert(Model model,
			@RequestParam("fileshopify") MultipartFile fileshopify,
			@RequestParam("shopname") String shopname,
			@RequestParam("shopurl") String shopurl, HttpServletRequest request) {
		List<Shopify> lsShopify = new ArrayList<Shopify>();
		try {
			InputStream excelFile = fileshopify.getInputStream();
			Workbook workbook = new XSSFWorkbook(excelFile);
			Sheet datatypeSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = datatypeSheet.iterator();
			while (iterator.hasNext()) {
				Row currentRow = iterator.next();
				Iterator<Cell> cellIterator = currentRow.iterator();
				Shopify shopify = new Shopify();
				while (cellIterator.hasNext()) {
					Cell currentCell = cellIterator.next();
					int index = currentCell.getColumnIndex();
					switch (index) {
					case 0:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							shopify.setHandle(currentCell.getStringCellValue());
						} else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
							shopify.setHandle(String.valueOf(currentCell
									.getNumericCellValue()));
						}
						break;
					case 1:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							shopify.setTitle(currentCell.getStringCellValue());
						} else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
							shopify.setTitle(String.valueOf(currentCell
									.getNumericCellValue()));
						}
						break;
					case 2:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							shopify.setBody(currentCell.getStringCellValue());
						} else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
							shopify.setBody(String.valueOf(currentCell
									.getNumericCellValue()));
						}
						break;
					case 3:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							shopify.setSize(currentCell.getStringCellValue());
						} else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
							shopify.setSize(String.valueOf(currentCell
									.getNumericCellValue()));
						}
						break;
					case 4:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
//							shopify.setBarcode(currentCell.getStringCellValue());
						} else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
							Double e1Val = currentCell
									.getNumericCellValue();
							BigDecimal bd = new BigDecimal(e1Val.toString());
							long lonVal = bd.longValue();
							shopify.setBarcode(lonVal);
						}
						break;
					case 5:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							shopify.setPrice(currentCell.getStringCellValue());
						} else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
							shopify.setPrice(String.valueOf(currentCell
									.getNumericCellValue()));
						}
						break;
					case 6:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							shopify.setImageSrc(currentCell.getStringCellValue());
						} else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
							shopify.setImageSrc(String.valueOf(currentCell
									.getNumericCellValue()));
						}
						break;
					case 7:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							shopify.setVendor(currentCell.getStringCellValue());
						} else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
							shopify.setVendor(String.valueOf(currentCell
									.getNumericCellValue()));
						}
						break;
					default:
						break;
					}
				}
				if(shopify.getTitle() != null && !shopify.getTitle().isEmpty()){
					lsShopify.add(shopify);
				}
			}
		List<Item> items = new ArrayList<Item>();
		int i = 1000;
		for (Shopify shopify : lsShopify) {
			Item item = new Item();
			item.setId(String.valueOf(i));
			String size = shopify.getSize();
			String title = shopify.getTitle();
			String tileItem = title.concat(" ").concat(size);
			item.setTitle(tileItem);
			item.setDescription(shopify.getBody());
			item.setLink(shopurl.concat("/products/").concat(shopify.getHandle()));
			item.setImage_link(shopify.getImageSrc());
			item.setPrice(shopify.getPrice());
			item.setCondition("new");
			item.setAvailability("in stock");
			item.setIdentifier_exists("TRUE");
			item.setBrand(shopify.getVendor());
			String barcode = String.valueOf(shopify.getBarcode());
			item.setGtin(barcode);
			items.add(item);
			i++;
		}
		Merchant merchant = new Merchant();
		merchant.setTitle(shopname);
		merchant.setLink(shopurl);
		merchant.setDescription("");
		merchant.setItems(items);
		JAXBContext jaxbContext = JAXBContext.newInstance(Merchant.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	     
	    //Marshal the employees list in console
	    jaxbMarshaller.marshal(merchant, System.out);
	     
	    //Marshal the employees list in file
	    jaxbMarshaller.marshal(merchant, new File("D:\\merchant.xml"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/convertfile";
	}

	@RequestMapping(value = "/admin/deleteorder", method = RequestMethod.POST)
	public String deleteOrder(Model model, @RequestParam(name = "id") int id) {
		Order orderDelete = orderService.findById(id);
		orderService.deleteOrder(orderDelete);
		return "redirect:/admin/all";
	}

	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String login(Model model) {
		return "login";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String register(Model model,
			@RequestParam("username") String username,
			@RequestParam("password") String password,
			@RequestParam(name = "phone", required = false) String phone,
			@RequestParam(name = "fullname", required = false) String fullname,
			@RequestParam(name = "address", required = false) String address,
			@RequestParam(name = "product", required = false) String product) {
		User user = new User();
		if (username.contains("admin")) {
			user.setIsAdmin(Constants.ROLE_ADMIN);
		} else {
			user.setIsAdmin(Constants.ROLE_SUPPLIER);
		}
		user.setUsername(username);
		user.setPassword(password);
		user.setPhone(phone);
		user.setFullname(fullname);
		user.setAddress(address);
		user.setProduct(product);
		userService.saveUser(user);
		return "login";
	}

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String register(Model model) {
		return "register";
	}

	@RequestMapping(value = "/error", method = RequestMethod.GET)
	public String error(Model model) {
		return "error";
	}

	@RequestMapping(value = "/access-denied", method = RequestMethod.GET)
	public String access(Model model) {
		return "accessdenied";
	}
}
