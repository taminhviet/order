package shoes.sup.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.google.api.services.content.ShoppingContent;
import com.google.api.services.content.model.AccountStatus;
import com.google.api.services.content.model.AccountStatusAccountLevelIssue;
import com.google.api.services.content.model.Datafeed;
import com.google.api.services.content.model.DatafeedTarget;
import com.google.api.services.content.model.DatafeedsFetchNowResponse;
import com.google.api.services.content.model.DatafeedsListResponse;
import com.google.api.services.content.model.FreeListingsProgramStatus;
import com.google.api.services.content.model.FreeListingsProgramStatusRegionStatus;
import com.google.api.services.content.model.RequestReviewFreeListingsRequest;
import com.google.api.services.content.model.RequestReviewShoppingAdsRequest;
import com.google.api.services.content.model.ShoppingAdsProgramStatus;
import com.google.api.services.content.model.ShoppingAdsProgramStatusRegionStatus;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.icoderman.woocommerce.ApiVersionType;
import com.icoderman.woocommerce.EndpointBaseType;
import com.icoderman.woocommerce.WooCommerce;
import com.icoderman.woocommerce.WooCommerceAPI;
import com.icoderman.woocommerce.oauth.OAuthConfig;
import com.paypal.base.rest.APIContext;

import shoes.sup.model.AmountCapture;
import shoes.sup.model.Authorizations;
import shoes.sup.model.BillingWoo;
import shoes.sup.model.Capture;
import shoes.sup.model.CaptureFail;
import shoes.sup.model.CaptureFailDetail;
import shoes.sup.model.CaptureResponse;
import shoes.sup.model.DayPP;
import shoes.sup.model.Dispute;
import shoes.sup.model.DisputeAPI;
import shoes.sup.model.DisputeAPIOutcome;
import shoes.sup.model.DisputeAmount;
import shoes.sup.model.DisputeDetailAPI;
import shoes.sup.model.DisputeTransaction;
import shoes.sup.model.GMC;
import shoes.sup.model.LineItemWoo;
import shoes.sup.model.Link;
import shoes.sup.model.ListDisputeAPI;
import shoes.sup.model.MetaWoo;
import shoes.sup.model.OfferReturn;
import shoes.sup.model.Order;
import shoes.sup.model.OrderWoo;
import shoes.sup.model.PaypalAccount;
import shoes.sup.model.ProductGMC;
import shoes.sup.model.ProductWoo;
import shoes.sup.model.PublishableKey;
import shoes.sup.model.ReturnShippingAddress;
import shoes.sup.model.SaleAPI;
import shoes.sup.model.ShippingWoo;
import shoes.sup.model.Store;
import shoes.sup.model.WooNote;
import shoes.sup.model.WooPPCheckout;
import shoes.sup.model.WooStripe;
import shoes.sup.service.DayPPService;
import shoes.sup.service.DisputeService;
import shoes.sup.service.EmailService;
import shoes.sup.service.GMCService;
import shoes.sup.service.OrderService;
import shoes.sup.service.PaypalService;
import shoes.sup.service.ProductGMCService;
import shoes.sup.service.StoreService;
import shoes.sup.util.Constants;
import shopping.common.AuthenticatorGMC;

@Component
public class ScheduleController {

	@Autowired
	StoreService storeService;

	@Autowired
	OrderService orderService;

	@Autowired
	PaypalService paypalService;

	@Autowired
	DisputeService disputeService;

	@Autowired
	DayPPService dayPPService;

	@Autowired
	GMCService gmcService;
	
	@Autowired
	ProductGMCService productGmcService;
	
	@Autowired
    private EmailService emailService;

	@Scheduled(fixedRate = 50000000)
	public void disputeSchedule() {
		List<PaypalAccount> lsPaypal = paypalService.findByActive(Constants.ACTIVE);
		try {
			if (lsPaypal != null && !lsPaypal.isEmpty()) {
				for (PaypalAccount paypalAccount : lsPaypal) {
					try {
						String clientID = paypalAccount.getClientid();
						String clientSecret = paypalAccount.getClientsecret();
						APIContext apiContext = new APIContext(clientID, clientSecret, Constants.EXECUTION_MODE);
						String fetch = apiContext.fetchAccessToken();
						List<DisputeAPI> lsDisputeAPI = new ArrayList<DisputeAPI>();
						Gson gson = new Gson();
						for (int i = 1; i < 2; i++) {
							HttpClient client = HttpClientBuilder.create().build();
							String url = Constants.LIST_DISPUTE + "?page_size=50&page=" + i;
							HttpGet httpGet = new HttpGet(url);
							httpGet.setHeader(Constants.AUTHOR, fetch);
							httpGet.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
							HttpResponse response = client.execute(httpGet);
							if (response.getStatusLine().getStatusCode() != 200) {
								continue;
							}
							BufferedReader br = new BufferedReader(
									new InputStreamReader((response.getEntity().getContent())));

							String output;
							String listDispute = null;
							while ((output = br.readLine()) != null) {
								listDispute = output;
							}
							Type collectionTypeDispute = new TypeToken<ListDisputeAPI>() {
							}.getType();
							ListDisputeAPI listDisputeAPI = gson.fromJson(listDispute, collectionTypeDispute);
							lsDisputeAPI.addAll(listDisputeAPI.getItems());
						}

						if (!lsDisputeAPI.isEmpty()) {
							if (lsDisputeAPI != null && !lsDisputeAPI.isEmpty()) {
								for (DisputeAPI disputeAPI : lsDisputeAPI) {
									String disputeId = disputeAPI.getDispute_id();
//									System.out.println(disputeId);
									String disputeState = disputeAPI.getDispute_state();
									String disputeReason = disputeAPI.getReason();
									String disputeStatus = disputeAPI.getStatus();

									APIContext apiContextNew = new APIContext(clientID, clientSecret,
											Constants.EXECUTION_MODE);
									fetch = apiContextNew.fetchAccessToken();
									URL urlDispute = new URL(Constants.LIST_DISPUTE + "/" + disputeId);
									HttpURLConnection connDispute = (HttpURLConnection) urlDispute.openConnection();
									connDispute.setRequestProperty(Constants.AUTHOR, fetch);

									connDispute.setRequestProperty(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
									connDispute.setRequestMethod(Constants.GET_METHOD);
									String outputDispute;
									StringBuffer responseDispute = new StringBuffer();
									try {
										BufferedReader inDispute = new BufferedReader(
												new InputStreamReader(connDispute.getInputStream()));
										while ((outputDispute = inDispute.readLine()) != null) {
											responseDispute.append(outputDispute);
										}
										inDispute.close();
									} catch (Exception e) {
//										System.out.println(paypalAccount.getEmail());
//										System.out.println(e.getMessage());
										continue;
									}
									// printing result from response
									String disputeDetail = responseDispute.toString();

									Type collectionTypeDisputeDetail = new TypeToken<DisputeDetailAPI>() {
									}.getType();
									DisputeDetailAPI disputeDetailAPI = gson.fromJson(disputeDetail,
											collectionTypeDisputeDetail);
									List<DisputeTransaction> lsTransaction = disputeDetailAPI.getDisputed_transactions();
									DisputeTransaction disputeTransaction = lsTransaction.get(0);
									// String buyer_transaction_id =
									// disputeTransaction.getBuyer_transaction_id();
									String seller_transaction_id = disputeTransaction.getSeller_transaction_id();
									String seller_response_due_date = disputeDetailAPI.getSeller_response_due_date();
									shoes.sup.model.Dispute checkExist = disputeService.findByDisputeId(disputeId);
									if (checkExist != null) {
										checkExist.setReason(disputeReason);
										checkExist.setStatus(disputeStatus);
										if (Constants.DISPUTE_RESOLVED.equalsIgnoreCase(disputeStatus)) {
											DisputeAPIOutcome disputeAPIOutcome = disputeDetailAPI.getDispute_outcome();
											if (disputeAPIOutcome != null) {
												checkExist.setOutcome_code(disputeAPIOutcome.getOutcome_code());
											}
										}
										checkExist.setDispute_state(disputeState);
										checkExist.setSeller_transaction_id(seller_transaction_id);
										checkExist.setSeller_response_due_date(seller_response_due_date);
										DisputeAmount disputeAmount = disputeDetailAPI.getDispute_amount();
										checkExist.setCurrency(disputeAmount.getCurrency_code());
										checkExist.setValue(disputeAmount.getValue());
										disputeService.saveDispute(checkExist);
									} else {
										if (!Constants.DISPUTE_RESOLVED.equalsIgnoreCase(disputeStatus)) {
											shoes.sup.model.Dispute dispute = new Dispute();
											dispute.setDisputeid(disputeId);
											dispute.setReason(disputeReason);
											dispute.setStatus(disputeStatus);
											dispute.setDispute_state(disputeState);
											dispute.setSeller_transaction_id(seller_transaction_id);
											dispute.setSeller_response_due_date(seller_response_due_date);
											DisputeAmount disputeAmount = disputeDetailAPI.getDispute_amount();
											dispute.setCurrency(disputeAmount.getCurrency_code());
											dispute.setValue(disputeAmount.getValue());
											disputeService.saveDispute(dispute);
										}
									}
									Order order = orderService.findByTransaction_id(seller_transaction_id);
									if (order != null
											&& !Constants.DISPUTE_RESOLVED.equalsIgnoreCase(order.getDisputed())) {
										order.setDisputed(Constants.DISPUTED);
										order.setDispute_id(disputeId);
										order.setDispute_reason(disputeReason);
										order.setDispute_state(disputeState);
										order.setDispute_status(disputeStatus);
										if (Constants.MERCHANDISE_OR_SERVICE_NOT_AS_DESCRIBED
												.equalsIgnoreCase(disputeReason)) {
											List<Link> links = disputeDetailAPI.getLinks();
											for (Link link : links) {
												if (Constants.acknowledge_return_item.equalsIgnoreCase(link.getRel())) {
													order.setReturned_by_customer(Constants.CUSTOMER_RETURNED);
													order.setStatus(Constants.RETURNED);
												}
											}
										}
										DisputeAPIOutcome disputeAPIOutcome = disputeDetailAPI.getDispute_outcome();
										if (disputeAPIOutcome != null
												&& Constants.RESOLVED_BUYER_FAVOUR
														.equalsIgnoreCase(disputeAPIOutcome.getOutcome_code())
												&& !Constants.RESOLVED_BUYER_FAVOUR
														.equalsIgnoreCase(order.getOutcome_code())) {
											String capture_total = paypalAccount.getCapturetotal();
//											System.out.println(paypalAccount.getEmail() + " : " + capture_total);
											String price = order.getPrice();
											int open = price.indexOf("(");
											double priceRoot = Double.valueOf(price.substring(0, open));
											double newTotal = 0;
											if (capture_total != null) {
												newTotal = Double.valueOf(capture_total) - priceRoot;
											}
											if (newTotal <= 0) {
												paypalAccount.setStatus(Constants.DEACTIVE);
											}
											paypalAccount.setCapturetotal(String.valueOf(newTotal));
											paypalService.savePaypalAccount(paypalAccount);
										}
										if (Constants.DISPUTE_RESOLVED.equalsIgnoreCase(disputeState)
												|| Constants.DISPUTE_APPEALABLE.equalsIgnoreCase(disputeState)) {
											if (disputeAPIOutcome != null) {
												order.setOutcome_code(disputeAPIOutcome.getOutcome_code());
											}
										}
										order.setSeller_response_due_date(seller_response_due_date);
										if (order.getPay_pal_account_id() == null) {
											order.setPay_pal_account_id(paypalAccount.getEmail());
										}
										orderService.saveOrder(order);
									}
								}
							}
						}
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Scheduled(fixedRate = 1800000)
	public void scheduleTaskWoo() {
		List<Store> lsStore = storeService.searchByType(Constants.WOO_COMMERCE, Constants.ACTIVE);
		List<Order> existOrder = orderService.findByType(Constants.WOO_COMMERCE);
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		List<String> lsExist = new ArrayList<String>();
		for (Order order : existOrder) {
			lsExist.add(order.getOname());
		}
//		Calendar calendar = Calendar.getInstance();
//		boolean isNewDate = checkNewDate(calendar);
//		DateFormat f = new SimpleDateFormat("yyyy-MM-dd");
//		if(isNewDate){
//			List<PaypalAccount> lsPP = paypalService.findByActive(Constants.ACTIVE);
//			if(!lsPP.isEmpty()){
//				for (Iterator iterator = lsPP.iterator(); iterator.hasNext();) {
//					PaypalAccount paypalAccount = (PaypalAccount) iterator.next();
//					if(paypalAccount.getUsername()== null || paypalAccount.getUsername().isEmpty()){
//						iterator.remove();
//					}
//				}
//			}
//			//insert new PPDay
//			Date today = calendar.getTime();
//			String sdate= f.format(today);
//			for (PaypalAccount paypalAccount : lsPP) {
//				DayPP dayPPToday = dayPPService.findByDayByPP(paypalAccount.getUsername(),sdate);
//				if(dayPPToday == null) {
//					DayPP dayPPNew = new DayPP();
//					dayPPNew.setAccountpp(paypalAccount.getUsername());
//					dayPPNew.setDay(sdate);
//					dayPPNew.setDaytotal(0);
//					dayPPNew.setRound(1);
//					dayPPService.saveDayPP(dayPPNew);
//				}
//			}
//		}
		List<Order> listOrder = new ArrayList<Order>();
		try {
			for (Store store : lsStore) {
				String product = store.getProduct();
				List orders = orderTotal(store);

				String ppEmail = getWooPP(store);
//				String stripeKey = getWooStripe(store);
				if (ppEmail == null || ppEmail.isEmpty()) {
					ppEmail = store.getPpacc();
				}
				Gson gson = new Gson();
				for (Object object : orders) {
					String json = gson.toJson(object);
					OrderWoo orderWoo = gson.fromJson(json, OrderWoo.class);
					List<LineItemWoo> lsLineItem = orderWoo.getLine_items();
					if (!lsLineItem.isEmpty()) {
						for (LineItemWoo lineItemWoo : lsLineItem) {
							Order order = new Order();
							String storeName = store.getDomain();
							String storeprefix = store.getPrefix();
							if (storeprefix == null || storeprefix.isEmpty()) {
								storeprefix = "4";
							}
							String prefix = storeName.substring(0, Integer.valueOf(storeprefix));
							String oName = prefix + orderWoo.getId();
							order.setOname(oName);
							double price = orderWoo.getTotal();
							String currency = orderWoo.getCurrency();
							order.setPrice(String.valueOf(price).concat(" (").concat(currency).concat(")"));
							order.setOrderidapi(String.valueOf(orderWoo.getId()));
							order.setTransaction_id(orderWoo.getTransaction_id());
//							String paymentMethod = orderWoo.getPayment_method();
							String customer_Note = orderWoo.getCustomer_note();
//							if (!Constants.STRIPE.equalsIgnoreCase(paymentMethod)) {

//							} else {
//								order.setStripe_sk(stripeKey);
//								List<MetaWoo> metaData = orderWoo.getMeta_data();
//								for (MetaWoo meta : metaData) {
//									if (meta.getKey().contains("stripe_intent_id")) {
//										order.setStripe_intent_id(meta.getValue().toString());
//									}
//								}
//							}
							if (!lsExist.contains(oName)) {
								order.setOname(oName);
								order.setStoredomain(store.getDomain());
								order.setPhone(orderWoo.getBilling().getPhone());
								String email = orderWoo.getBilling().getEmail();

								formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
								String dateCreate = orderWoo.getDate_created();
								Date date = (Date) formatter.parse(dateCreate);
//								if (customer_Note == null || customer_Note.isEmpty()) {
//									order.setPay_pal_account_id(ppEmail);
//								} else {
//									if (customer_Note.contains(Constants.SLASH)) {
//										String[] noteInfo = customer_Note.split(Constants.SLASH);
//										String domainB = noteInfo[0];
//										if (domainB.contains("Gateways")) {
//											domainB = domainB.substring(15, domainB.length()).trim();
//										}
//										String clientID = noteInfo[1];
//										if (clientID.contains("Comments")) {
//											clientID = clientID.substring(0, clientID.length() - 19).trim();
//										}
//										order.setSiteb(domainB);
//										PaypalAccount ppal = null;
//										if (!clientID.contains("@")) {
//											ppal = paypalService.findByClientID(clientID);
//										} else {
//											ppal = paypalService.findByEmail(clientID);
//										}
//										if (ppal != null) {
//											order.setPay_pal_account_id(ppal.getEmail());
//											Calendar cal = Calendar.getInstance();
//											cal.add(cal.DATE, 21);
//											String wd = formatter.format(cal.getTime());
//											ppal.setWithdraw(wd);
//											ppal.setQueue(Constants.WAIT_21);
//											String capture_total = ppal.getCapturetotal();
//											if (capture_total == null) {
//												ppal.setCapturetotal(String.valueOf(price));
//											} else {
//												double newTotal = price + Double.valueOf(capture_total);
//												ppal.setCapturetotal(String.valueOf(newTotal));
//											}
//											paypalService.savePaypalAccount(ppal);
//										}
//									}
								order.setAdminnote(customer_Note);
//								}
								String emailPPNote = getEmailInNote(store, order, gson);
								if (emailPPNote != null) {
									String siteb = Constants.EMPTY;
									if (emailPPNote.contains(Constants.SLASH)) {
										String[] noteInfo = emailPPNote.split(Constants.SLASH);
										emailPPNote = noteInfo[0];
										siteb = noteInfo[1].trim();
										order.setSiteb(siteb);
									}
									PaypalAccount ppal = paypalService.findByEmail(emailPPNote);
									ppal.setLastdomain(siteb);
									order.setPay_pal_account_id(emailPPNote);
									Calendar cal = Calendar.getInstance();
									cal.add(cal.DATE, 21);
									String wd = formatter.format(cal.getTime());
									ppal.setWithdraw(wd);
									ppal.setQueue(Constants.WAIT_21);
									String capture_total = ppal.getCapturetotal();
									if (capture_total == null) {
										ppal.setCapturetotal(String.valueOf(price));
									} else {
										double newTotal = price + Double.valueOf(capture_total);
										ppal.setCapturetotal(String.valueOf(newTotal));
									}
									paypalService.savePaypalAccount(ppal);
								}
								order.setCreateDate(date);
								if (email != null && !email.isEmpty()) {
									order.setEmail(email);
								}
								order.setOrderidapi(String.valueOf(orderWoo.getId()));
								// set shipping address
								ShippingWoo shippingAddress = orderWoo.getShipping();
								if (shippingAddress != null && !shippingAddress.getAddress_1().isEmpty()) {
									StringBuilder sb = new StringBuilder();
									sb.append("Name: ");
									sb.append(shippingAddress.getFirst_name());
									sb.append(" ");
									sb.append(shippingAddress.getLast_name());
									order.setAddressname(
											shippingAddress.getFirst_name() + " " + shippingAddress.getLast_name());
									sb.append(", Address 1: ");
									sb.append(shippingAddress.getAddress_1());
									order.setAddress1(shippingAddress.getAddress_1());
									sb.append(", Address 2: ");
									sb.append(shippingAddress.getAddress_2());
									order.setAddress2(shippingAddress.getAddress_2());
									sb.append(", City: ");
									sb.append(shippingAddress.getCity());
									order.setCity(shippingAddress.getCity());
									sb.append(", Zip: ");
									sb.append(shippingAddress.getPostcode());
									order.setZip(shippingAddress.getPostcode());
									sb.append(", Province:");
									sb.append(shippingAddress.getState());
									order.setProvince(shippingAddress.getState());
									sb.append(", Country: ");
									sb.append(shippingAddress.getCountry());
									order.setCountry(shippingAddress.getCountry());
									order.setAddress(sb.toString());
								} else {
									BillingWoo billingWoo = orderWoo.getBilling();
									StringBuilder sb = new StringBuilder();
									sb.append("Name: ");
									sb.append(billingWoo.getFirst_name());
									sb.append(" ");
									sb.append(billingWoo.getLast_name());
									order.setAddressname(billingWoo.getFirst_name() + " " + billingWoo.getLast_name());
									sb.append(", Address 1: ");
									sb.append(billingWoo.getAddress_1());
									order.setAddress1(billingWoo.getAddress_1());
									sb.append(", Address 2: ");
									sb.append(billingWoo.getAddress_2());
									order.setAddress2(billingWoo.getAddress_2());
									sb.append(", City: ");
									sb.append(billingWoo.getCity());
									order.setCity(billingWoo.getCity());
									sb.append(", Zip: ");
									sb.append(billingWoo.getPostcode());
									order.setZip(billingWoo.getPostcode());
									sb.append(", Province:");
									sb.append(billingWoo.getState());
									order.setProvince(billingWoo.getState());
									sb.append(", Country: ");
									sb.append(billingWoo.getCountry());
									order.setCountry(billingWoo.getCountry());
									order.setAddress(sb.toString());
								}
								int product_id = lineItemWoo.getProduct_id();
								StringBuilder sb = new StringBuilder();
								String www = store.getW();
								if (www == null) {
									www = Constants.EMPTY;
								}
								if (www.equalsIgnoreCase(Constants.NO_WWW)
										|| www.equalsIgnoreCase(Constants.FALSE_WWW)) {
									sb.append("https://");
								} else {
									sb.append("https://www.");
								}
								sb.append(storeName);
								sb.append("/wp-json/wc/v3/products/");
								sb.append(product_id);
								sb.append("?consumer_key=");
								sb.append(store.getApikey());
								sb.append("&consumer_secret=");
								sb.append(store.getPassword());
								String urlProduct = sb.toString();
								String productDetail = getWooInfo(urlProduct);
								if (!productDetail.isEmpty()) {
									ProductWoo productWoo = gson.fromJson(productDetail, ProductWoo.class);
									String imageUrl = productWoo.getImages().get(0).getSrc();
									String style = Constants.EMPTY;
									if (productWoo.getCategories() != null && !productWoo.getCategories().isEmpty()) {
										style = productWoo.getCategories().get(0).getName();
									}
									order.setStyle(style);
									order.setImageurl(imageUrl);
								}
								String product_name = lineItemWoo.getName();
								order.setProductname(product_name);
								int quantity = lineItemWoo.getQuantity();
								order.setQuantity(String.valueOf(quantity));
								String variant = Constants.EMPTY;
								if (product_name.contains("-")) {
									int indexm = product_name.lastIndexOf("-");
									variant = product_name.subSequence(indexm + 1, product_name.length()).toString()
											.trim();
								}
								if (variant.isEmpty()) {
									List<MetaWoo> lsMetaWoo = lineItemWoo.getMeta_data();
									if (!lsMetaWoo.isEmpty()) {
										MetaWoo metaVarient = lsMetaWoo.get(0);
										if (metaVarient != null) {
											variant = metaVarient.getValue().toString();
											if (variant.length() >= 10) {
												variant = variant.substring(0, 10).trim();
											}
											order.setProductname(order.getProductname() + variant);
										}
									}
								}
								order.setVariant(variant);

								order.setApistatus(orderWoo.getStatus());
								if (product != null) {
									order.setProduct(product);
								}
								String status = orderWoo.getStatus();
								order.setPayment_status(status);
								if (Constants.WOO_PROCESSING.equalsIgnoreCase(status)
										|| Constants.WOO_ON_HOLD.equalsIgnoreCase(status)) {
									if (Constants.WOO_PROCESSING.equalsIgnoreCase(status)) {
										order.setCapture_status(Constants.COMPLETED);
									}
									listOrder.add(order);
								}
								// update change PP
//								updatePPWoo(f, ppEmail, price, store);
//								updateChangePPWoo(ppEmail, price, store);
							}
						}
					}
				}
				Collections.reverse(listOrder);
				boolean isUsePPJump = false;
				for (Order order : listOrder) {
					order.setStatus(Constants.NEW_ORDER);
					order.setType(Constants.WOO_COMMERCE);
					String siteB = order.getSiteb();
					String ppAcc = order.getPay_pal_account_id();
					if (siteB != null && ppAcc != null && !siteB.isEmpty() && !ppAcc.isEmpty()) {
						isUsePPJump = true;
					}
					String ppEmailSave = order.getPay_pal_account_id();
					if (ppEmailSave != null) {
						isUsePPJump = true;
					}
					orderService.saveOrder(order);
				}

				// start change PP
				if (!listOrder.isEmpty() && !isUsePPJump) {
					String ppAcc = store.getPpacc();
					PaypalAccount paypalAccount = paypalService.findByEmail(ppEmail);
					Calendar cal = Calendar.getInstance();
					cal.add(cal.DATE, 21);
					String wd = formatter.format(cal.getTime());
					paypalAccount.setWithdraw(wd);
					paypalAccount.setQueue(Constants.WAIT_21);
					paypalService.savePaypalAccount(paypalAccount);
					String userName = paypalAccount.getUsername();
					String commaUsername = Constants.COMMA.concat(userName);
					if (ppAcc.contains(commaUsername)) {
						ppAcc = ppAcc.replace(commaUsername, Constants.EMPTY);
					} else {
						String commaUserName2 = userName.concat(Constants.COMMA);
						if (ppAcc.contains(commaUserName2)) {
							ppAcc = ppAcc.replace(commaUserName2, Constants.EMPTY);
						}
					}
					store.setPpacc(ppAcc);
					storeService.saveStore(store);
					boolean isChange = false;
					if (ppAcc.contains(Constants.COMMA)) {
						String[] accPPArray = ppAcc.split(Constants.COMMA);
						for (String ppusername : accPPArray) {
							PaypalAccount newPaypalAccount = paypalService.findByUsername(ppusername);
							if (newPaypalAccount != null) {
								isChange = changePPWoo(store, newPaypalAccount);
								if (isChange) {
									break;
								}
							}
						}
					} else {
						PaypalAccount newPaypalAccount = paypalService.findByUsername(ppAcc);
						if (newPaypalAccount != null) {
							isChange = changePPWoo(store, newPaypalAccount);
							if (isChange) {
								break;
							}
						}
					}

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Scheduled(fixedRate = 3600000)
	public void cronPaypalCheck() {
		List<PaypalAccount> lsPP = paypalService.findByActive(Constants.ACTIVE);
		if (!lsPP.isEmpty()) {
			for (Iterator iterator = lsPP.iterator(); iterator.hasNext();) {
				PaypalAccount paypalAccount = (PaypalAccount) iterator.next();
				if (paypalAccount.getUsername() == null || paypalAccount.getUsername().isEmpty()) {
					iterator.remove();
				}
			}
		}
		for (PaypalAccount paypalAccount : lsPP) {
			StringBuilder sb = new StringBuilder();
			String userName = paypalAccount.getUsername();
			sb.append("https://api-3t.paypal.com/nvp?&user=");
			sb.append(userName);
			sb.append("&pwd=");
			sb.append(paypalAccount.getPassword());
			sb.append("&signature=");
			sb.append(paypalAccount.getSignature());
			sb.append(
					"&version=70.0&METHOD=SetExpressCheckout&RETURNURL=http://www.paypal.com/test.php&CANCELURL=http://www.paypal.com/test.php&PAYMENTACTION=Sale&AMT=50&CURRENCYCODE=USD");
			String url = sb.toString();
			HttpClient client = HttpClientBuilder.create().build();
			HttpGet httpGet = new HttpGet(url);
			try {
				HttpResponse response = client.execute(httpGet);
				BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));

				String output;
				String statuscheck = null;
				while ((output = br.readLine()) != null) {
					statuscheck = output;
				}
				if (statuscheck.contains("Success")) {
					paypalAccount.setStatus(Constants.PP_LIVE);
					paypalService.savePaypalAccount(paypalAccount);
				} else {
					paypalAccount.setStatus(Constants.PP_LIMIT);
					//paypalAccount.setActive(Constants.DEACTIVE);
					paypalService.savePaypalAccount(paypalAccount);
					List<Order> lsOrderLimit = orderService.findByPaypalAccountid(paypalAccount.getEmail());
					for (Order ol : lsOrderLimit) {
						ol.setPaypalstatus(Constants.PP_LIMIT);
						orderService.saveOrder(ol);
					}
					emailService.sendEmail("sneakers.hotline@gmail.com", "Paypal Limit Account", paypalAccount.getEmail());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
//
//	// check payer can not pay
//	@Scheduled(fixedDelay = 5000000)
//	public void checkPayerFail() {
//		List<Order> lsOrder = orderService.findPayerFail(Constants.CAPTURED_FAILED, Constants.PAYER_CANNOT_PAY);
//		Gson gson = new Gson();
//		for (Order order : lsOrder) {
//			capturePaypal(order, gson, null);
//		}
//	}
//
//	@Scheduled(fixedDelay = 36000000)
//	public void autocapture(){
//		List<Order> lsOrder = orderService.findOnHold(Constants.WOO_ON_HOLD);
//		Gson gson = new Gson();
//		for (Order order : lsOrder) {
//			Date dateBuy = order.getCreateDate();
//			boolean isACaptured = inDays(dateBuy);
//			if(isACaptured){
//				capturePaypal(order, gson, null);
//			}
//		}
//	}

	// auto return refund
//		@Scheduled(fixedDelay = 100000000)
//		public void autoreturn(){
//			List<Order> lsOrder = orderService.findByDisputed(Constants.DISPUTED);
//			for (Iterator iterator = lsOrder.iterator(); iterator.hasNext();) {
//				Order order = (Order) iterator.next();
//				if (!Constants.MERCHANDISE_OR_SERVICE_NOT_AS_DESCRIBED
//						.equalsIgnoreCase(order.getDispute_reason()) || !Constants.WAITING_FOR_SELLER_RESPONSE.equalsIgnoreCase(order.getDispute_status())) {
//					iterator.remove();
//				}
//			}
//			for (Order order : lsOrder) {
//				String email = order.getPay_pal_account_id();
//				PaypalAccount paypalAccount = paypalService.findByEmail(email);
//				String address = paypalAccount.getAddress();
//				if(address != null && !address.isEmpty()) {
//					returnRefund(order,paypalAccount);
//				}
//			}
//		}
	@Scheduled(fixedRate = 7200000)
	public void cronCyberCheck() {
		List<String> hostCyber = new ArrayList<>();
		hostCyber.add("81.0.248.99");
		hostCyber.add("154.12.236.187");
		hostCyber.add("154.38.191.254");
		hostCyber.add("194.163.182.175");
		hostCyber.add("45.10.163.25");
		hostCyber.add("144.126.150.61");
		hostCyber.add("158.220.86.84");
		hostCyber.add("157.173.118.40");
		for (String host : hostCyber) {
			boolean isConnected = checkCyberPanel(host);
			if(!isConnected) {
				emailService.sendEmail("sneakers.hotline@gmail.com", "Cyber Panel Down", host);
			}
		}
		
	}
	@Scheduled(fixedRate = 18000000)
	public void cronGMCCheck() {
		List<GMC> lsGMC = gmcService.findNotDisable();
		for (GMC gmc : lsGMC) {
			String previousStatus = gmc.getEligibility();
			String statusGMC = checkAccountStatus(gmc);
			if (Constants.SUSPENDED.equals(statusGMC) || Constants.REACTIVE.equals(statusGMC)) {
				gmc.setStatus(statusGMC);
			}
			String status = gmc.getStatus();
			String eligible = gmc.getRevieweligibility();
			String vps_server = gmc.getVpsserver();
			if (Constants.SUSPENDED.equals(statusGMC)){
//				if(!"VPS_FR".equals(vps_server) && "VPS_DE".equals(vps_server)){
//					setFeedCountryJP(gmc);
//				}
				// delete all sup feed
				deleteSupfeedProduct(gmc);
			}
			if (Constants.SUSPENDED.equals(status) && Constants.ELIGIBLE.equals(eligible)) {
				// start review
				requestReviewGMC(gmc);
			}
			String country = gmc.getCountry();
//			if("VPS_FR".equals(vps_server)){
//				country = "FR";
//				gmc.setCountry(country);
//			}else if("VPS_DE".equals(vps_server)){
//				country = "DE";
//				gmc.setCountry(country);
//			}
			GMC updatestatus = checkGoogleProgram(gmc, country);
			if(Constants.PENDING_REVIEW.equals(previousStatus) && Constants.APPROVED.equals(updatestatus.getEligibility())) {
				String note = gmc.getNote();
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

		        // Get today's date
		        LocalDate today = LocalDate.now();

		        // Format the date as a string
		        String formattedDate = today.format(formatter);
		        if(note == null) {
		        	note =  Constants.EMPTY;
		        }
		        note = note + "Approved: " + formattedDate;
		        updatestatus.setNote(note);
			}
//			if (updatestatus.getRevieweligibility() != null
//					&& updatestatus.getRevieweligibility().contains("UNSPECIFIED")) {
//				// set Country to JP
//				updatestatus = checkGoogleProgram(gmc, "JP");
//				if (updatestatus.getRevieweligibility() != null
//						&& !updatestatus.getRevieweligibility().contains("UNSPECIFIED")) {
//					if(!"VPS_FR".equals(vps_server) && "VPS_DE".equals(vps_server)){
//						updatestatus.setCountry("JP");
//					}
//				}
//			}
			if(Constants.SUSPENDED.equals(status) && Constants.INELIGIBLE_GMC.equals(eligible)) {
				updatestatus.setStatus(Constants.DISABLE);
			}
			gmcService.saveGMC(updatestatus);
		}
	}
	
	private boolean checkCyberPanel(String url) {
		try {
            Socket socket = new Socket(url, 8090);
            socket.close();
            return true;
        } catch (Exception e) {
        	return false;
        }
	}
	
	//set feed Country JP
	private void setFeedCountryJP(GMC gmc) {
		try {
			List<Datafeed> lsFeed = listDatafeedsForMerchant(gmc);
			for (Datafeed datafeed : lsFeed) {
				long feedID = datafeed.getId();
				String country = "JP";
				List<DatafeedTarget> targets = datafeed.getTargets();
				for (DatafeedTarget target : targets) {
					List<String> targetCountry = target.getTargetCountries();
					if(targetCountry.size() >= 2) {
						String message = updatefeedGMC(gmc, feedID, country);
						if(Constants.SUCCESSED.equals(message)) {
							fetchGMC(gmc, feedID);
						}
					}
				}
			}

		} catch (Exception e2) {
			e2.printStackTrace();
		}
	}
	private void deleteSupfeedProduct(GMC gmc) {
		List<ProductGMC> lsProductGMCId = productGmcService.findByGmcid(gmc.getMerchantId());
		List<ProductGMC> lsDeleted = new ArrayList<>();
		for (ProductGMC productGMC : lsProductGMCId) {
			String tmtitle = productGMC.getProducttmtitle();
			double productPrice = Double.valueOf(productGMC.getPrice());
			if(productPrice >= 99 || (tmtitle != null && !tmtitle.isEmpty())) {
				lsDeleted.add(productGMC);
			}
		}
		for (ProductGMC productGMCdelete : lsDeleted) {
			deleteSupfeedsGMC(gmc, productGMCdelete.getProductid(),productGMCdelete);
		}
	}
	private boolean deleteSupfeedsGMC(GMC gmc, String productid,ProductGMC productdelete) {
		try {
			AuthenticatorGMC gmcAuth = new AuthenticatorGMC(gmc);
			ShoppingContent content = gmcAuth.initBuilder();
			Long merchantInt = Long.valueOf(gmc.getMerchantId());
			String supfeedid = gmc.getSupfeedid();
			List<String> lssupfeed = new ArrayList<>();
			if(supfeedid.contains(Constants.COMMA)) {
				supfeedid = productdelete.getSupfeedid();
				if(supfeedid == null || supfeedid.isEmpty()) {
					supfeedid = gmc.getSupfeedid();
					String[] arraySupFeed = supfeedid.split(Constants.COMMA);
					for (String sup : arraySupFeed) {
						if(sup.contains(":")) {
							sup = sup.substring(supfeedid.indexOf(":")+1, sup.length());
							lssupfeed.add(sup);
						}
					}
				}else {
					lssupfeed.add(supfeedid);
				}
			}
			for (String supfeed : lssupfeed) {
				try {
					Long supfeedInt = Long.valueOf(supfeed);
					content.products().delete(BigInteger.valueOf(merchantInt), productid)
					.set("feedId", BigInteger.valueOf(supfeedInt)).execute();
				} catch (Exception e) {
					e.fillInStackTrace();
				}
			}
		} catch (Exception e) {
			e.fillInStackTrace();
			return false;
		}
		return true;
	}
	private String fetchGMC(GMC gmc, long feedid) {
		String message = Constants.FAILED;
		try {
			AuthenticatorGMC gmcAuth = new AuthenticatorGMC(gmc);
			Long gmcID = Long.valueOf(gmc.getMerchantId());
			ShoppingContent content = gmcAuth.initBuilder();
			DatafeedsFetchNowResponse fetch = content.datafeeds()
					.fetchnow(BigInteger.valueOf(gmcID), BigInteger.valueOf(feedid)).execute();
			message = Constants.SUCCESSED;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return message;
	}
	private String updatefeedGMC(GMC gmc, long feedid, String country) {
		String message = Constants.FAILED;
		try {
			AuthenticatorGMC gmcAuth = new AuthenticatorGMC(gmc);
			Long gmcID = Long.valueOf(gmc.getMerchantId());
			ShoppingContent content = gmcAuth.initBuilder();
			List<Datafeed> lsFeed = listDatafeedsForMerchant(gmc);
			List<String> targetCountry = new ArrayList<>();
			if(country.contains(Constants.HYPHEN)) {
				String[] arrayCountry = country.split(Constants.HYPHEN);
				for (String newcountry : arrayCountry) {
					targetCountry.add(newcountry);
				}
			}else {
				targetCountry.add(country);
			}
			for (Datafeed datafeed : lsFeed) {
				long feedIDExist = datafeed.getId();
				if(feedid == feedIDExist) {
					datafeed.getTargets().get(0).setTargetCountries(targetCountry);
					Datafeed result = content.datafeeds().update(BigInteger.valueOf(gmcID),BigInteger.valueOf(feedid), datafeed).execute();
				}
			}
			message = Constants.SUCCESSED;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return message;
	}
	private List<Datafeed> listDatafeedsForMerchant(GMC gmc) throws IOException {
		List<Datafeed> lsFeed = new ArrayList<>();
		AuthenticatorGMC gmcAuth = new AuthenticatorGMC(gmc);
		Long gmcID = Long.valueOf(gmc.getMerchantId());
		ShoppingContent content = gmcAuth.initBuilder();
		com.google.api.services.content.ShoppingContent.Datafeeds.List datafeedsList = content.datafeeds()
				.list(BigInteger.valueOf(gmcID));
		DatafeedsListResponse page = null;

		do {
			if (page != null) {
				datafeedsList.setPageToken(page.getNextPageToken());
			}
			page = datafeedsList.execute();
			if (page.getResources() == null) {
			}else {
				for (Datafeed datafeed : page.getResources()) {
					lsFeed.add(datafeed);
				}
			}
			
			
		} while (page.getNextPageToken() != null);
		return lsFeed;
	}
	
	private void requestReviewGMC(GMC gmc) {
		try {
			AuthenticatorGMC gmcAuth = new AuthenticatorGMC(gmc);
			ShoppingContent content = gmcAuth.initBuilder();
			Long gmcID = Long.valueOf(gmc.getMerchantId());
			String suspendedType = gmc.getSuspendedtype();
			if (suspendedType != null && Constants.GOOGLE_ADS.equals(suspendedType)) {
				RequestReviewShoppingAdsRequest request = new RequestReviewShoppingAdsRequest();
				request.setRegionCode(gmc.getCountry());
				content.shoppingadsprogram().requestreview(gmcID, request).execute();
			} else {
				RequestReviewFreeListingsRequest request = new RequestReviewFreeListingsRequest();
				request.setRegionCode(gmc.getCountry());
				content.freelistingsprogram().requestreview(gmcID, request).execute();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String checkAccountStatus(GMC gmc) {
		String status = Constants.ENABLE;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        // Get today's date
        LocalDate today = LocalDate.now();

        // Format the date as a string
        String formattedDate = today.format(formatter);
		try {
			AuthenticatorGMC gmcAuth = new AuthenticatorGMC(gmc);
			ShoppingContent content = gmcAuth.initBuilder();
			Long intvalue = Long.valueOf(gmc.getMerchantId());
			String gmcStatus = gmc.getStatus();
			AccountStatus accountStatus = content.accountstatuses()
					.get(BigInteger.valueOf(intvalue), BigInteger.valueOf(intvalue)).execute();
			List<AccountStatusAccountLevelIssue> lsIssue = accountStatus.getAccountLevelIssues();
			if (lsIssue != null && !lsIssue.isEmpty()) {
				for (AccountStatusAccountLevelIssue accountStatusAccountLevelIssue : lsIssue) {
					status = accountStatusAccountLevelIssue.getTitle();
					if (status.contains(Constants.SUSPENDED) || status.contains(Constants.MISPRESENTATION)) {
						status = Constants.SUSPENDED;
						if(Constants.ENABLE.equals(gmcStatus)) {
							gmc.setSuspenddate(formattedDate);
							gmcService.saveGMC(gmc);
						}
						break;
					}
				}
			}
			if (Constants.SUSPENDED.equals(gmcStatus) && Constants.ENABLE.equals(status)) {
				status = Constants.REACTIVE;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	private GMC checkGoogleProgram(GMC gmc, String country) {
		String eligible = Constants.APPROVED;
		try {
			AuthenticatorGMC gmcAuth = new AuthenticatorGMC(gmc);
			ShoppingContent content = gmcAuth.initBuilder();
			Long gmcID = Long.valueOf(gmc.getMerchantId());
			FreeListingsProgramStatus programStatus = content.freelistingsprogram().get(gmcID).execute();
			if (programStatus != null && !programStatus.getGlobalState().equals("NO_OFFERS_UPLOADED")) {
				List<FreeListingsProgramStatusRegionStatus> lsStatus = programStatus.getRegionStatuses();
				for (FreeListingsProgramStatusRegionStatus freeListingProgramStatusRegionStatus : lsStatus) {
					List<String> countrycodes = freeListingProgramStatusRegionStatus.getRegionCodes();
					for (String countryCode : countrycodes) {
						if (country.equals(countryCode)) {
							String eligibilereview = freeListingProgramStatusRegionStatus.getReviewEligibilityStatus();
							if (eligibilereview == null) {
								eligibilereview = freeListingProgramStatusRegionStatus.getReviewIneligibilityReason();
							}
							eligible = freeListingProgramStatusRegionStatus.getEligibilityStatus();
							gmc.setEligibility(eligible);
							gmc.setRevieweligibility(eligibilereview);
							if (Constants.DISAPPROVED.equals(eligible)) {
								gmc.setSuspendedtype(Constants.FREE_LISTING);
							}
							break;
						}
					}

				}
			}
			//if (Constants.SUSPENDED.equals(gmc.getStatus()) && Constants.APPROVED.equals(eligible)) {
				 //start check google ads
//				ShoppingAdsProgramStatus adsStatus = content.shoppingadsprogram().get(gmcID).execute();
//				if (adsStatus != null) {
//					String globalState = adsStatus.getGlobalState();
//					if (Constants.ENABLED.equals(globalState)) {
//						List<ShoppingAdsProgramStatusRegionStatus> lsStatus = adsStatus.getRegionStatuses();
//						for (ShoppingAdsProgramStatusRegionStatus shoppingAdsProgramStatusRegionStatus : lsStatus) {
//							List<String> countrycodes = shoppingAdsProgramStatusRegionStatus.getRegionCodes();
//							for (String countryCode : countrycodes) {
//								if (country.equals(countryCode)) {
//									String eligibilereview = shoppingAdsProgramStatusRegionStatus
//											.getReviewEligibilityStatus();
//									if (eligibilereview == null) {
//										eligibilereview = shoppingAdsProgramStatusRegionStatus
//												.getReviewIneligibilityReason();
//									}
//									eligible = shoppingAdsProgramStatusRegionStatus.getEligibilityStatus();
//									gmc.setEligibility(eligible);
//									gmc.setRevieweligibility(eligibilereview);
//									if (Constants.DISAPPROVED.equals(eligible)) {
//										gmc.setSuspendedtype(Constants.GOOGLE_ADS);
//									}
//									break;
//								}
//							}
//						}
//					}
//				}
			//}
		} catch (Exception e) {
			eligible = Constants.FAILED;
			e.printStackTrace();
		}
		return gmc;
	}

	private boolean inDays(Date startDate) {
		Date today = new Date();
		long diff = today.getTime() - startDate.getTime();
		long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
		if (days >= 26 && days <= 27) {
			return true;
		}
		return false;
	}

	private String getWooInfo(String urlWoo) {
		String responseString = Constants.EMPTY;
		try {
			URL url = new URL(urlWoo);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.addRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
			conn.setRequestMethod("GET");

			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String output;

			StringBuffer response = new StringBuffer();
			while ((output = in.readLine()) != null) {
				response.append(output);
			}

			in.close();
			// printing result from response
			responseString = response.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseString;
	}

	private boolean changePPWoo(Store store, PaypalAccount paypalAccount) {
		try {
			StringBuilder sb = new StringBuilder();
			String www = store.getW();
			if (www == null) {
				www = Constants.EMPTY;
			}
			if (www.equalsIgnoreCase(Constants.NO_WWW) || www.equalsIgnoreCase(Constants.FALSE_WWW)) {
				sb.append("https://");
			} else {
				sb.append("https://www.");
			}
			sb.append(store.getDomain());
			sb.append("/wp-json/custom-plugin/paypal_update?key=80943f212d5a4182a45fa86c02cc622d&api_username=");
			sb.append(paypalAccount.getUsername());
			sb.append("&api_password=");
			sb.append(paypalAccount.getPassword());
			sb.append("&api_signature=");
			sb.append(paypalAccount.getSignature());
			String url = sb.toString();
			HttpClient client = HttpClientBuilder.create().build();
			HttpGet httpGet = new HttpGet(url);
			HttpResponse response = client.execute(httpGet);
			int statusCode = response.getStatusLine().getStatusCode();
			if (HttpURLConnection.HTTP_CREATED == statusCode || HttpURLConnection.HTTP_OK == statusCode) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public String getWooPP(Store store) {
		String email = null;
		try {
			StringBuilder apiUrl = new StringBuilder();
			String www = store.getW();
			if (www == null) {
				www = Constants.EMPTY;
			}
			if (www.equalsIgnoreCase(Constants.NO_WWW) || www.equalsIgnoreCase(Constants.FALSE_WWW)) {
				apiUrl.append("https://");
			} else {
				apiUrl.append("https://www.");
			}
			apiUrl.append(store.getDomain());
			String url = apiUrl.toString();
			OAuthConfig config = new OAuthConfig(url, store.getApikey(), store.getPassword());
			WooCommerce wooCommerce = new WooCommerceAPI(config, ApiVersionType.V2);
			Gson gson = new Gson();
			Map<String, String> params = new HashMap<>();
			List payments = wooCommerce.getAll(Constants.WOO_PP_GW, params);
			for (Object object : payments) {
				String json = gson.toJson(object);
				if (json.contains(Constants.WOO_PP_ID)) {
					WooPPCheckout wooPPCheckout = gson.fromJson(json, WooPPCheckout.class);
					String id = wooPPCheckout.getId();
					if (Constants.WOO_PP_ID.equals(id)) {
						email = wooPPCheckout.getSettings().getApi_username().getValue();
						if (email != null && email.contains("_api1")) {
							email = email.replace("_api1.", "@");
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return email;
	}

	public String getWooStripe(Store store) {
		String key = null;
		try {
			StringBuilder apiUrl = new StringBuilder();
			String www = store.getW();
			if (www == null) {
				www = Constants.EMPTY;
			}
			if (www.equalsIgnoreCase(Constants.NO_WWW) || www.equalsIgnoreCase(Constants.FALSE_WWW)) {
				apiUrl.append("https://");
			} else {
				apiUrl.append("https://www.");
			}
			apiUrl.append(store.getDomain());
			String url = apiUrl.toString();
			OAuthConfig config = new OAuthConfig(url, store.getApikey(), store.getPassword());
			WooCommerce wooCommerce = new WooCommerceAPI(config, ApiVersionType.V2);
			Gson gson = new Gson();
			Map<String, String> params = new HashMap<>();
			List payments = wooCommerce.getAll(Constants.WOO_PP_GW, params);
			for (Object object : payments) {
				String json = gson.toJson(object);
				if (json.contains(Constants.STRIPE) && json.contains(Constants.PUBLISHABLE_KEY)) {
					WooStripe wooStripe = gson.fromJson(json, WooStripe.class);
					String id = wooStripe.getId();
					PublishableKey sk = wooStripe.getSettings().getPublishable_key();
					if (sk != null) {
						key = sk.getValue();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return key;
	}

	private String getEmailInNote(Store store, Order order, Gson gson) {
		String email = null;
		StringBuilder sb = new StringBuilder();
		String www = store.getW();
		if (www == null) {
			www = Constants.EMPTY;
		}
		if (www.equalsIgnoreCase(Constants.NO_WWW) || www.equalsIgnoreCase(Constants.FALSE_WWW)) {
			sb.append("https://");
		} else {
			sb.append("https://www.");
		}
		sb.append(store.getDomain());
		sb.append("/wp-json/wc/v3/orders/");
		sb.append(order.getOrderidapi());
		sb.append("/notes");
		sb.append("?consumer_key=");
		sb.append(store.getApikey());
		sb.append("&consumer_secret=");
		sb.append(store.getPassword());
		String urlNote = sb.toString();
		String jsonNotes = getWooInfo(urlNote);
		Type collectionNotes = new TypeToken<Collection<WooNote>>() {
		}.getType();
		List<WooNote> notes = gson.fromJson(jsonNotes, collectionNotes);
		for (WooNote wooNote : notes) {
			if (wooNote.getNote().contains("@gmail.com") && wooNote.isCustomer_note()) {
				email = wooNote.getNote();
			}
		}
		return email;
	}

	public List orderTotal(Store store) {
		List totalOrder = new ArrayList();
		try {
			StringBuilder apiUrl = new StringBuilder();
			String www = store.getW();
			if (www == null) {
				www = Constants.EMPTY;
			}
			if (www.equalsIgnoreCase(Constants.NO_WWW) || www.equalsIgnoreCase(Constants.FALSE_WWW)) {
				apiUrl.append("https://");
			} else {
				apiUrl.append("https://www.");
			}
			apiUrl.append(store.getDomain());
			String url = apiUrl.toString();
			OAuthConfig config = new OAuthConfig(url, store.getApikey(), store.getPassword());
			WooCommerce wooCommerce = new WooCommerceAPI(config, ApiVersionType.V2);
			Map<String, String> params = new HashMap<>();
			params.put("status", "processing");
			params.put("per_page", "100");
			params.put("offset", "0");
			Map<String, String> param2 = new HashMap<>();
			param2.put("status", "processing");
			param2.put("per_page", "100");
			param2.put("offset", "1");
//			Map<String, String> param3 = new HashMap<>();
//			param3.put("status", "on-hold");
//			param3.put("per_page", "100");
//			param3.put("offset", "1");
//			Map<String, String> param4 = new HashMap<>();
//			param4.put("status", "on-hold");
//			param4.put("per_page", "100");
//			param4.put("offset", "0");
//		List products = wooCommerce.getAll(EndpointBaseType.PRODUCTS.getValue());
//		List products = wooCommerce.getAll(
//				EndpointBaseType.PRODUCTS.getValue(), params1);

//			List orders4 = wooCommerce.getAll(EndpointBaseType.ORDERS.getValue(), param4);
//			if (orders4 != null && !orders4.isEmpty()) {
//				totalOrder.addAll(orders4);
//			if (100 <= orders4.size()) {
//			List orders3 = wooCommerce.getAll(EndpointBaseType.ORDERS.getValue(), param3);
//			if (orders3 != null && !orders3.isEmpty()) {
//				totalOrder.addAll(orders3);
//			}
//			}
//			}
			List orders = wooCommerce.getAll(EndpointBaseType.ORDERS.getValue(), params);
			if (orders != null && !orders.isEmpty()) {
				totalOrder.addAll(orders);
				if (100 <= orders.size()) {
					List orders2 = wooCommerce.getAll(EndpointBaseType.ORDERS.getValue(), param2);
					if (orders2 != null && !orders2.isEmpty()) {
						totalOrder.addAll(orders2);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return totalOrder;
	}

	private boolean checkNewDate(Calendar calendar) {
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		if (0 == hour) {
			return true;
		} else {
			return false;
		}
	}

	private boolean capturePaypal(Order order, Gson gson, String partial) {
		try {
			String paypalEmail = order.getPay_pal_account_id();
			PaypalAccount paypalAccount = paypalService.findByEmail(paypalEmail);
			String clientID = paypalAccount.getClientid();
			String clientSecret = paypalAccount.getClientsecret();
			APIContext apiContext = new APIContext(clientID, clientSecret, Constants.EXECUTION_MODE);
			String fetch = apiContext.fetchAccessToken();
			HttpClient client = HttpClientBuilder.create().build();
			String url = Constants.CAPTURE_PP + order.getTransaction_id();
			HttpGet httpGet = new HttpGet(url);
			httpGet.setHeader(Constants.AUTHOR, fetch);
			httpGet.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
			HttpResponse response = client.execute(httpGet);
			int statusCodeAuthor = response.getStatusLine().getStatusCode();

			String storedomain = order.getStoredomain();
			if (statusCodeAuthor == 404) {
				Store store = storeService.getStoreByDomain(storedomain);
				String listPP = store.getPpacc();
				fetch = getFetchAgain(listPP, paypalEmail, order);
				if (fetch != null) {
					HttpGet httpGetnew = new HttpGet(url);
					httpGetnew.setHeader(Constants.AUTHOR, fetch);
					httpGetnew.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
					response = client.execute(httpGetnew);
				}
			}
			if (statusCodeAuthor != 200 && fetch == null) {
				order.setCapture_issue(Constants.INCORRECT_PAYPAL);
				orderService.saveOrder(order);
				return false;
			}
			BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));

			String output;
			String authorAPI = null;
			while ((output = br.readLine()) != null) {
				authorAPI = output;
			}
			Type collectionTypeAuthor = new TypeToken<Authorizations>() {
			}.getType();
			Authorizations authorization = gson.fromJson(authorAPI, collectionTypeAuthor);
			AmountCapture amountCapture = authorization.getAmount();
			String invoice_id = authorization.getInvoice_id();
			if (amountCapture == null) {
				HttpClient client1 = HttpClientBuilder.create().build();
				String url1 = Constants.SALE_PP + order.getTransaction_id();
				HttpGet httpGet1 = new HttpGet(url1);
				httpGet1.setHeader(Constants.AUTHOR, fetch);
				httpGet1.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
				HttpResponse response1 = client1.execute(httpGet1);
				if (response1.getStatusLine().getStatusCode() != 200) {
					System.out.println("error author: " + order.getOname());
				}
				BufferedReader br1 = new BufferedReader(new InputStreamReader((response1.getEntity().getContent())));

				String output1;
				String saleAPI = null;
				while ((output1 = br1.readLine()) != null) {
					saleAPI = output1;
				}
				Type collectionTypeSale = new TypeToken<SaleAPI>() {
				}.getType();
				SaleAPI saleApi = gson.fromJson(saleAPI, collectionTypeSale);
				amountCapture = new AmountCapture(saleApi.getAmount().getTotal(), saleApi.getAmount().getCurrency());

				invoice_id = saleApi.getInvoice_number();
			}
			Capture capture = new Capture(amountCapture, invoice_id, true);
			if (partial != null) {
				AmountCapture amountCaptureRoot = capture.getAmountCapture();
				amountCaptureRoot.setValue(partial);
				capture.setAmountCapture(amountCaptureRoot);
			}
			String jsonData = gson.toJson(capture);
			StringEntity params = new StringEntity(jsonData);
			HttpClient clientPost = HttpClientBuilder.create().build();
			HttpPost httpPost = new HttpPost(Constants.CAPTURE_PP + order.getTransaction_id() + "/capture");
			httpPost.setEntity(params);
			httpPost.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
			httpPost.setHeader(Constants.AUTHOR, fetch);
			HttpResponse responsePost = clientPost.execute(httpPost);
			int statusCode = responsePost.getStatusLine().getStatusCode();
			if (HttpStatus.SC_OK == statusCode || HttpStatus.SC_CREATED == statusCode) {
				BufferedReader brPost = new BufferedReader(
						new InputStreamReader((responsePost.getEntity().getContent())));

				String outputPost;
				String reponseAPI = null;
				while ((outputPost = brPost.readLine()) != null) {
					reponseAPI = outputPost;
				}
				Type collectionTypePost = new TypeToken<CaptureResponse>() {
				}.getType();
				CaptureResponse captureResponse = gson.fromJson(reponseAPI, collectionTypePost);
				String id = captureResponse.getId();
				if (id != null && !id.equalsIgnoreCase(order.getTransaction_id())) {
					order.setTransaction_id(id);
					order.setCapture_status(captureResponse.getStatus());
					String price = order.getPrice();
					int open = price.indexOf("(");
					double priceRoot = Double.valueOf(price.substring(0, open));
					String capture_total = paypalAccount.getCapturetotal();
					if (capture_total == null) {
						paypalAccount.setCapturetotal(String.valueOf(priceRoot));
					} else {
						double newTotal = priceRoot + Double.valueOf(capture_total);
						paypalAccount.setCapturetotal(String.valueOf(newTotal));
					}
					orderService.saveOrder(order);
				}
				return true;
			} else if (HttpStatus.SC_UNPROCESSABLE_ENTITY == statusCode) {
				BufferedReader brPost = new BufferedReader(
						new InputStreamReader((responsePost.getEntity().getContent())));

				String outputPost;
				String reponseAPI = null;
				while ((outputPost = brPost.readLine()) != null) {
					reponseAPI = outputPost;
				}
				Type collectionTypePost = new TypeToken<CaptureFail>() {
				}.getType();
				CaptureFail captureFail = gson.fromJson(reponseAPI, collectionTypePost);
				if (captureFail != null && captureFail.getDetails() != null) {
					order.setCapture_status(Constants.CAPTURED_FAILED);
					List<CaptureFailDetail> captureFailDetail = captureFail.getDetails();
					if (captureFailDetail != null && !captureFailDetail.isEmpty()) {
						order.setCapture_issue(captureFailDetail.get(0).getIssue());
					}
					orderService.saveOrder(order);
				}
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private String getFetchAgain(String listPP, String failPP, Order order) {
		try {
			String[] arrayPP = listPP.split(Constants.COMMA);
			PaypalAccount ppFail = paypalService.findByEmail(failPP);
			if (arrayPP.length > 0) {
				for (String pp : arrayPP) {
					if (!pp.equalsIgnoreCase(ppFail.getUsername())) {
						PaypalAccount paypalAccount = paypalService.findByUsername(pp);
						String clientID = paypalAccount.getClientid();
						String clientSecret = paypalAccount.getClientsecret();
						APIContext apiContext = new APIContext(clientID, clientSecret, Constants.EXECUTION_MODE);
						String fetch = apiContext.fetchAccessToken();
						HttpClient client = HttpClientBuilder.create().build();
						String url = Constants.CAPTURE_PP + order.getTransaction_id();
						HttpGet httpGet = new HttpGet(url);
						httpGet.setHeader(Constants.AUTHOR, fetch);
						httpGet.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
						HttpResponse response = client.execute(httpGet);
						int statusCodeAuthor = response.getStatusLine().getStatusCode();
						if (statusCodeAuthor == 200) {
							return fetch;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	private void updateChangePPWoo(String ppEmail, Double price, Store store) {
		PaypalAccount paypalAccount = paypalService.findByEmail(ppEmail);
		double sumprice = 0;
		int round = 1;
		if (paypalAccount != null) {
			if (paypalAccount.getRound() != null) {
				round = Integer.valueOf(paypalAccount.getRound());
			}
			if (paypalAccount.getSumprice() != null) {
				sumprice = Double.valueOf(paypalAccount.getSumprice());
			}
			int threshold = Integer.valueOf(store.getThresholdpp());
			int nowthreshold = threshold * round;
			sumprice = sumprice + price;
			paypalAccount.setSumprice(String.valueOf(sumprice));
			if (sumprice > nowthreshold) {
				String accPP = store.getPpacc();
				if (accPP != null && !accPP.isEmpty() && accPP.contains(Constants.COMMA)) {
					String[] accPPArray = accPP.split(Constants.COMMA);
					boolean isChange = false;
					for (String ppusername : accPPArray) {
						if (!ppusername.equalsIgnoreCase(paypalAccount.getUsername())) {
							PaypalAccount newPaypalAccount = paypalService.findByUsername(ppusername);
							if (newPaypalAccount != null) {
								String newPPSum = newPaypalAccount.getSumprice();
								if (newPPSum == null || Double.valueOf(newPPSum) < nowthreshold) {
									isChange = changePPWoo(store, newPaypalAccount);
									if (isChange) {
										break;
									}
								}
							}
						}
					}
				}
				round += 1;
				paypalAccount.setRound(String.valueOf(round));
			}
			paypalService.savePaypalAccount(paypalAccount);
		}
	}

	private void updatePPWoo(DateFormat f, String ppEmail, Double price, Store store) {
		try {
			Date orderDate = new Date();
			String orderDateformat = f.format(orderDate);
			PaypalAccount paypalAccount = paypalService.findByEmail(ppEmail);
			if (paypalAccount != null && paypalAccount.getUsername() != null) {
				DayPP dayPPOrder = dayPPService.findByDayByPP(paypalAccount.getUsername(), orderDateformat);
				if (dayPPOrder == null) {
					// check exist
					DayPP dayPPNew = new DayPP();
					dayPPNew.setAccountpp(paypalAccount.getUsername());
					dayPPNew.setDay(orderDateformat);
					dayPPNew.setDaytotal(price);
					dayPPNew.setRound(1);
					dayPPService.saveDayPP(dayPPNew);
				} else {
					// update total price of day PP
					double oldPrice = dayPPOrder.getDaytotal();
					double newPrice = oldPrice + price;
					dayPPOrder.setDaytotal(newPrice);
					dayPPService.saveDayPP(dayPPOrder);
					int round = dayPPOrder.getRound();
					double threshold = 500 * round;
					String thres = store.getThresholdpp();
					if (thres != null) {
						threshold = Double.valueOf(thres) * round;
					}
					// check change PP
					if (newPrice >= threshold) {
						String accPP = store.getPpacc();
						if (accPP != null && !accPP.isEmpty() && accPP.contains(",")) {
							String[] accPPArray = accPP.split(",");
							boolean isChange = false;
							for (String ppusername : accPPArray) {
								if (!ppusername.equalsIgnoreCase(dayPPOrder.getAccountpp())) {
									DayPP dayPPChange = dayPPService.findByDayByPP(ppusername, orderDateformat);
									if (dayPPChange == null || dayPPChange.getDaytotal() < threshold) {
										PaypalAccount newPaypalAccount = paypalService.findByUsername(ppusername);
										if (newPaypalAccount != null) {
											isChange = changePPWoo(store, newPaypalAccount);
											if (isChange) {
												break;
											}
										}
									}
								}
							}
							if (!isChange) {
								round = round + 1;
								dayPPOrder.setRound(round);
								dayPPService.saveDayPP(dayPPOrder);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void returnRefund(Order order, PaypalAccount paypalAccount) {
		if (!Constants.CUSTOMER_RETURNED.equalsIgnoreCase(order.getReturned_by_customer())) {
			String clientID = paypalAccount.getClientid();
			String clientSecret = paypalAccount.getClientsecret();
			String address_line_1 = Constants.EMPTY;
			String admin_area_4 = Constants.EMPTY;
			String admin_area_2 = Constants.EMPTY;
			String admin_area_1 = Constants.EMPTY;
			String postal_code = Constants.EMPTY;
			String country_code = Constants.EMPTY;
			try {
				APIContext apiContext = new APIContext(clientID, clientSecret, Constants.EXECUTION_MODE);
				String fetch = apiContext.fetchAccessToken();
				Gson gson = new Gson();
				String address = paypalAccount.getAddress();
				if (address.contains(Constants.COMMA)) {
					String[] aAdd = address.split(Constants.COMMA);
					if (aAdd.length > 0) {
						address_line_1 = aAdd[0];
						admin_area_4 = aAdd[1];
						admin_area_2 = aAdd[2];
						admin_area_1 = aAdd[3];
						postal_code = aAdd[4];
						country_code = aAdd[5];
					}
				}
				ReturnShippingAddress return_shipping_address = new ReturnShippingAddress(address_line_1, admin_area_4,
						admin_area_2, admin_area_1, postal_code, country_code);
				OfferReturn offerReturn = new OfferReturn();
				offerReturn.setNote(Constants.RETURN_NOTE);
				offerReturn.setReturn_shipping_address(return_shipping_address);
				offerReturn.setOffer_type(Constants.REFUND_WITH_RETURN);
				String jsonBody = gson.toJson(offerReturn);
				StringEntity params = new StringEntity(jsonBody);
				HttpClient client = HttpClientBuilder.create().build();
				HttpPost httpPost = new HttpPost(
						Constants.LIST_DISPUTE + "/" + order.getDispute_id() + "/accept-claim");
				httpPost.setEntity(params);
				httpPost.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
				httpPost.setHeader(Constants.AUTHOR, fetch);
				HttpResponse response = client.execute(httpPost);
				int statusCode = response.getStatusLine().getStatusCode();
				if (HttpStatus.SC_OK == statusCode || HttpStatus.SC_CREATED == statusCode) {
					order.setDispute_state(Constants.DISPUTE_RESOLVED);
					orderService.saveOrder(order);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
