package shoes.sup.controller;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.paypal.base.rest.APIContext;

import shoes.sup.model.Amount;
import shoes.sup.model.AmountCapture;
import shoes.sup.model.AmountRefund;
import shoes.sup.model.Authorizations;
import shoes.sup.model.Capture;
import shoes.sup.model.CaptureFail;
import shoes.sup.model.CaptureFailDetail;
import shoes.sup.model.CapturePaypal;
import shoes.sup.model.CaptureResponse;
import shoes.sup.model.CostGson;
import shoes.sup.model.Evidence;
import shoes.sup.model.EvidenceInfo;
import shoes.sup.model.Evidences;
import shoes.sup.model.GMC;
import shoes.sup.model.GMCNote;
import shoes.sup.model.IdInput;
import shoes.sup.model.MessageDispute;
import shoes.sup.model.MessageDisputeAjax;
import shoes.sup.model.NoteRefund;
import shoes.sup.model.OfferReturn;
import shoes.sup.model.Order;
import shoes.sup.model.PaypalAccount;
import shoes.sup.model.ReturnShippingAddress;
import shoes.sup.model.SaleAPI;
import shoes.sup.model.SaleAPINew;
import shoes.sup.model.Status;
import shoes.sup.model.Store;
import shoes.sup.model.Tracker;
import shoes.sup.model.Trackers;
import shoes.sup.model.TrackingInfoDetail;
import shoes.sup.model.TrackingSup;
import shoes.sup.model.User;
import shoes.sup.service.DisputeService;
import shoes.sup.service.GMCService;
import shoes.sup.service.OrderService;
import shoes.sup.service.PaypalService;
import shoes.sup.service.StoreService;
import shoes.sup.service.UserService;
import shoes.sup.util.Constants;

@Controller
public class AdminController {

	@Autowired
	UserService userService;

	@Autowired
	OrderService orderService;

	@Autowired
	StoreService storeService;

	@Autowired
	PaypalService paypalServce;

	@Autowired
	DisputeService disputService;
	
	@Autowired
	GMCService gmcService;

	@RequestMapping(value = "/admin/deleteorder", method = RequestMethod.GET)
	public String deleteOrder(HttpServletRequest request) {
		orderService.clearOrder();
		return "deleteorder";
	}

	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public String admin(Model model) {
		List<Order> lsOrder = orderService.findOrder200();
		model.addAttribute("lsOrder", lsOrder);

		List<User> lsSupplier = userService.findSupplier();
		model.addAttribute("lsSupplier", lsSupplier);
		return "admin";
	}
	@RequestMapping(value = "/admin/changepw", method = RequestMethod.GET)
	public String changepw(Model model) {
		return "changepw";
	}

	@RequestMapping(value = "/admin/changepassword", method = RequestMethod.POST)
	public String changepassword(Model model,@RequestParam(name = "newpw") String newpw) {
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		String currentPrincipalName = authentication.getName();
		if (currentPrincipalName.equalsIgnoreCase("anonymousUser")) {
			return "accessdenied";
		}
		User user = userService.findbyUsername(currentPrincipalName);
		user.setPassword(newpw);
		userService.saveUser(user);
		return "login";
	}
	
	@RequestMapping(value = "/admin/listsupplier", method = RequestMethod.GET)
	public String listSupplier(Model model) {
		List<User> lsSupplier = userService.findSupplier();
		model.addAttribute("lsSupplier", lsSupplier);
		return "listsupplier";
	}

	@RequestMapping(value = "/admin/reneworder", method = RequestMethod.POST)
	public String revewOrder(Model model, @RequestParam(name = "id") int id) {
		Order orderRenew = orderService.findById(id);
		orderRenew.setSupplier(null);
		orderRenew.setStatus(Constants.NEW_ORDER);
		orderRenew.setNote(null);
		orderRenew.setTrackingnumber(null);
		orderService.saveOrder(orderRenew);
		return "redirect:/admin/all";
	}

	@RequestMapping(value = "/admin/returnedorder", method = RequestMethod.GET)
	public String orderReturned(Model model) {
		List<Order> lsReturnOrder = orderService
				.findByStatus(Constants.RETURNED);
		model.addAttribute("lsReturnOrder", lsReturnOrder);
		List<Store> lsStore = storeService.getAllStore();
		model.addAttribute("lsStore", lsStore);
		return "returnedorder";
	}

	@RequestMapping(value = "/admin/returntracking", method = RequestMethod.POST)
	public String returntracking(Model model, @RequestParam("id") int id,
			@RequestParam("trackingreturn") String trackingreturn) {
		Order order = orderService.findById(id);
		order.setTrackingreturn(trackingreturn);
		orderService.saveOrder(order);
		return "redirect:/admin/returnedorder";
	}
	@RequestMapping(value = "/admin/refundwoo", method = RequestMethod.POST)
	public String refundwoo(Model model, @RequestParam("id") int id) {
		try {
			Order order = orderService.findById(id);
			String storeDomain = order.getStoredomain();
			Store store = storeService.getStoreByDomain(storeDomain);
			String ck = store.getApikey();
			String cs = store.getPassword();
			Gson gson = new Gson();
			String price = order.getPrice();
			int open = price.indexOf("(");
			String priceRoot = price.substring(0, open).trim();
			Amount amount = new Amount(priceRoot);
			String jsonData = gson.toJson(amount);
			StringEntity params =new StringEntity(jsonData);
			HttpClient client = HttpClientBuilder.create().build();
			String idwoo = order.getOname().substring(4, order.getOname().length());
			String url = "https://" + storeDomain + "/wp-json/wc/v3/orders/"+idwoo+"/refunds?consumer_key="+ck +"&consumer_secret="+cs;
			HttpPost httpPost = new HttpPost(url);
			httpPost.setEntity(params);
			httpPost.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
			HttpResponse response = client.execute(httpPost);
			int statusCode = response.getStatusLine().getStatusCode();
			if(HttpURLConnection.HTTP_CREATED == statusCode){
				order.setPayment_status(Constants.REFUNDED);
				orderService.saveOrder(order);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return "redirect:/admin/all";
	}
	@RequestMapping(value = "/admin/ajaxrefundwoo",method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ajaxrefundWoo(@RequestBody String json){
		String result = Constants.FAILED;
		if(json.contains("\"")){
			json = json.replace("\"", "");
		}
		try {
			Order order = orderService.findById(Integer.valueOf(json));
			String storeDomain = order.getStoredomain();
			Store store = storeService.getStoreByDomain(storeDomain);
			String ck = store.getApikey();
			String cs = store.getPassword();
			int prefixleng = Integer.valueOf(store.getPrefix() == null ? "4" : store.getPrefix());
			Gson gson = new Gson();
			String price = order.getPrice();
			int open = price.indexOf("(");
			String priceRoot = price.substring(0, open).trim();
			Amount amount = new Amount(priceRoot);
			String jsonData = gson.toJson(amount);
			StringEntity params =new StringEntity(jsonData);
			HttpClient client = HttpClientBuilder.create().build();
			String idwoo = order.getOname().substring(prefixleng, order.getOname().length());
			String url = "https://" + storeDomain + "/wp-json/wc/v3/orders/"+idwoo+"/refunds?consumer_key="+ck +"&consumer_secret="+cs;
			HttpPost httpPost = new HttpPost(url);
			httpPost.setEntity(params);
			httpPost.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
			HttpResponse response = client.execute(httpPost);
			int statusCode = response.getStatusLine().getStatusCode();
			if(HttpURLConnection.HTTP_CREATED == statusCode){
				order.setPayment_status(Constants.REFUNDED);
				order.setStatus(Constants.CANCEL_SHOPIFY);
				orderService.saveOrder(order);
				result = Constants.SUCCESS;
			}else {
				//boolean isVoidPP = voidcapturePP(order, gson);
//				if(isVoidPP){
//					order.setPayment_status(Constants.REFUNDED);
//					order.setStatus(Constants.CANCEL_SHOPIFY);
//					orderService.saveOrder(order);
//					result = Constants.SUCCESS;
//				}
				Status status = new Status(Constants.WOO_REFUNDED);
				String jsonDataPut = gson.toJson(status);
				StringEntity paramsPut =new StringEntity(jsonDataPut);
				HttpClient clientPut = HttpClientBuilder.create().build();
				String urlPut = "https://" + storeDomain + "/wp-json/wc/v3/orders/"+idwoo+"?consumer_key="+ck +"&consumer_secret="+cs;
				HttpPut httpPut = new HttpPut(urlPut);
				httpPut.setEntity(paramsPut);
				httpPut.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
				HttpResponse responsePut = clientPut.execute(httpPut);
				int statusCodePut = responsePut.getStatusLine().getStatusCode();
				if(HttpURLConnection.HTTP_OK == statusCodePut){
					order.setPayment_status(Constants.REFUNDED);
					order.setStatus(Constants.CANCEL_SHOPIFY);
					orderService.saveOrder(order);
					result = Constants.SUCCESS;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResponseEntity.ok(result);
	}

	@RequestMapping(value = "/admin/exportall", method = RequestMethod.GET)
	public ResponseEntity<InputStreamResource> exportAll(Model model)
			throws IOException {
		List<Order> lsReturnOrder = orderService
				.findByStatus(Constants.RETURNED);
		model.addAttribute("lsReturnOrder", lsReturnOrder);
		ByteArrayInputStream in = orderToExcel(lsReturnOrder);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=returned.xlsx");
		return ResponseEntity.ok().headers(headers)
				.body(new InputStreamResource(in));
	}

	@RequestMapping(value = "/admin/exportbystore", method = RequestMethod.GET)
	public ResponseEntity<InputStreamResource> exportbystore(Model model,
			@RequestParam("store") String store) throws IOException {
		List<Order> lsReturnOrder = orderService.findByStatusStore(
				Constants.RETURNED, store);
		model.addAttribute("lsReturnOrder", lsReturnOrder);
		ByteArrayInputStream in = orderToExcel(lsReturnOrder);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=returned.xlsx");
		return ResponseEntity.ok().headers(headers)
				.body(new InputStreamResource(in));
	}

	@RequestMapping(value = "/admin/returnedorder", method = RequestMethod.POST)
	public String returnOrder(Model model, @RequestParam(name = "id") int id) {
		Order orderRenew = orderService.findById(id);
		orderRenew.setStatus(Constants.RETURNED);
		orderService.saveOrder(orderRenew);
		return "redirect:/admin/all";
	}

	@RequestMapping(value = "/admin/deletesupplier", method = RequestMethod.POST)
	public String deleteSupplier(Model model,
			@RequestParam("supplier") String supplier) {
		User user = userService.findbyUsername(supplier);
		userService.deleteUser(user);
		return "redirect:/admin/listsupplier";
	}

	@RequestMapping(value = "/admin/assignone", method = RequestMethod.POST)
	public String assignone(@RequestParam("supplier") String supplier,
			HttpServletRequest request) {
		List<Order> lsOrder = orderService.findBySupplier(null);
		for (Order order : lsOrder) {
			order.setSupplier(supplier);
			order.setStatus(Constants.NEW_ORDER);
			orderService.saveOrder(order);
		}
		return "redirect:/admin";
	}


	@RequestMapping(value = "/admin/dispute", method = RequestMethod.GET)
	public String listDispute(Model model) {
		List<Order> lsDispute = orderService.findByDisputed(Constants.DISPUTED);
		if (lsDispute != null && !lsDispute.isEmpty()) {
			for (Iterator iterator = lsDispute.iterator(); iterator.hasNext();) {
				Order order = (Order) iterator.next();
				String disputeState =order.getDispute_state();
				if (Constants.DISPUTE_RESOLVED.equalsIgnoreCase(disputeState) || Constants.DISPUTE_APPEALABLE.equalsIgnoreCase(disputeState)) {
					iterator.remove();
				}
			}
		}
		for (Iterator iterator = lsDispute.iterator(); iterator.hasNext();) {
			Order order = (Order) iterator.next();
			String disputeStatus =order.getDispute_status();
			if(!Constants.WAITING_FOR_SELLER_RESPONSE.equalsIgnoreCase(disputeStatus) && !Constants.OPEN.equalsIgnoreCase(disputeStatus)){
				iterator.remove();
			}
		}
		Collections.sort(lsDispute, new Comparator<Order>() {
	    	DateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");

			@Override
			public int compare(Order o1, Order o2) {
				try {
					Date do1 = null;
					String so1 = o1.getSeller_response_due_date();
					if(so1 != null){
						do1 = f.parse(o1.getSeller_response_due_date());
					}else {
						do1 = new Date();
					}
					String so2 = o2.getSeller_response_due_date();
					Date do2 = null;
					if(so2 != null){
						do2 = f.parse(o2.getSeller_response_due_date());
					}else {
						do2 = new Date();
					}
					return do1.compareTo(do2);
				} catch (ParseException e) {
					throw new IllegalArgumentException(e);
				}
			}
	    	
		});
		model.addAttribute("lsDispute", lsDispute);
		return "listdispute";
	}
	
	@RequestMapping(value = "/admin/returnrefund", method = RequestMethod.POST)
	public String returnRefund(Model model,@RequestParam("id") int id,
			@RequestParam("address_line_1") String address_line_1,@RequestParam("admin_area_4") String admin_area_4,
			@RequestParam("admin_area_2") String admin_area_2,@RequestParam("admin_area_1") String admin_area_1,
			@RequestParam("postal_code") String postal_code,@RequestParam("country_code") String country_code) {
		Order order = orderService.findById(id);
		if(!Constants.CUSTOMER_RETURNED.equalsIgnoreCase(order.getReturned_by_customer())){
			String paypalEmail = order.getPay_pal_account_id();
			PaypalAccount paypalAccount = paypalServce.findByEmail(paypalEmail);
			String clientID = paypalAccount.getClientid();
			String clientSecret = paypalAccount.getClientsecret();
			try {
				APIContext apiContext = new APIContext(clientID,
						clientSecret, Constants.EXECUTION_MODE);
				String fetch = apiContext.fetchAccessToken();
				Gson gson = new Gson();
				ReturnShippingAddress return_shipping_address = new ReturnShippingAddress(address_line_1, admin_area_4, admin_area_2, admin_area_1, postal_code, country_code);
//				Refund_Amount refund_Amount = new Refund_Amount();
//				Money money = new Money();
//				OfferAmount offerAmount = new OfferAmount();
//				String price = order.getPrice();
//				int open = price.indexOf("(");
//				int close = price.indexOf(")");
//				String priceRoot = price.substring(0, open);
//				String currency = price.substring(open+1,close);
//				offerAmount.setValue(priceRoot.trim());
//				offerAmount.setCurrency_code(currency);
				OfferReturn offerReturn = new OfferReturn();
				offerReturn.setNote(Constants.RETURN_NOTE);
//				offerReturn.setRefund_amount(refund_Amount);
//				offerReturn.setOffer_amount(offerAmount);
				offerReturn.setReturn_shipping_address(return_shipping_address);
//				offerReturn.setMoney(money);
				offerReturn.setOffer_type(Constants.REFUND_WITH_RETURN);
				String jsonBody = gson.toJson(offerReturn);
				StringEntity params =new StringEntity(jsonBody);
				HttpClient client = HttpClientBuilder.create().build();
				HttpPost httpPost = new HttpPost(Constants.LIST_DISPUTE + "/" +order.getDispute_id() + "/accept-claim");
				httpPost.setEntity(params);
				httpPost.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
				httpPost.setHeader(Constants.AUTHOR, fetch);
				HttpResponse response = client.execute(httpPost);
				int statusCode = response.getStatusLine().getStatusCode();
				if(HttpStatus.SC_OK == statusCode || HttpStatus.SC_CREATED == statusCode){
					order.setDispute_state(Constants.DISPUTE_RESOLVED);
					orderService.saveOrder(order);
				}
				System.out.println("Return Refund: " + order.getOname() + "- " + order.getDispute_id() + " "+ statusCode);
		
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return "redirect:/admin/dispute";
	}
	@RequestMapping(value = "/admin/resolveddispute", method = RequestMethod.POST)
	public String resolvedDispute(Model model,@RequestParam("id") int id) {
		Order order = orderService.findById(id);
		order.setDispute_state(Constants.DISPUTE_RESOLVED);
		orderService.saveOrder(order);
		return "redirect:/admin/dispute";
	}
	@RequestMapping(value = "/admin/refunddispute", method = RequestMethod.POST)
	public String refundDispute(Model model,@RequestParam("id") int id) {
		Order order = orderService.findById(id);
		String paypalEmail = order.getPay_pal_account_id();
		PaypalAccount paypalAccount = paypalServce.findByEmail(paypalEmail);
		String clientID = paypalAccount.getClientid();
		String clientSecret = paypalAccount.getClientsecret();
		try {
			APIContext apiContext = new APIContext(clientID,
					clientSecret, Constants.EXECUTION_MODE);
			String fetch = apiContext.fetchAccessToken();
			Gson gson = new Gson();
			NoteRefund noteRefund = new NoteRefund(Constants.REFUND_MESSAGE);
			String jsonData = gson.toJson(noteRefund);
			StringEntity params =new StringEntity(jsonData);
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost httpPost = new HttpPost(Constants.LIST_DISPUTE + "/" +order.getDispute_id() + "/accept-claim");
			httpPost.setEntity(params);
			httpPost.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
			httpPost.setHeader(Constants.AUTHOR, fetch);
			HttpResponse response = client.execute(httpPost);
			order.setDispute_state(Constants.DISPUTE_RESOLVED);
			orderService.saveOrder(order);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/admin/dispute";
	}
	@RequestMapping(value = "/admin/senddisputemessage", method = RequestMethod.POST)
	public String senddisputemessage(Model model,@RequestParam("id") int id,@RequestParam("message") String message) {
		Order order = orderService.findById(id);
		String paypalEmail = order.getPay_pal_account_id();
		PaypalAccount paypalAccount = paypalServce.findByEmail(paypalEmail);
		String clientID = paypalAccount.getClientid();
		String clientSecret = paypalAccount.getClientsecret();
		try {
			APIContext apiContext = new APIContext(clientID,
					clientSecret, Constants.EXECUTION_MODE);
			String fetch = apiContext.fetchAccessToken();
			Gson gson = new Gson();
			MessageDispute messageDipsute = new MessageDispute(message);
			String jsonData = gson.toJson(messageDipsute);
			StringEntity params =new StringEntity(jsonData);
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost httpPost = new HttpPost(Constants.LIST_DISPUTE + "/" +order.getDispute_id() + "/send-message");
			httpPost.setEntity(params);
			httpPost.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
			httpPost.setHeader(Constants.AUTHOR, fetch);
			HttpResponse response = client.execute(httpPost);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/admin/dispute";
	}
	@RequestMapping(value = "/admin/updatetrack", method = RequestMethod.POST)
	public String updatetrack(Model model, @RequestParam("id") int id,
			@RequestParam("service") String service,@RequestParam("track") String track,@RequestParam("note") String note,@RequestParam(name = "evidencefile", required = false) MultipartFile evidencefile) {
		Order order = orderService.findById(id);
		String paypalEmail = order.getPay_pal_account_id();
		PaypalAccount paypalAccount = paypalServce.findByEmail(paypalEmail);
		String clientID = paypalAccount.getClientid();
		String clientSecret = paypalAccount.getClientsecret();
		try {
			APIContext apiContext = new APIContext(clientID, clientSecret,
					Constants.EXECUTION_MODE);
			String fetch = apiContext.fetchAccessToken();
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost httpPost = new HttpPost(Constants.LIST_DISPUTE + "/" +order.getDispute_id() + "/provide-evidence");
			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			Gson gson = new Gson();
			TrackingInfoDetail trackingInfoDetail = new TrackingInfoDetail(
					service, track);
			List<TrackingInfoDetail> trackingInfoDetails = new ArrayList<TrackingInfoDetail>();
			trackingInfoDetails.add(trackingInfoDetail);
			EvidenceInfo evidenceInfo = new EvidenceInfo();
			evidenceInfo.setTracking_info(trackingInfoDetails);
			Evidence evidence = new Evidence();
			evidence.setEvidence_type("PROOF_OF_FULFILLMENT");
			evidence.setEvidence_info(evidenceInfo);
			evidence.setNotes(note);
			Evidences evidences = new Evidences();
			List<Evidence> evidencess = new ArrayList<Evidence>();
			evidencess.add(evidence);
			evidences.setEvidences(evidencess);
			String json = gson.toJson(evidences);
			builder.addBinaryBody("input", new ByteArrayInputStream(json.getBytes()), ContentType.APPLICATION_JSON, "input.json");
			if(evidencefile != null && evidencefile.getOriginalFilename() != null && !evidencefile.getOriginalFilename().isEmpty()){
				builder.addBinaryBody("file1", evidencefile.getInputStream(),ContentType.IMAGE_JPEG, evidencefile.getOriginalFilename());
			}
			HttpEntity multipart = builder.build();
		    httpPost.setEntity(multipart);
//			httpPost.setHeader(Constants.CONTENT_TYPE, Constants.MULTIPART_RELATED + "; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
			httpPost.setHeader(Constants.AUTHOR, fetch);
			HttpResponse response = client.execute(httpPost);
			int statusCode = response.getStatusLine().getStatusCode();
			if(HttpStatus.SC_OK == statusCode || HttpStatus.SC_CREATED == statusCode){
				order.setDispute_state(Constants.DISPUTE_RESOLVED);
				orderService.saveOrder(order);
			}
			System.out.println("Evidence: " + order.getOname() + "- " + order.getDispute_id() + " "+ statusCode);
//			BufferedReader rd = new BufferedReader(
//            new InputStreamReader(response.getEntity().getContent()));
//
//			StringBuffer result = new StringBuffer();
//			String line = "";
//			while ((line = rd.readLine()) != null) {
//				result.append(line);
//			}
//
//			System.out.println(result.toString());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return "redirect:/admin/dispute";
	}

	@RequestMapping(value = "/admin/deletedispute", method = RequestMethod.GET)
	public String deleteDispute(Model model) {
		disputService.deleteDispute();
		return "deletedispute";
	}


	@RequestMapping(value = "/admin/assignsup", method = RequestMethod.POST)
	public String assign(@RequestParam("id") int id,
			@RequestParam("supplier") String supplier,
			HttpServletRequest request) {
		Order order = orderService.findById(id);
		order.setSupplier(supplier);
		order.setStatus(Constants.NEW_ORDER);
		order.setNote(null);
		orderService.saveOrder(order);
		return "redirect:/admin";
	}

	@RequestMapping(value = "/admin/getone", method = RequestMethod.GET)
	public String getoneAdmin(Model model,
			@RequestParam("orderid") String orderid) {
		List<Order> lsOrder = orderService.findAdminByName(orderid);
		model.addAttribute("lsOrder", lsOrder);
		List<String> listCarrierPP = listCarrierPP();
		List<String> listCarrierPPUK = listCarrierPPUK();
		List<String> listCarrierPPAU = listCarrierPPAU();
		List<String> listCarrierPPDE = listCarrierPPDE();
		List<String> listCarrierPPUS = listCarrierPPUS();
		List<String> listCarrierPPFR = listCarrierPPFR();
		List<String> listCarrierPPES = listCarrierPPES();
		
		model.addAttribute("listCarrierPP", listCarrierPP);
		model.addAttribute("lsCarrierUK", listCarrierPPUK);
		model.addAttribute("lsCarrierAU", listCarrierPPAU);
		model.addAttribute("lsCarrierDE", listCarrierPPDE);
		model.addAttribute("lsCarrierUS", listCarrierPPUS);
		model.addAttribute("lsCarrierFR", listCarrierPPFR);
		model.addAttribute("lsCarrierES", listCarrierPPES);
		return "allorder";
	}
	@RequestMapping(value = "/admin/getonetransaction", method = RequestMethod.GET)
	public String getbyTransaction(Model model,
			@RequestParam("transactionid") String transactionid) {
		Order order = orderService.findByTransaction_id(transactionid.trim());
		List<Order> lsOrder = new ArrayList<Order>();
		if(order != null){
			lsOrder.add(order);
		}
		model.addAttribute("lsOrder", lsOrder);
		List<String> listCarrierPP = listCarrierPP();
		List<String> listCarrierPPUK = listCarrierPPUK();
		List<String> listCarrierPPAU = listCarrierPPAU();
		List<String> listCarrierPPDE = listCarrierPPDE();
		List<String> listCarrierPPUS = listCarrierPPUS();
		List<String> listCarrierPPFR = listCarrierPPFR();
		List<String> listCarrierPPES = listCarrierPPES();
		
		model.addAttribute("listCarrierPP", listCarrierPP);
		model.addAttribute("lsCarrierUK", listCarrierPPUK);
		model.addAttribute("lsCarrierAU", listCarrierPPAU);
		model.addAttribute("lsCarrierDE", listCarrierPPDE);
		model.addAttribute("lsCarrierUS", listCarrierPPUS);
		model.addAttribute("lsCarrierFR", listCarrierPPFR);
		model.addAttribute("lsCarrierES", listCarrierPPES);
		return "allorder";
	}

	@RequestMapping(value = "/admin/getonestore", method = RequestMethod.GET)
	public String getonestore(Model model,
			@RequestParam("storedomain") String storedomain) {
		List<Order> lsOrder = orderService.findByStoreDomain(storedomain);
		if (lsOrder != null && !lsOrder.isEmpty()) {
			model.addAttribute("lsOrder", lsOrder);
		}
		List<String> listCarrierPP = listCarrierPP();
		List<String> listCarrierPPUK = listCarrierPPUK();
		List<String> listCarrierPPAU = listCarrierPPAU();
		List<String> listCarrierPPDE = listCarrierPPDE();
		List<String> listCarrierPPUS = listCarrierPPUS();
		List<String> listCarrierPPFR = listCarrierPPFR();
		List<String> listCarrierPPES = listCarrierPPES();
		
		model.addAttribute("listCarrierPP", listCarrierPP);
		model.addAttribute("lsCarrierUK", listCarrierPPUK);
		model.addAttribute("lsCarrierAU", listCarrierPPAU);
		model.addAttribute("lsCarrierDE", listCarrierPPDE);
		model.addAttribute("lsCarrierUS", listCarrierPPUS);
		model.addAttribute("lsCarrierFR", listCarrierPPFR);
		model.addAttribute("lsCarrierES", listCarrierPPES);
		return "allorder";
	}

	@RequestMapping(value = "/admin/exportstore", method = RequestMethod.GET)
	public ResponseEntity<InputStreamResource> exportonestore(Model model,
			@RequestParam("storedomain") String storedomain) throws IOException {
		List<Order> lsOrder = orderService.findByStoreDomain(storedomain);
		if (lsOrder != null && !lsOrder.isEmpty()) {
			model.addAttribute("lsOrder", lsOrder);
			ByteArrayInputStream in = orderToExcel(lsOrder);
			HttpHeaders headers = new HttpHeaders();
			headers.add("Content-Disposition", "attachment; filename=order.xlsx");
			return ResponseEntity.ok().headers(headers)
					.body(new InputStreamResource(in));
		}
		return null;
	}
	
	@RequestMapping(value = "/admin/note", method = RequestMethod.POST)
	public String note(Model model, @RequestParam("id") int id,
			@RequestParam("adminnote") String adminnote) {
		Order order = orderService.findById(id);
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		String currentPrincipalName = authentication.getName();
		String adminNote = order.getAdminnote();
		if (adminNote == null || adminNote.isEmpty()) {
			order.setAdminnote(currentPrincipalName + " : " + adminnote);
		} else {
			order.setAdminnote(adminNote + "," + currentPrincipalName + " : "
					+ adminnote);
		}
		// order.setNote(adminnote);
		orderService.saveOrder(order);
		return "redirect:/admin/all";
	}

	@RequestMapping(value = "/admin/assignclick", method = RequestMethod.POST)
	public String assignClick(
			@RequestParam(name = "hiddenrow", required = false) String hiddenrow,
			@RequestParam("supplier") String supplier) {
		String ids = hiddenrow;
		if (!ids.isEmpty()) {
			if (!ids.contains(",")) {
				Order order = orderService.findById(Integer.valueOf(ids));
				order.setSupplier(supplier);
				orderService.saveOrder(order);
			} else {
				String[] lsId = ids.split(",");
				for (String id : lsId) {
					Order order = orderService.findById(Integer.valueOf(id));
					order.setSupplier(supplier);
					orderService.saveOrder(order);
				}
			}
		}
		return "redirect:/admin";
	}
	
	@RequestMapping(value = "/admin/exportorder", method = RequestMethod.POST)
	public ResponseEntity<InputStreamResource> exportorder(Model model,
			@RequestParam(name = "hiddenroworder", required = false) String hiddenroworder) throws IOException {
		String ids = hiddenroworder;
		List<Order> lsOrder = new ArrayList<>();
		if (!ids.isEmpty()) {
			if (!ids.contains(",")) {
				Order order = orderService.findById(Integer.valueOf(ids));
				lsOrder.add(order);
			} else {
				String[] lsId = ids.split(",");
				for (String id : lsId) {
					Order order = orderService.findById(Integer.valueOf(id));
					lsOrder.add(order);
				}
			}
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd HHmm");
        String formattedDate = sdf.format(new Date());
        String filename = "AllOrder" + formattedDate + ".xlsx";
        Collections.reverse(lsOrder);
		ByteArrayInputStream in = orderToExcelFF(lsOrder);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename="+filename);
		return ResponseEntity.ok().headers(headers)
				.body(new InputStreamResource(in));
	}
	
	@RequestMapping(value = "/admin/ajaxrefundpp",method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ajaxrefundPP(@RequestBody String json){
		String result = Constants.FAILED;
		if(json.contains("\"")){
			json = json.replace("\"", "");
		}
		Order order = orderService.findById(Integer.valueOf(json));
		Gson gson = new Gson();
		boolean isRefund = refundPaypal(order, gson);
		if(isRefund){
			result = Constants.SUCCESS;
		}else {
			boolean isVoid = voidcapturePP(order, gson);
			if(isVoid){
				result = Constants.SUCCESS;
			}
		}
		if(Constants.SUCCESS.equalsIgnoreCase(result)){
			order.setPayment_status(Constants.REFUNDED);
			order.setStatus(Constants.CANCEL_SHOPIFY);
			orderService.saveOrder(order);
		}
		return ResponseEntity.ok(result);
	}
	
	@RequestMapping(value = "/admin/ajaxcapturepp",method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ajaxcapturePP(@RequestBody String json){
		String result = Constants.FAILED;
		if(json.contains("\"")){
			json = json.replace("\"", "");
		}
		Gson gson = new Gson();
		CapturePaypal capturePP = gson.fromJson(json, CapturePaypal.class);
		String id = capturePP.getId();
		String partial = capturePP.getPartial();
		Order order = orderService.findById(Integer.valueOf(id));
		String paypalEmail = order.getPay_pal_account_id();
		if(paypalEmail != null && !paypalEmail.isEmpty()) {
			boolean isCapture= capturePaypal(order, gson, partial);
			try {
				if(isCapture){
					String storeDomain = order.getStoredomain();
					Store store = storeService.getStoreByDomain(storeDomain);
					if(store.getState().equals(Constants.ACTIVE)){
						if(store.getType() == Constants.WOO_COMMERCE) {
							String ck = store.getApikey();
							String cs = store.getPassword();
							Status status = new Status(Constants.WOO_PROCESSING);
							String idwoo = order.getOname().substring(4, order.getOname().length());
							String jsonDataPut = gson.toJson(status);
							StringEntity paramsPut =new StringEntity(jsonDataPut);
							HttpClient clientPut = HttpClientBuilder.create().build();
							String urlPut = "https://www." + order.getStoredomain() + "/wp-json/wc/v3/orders/"+idwoo+"?consumer_key="+ck +"&consumer_secret="+cs;
							HttpPut httpPut = new HttpPut(urlPut);
							httpPut.setEntity(paramsPut);
							httpPut.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
							HttpResponse responsePut = clientPut.execute(httpPut);
							int statusCodePut = responsePut.getStatusLine().getStatusCode();
							if(HttpURLConnection.HTTP_CREATED == statusCodePut || HttpURLConnection.HTTP_OK == statusCodePut){
								order.setPayment_status(Constants.CAPTURED);
								order.setStatus(Constants.WOO_COMPLETED);
								orderService.saveOrder(order);
								result = Constants.SUCCESS;
							}
						}
					}
					order.setPayment_status(Constants.CAPTURED);
					order.setStatus(Constants.WOO_COMPLETED);
					orderService.saveOrder(order);
					result = Constants.SUCCESS;
				}else {
					order.setPayment_status(Constants.CAPTURED_FAILED);
					orderService.saveOrder(order);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return ResponseEntity.ok(result);
	}
	
	@RequestMapping(value = "/admin/ajaxcost",method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ajaxcost(@RequestBody String json){
		String result = Constants.FAILED;
		Gson gson = new Gson();
		CostGson cost = gson.fromJson(json, CostGson.class);
		int id = cost.getId();
		Order order = orderService.findById(id);
		String costSet = cost.getOrdercost();
		order.setTransaction_id(costSet);
		orderService.saveOrder(order);
		result = Constants.SUCCESS;
		return ResponseEntity.ok(result);
	}
	@RequestMapping(value = "/admin/ajaxnote",method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ajaxnote(@RequestBody String json){
		String result = Constants.FAILED;
		Gson gson = new Gson();
		CostGson cost = gson.fromJson(json, CostGson.class);
		int id = cost.getId();
		Order order = orderService.findById(id);
		String noteSet = cost.getOrdercost();
		order.setNote(noteSet);
		orderService.saveOrder(order);
		result = Constants.SUCCESS;
		return ResponseEntity.ok(result);
	}
	
	@RequestMapping(value = "/admin/ajaxgmccountry",method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ajaxgmccountry(@RequestBody String json){
		String result = Constants.FAILED;
		Gson gson = new Gson();
		GMCNote noteGMC = gson.fromJson(json, GMCNote.class);
		String id = noteGMC.getId();
		GMC gmc = gmcService.findByID(id);
		String countryset = noteGMC.getNote();
		gmc.setCountry(countryset);
		gmcService.saveGMC(gmc);
		result = Constants.SUCCESS;
		return ResponseEntity.ok(result);
	}
	@RequestMapping(value = "/admin/ajaxcheckbalance",method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ajaxcheckbalance(@RequestBody String json){
		String result = "FAILED";
		Gson gson = new Gson();
		CostGson cost = gson.fromJson(json, CostGson.class);
		int id = cost.getId();
		Order order = orderService.findById(id);
		String paypalEmail = order.getPay_pal_account_id();
		PaypalAccount paypalAccount = paypalServce.findByEmail(paypalEmail);
		if(paypalAccount != null) {
			StringBuilder sb = new StringBuilder();
			String userName = paypalAccount.getUsername();
			sb.append("https://api-3t.paypal.com/nvp?&user=");
			sb.append(userName);
			sb.append("&pwd=");
			sb.append(paypalAccount.getPassword());
			sb.append("&signature=");
			sb.append(paypalAccount.getSignature());
			sb.append(
					"&version=109.0&METHOD=GetBalance&RETURNALLCURRENCIES=1");
			String url = sb.toString();
			HttpClient client = HttpClientBuilder.create().build();
			HttpGet httpGet = new HttpGet(url);
			try {
				HttpResponse response = client.execute(httpGet);
				BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));

				String output;
				String statuscheck = null;
				while ((output = br.readLine()) != null) {
					statuscheck = output;
				}
				String balance = checkBalance(statuscheck);
				if("0".equals(balance)) {
					result = Constants.WAIT_21;
					order.setNote(Constants.WAIT_21);
				}else {
					result = "Blance:" + balance; 
					order.setNote(result);
				}
				orderService.saveOrder(order);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return ResponseEntity.ok(result);
	}
	@RequestMapping(value = "/admin/ajaxmessagedispute",method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ajaxmessagedispute(@RequestBody String json){
		String result = Constants.FAILED;
		Gson gson = new Gson();
		MessageDisputeAjax ajaxMessage = gson.fromJson(json, MessageDisputeAjax.class);
		int id = ajaxMessage.getId();
		Order order = orderService.findById(id);
		String message = ajaxMessage.getMessage();
		String paypalEmail = order.getPay_pal_account_id();
		PaypalAccount paypalAccount = paypalServce.findByEmail(paypalEmail);
		String clientID = paypalAccount.getClientid();
		String clientSecret = paypalAccount.getClientsecret();
		try {
			APIContext apiContext = new APIContext(clientID,
					clientSecret, Constants.EXECUTION_MODE);
			String fetch = apiContext.fetchAccessToken();
			MessageDispute messageDipsute = new MessageDispute(message);
			String jsonData = gson.toJson(messageDipsute);
			StringEntity params =new StringEntity(jsonData);
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost httpPost = new HttpPost(Constants.LIST_DISPUTE + "/" +order.getDispute_id() + "/send-message");
			httpPost.setEntity(params);
			httpPost.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
			httpPost.setHeader(Constants.AUTHOR, fetch);
			HttpResponse response = client.execute(httpPost);
			if(HttpURLConnection.HTTP_OK == response.getStatusLine().getStatusCode()) {
				result = Constants.SUCCESS;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResponseEntity.ok(result);
	}
	
	@RequestMapping(value = "/admin/ajaxdeleteorder",method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ajaxdeleteorder(@RequestBody String json){
		String result = Constants.FAILED;
		if(json.contains("\"")){
			json = json.replace("\"", "");
		}
		Gson gson = new Gson();
		IdInput idInput = gson.fromJson(json, IdInput.class);
		String id = idInput.getId();
		Order order = orderService.findById(Integer.valueOf(id));
		orderService.deleteOrder(order);
		result = Constants.SUCCESS;
		return ResponseEntity.ok(result);
	}
	
	
	@RequestMapping(value = "/admin/ajaxdeliver",method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ajaxdeliver(@RequestBody String json){
		String result = Constants.FAILED;
		Gson gson = new Gson();
		TrackingSup trackingSup = gson.fromJson(json, TrackingSup.class);
		int id = trackingSup.getId();
		String trackingnumber = trackingSup.getTrackingnumber();
		String carrier = trackingSup.getCarrier();
		String notecarrier = trackingSup.getNotecarrier();
		boolean isSendEmail = false;
		if(notecarrier.equalsIgnoreCase("true")) {
			isSendEmail = true;
		}
		String partial = trackingSup.getPartial();
		Order order = orderService.findById(id);
		if(trackingnumber.contains("Will")){
			trackingnumber = trackingnumber.replace("Will", Constants.EMPTY).trim();
		}
		order.setTrackingnumber(trackingnumber);
		order.setCarrier(carrier);
		order.setNotecarrier(carrier);
		order.setStatus(Constants.DELIVERED);
		String paypalEmail = order.getPay_pal_account_id();
		String clientID = null;
		String clientSecret = null;
		String transactionid = null;
		String domain = order.getStoredomain();
		Store store = storeService.getStoreByDomain(domain);
		if(paypalEmail != null){
			PaypalAccount paypalAccount = paypalServce.findByEmail(paypalEmail);
			if(paypalAccount != null) {
				clientID = paypalAccount.getClientid();
				clientSecret = paypalAccount.getClientsecret();
				transactionid = order.getTransaction_id();
			}
			String capture = order.getCapture_status();
			if(capture == null || !Constants.COMPLETED.equalsIgnoreCase(capture) || !Constants.WOO_PROCESSING.equalsIgnoreCase(order.getPayment_status())){
				// capture PP
				boolean isCapture = capturePaypal(order, gson, partial);
				if(isCapture){
					result = Constants.SUCCESSED;
					order.setPayment_status(Constants.CAPTURED);
					order.setStatus(Constants.DELIVERED);
				}else {
					order.setPayment_status(Constants.CAPTURED_FAILED);
					order.setStatus(Constants.DELIVERED);
				}
			}
			try {
				APIContext apiContext = new APIContext(clientID, clientSecret,
						Constants.EXECUTION_MODE);
				String fetch = apiContext.fetchAccessToken();
				HttpClient clientPPTrack = HttpClientBuilder.create().build();
				HttpPost httpPostPPTrack = new HttpPost(Constants.TRACK_PP);
				Order orderUpdate = orderService.findById(id);
				if(!transactionid.equalsIgnoreCase(orderUpdate.getTransaction_id())){
					transactionid = orderUpdate.getTransaction_id();
				}
				Tracker tracker = new Tracker(transactionid, trackingnumber, Constants.SHIPPED, carrier, isSendEmail);
				List<Tracker> lstrackers = new ArrayList<Tracker>();
				lstrackers.add(tracker);
				Trackers trackers = new Trackers(lstrackers);
				String jsonDataPP = gson.toJson(trackers);
				StringEntity paramsPP = new StringEntity(jsonDataPP);
				httpPostPPTrack.setEntity(paramsPP);
				httpPostPPTrack.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);	
				httpPostPPTrack.setHeader(Constants.AUTHOR, fetch);
				HttpResponse responsePP = clientPPTrack.execute(httpPostPPTrack);
				int statuscode = responsePP.getStatusLine().getStatusCode();
				if(HttpURLConnection.HTTP_CREATED == statuscode || HttpURLConnection.HTTP_OK == statuscode){
					String newURL = Constants.TRACK_PP_NEW + transactionid + "-" + trackingnumber;
					HttpPut httpPutPPTrack = new HttpPut(newURL);
					String jsonDataPPPut = gson.toJson(tracker);
					StringEntity paramsPPPut = new StringEntity(jsonDataPPPut);
					httpPutPPTrack.setEntity(paramsPPPut);
					httpPutPPTrack.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);	
					httpPutPPTrack.setHeader(Constants.AUTHOR, fetch);
					HttpResponse responsePPPut = clientPPTrack.execute(httpPutPPTrack);
					int statuscodePut = responsePPPut.getStatusLine().getStatusCode();
					result = Constants.SUCCESSED;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else {
			try {
				String ck = store.getApikey();
				String cs = store.getPassword();
				Date today = Calendar.getInstance().getTime();        
				Map<String, Object> trackingInfo = new HashMap<>();
				trackingInfo.put("tracking_provider", carrier);
				trackingInfo.put("custom_tracking_provider", "");
				trackingInfo.put("custom_tracking_link", "");
				trackingInfo.put("tracking_number", trackingnumber);
				trackingInfo.put("tracking_product_code", "");
				trackingInfo.put("date_shipped", "");
				trackingInfo.put("products_list", "");
				trackingInfo.put("status_shipped", "");
				trackingInfo.put("tracking_id", "");
				
				Map<String, Object> metadata = new HashMap<>();
				metadata.put("key", "_wc_shipment_tracking_items");
				metadata.put("value", new Object[]{trackingInfo});
				
				// Prepare the order update payload
				Map<String, Object> payload = new HashMap<>();
				payload.put("meta_data", new Object[]{metadata});
				
				// Convert payload to JSON
				String jsonPayload = gson.toJson(payload);
				StringEntity params =new StringEntity(jsonPayload);
				HttpClient clientPut = HttpClientBuilder.create().build();
				String storeprefix = store.getPrefix() == null ? "4" : store.getPrefix();
				int prefixleng = Integer.valueOf(storeprefix);
				String idwoo = order.getOname().substring(prefixleng, order.getOname().length());
				String urlPut = "https://" + domain + "/wp-json/wc/v3/orders/"+idwoo+"?consumer_key="+ck +"&consumer_secret="+cs;
				HttpPut httpPut = new HttpPut(urlPut);
				httpPut.setEntity(params);
				httpPut.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
				HttpResponse responsePut = clientPut.execute(httpPut);
				int statusCodePut = responsePut.getStatusLine().getStatusCode();
				if(HttpURLConnection.HTTP_OK == statusCodePut){
					orderService.saveOrder(order);
					result = Constants.SUCCESS;
					// Update the order status to 'completed'
		            Map<String, Object> statusPayload = new HashMap<>();
		            statusPayload.put("status", "completed");

		            String statusJsonPayload = gson.toJson(statusPayload);
		            StringEntity statusParams = new StringEntity(statusJsonPayload);

		            HttpPut statusHttpPut = new HttpPut(urlPut);
		            statusHttpPut.setEntity(statusParams);
		            statusHttpPut.setHeader("Content-Type", "application/json");

		            // Execute the request to update the order status
		            HttpResponse statusResponse = clientPut.execute(statusHttpPut);
		            int statusCode = statusResponse.getStatusLine().getStatusCode();
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
		orderService.saveOrder(order);
		return ResponseEntity.ok(result);
	}

	@RequestMapping(value = "/admin/uploadfile", method = RequestMethod.POST)
	public String uploadFile(@RequestParam("file") MultipartFile file,
			HttpServletRequest request) {
		List<Order> lsOrder = new ArrayList<Order>();
		try {
			String nameFile = file.getOriginalFilename();
			InputStream excelFile = file.getInputStream();
			Workbook workbook = new XSSFWorkbook(excelFile);
			Sheet datatypeSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = datatypeSheet.iterator();
			while (iterator.hasNext()) {
				Row currentRow = iterator.next();
				Iterator<Cell> cellIterator = currentRow.iterator();
				Order order = new Order();
				Date date = new Date();
				while (cellIterator.hasNext()) {
					Cell currentCell = cellIterator.next();
					int index = currentCell.getColumnIndex();
					// if (currentCell.getCellTypeEnum() == CellType.STRING) {
					// System.out.print(index + ":");
					// System.out.println(currentCell.getStringCellValue()
					// .trim().replace("\n", "").replace("\r", ""));
					// } else if (currentCell.getCellTypeEnum() ==
					// CellType.NUMERIC) {
					// System.out.print(currentCell.getColumnIndex() + ":");
					// System.out.println(currentCell.getNumericCellValue());
					// }
					switch (index) {
					case 0:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							order.setOname(currentCell.getStringCellValue());
						} else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
							order.setOname(String.valueOf(currentCell
									.getNumericCellValue()));
						}
						break;
					case 1:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							order.setAddress(currentCell.getStringCellValue());
						} else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
							order.setAddress(String.valueOf(currentCell
									.getNumericCellValue()));
						}
						break;
					case 2:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							order.setImageurl(currentCell.getStringCellValue());
						} else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
							order.setImageurl(String.valueOf(currentCell
									.getNumericCellValue()));
						}
						break;
					case 3:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							order.setProductname(currentCell
									.getStringCellValue());
						} else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
							order.setProductname(String.valueOf(currentCell
									.getNumericCellValue()));
						}

						break;
					case 4:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							order.setVariant(currentCell.getStringCellValue());
						} else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
							order.setVariant(String.valueOf(currentCell
									.getNumericCellValue()));
						}
						break;
					case 5:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							order.setQuantity(currentCell.getStringCellValue());
						} else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
							order.setQuantity(String.valueOf(currentCell
									.getNumericCellValue()));
						}
						break;
					case 6:
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							order.setStyle(currentCell.getStringCellValue());
						} else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
							order.setStyle(String.valueOf(currentCell
									.getNumericCellValue()));
						}
						break;
					default:
						break;
					}
				}
				order.setCreateDate(date);
				order.setStatus(Constants.NEW_ORDER);
				lsOrder.add(order);
				// break;
			}
			if (!lsOrder.isEmpty()) {
				for (Iterator iterator2 = lsOrder.iterator(); iterator2
						.hasNext();) {
					Order order = (Order) iterator2.next();
					orderService.saveOrder(order);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/admin";
	}
	
	private ByteArrayInputStream orderToExcelFF(List<Order> Orders)
			throws IOException {
		String[] COLUMNs = { "Order ID", "type of issue", "Issues details", "Status",
				"Price", "Quantity","PRODUCT NAME","Size","Link Picture","Country","Name","Full Shipping Address","CITY","STATE PROVINCE","RECEIPT ZIP","PHONE","IMAGE","PRICE","Customer Note"};
		try {
			Workbook workbook = new XSSFWorkbook();
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			CreationHelper createHelper = workbook.getCreationHelper();

			Sheet sheet = workbook.createSheet("All Order");
			Font headerFont = workbook.createFont();
			headerFont.setBold(true);
			headerFont.setColor(IndexedColors.BLACK.getIndex());
			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			// Row for Header
			Row headerRow = sheet.createRow(0);
			// Header
			for (int col = 0; col < COLUMNs.length; col++) {
				Cell cell = headerRow.createCell(col);
				cell.setCellValue(COLUMNs[col]);
				cell.setCellStyle(headerCellStyle);
			}

			int rowIdx = 1;
			for (Order order : Orders) {
				Row row = sheet.createRow(rowIdx++);
				String size = order.getVariant();
				String productname = order.getProductname().trim();
				if(productname.contains("US")) {
					if(productname.contains("Women")) {
						size = "Women " + size;
					}else {
						size = "Men " + size;
					}
				}
				row.createCell(0).setCellValue(order.getOname());
				row.createCell(1).setCellValue("");
				row.createCell(2).setCellValue("");
				row.createCell(3).setCellValue("");
				row.createCell(4).setCellValue("");
				row.createCell(5).setCellValue(order.getQuantity());
				row.createCell(6).setCellValue(productname);
				row.createCell(7).setCellValue(size);
				row.createCell(8).setCellValue(order.getImageurl());
				row.createCell(9).setCellValue(order.getCountry());
				row.createCell(10).setCellValue(order.getAddressname());
				row.createCell(11).setCellValue(order.getAddress1() + " " + order.getAddress2());
				row.createCell(12).setCellValue(order.getCity());
				row.createCell(13).setCellValue(order.getProvince());
				row.createCell(14).setCellValue(order.getZip());
				row.createCell(15).setCellValue(order.getPhone());
				row.createCell(16).setCellValue("");
				row.createCell(17).setCellValue("");
				row.createCell(18).setCellValue(order.getNote());
			}
			workbook.write(out);
			return new ByteArrayInputStream(out.toByteArray());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	private ByteArrayInputStream orderToExcel(List<Order> Orders)
			throws IOException {
		String[] COLUMNs = { "Order ID", "Supplier", "Address", "Product Name",
				"Price", "Store" };
		try {
			Workbook workbook = new XSSFWorkbook();
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			CreationHelper createHelper = workbook.getCreationHelper();

			Sheet sheet = workbook.createSheet("Returned Order");
			Font headerFont = workbook.createFont();
			headerFont.setBold(true);
			headerFont.setColor(IndexedColors.BLACK.getIndex());
			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			// Row for Header
			Row headerRow = sheet.createRow(0);
			// Header
			for (int col = 0; col < COLUMNs.length; col++) {
				Cell cell = headerRow.createCell(col);
				cell.setCellValue(COLUMNs[col]);
				cell.setCellStyle(headerCellStyle);
			}

			int rowIdx = 1;
			for (Order order : Orders) {
				Row row = sheet.createRow(rowIdx++);

				row.createCell(0).setCellValue(order.getOname());
				row.createCell(1).setCellValue(order.getSupplier());
				row.createCell(2).setCellValue(order.getAddress());
				row.createCell(3).setCellValue(order.getProductname());
				row.createCell(4).setCellValue(order.getPrice());
				row.createCell(5).setCellValue(order.getStoredomain());
			}
			workbook.write(out);
			return new ByteArrayInputStream(out.toByteArray());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value = "/admin/all", method = RequestMethod.GET)
	public String all(Model model) {
		List<Order> lsOrder = orderService.findOrder2000();
		model.addAttribute("lsOrder", lsOrder);
		List<String> listCarrierPP = listCarrierPP();
		List<String> listCarrierPPUK = listCarrierPPUK();
		List<String> listCarrierPPAU = listCarrierPPAU();
		List<String> listCarrierPPDE = listCarrierPPDE();
		List<String> listCarrierPPUS = listCarrierPPUS();
		List<String> listCarrierPPFR = listCarrierPPFR();
		List<String> listCarrierPPES = listCarrierPPES();
		List<String> listCarrierAdvance = listCarrierAdvance();
		
		model.addAttribute("listCarrierPP", listCarrierPP);
		model.addAttribute("lsCarrierUK", listCarrierPPUK);
		model.addAttribute("lsCarrierAU", listCarrierPPAU);
		model.addAttribute("lsCarrierDE", listCarrierPPDE);
		model.addAttribute("lsCarrierUS", listCarrierPPUS);
		model.addAttribute("lsCarrierFR", listCarrierPPFR);
		model.addAttribute("listCarrierAdvance", listCarrierAdvance);
		model.addAttribute("lsCarrierES", listCarrierPPES);
		return "allorder";
	}
	private List<String> listCarrierAdvance(){
		List<String> lsCarrier = new ArrayList<String>();
		lsCarrier.add(Constants.evri);
		lsCarrier.add(Constants.australia_post);
		lsCarrier.add(Constants.usps);
		lsCarrier.add(Constants.dhl_paket);
		lsCarrier.add(Constants.dhl);
		lsCarrier.add(Constants.royal_mail);
		lsCarrier.add(Constants.fedex);
		lsCarrier.add(Constants.canada_post);
		lsCarrier.add(Constants.la_poste);
		lsCarrier.add(Constants.ctt_express_spain);
		return lsCarrier;
	}
	private List<String> listCarrierPP(){
		List<String> lsCarrier = new ArrayList<String>();
		lsCarrier.add(Constants.USPS);
		lsCarrier.add(Constants.PLEASE_SELECT);
		lsCarrier.add(Constants.FEDEX);
		lsCarrier.add(Constants.FOUR_PX_EXPRESS);
		lsCarrier.add(Constants.UPS);
		lsCarrier.add(Constants.EMS);
		lsCarrier.add(Constants.AIRBORNE_EXPRESS);
		lsCarrier.add(Constants.DHL);
		lsCarrier.add(Constants.AIRSURE);
		lsCarrier.add(Constants.ROYAL_MAIL);
		lsCarrier.add(Constants.PARCELFORCE);
		lsCarrier.add(Constants.ARAMEX);
		lsCarrier.add(Constants.SWIFTAIR);
		lsCarrier.add(Constants.UK_PARCELFORCE);
		lsCarrier.add(Constants.UK_ROYALMAIL_SPECIAL);
		lsCarrier.add(Constants.UK_ROYALMAIL_RECORDED);
		lsCarrier.add(Constants.UK_ROYALMAIL_INT_SIGNED);
		lsCarrier.add(Constants.UK_ROYALMAIL_AIRSURE);
		lsCarrier.add(Constants.UK_UPS);
		lsCarrier.add(Constants.UK_FEDEX);
		lsCarrier.add(Constants.UK_AIRBORNE_EXPRESS);
		lsCarrier.add(Constants.UK_DHL);
		lsCarrier.add(Constants.UK_OTHER);
		lsCarrier.add(Constants.UK_CANNOT_PROV_TRACK);
		lsCarrier.add(Constants.CA_CANADA_POST);
		lsCarrier.add(Constants.CA_PUROLATOR);
		lsCarrier.add(Constants.CA_CANPAR);
		lsCarrier.add(Constants.CA_LOOMIS);
		lsCarrier.add(Constants.CA_TNT);
		lsCarrier.add(Constants.CA_OTHER);
		lsCarrier.add(Constants.CA_CANNOT_PROV_TRACK);
		lsCarrier.add(Constants.DE_DP_DHL_WITHIN_EUROPE);
		lsCarrier.add(Constants.DE_DP_DHL_T_AND_T_EXPRESS);
		lsCarrier.add(Constants.DE_DHL_DP_INTL_SHIPMENTS);
		lsCarrier.add(Constants.DE_GLS);
		lsCarrier.add(Constants.DE_DPD_DELISTACK);
		lsCarrier.add(Constants.DE_HERMES);
		lsCarrier.add(Constants.DE_UPS);
		lsCarrier.add(Constants.DE_FEDEX);
		lsCarrier.add(Constants.DE_TNT);
		lsCarrier.add(Constants.DE_OTHER);
		lsCarrier.add(Constants.FR_CHRONOPOST);
		lsCarrier.add(Constants.FR_COLIPOSTE);
		lsCarrier.add(Constants.FR_DHL);
		lsCarrier.add(Constants.FR_UPS);
		lsCarrier.add(Constants.FR_TNT);
		lsCarrier.add(Constants.FR_GLS);
		lsCarrier.add(Constants.IT_POSTE_ITALIA);
		lsCarrier.add(Constants.IT_DHL);
		lsCarrier.add(Constants.IT_UPS);
		lsCarrier.add(Constants.IT_FEDEX);
		lsCarrier.add(Constants.IT_TNT);
		lsCarrier.add(Constants.IT_GLS);
		lsCarrier.add(Constants.IT_OTHER);
		lsCarrier.add(Constants.AU_AUSTRALIA_POST);
		lsCarrier.add(Constants.AU_AUSTRALIA_POST_EP_PLAT);
		lsCarrier.add(Constants.AU_AUSTRALIA_POST_EPARCEL);
		lsCarrier.add(Constants.AU_AUSTRALIA_POST_EMS);
		lsCarrier.add(Constants.AU_DHL);
		lsCarrier.add(Constants.AU_STAR_TRACK_EXPRESS);
		lsCarrier.add(Constants.AU_UPS);
		lsCarrier.add(Constants.AU_FEDEX);
		lsCarrier.add(Constants.AU_TNT);
		lsCarrier.add(Constants.AU_TOLL_IPEC);
		lsCarrier.add(Constants.AU_OTHER);
		lsCarrier.add(Constants.FR_SUIVI);
		lsCarrier.add(Constants.IT_EBOOST_SDA);
		lsCarrier.add(Constants.ES_CORREOS_DE_ESPANA);
		lsCarrier.add(Constants.ES_DHL);
		lsCarrier.add(Constants.ES_UPS);
		lsCarrier.add(Constants.ES_FEDEX);
		lsCarrier.add(Constants.ES_OTHER);
		lsCarrier.add(Constants.AT_AUSTRIAN_POST_EMS);
		lsCarrier.add(Constants.AT_AUSTRIAN_POST_PPRIME);
		lsCarrier.add(Constants.AT_AUSTRIAN_POST_PPRIME);
		lsCarrier.add(Constants.CN_CHINA_POST_EMS);
		lsCarrier.add(Constants.CN_FEDEX);
		lsCarrier.add(Constants.CN_TNT);
		lsCarrier.add(Constants.CN_UPS);
		lsCarrier.add(Constants.CN_OTHER);
		return lsCarrier;
	}
	private List<String> listCarrierPPES(){
		List<String> lsCarrier = new ArrayList<String>();
		lsCarrier.add(Constants.PLEASE_SELECT);
		lsCarrier.add(Constants.FEDEX);
		lsCarrier.add(Constants.UPS);
		lsCarrier.add(Constants.FOUR_PX_EXPRESS);
		lsCarrier.add(Constants.EMS);
		lsCarrier.add(Constants.USPS);
		lsCarrier.add(Constants.AIRBORNE_EXPRESS);
		lsCarrier.add(Constants.DHL);
		lsCarrier.add(Constants.AIRSURE);
		lsCarrier.add(Constants.ROYAL_MAIL);
		lsCarrier.add(Constants.PARCELFORCE);
		lsCarrier.add(Constants.SWIFTAIR);
		lsCarrier.add(Constants.ES_CORREOS_DE_ESPANA);
		lsCarrier.add(Constants.ES_DHL);
		lsCarrier.add(Constants.ES_UPS);
		lsCarrier.add(Constants.ES_FEDEX);
		lsCarrier.add(Constants.ES_OTHER);
		lsCarrier.add(Constants.CN_CHINA_POST_EMS);
		lsCarrier.add(Constants.CN_FEDEX);
		lsCarrier.add(Constants.CN_TNT);
		lsCarrier.add(Constants.CN_UPS);
		lsCarrier.add(Constants.CN_OTHER);
		return lsCarrier;
	}
	
	private List<String> listCarrierPPFR(){
		List<String> lsCarrier = new ArrayList<String>();
		lsCarrier.add(Constants.PLEASE_SELECT);
		lsCarrier.add(Constants.FEDEX);
		lsCarrier.add(Constants.LAPOSTE);
		lsCarrier.add(Constants.FOUR_PX_EXPRESS);
		lsCarrier.add(Constants.UPS);
		lsCarrier.add(Constants.EMS);
		lsCarrier.add(Constants.USPS);
		lsCarrier.add(Constants.AIRBORNE_EXPRESS);
		lsCarrier.add(Constants.DHL);
		lsCarrier.add(Constants.AIRSURE);
		lsCarrier.add(Constants.ROYAL_MAIL);
		lsCarrier.add(Constants.PARCELFORCE);
		lsCarrier.add(Constants.SWIFTAIR);
		lsCarrier.add(Constants.FR_CHRONOPOST);
		lsCarrier.add(Constants.FR_COLIPOSTE);
		lsCarrier.add(Constants.FR_DHL);
		lsCarrier.add(Constants.FR_UPS);
		lsCarrier.add(Constants.FR_TNT);
		lsCarrier.add(Constants.FR_GLS);
		lsCarrier.add(Constants.CN_CHINA_POST_EMS);
		lsCarrier.add(Constants.CN_FEDEX);
		lsCarrier.add(Constants.CN_TNT);
		lsCarrier.add(Constants.CN_UPS);
		lsCarrier.add(Constants.CN_OTHER);
		return lsCarrier;
	}
	
	private List<String> listCarrierPPUK(){
		List<String> lsCarrier = new ArrayList<String>();
		lsCarrier.add(Constants.ROYAL_MAIL);
		lsCarrier.add(Constants.EVRi);
		lsCarrier.add(Constants.YODEL);
		lsCarrier.add(Constants.PLEASE_SELECT);
		lsCarrier.add(Constants.FOUR_PX_EXPRESS);
		lsCarrier.add(Constants.FEDEX);
		lsCarrier.add(Constants.UPS);
		lsCarrier.add(Constants.EMS);
		lsCarrier.add(Constants.USPS);
		lsCarrier.add(Constants.AIRBORNE_EXPRESS);
		lsCarrier.add(Constants.DHL);
		lsCarrier.add(Constants.AIRSURE);
		lsCarrier.add(Constants.PARCELFORCE);
		lsCarrier.add(Constants.SWIFTAIR);
		lsCarrier.add(Constants.UK_PARCELFORCE);
		lsCarrier.add(Constants.UK_ROYALMAIL_SPECIAL);
		lsCarrier.add(Constants.UK_ROYALMAIL_RECORDED);
		lsCarrier.add(Constants.UK_ROYALMAIL_INT_SIGNED);
		lsCarrier.add(Constants.UK_ROYALMAIL_AIRSURE);
		lsCarrier.add(Constants.UK_UPS);
		lsCarrier.add(Constants.UK_FEDEX);
		lsCarrier.add(Constants.UK_AIRBORNE_EXPRESS);
		lsCarrier.add(Constants.UK_DHL);
		lsCarrier.add(Constants.UK_OTHER);
		lsCarrier.add(Constants.UK_CANNOT_PROV_TRACK);
		lsCarrier.add(Constants.CN_CHINA_POST_EMS);
		lsCarrier.add(Constants.CN_FEDEX);
		lsCarrier.add(Constants.CN_TNT);
		lsCarrier.add(Constants.CN_UPS);
		lsCarrier.add(Constants.CN_OTHER);
		return lsCarrier;
	}
	private List<String> listCarrierPPUS(){
		List<String> lsCarrier = new ArrayList<String>();
		lsCarrier.add(Constants.USPS);
		lsCarrier.add(Constants.PLEASE_SELECT);
		lsCarrier.add(Constants.FEDEX);
		lsCarrier.add(Constants.FOUR_PX_EXPRESS);
		lsCarrier.add(Constants.UPS);
		lsCarrier.add(Constants.EMS);
		lsCarrier.add(Constants.AIRBORNE_EXPRESS);
		lsCarrier.add(Constants.DHL);
		lsCarrier.add(Constants.AIRSURE);
		lsCarrier.add(Constants.ROYAL_MAIL);
		lsCarrier.add(Constants.PARCELFORCE);
		lsCarrier.add(Constants.SWIFTAIR);
		lsCarrier.add(Constants.CN_CHINA_POST_EMS);
		lsCarrier.add(Constants.CN_FEDEX);
		lsCarrier.add(Constants.CN_TNT);
		lsCarrier.add(Constants.CN_UPS);
		lsCarrier.add(Constants.CN_OTHER);
		return lsCarrier;
	}
	private List<String> listCarrierPPDE(){
		List<String> lsCarrier = new ArrayList<String>();
		lsCarrier.add(Constants.DE_DHL);
		lsCarrier.add(Constants.DHL);
		lsCarrier.add(Constants.PLEASE_SELECT);
		lsCarrier.add(Constants.FOUR_PX_EXPRESS);
		lsCarrier.add(Constants.FEDEX);
		lsCarrier.add(Constants.UPS);
		lsCarrier.add(Constants.EMS);
		lsCarrier.add(Constants.USPS);
		lsCarrier.add(Constants.AIRBORNE_EXPRESS);
		lsCarrier.add(Constants.AIRSURE);
		lsCarrier.add(Constants.ROYAL_MAIL);
		lsCarrier.add(Constants.PARCELFORCE);
		lsCarrier.add(Constants.SWIFTAIR);
		lsCarrier.add(Constants.DE_DP_DHL_WITHIN_EUROPE);
		lsCarrier.add(Constants.DE_DP_DHL_T_AND_T_EXPRESS);
		lsCarrier.add(Constants.DE_DHL_DP_INTL_SHIPMENTS);
		lsCarrier.add(Constants.DE_GLS);
		lsCarrier.add(Constants.DE_DPD_DELISTACK);
		lsCarrier.add(Constants.DE_HERMES);
		lsCarrier.add(Constants.DE_UPS);
		lsCarrier.add(Constants.DE_FEDEX);
		lsCarrier.add(Constants.DE_TNT);
		lsCarrier.add(Constants.DE_OTHER);
		lsCarrier.add(Constants.CN_CHINA_POST_EMS);
		lsCarrier.add(Constants.CN_FEDEX);
		lsCarrier.add(Constants.CN_TNT);
		lsCarrier.add(Constants.CN_UPS);
		lsCarrier.add(Constants.CN_OTHER);
		return lsCarrier;
	}
	
	private List<String> listCarrierPPAU(){
		List<String> lsCarrier = new ArrayList<String>();
		lsCarrier.add(Constants.AU_AUSTRALIA_POST);
		lsCarrier.add(Constants.FOUR_PX_EXPRESS);
		lsCarrier.add(Constants.FEDEX);
		lsCarrier.add(Constants.UPS);
		lsCarrier.add(Constants.EMS);
		lsCarrier.add(Constants.USPS);
		lsCarrier.add(Constants.AIRBORNE_EXPRESS);
		lsCarrier.add(Constants.DHL);
		lsCarrier.add(Constants.AIRSURE);
		lsCarrier.add(Constants.ROYAL_MAIL);
		lsCarrier.add(Constants.PARCELFORCE);
		lsCarrier.add(Constants.SWIFTAIR);
		lsCarrier.add(Constants.AU_AUSTRALIA_POST_EP_PLAT);
		lsCarrier.add(Constants.AU_AUSTRALIA_POST_EPARCEL);
		lsCarrier.add(Constants.AU_AUSTRALIA_POST_EMS);
		lsCarrier.add(Constants.AU_DHL);
		lsCarrier.add(Constants.AU_STAR_TRACK_EXPRESS);
		lsCarrier.add(Constants.AU_UPS);
		lsCarrier.add(Constants.AU_FEDEX);
		lsCarrier.add(Constants.AU_TNT);
		lsCarrier.add(Constants.AU_TOLL_IPEC);
		lsCarrier.add(Constants.AU_OTHER);
		lsCarrier.add(Constants.CN_CHINA_POST_EMS);
		lsCarrier.add(Constants.CN_FEDEX);
		lsCarrier.add(Constants.CN_TNT);
		lsCarrier.add(Constants.CN_UPS);
		lsCarrier.add(Constants.CN_OTHER);
		lsCarrier.add(Constants.PLEASE_SELECT);
		return lsCarrier;
	}
	
	private boolean voidcapturePP(Order order, Gson gson){
		try {
			String paypalEmail = order.getPay_pal_account_id();
			PaypalAccount paypalAccount = paypalServce.findByEmail(paypalEmail);
			String clientID = paypalAccount.getClientid();
			String clientSecret = paypalAccount.getClientsecret();
			APIContext apiContext = new APIContext(clientID,
					clientSecret, Constants.EXECUTION_MODE);
			String fetch = apiContext.fetchAccessToken();
			HttpClient clientPost = HttpClientBuilder.create().build();
			HttpPost httpPost = new HttpPost(Constants.CAPTURE_PP+ order.getTransaction_id()+"/void");
			httpPost.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
			httpPost.setHeader(Constants.AUTHOR, fetch);
			HttpResponse responsePost = clientPost.execute(httpPost);
			int statusCode = responsePost.getStatusLine().getStatusCode();
			if(HttpStatus.SC_OK == statusCode || HttpStatus.SC_CREATED == statusCode || HttpStatus.SC_NO_CONTENT == statusCode){
				return true;
			}else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private boolean capturePaypal(Order order, Gson gson, String partial){
		try {
			String paypalEmail = order.getPay_pal_account_id();
			PaypalAccount paypalAccount = paypalServce.findByEmail(paypalEmail);
//			String domain = order.getStoredomain();
			if(paypalAccount != null) {
				String clientID = paypalAccount.getClientid();
				String clientSecret = paypalAccount.getClientsecret();
				APIContext apiContext = new APIContext(clientID,
						clientSecret, Constants.EXECUTION_MODE);
				String fetch = apiContext.fetchAccessToken();
				HttpClient client = HttpClientBuilder.create().build();
				String url = Constants.CAPTURE_PP+ order.getTransaction_id();
				HttpGet httpGet =  new HttpGet(url);
				httpGet.setHeader(Constants.AUTHOR, fetch);
				httpGet.setHeader(Constants.CONTENT_TYPE,
						Constants.APPLICATION_JSON);
				HttpResponse response = client.execute(httpGet);
				int statusCodeAuthor = response.getStatusLine().getStatusCode();
				
				if (statusCodeAuthor == 404){
					String storedomain = order.getStoredomain();
					Store store = storeService.getStoreByDomain(storedomain);
					String listPP = store.getPpacc();
					fetch = getFetchAgain(listPP, paypalEmail, order);
					if(fetch != null){
						HttpGet httpGetnew =  new HttpGet(url);
						httpGetnew.setHeader(Constants.AUTHOR, fetch);
						httpGetnew.setHeader(Constants.CONTENT_TYPE,
								Constants.APPLICATION_JSON);
						response = client.execute(httpGetnew);
					}
				}
				if (statusCodeAuthor != 200 && fetch == null) {
					order.setCapture_issue(Constants.INCORRECT_PAYPAL);
					orderService.saveOrder(order);
					return false;
				}
				BufferedReader br = new BufferedReader(new InputStreamReader(
						(response.getEntity().getContent())));
				
				String output;
				String authorAPI = null;
				while ((output = br.readLine()) != null) {
					authorAPI = output;
				}
				Type collectionTypeAuthor = new TypeToken<Authorizations>() {
				}.getType();
				Authorizations authorization = gson.fromJson(authorAPI,
						collectionTypeAuthor);
				AmountCapture amountCapture = authorization.getAmount();
				String invoice_id = authorization.getInvoice_id();
				if(amountCapture == null){
					HttpClient client1 = HttpClientBuilder.create().build();
					String url1 = Constants.SALE_PP+ order.getTransaction_id();
					HttpGet httpGet1 =  new HttpGet(url1);
					httpGet1.setHeader(Constants.AUTHOR, fetch);
					httpGet1.setHeader(Constants.CONTENT_TYPE,
							Constants.APPLICATION_JSON);
					HttpResponse response1 = client1.execute(httpGet1);
					if (response1.getStatusLine().getStatusCode() != 200) {
						System.out.println("error author: " + order.getOname());
					}
					BufferedReader br1 = new BufferedReader(new InputStreamReader(
							(response1.getEntity().getContent())));
					
					String output1;
					String saleAPI = null;
					while ((output1 = br1.readLine()) != null) {
						saleAPI = output1;
					}
					Type collectionTypeSale = new TypeToken<SaleAPI>() {
					}.getType();
					SaleAPI saleApi = gson.fromJson(saleAPI,
							collectionTypeSale);
					amountCapture = new AmountCapture(saleApi.getAmount().getTotal(), saleApi.getAmount().getCurrency());
					
					invoice_id = saleApi.getInvoice_number();
				}
				Capture capture = new Capture(amountCapture, invoice_id, true);
				if(partial != null) {
					AmountCapture amountCaptureRoot = capture.getAmountCapture();
					amountCaptureRoot.setValue(partial);
					capture.setAmountCapture(amountCaptureRoot);
				}
				String jsonData = gson.toJson(capture);
				StringEntity params =new StringEntity(jsonData);
				HttpClient clientPost = HttpClientBuilder.create().build();
				HttpPost httpPost = new HttpPost(Constants.CAPTURE_PP+ order.getTransaction_id()+"/capture");
				httpPost.setEntity(params);
				httpPost.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
				httpPost.setHeader(Constants.AUTHOR, fetch);
				HttpResponse responsePost = clientPost.execute(httpPost);
				int statusCode = responsePost.getStatusLine().getStatusCode();
				if(HttpStatus.SC_OK == statusCode || HttpStatus.SC_CREATED == statusCode){
					BufferedReader brPost = new BufferedReader(new InputStreamReader(
							(responsePost.getEntity().getContent())));
					
					String outputPost;
					String reponseAPI = null;
					while ((outputPost = brPost.readLine()) != null) {
						reponseAPI = outputPost;
					}
					Type collectionTypePost = new TypeToken<CaptureResponse>() {
					}.getType();
					CaptureResponse captureResponse = gson.fromJson(reponseAPI,
							collectionTypePost);
					String id = captureResponse.getId();
					if(id != null && !id.equalsIgnoreCase(order.getTransaction_id())){
						order.setTransaction_id(id);
						order.setCapture_status(captureResponse.getStatus());
						String price = order.getPrice();
						int open = price.indexOf("(");
						double priceRoot = Double.valueOf(price.substring(0, open));
						String capture_total = paypalAccount.getCapturetotal();
						if(capture_total == null) {
							paypalAccount.setCapturetotal(String.valueOf(priceRoot));
						}else {
							double newTotal = priceRoot + Double.valueOf(capture_total);
							paypalAccount.setCapturetotal(String.valueOf(newTotal));
						}
						orderService.saveOrder(order);
						//update withdrawDate
						Calendar cal = Calendar.getInstance();
						cal.add(Calendar.DATE, 21);
						SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
						String wd = format1.format(cal.getTime());
						paypalAccount.setWithdraw(wd);
						paypalServce.savePaypalAccount(paypalAccount);
						
					}
					return true;
				}else if (HttpStatus.SC_UNPROCESSABLE_ENTITY == statusCode){
					BufferedReader brPost = new BufferedReader(new InputStreamReader(
							(responsePost.getEntity().getContent())));
					
					String outputPost;
					String reponseAPI = null;
					while ((outputPost = brPost.readLine()) != null) {
						reponseAPI = outputPost;
					}
					Type collectionTypePost = new TypeToken<CaptureFail>() {
					}.getType();
					CaptureFail captureFail = gson.fromJson(reponseAPI, collectionTypePost);
					if(captureFail != null && captureFail.getDetails() != null){
						order.setCapture_status(Constants.CAPTURED_FAILED);
						List<CaptureFailDetail> captureFailDetail = captureFail.getDetails();
						if(captureFailDetail != null && !captureFailDetail.isEmpty()){
							order.setCapture_issue(captureFailDetail.get(0).getIssue());
						}
						orderService.saveOrder(order);
					}
					return false;
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	
	private String getFetchAgain(String listPP, String failPP, Order order){
		try {
			String[] arrayPP = listPP.split(Constants.COMMA);
			PaypalAccount ppFail = paypalServce.findByEmail(failPP);
			if(arrayPP.length > 0){
				for (String pp : arrayPP) {
					if(!pp.equalsIgnoreCase(ppFail.getUsername())){
						PaypalAccount paypalAccount = paypalServce.findByUsername(pp);
						String clientID = paypalAccount.getClientid();
						String clientSecret = paypalAccount.getClientsecret();
						APIContext apiContext = new APIContext(clientID,
								clientSecret, Constants.EXECUTION_MODE);
						String fetch = apiContext.fetchAccessToken();
						HttpClient client = HttpClientBuilder.create().build();
						String url = Constants.CAPTURE_PP+ order.getTransaction_id();
						HttpGet httpGet =  new HttpGet(url);
						httpGet.setHeader(Constants.AUTHOR, fetch);
						httpGet.setHeader(Constants.CONTENT_TYPE,
								Constants.APPLICATION_JSON);
						HttpResponse response = client.execute(httpGet);
						int statusCodeAuthor = response.getStatusLine().getStatusCode();
						if (statusCodeAuthor == 200) {
							return fetch;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	private String converWooTrack(String carrier){
		String returnCarrier = carrier;
		if(carrier.equalsIgnoreCase(Constants.FEDEX)){
			returnCarrier = Constants.Fedex;
		}
		return returnCarrier;
	}
	private boolean refundPaypal(Order order, Gson gson){
		try {
			
		String paypalEmail = order.getPay_pal_account_id();
		PaypalAccount paypalAccount = paypalServce.findByEmail(paypalEmail);
		String clientID = paypalAccount.getClientid();
		String clientSecret = paypalAccount.getClientsecret();
		APIContext apiContext = new APIContext(clientID,
				clientSecret, Constants.EXECUTION_MODE);
		String fetch = apiContext.fetchAccessToken();
		HttpClient client = HttpClientBuilder.create().build();
		String url = Constants.CAPTURE_DETAIL+ order.getTransaction_id();
		HttpGet httpGet =  new HttpGet(url);
		httpGet.setHeader(Constants.AUTHOR, fetch);
		httpGet.setHeader(Constants.CONTENT_TYPE,
				Constants.APPLICATION_JSON);
		HttpResponse response = client.execute(httpGet);
		if (response.getStatusLine().getStatusCode() != 200) {
			System.out.println("error refund: " + order.getOname());
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(
				(response.getEntity().getContent())));

		String output;
		String saleAPI = null;
		while ((output = br.readLine()) != null) {
			saleAPI = output;
		}
		Type collectionTypeSale = new TypeToken<SaleAPINew>() {
		}.getType();
		SaleAPINew saleApi = gson.fromJson(saleAPI,
				collectionTypeSale);
		if(saleApi != null){
			String total = saleApi.getAmount().getValue();
			String currency = saleApi.getAmount().getCurrency_code();
			String invoice_number = saleApi.getInvoice_id();
			AmountCapture amount = new AmountCapture(total, currency);
			AmountRefund saleRefundApi = new AmountRefund(amount, invoice_number,"Out of stock");
			String jsonData = gson.toJson(saleRefundApi);
			StringEntity params =new StringEntity(jsonData);
			HttpClient clientPost = HttpClientBuilder.create().build();
			HttpPost httpPost = new HttpPost(Constants.REFUND_PP+ order.getTransaction_id()+"/refund");
			httpPost.setEntity(params);
			httpPost.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
			httpPost.setHeader(Constants.AUTHOR, fetch);
			HttpResponse responsePost = clientPost.execute(httpPost);
			int statusCode = responsePost.getStatusLine().getStatusCode();
			if(HttpStatus.SC_OK == statusCode || HttpStatus.SC_CREATED == statusCode){
				order.setStatus(Constants.CANCEL_SHOPIFY);
				order.setPayment_status(Constants.REFUNDED);
				if(Constants.DISPUTED.equalsIgnoreCase(order.getDisputed())){
					order.setDispute_state(Constants.DISPUTE_RESOLVED);
				}
				orderService.saveOrder(order);
				String capture_total = paypalAccount.getCapturetotal();
				String price = order.getPrice();
				int open = price.indexOf("(");
				double priceRoot = Double.valueOf(price.substring(0, open));
				double newTotal = Double.valueOf(capture_total) - priceRoot;
				paypalAccount.setCapturetotal(String.valueOf(newTotal));
				paypalServce.savePaypalAccount(paypalAccount);
				return true;
			}else {
				return false;
			}
		}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	private String checkBalance(String balance) {
		String available = "0";
		if(balance.contains("&L_CURRENCYCODE0")) {
			String amountArary = balance.substring(0,balance.indexOf("&L_CURRENCYCODE0"));
			if(amountArary.contains("L_AMT0")) {
				int indexamt0 = amountArary.indexOf("L_AMT0");
				String amt0 = amountArary.substring(indexamt0 +7, indexamt0 + 8);
				int amount = Integer.valueOf(amt0);
				if(amount > 0) {
					available = amountArary.substring(indexamt0 +7, indexamt0 + 10);
				}
			}
			if(amountArary.contains("L_AMT1")) {
				int indexamt1 = amountArary.indexOf("L_AMT1");
				String amt1 = amountArary.substring(indexamt1 +7, indexamt1 + 8);
				int amount = Integer.valueOf(amt1);
				if(amount > 0) {
					available = amountArary.substring(indexamt1 +7, indexamt1 + 10);
				}
			}
			if(amountArary.contains("L_AMT2")) {
				int indexamt2 = amountArary.indexOf("L_AMT2");
				String amt2 = amountArary.substring(indexamt2 +7, indexamt2 + 8);
				int amount = Integer.valueOf(amt2);
				if(amount > 0) {
					available = amountArary.substring(indexamt2 +7, indexamt2 + 10);
				}
			}
			if(amountArary.contains("L_AMT3")) {
				int indexamt3 = amountArary.indexOf("L_AMT3");
				String amt3 = amountArary.substring(indexamt3 +7, indexamt3 + 8);
				int amount = Integer.valueOf(amt3);
				if(amount > 0) {
					available = amountArary.substring(indexamt3 +7, indexamt3 + 10);
				}
			}
		}
		return available;
	}
	
}
