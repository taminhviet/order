import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.google.gson.Gson;
import com.paypal.api.payments.Currency;
import com.paypal.api.payments.Payout;
import com.paypal.api.payments.PayoutBatch;
import com.paypal.api.payments.PayoutItem;
import com.paypal.api.payments.PayoutSenderBatchHeader;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;
import com.stripe.exception.StripeException;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import shoes.sup.model.APISkyResponse;
import shoes.sup.model.ChewyProduct;
import shoes.sup.model.Export;
import shoes.sup.model.PayoutPP;
import shoes.sup.model.Permalink;
import shoes.sup.model.ProductPrice;
import shoes.sup.model.SkyTrackBody;
import shoes.sup.model.TrackingData;
import shoes.sup.model.TrackingNumber;
import shoes.sup.model.WooLink;
import shoes.sup.model.XMLProduct;
import shoes.sup.service.GMCService;
import shoes.sup.service.OrderService;
import shoes.sup.util.Constants;

public class TestMain {

	@Autowired
	public static OrderService orderService;
	private static int count = 0;
	private static String ck = "ck_d513d5454e13f23cd8695c380afccd314dbdd657";
	private static String cs = "cs_d79cc05c37ec136deea858eaa88850a8227016a8";
	private static String domainProduct = "https://koaris.fr/shop/";
	private static String domain = "koaris.fr/shop";
	private static String folder = "koaris";
	public static final String portraitId = "portraitId";
	public static final String EMPTY = "";
	public static final String SPACE = " ";
	public static final String QUANTITY = "21";
	public static final String TRUE = "TRUE";
	public static final int FEDEX = 6;
	public static final int USPS = 109;
	public static final int UK = 2;
	public static final int US = 1;
	public static final String FEDEX_URL = "fedex";
	public static final String USPS_URL = "usps";
	@Autowired
	public static GMCService gmcservice;

	public static void main(String[] args) throws IOException, StripeException {
		findFile();
	}
	private static void payoutPP() throws IOException {
		try {
			String clientID = "AeXST7JXBXd5Ew8bN9uh0cXtrDqX4CVLJxiYdNXEaogMKYOG7CJU-Eblovsl0T8QFkDq1_Csa0t4rcKg";
			String clientSecret = "EJ_QQXU4LLdUsw2pN5zf3LWjI8YeRGTE-9EXfNKC4GNhMRjax4k6wm-H5o5Gaj9vuU0zTTuVXhCtePZS";
			APIContext apiContext = new APIContext(clientID,
					clientSecret, Constants.EXECUTION_MODE);
			String fetch = apiContext.fetchAccessToken();
			URL url = new URL("https://api-m.paypal.com/v1/payments/payouts");
			HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
			httpConn.setRequestMethod("POST");

			httpConn.setRequestProperty("Content-Type", "application/json");
			httpConn.setRequestProperty("Authorization", fetch);

			httpConn.setDoOutput(true);
			// Create the JSON payload for the payout
	        String payoutJson = "{"
	            + "\"sender_batch_header\": {"
	            + "\"sender_batch_id\": \"Payouts_2024_100001\","
	            + "\"email_subject\": \"You have a payout!\","
	            + "\"email_message\": \"You have received a payout of 10 AUD! Thanks for using our service!\""
	            + "},"
	            + "\"items\": ["
	            + "{"
	            + "\"recipient_type\": \"EMAIL\","
	            + "\"amount\": {"
	            + "\"value\": \"10.00\","
	            + "\"currency\": \"AUD\""
	            + "},"
	            + "\"note\": \"Thanks for your patronage!\","
	            + "\"sender_item_id\": \"product1\","
	            + "\"receiver\": \"deoshike@gmail.com\""
	            + "}"
	            + "]"
	            + "}";

	        // Send the JSON payload to PayPal API
	        OutputStreamWriter writer = new OutputStreamWriter(httpConn.getOutputStream());
	        writer.write(payoutJson);
	        writer.flush();
	        writer.close();
	        httpConn.getOutputStream().close();

	        // Get the response from the PayPal API
	        InputStream responseStream = httpConn.getResponseCode() / 100 == 2
	                ? httpConn.getInputStream()
	                : httpConn.getErrorStream();
	        Scanner s = new Scanner(responseStream).useDelimiter("\\A");
	        String response = s.hasNext() ? s.next() : "";
	        System.out.println(response);

        } catch (PayPalRESTException e) {
            e.printStackTrace();
        }
		
	}
	
	public static void payoutbatchPP() {
		try {
            // Initialize API context
			String clientID = "AeXST7JXBXd5Ew8bN9uh0cXtrDqX4CVLJxiYdNXEaogMKYOG7CJU-Eblovsl0T8QFkDq1_Csa0t4rcKg";
			String clientSecret = "EJ_QQXU4LLdUsw2pN5zf3LWjI8YeRGTE-9EXfNKC4GNhMRjax4k6wm-H5o5Gaj9vuU0zTTuVXhCtePZS";
			APIContext apiContext = new APIContext(clientID,
					clientSecret, Constants.EXECUTION_MODE);

            // Create a new payout object
            Payout payout = new Payout();

            // Create sender batch header
            PayoutSenderBatchHeader senderBatchHeader = new PayoutSenderBatchHeader()
                    .setSenderBatchId("batch-" + System.currentTimeMillis())
                    .setEmailSubject("You have a payout!");

            // Create payout items
            List<PayoutItem> items = new ArrayList<>();
            items.add(new PayoutItem()
                    .setRecipientType("EMAIL")
                    .setAmount(new Currency().setCurrency("AUD").setValue("15.00"))
                    .setReceiver("deoshike@gmail.com")
                    .setSenderItemId("item-1"));

            // Set batch header and items
            payout.setSenderBatchHeader(senderBatchHeader);
            payout.setItems(items);

            // Execute the payout
            PayoutBatch payoutBatch = payout.create(apiContext, new HashMap<String, String>());

            System.out.println("Payout Batch ID: " + payoutBatch.getBatchHeader().getPayoutBatchId());
            System.out.println("Batch Status: " + payoutBatch.getBatchHeader().getBatchStatus());

        } catch (PayPalRESTException e) {
            e.printStackTrace();
        }
	}
	
	public static void findFile() {
		// Replace with the directory you want to start the search from
      String startDirectory = "E:\\Firefox\\Profiles";
      String textToSearch = "kulwinderwatson";

      File directory = new File(startDirectory);
      File[] files = directory.listFiles();
      for (File file : files) {
      	if (file.isDirectory()) {
              String filename = file.getName();
              if(filename.contains(textToSearch)) {
              	System.out.println(filename);
              }
          }
      }
	}
	
	public static File findDirectoryByNameContainingText(File directory, String targetDirectoryName, String textToSearch) {
        if (!directory.isDirectory()) {
            return null;
        }

        // Check if the current directory is the target directory
        if (directory.getName().equals(targetDirectoryName)) {
            // Search for the text within the files in this directory
            if (containsText(directory, textToSearch)) {
                return directory;
            }
        }

        // Recursively search in subdirectories
        File[] files = directory.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    File found = findDirectoryByNameContainingText(file, targetDirectoryName, textToSearch);
                    if (found != null) {
                        return found;
                    }
                }
            }
        }
        return null;
    }

    public static boolean containsText(File directory, String textToSearch) {
        File[] files = directory.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isFile()) {
                    try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
                        String line;
                        while ((line = reader.readLine()) != null) {
                            if (line.contains(textToSearch)) {
                                return true;
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return false;
    }
	
	private static void showTracking(String trackingnumber) {
		try {
			Gson gson = new Gson();
			TrackingNumber tracking = new TrackingNumber(trackingnumber);
			String jsonBody = gson.toJson(tracking);
			StringEntity params =new StringEntity(jsonBody);
			HttpClient client = HttpClientBuilder.create().build();
			String url = "https://api.iskytracking.com/v2/tracking/" + FEDEX_URL;
			HttpPost httpPost = new HttpPost(url);
			httpPost.setEntity(params);
			httpPost.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
			httpPost.setHeader(Constants.AUTHOR, "Bearer VHQ9VV4YJZ3MASWHJZKE");
			HttpResponse response = client.execute(httpPost);
			int statusCode = response.getStatusLine().getStatusCode();
			BufferedReader br = new BufferedReader(new InputStreamReader(
					(response.getEntity().getContent())));
			
			String output;
			String responeJson = null;
			while ((output = br.readLine()) != null) {
				responeJson = output;
			}
			System.out.println(responeJson);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	private static void findTracking() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateOrder = "2024-06-05 00:00:00";
		Gson gson = new Gson();
		try {
			Date orderDate = dateFormat.parse(dateOrder);
			int London = 22051;
			String date_range = "06/02/2024 - 06/06/2024";
			String zip_code = "";
			int start = 0;
			SkyTrackBody tracktrbody = new SkyTrackBody(UK, zip_code, FEDEX, start, London, date_range, 10000);
			String jsonBody = gson.toJson(tracktrbody);
			StringEntity params =new StringEntity(jsonBody);
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost httpPost = new HttpPost("https://api.iskytracking.com/v2/tracking");
			httpPost.setEntity(params);
			httpPost.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
			httpPost.setHeader(Constants.AUTHOR, "Bearer VHQ9VV4YJZ3MASWHJZKE");
			HttpResponse response = client.execute(httpPost);
			int statusCode = response.getStatusLine().getStatusCode();
			BufferedReader br = new BufferedReader(new InputStreamReader(
					(response.getEntity().getContent())));
			
			String output;
			String responeJson = null;
			while ((output = br.readLine()) != null) {
				responeJson = output;
			}
			APISkyResponse apiResponse = gson.fromJson(responeJson, APISkyResponse.class);
			
			List<TrackingData> trackingDataList = apiResponse.getData();
			
	        // Print the tracking data
	        for (TrackingData trackingData : trackingDataList) {
	        	String zipcode = trackingData.getZipcode();
	        	String status = trackingData.getStatus();
	        	String packedat = trackingData.getPackedAt();
	        	Date packagedDate = dateFormat.parse(packedat);
	        	if(zipcode.startsWith("SW1") && orderDate.before(packagedDate)) {
	        		System.out.println("ID: " + trackingData.getId());
	                System.out.println("Tracking Number: " + trackingData.getTrackingNumber());
	                System.out.println("Tracking Type: " + trackingData.getTrackingType());
	                System.out.println("Scheduled Delivery: " + trackingData.getScheduledDelivery());
	                System.out.println("Packed At: " + trackingData.getPackedAt());
	                System.out.println("Pickup At: " + trackingData.getPickupAt());
	                System.out.println("Shipping Address: " + trackingData.getShippingAddress());
	                System.out.println("Zipcode: " + trackingData.getZipcode());
	                System.out.println("Country: " + trackingData.getCountry());
	                System.out.println("State: " + trackingData.getState());
	                System.out.println("Weight: " + trackingData.getWeight());
	                System.out.println("Status: " + trackingData.getStatus());
	                System.out.println("Scheduled Vol: " + trackingData.getScheduledVol());
	                System.out.println("URL: " + trackingData.getUrl());
	                System.out.println("PRPT: " + trackingData.getPrpt());
	                System.out.println("ED: " + trackingData.getEd());
	        	}
	        }
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	private static void copyimagetoparent() {
		// Define the path of the parent folder
		Path parentFolderPath = Paths.get("E:/Working/Drop/AUS-20240604T151229Z-001/AUS");

		try {
			// Iterate over all subfolders
			java.nio.file.Files.walkFileTree(parentFolderPath, new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
					if (!dir.equals(parentFolderPath)) { // Skip the parent folder itself
						// Process each subfolder
						processSubfolder(dir, parentFolderPath);
					}
					return FileVisitResult.CONTINUE;
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void processSubfolder(Path subfolderPath, Path parentFolderPath) {
        try {
            // Find the only file in the subfolder
            java.nio.file.Files.walk(subfolderPath, 1) // The '1' limits the search to the subfolder level only
                .filter(java.nio.file.Files::isRegularFile)
                .findFirst()
                .ifPresent(file -> {
                    // Construct the new file name using the subfolder's name
                    String subfolderName = subfolderPath.getFileName().toString();
                    Path targetFile = parentFolderPath.resolve(subfolderName + getFileExtension(file));
                    
                    try {
                        // Copy the file to the parent folder and rename it
                    	java.nio.file.Files.copy(file, targetFile, StandardCopyOption.REPLACE_EXISTING);
                        System.out.println("Copied and renamed file: " + file + " to " + targetFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	
	private static BufferedImage readImage(String imagePath) {
        try {
            return ImageIO.read(new File(imagePath));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String extractTextFromImage(BufferedImage image) {
        ITesseract tesseract = new Tesseract();
        // Set the Tesseract data path, point to the tessdata directory
        tesseract.setDatapath("C:/Program Files/Tesseract-OCR/tessdata");

        try {
            return tesseract.doOCR(image);
        } catch (TesseractException e) {
            e.printStackTrace();
            return null;
        }
    }

    // Helper method to get the file extension
    private static String getFileExtension(Path file) {
        String fileName = file.getFileName().toString();
        int dotIndex = fileName.lastIndexOf('.');
        return (dotIndex == -1) ? "" : fileName.substring(dotIndex);
    }

	private static void checkPayment() {
		// Specify the path to your Excel file
		String filePath = "E:\\Payment\\Fulfill.xlsx";
		String filePathPayout = "E:\\Payment\\Payout.xlsx";
		List<String> lsOrder = new ArrayList<>();

		try {
			FileInputStream fileInputStream = new FileInputStream(new File(filePath));

			// Create a workbook instance
			Workbook workbook = WorkbookFactory.create(fileInputStream);

			// Get the first sheet of the workbook
			Sheet sheet = workbook.getSheetAt(0);

			// Iterate through each row of the sheet
			for (Row row : sheet) {
				int rowindex = row.getRowNum();
				if (rowindex == 0) {
					continue;
				}
				// Iterate through each cell of the row
				for (Cell cell : row) {
					int colindex = cell.getColumnIndex();
					String ordername = cell.getStringCellValue();
					if(ordername.startsWith("sneak")) {
						ordername = ordername.replace("sneake", "");
						ordername = ordername.replace("sneak", "");
					}
					lsOrder.add(ordername);
				}
			}

			// Close the workbook and input stream
			workbook.close();
			fileInputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		List<PayoutPP> lsPayout = new ArrayList<>();
		try {
			FileInputStream fileInputStream = new FileInputStream(new File(filePathPayout));

			// Create a workbook instance
			Workbook workbook = WorkbookFactory.create(fileInputStream);

			// Get the first sheet of the workbook
			Sheet sheet = workbook.getSheetAt(0);

			// Iterate through each row of the sheet
			for (Row row : sheet) {
				int rowindex = row.getRowNum();
				if (rowindex == 0) {
					continue;
				}
				// Iterate through each cell of the row
				PayoutPP payoutpp = new PayoutPP();
				for (Cell cell : row) {
					int colindex = cell.getColumnIndex();
					switch (colindex) {
					case 0:
						String col1 = cell.getStringCellValue();
						col1 = col1.replace("TIGER68VITA", "");
						col1 = col1.replace("-", "");
						col1 = col1.toLowerCase();
						if (col1.contains("zoom")) {
							col1 = col1.replace("zoom", "zoomxr");
						}
						if (col1.contains("sneakh")) {
							col1 = col1.replace("sneakh", "");
						}
						if (col1.contains("ott")) {
							col1 = col1.replace("ott", "otta");
						}
						if (col1.contains("par")) {
							col1 = col1.replace("par", "pari");
						}
						payoutpp.setOrder(col1.trim());
						break;
					case 1:
						payoutpp.setCurrency(cell.getStringCellValue().trim());
						break;
					case 2:
						payoutpp.setGross(cell.getNumericCellValue());
						break;
					case 3:
						payoutpp.setDate(cell.getDateCellValue());
						break;
					case 4:
						payoutpp.setType(cell.getStringCellValue().trim());
						break;

					default:
						break;
					}
				}
				lsPayout.add(payoutpp);
			}

			// Close the workbook and input stream
			workbook.close();
			fileInputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		for (String order : lsOrder) {
			// Check if the orderToCheck exists in the orderList using Java 8 streams
			boolean exists = lsPayout.stream().anyMatch(payout -> payout.getOrder().equals(order));
			if (!exists) {
				if(!order.isEmpty()) {
					System.out.println("Order not in payout: " + order);
				}
			}
		}
		double totalGross = 0;
		double totalNet = 0;

		Map<Date, Double> totalGrossByDate = new HashMap<>();
		for (PayoutPP pay : lsPayout) {
			if (pay.getCurrency().equalsIgnoreCase("USD")) {
				Date date = pay.getDate();
				double gross = pay.getGross();
				String ordername = pay.getOrder();
				String type = pay.getType();
				totalGrossByDate.put(date, totalGrossByDate.getOrDefault(date, 0.0) + gross);
				if ("General Currency Conversion".equalsIgnoreCase(type)) {
					totalNet += gross;
				} else if ("Express Checkout Payment".equalsIgnoreCase(type)) {
					gross = gross - (gross * 0.044);
					// System.out.println(ordername + "-" +gross);
					totalNet += gross;
				}
				if (!lsOrder.contains(ordername)) {
					System.out.println("Order not fulfill: " + ordername);
				}
				// System.out.println(pay.getOrder() + " - " + pay.getGross() + " - " +
				// pay.getDate());
			}
		}
		System.out.println("Net: " + totalNet);
		// Print total gross by date
		List<Map.Entry<Date, Double>> entryList = new ArrayList<>(totalGrossByDate.entrySet());
		entryList.sort(Comparator.comparing(Map.Entry::getKey));
		for (Map.Entry<Date, Double> entry : entryList) {
			System.out.println("Date: " + entry.getKey() + ", Total Gross: " + entry.getValue());
		}
	}

	private static List<Export> getProductMultiColor(Document doc, String source) {
		Set<String> setLinkIMage = new HashSet<String>();
		List<Export> lsExport = new ArrayList<Export>();
		String firstLink = getFirstLinkImage(doc);
		if (!setLinkIMage.contains(firstLink)) {
			setLinkIMage.add(firstLink);
			List<String> ImageURLs = getAllLinkImageMulti(firstLink, source);
			lsExport = addExportProduct(ImageURLs, doc);
		} else {
			System.out.println("exist image url");
		}
		return lsExport;

	}

	private static String getFirstLinkImage(Document doc) {
		Elements srcImg = doc.getElementsByTag("img");
		String linkimage = null;
		for (Element element : srcImg) {
			if (element.attr("src").contains("1280") || element.attr("src").contains("864")) {
				linkimage = element.attr("src");
				break;
			}
		}
		System.out.println(linkimage);
		return linkimage;
	}

	private static List<Export> addExportProduct(List<String> ImageURLs, Document doc) {
		List<Export> lsExport = new ArrayList<Export>();
		int i = 0;
		int k = 0;
		double j = 6;

		StringBuilder sb = new StringBuilder();
		for (int it = 0; it < ImageURLs.size(); it++) {
			if (it == 0) {
				sb.append(ImageURLs.get(it));
			} else {
				sb.append(";");
				sb.append(ImageURLs.get(it));
			}

		}

		for (String imageUrl : ImageURLs) {
			Export export = new Export();
			Element elementTitle = doc.getElementById("pdp_product_title");
			String title = elementTitle.text();
			String subTitle = EMPTY;

			Elements elementSubTitle = doc.getElementsByAttributeValue("data-test", "product-sub-title");
			for (Element element : elementSubTitle) {
				subTitle = element.text();
				break;
			}
			Elements elementPrice = doc.getElementsByAttributeValue("data-test", "product-price");
			String price = EMPTY;
			for (Element element : elementPrice) {
				price = element.text();
				break;
			}
			Elements estyleCode = doc.getElementsByClass("description-preview__style-color");
			String styleCode = EMPTY;
			for (Element element : estyleCode) {
				String fullStyleText = element.text();
				int lastIndex = fullStyleText.lastIndexOf(":");
				styleCode = (fullStyleText.substring(lastIndex + 1, fullStyleText.length()).trim());
			}
			String handle = title.concat("-").concat(styleCode).replace(SPACE, "-").toLowerCase();
			title = title.concat(SPACE).concat(subTitle).concat(SPACE).concat(styleCode);
			String des = getHTMLDesNike(doc);
			des = des.replace("\"", "\"\"");
			handle = handle.replaceAll("[^a-zA-Z0-9-]", "");
			export.setHandle(handle);
			if (k == 0) {
				export.setTitle(title);
				export.setBody("\"".concat(des).concat("\""));
				export.setWoocommerce(sb.toString());
			} else {
				export.setTitle(EMPTY);
				export.setBody(EMPTY);
			}
			k++;

			export.setVendor("Nike");
			export.setType(subTitle);
			export.setPublished(TRUE);
			export.setOption1name("Size");
			String valueSize = "US " + j;
			j = j + 0.5;
			if (valueSize.contains(".0")) {
				valueSize = valueSize.replace(".0", "");
			}
			export.setOption1value(valueSize);
			export.setVariantSKU(EMPTY);
			export.setVariantTracker("shopify");
			export.setQuantity(QUANTITY);
			export.setVariantPolicy("deny");
			export.setVariantFulfillment("manual");
			export.setPrice(price);
			export.setCompare(price);
			export.setRequireShipping(TRUE);
			export.setVariantTax(TRUE);
			export.setVariantBarcode(EMPTY);
			export.setImageURL(imageUrl);

			export.setImagePosition(String.valueOf(++i));
			export.setWeightUnit("lb");

			lsExport.add(export);
		}
		if (ImageURLs.size() < 11) {
			int noImage = 11 - ImageURLs.size();
			for (int l = 0; l < noImage; l++) {
				Export export = new Export();
				Element elementTitle = doc.getElementById("pdp_product_title");
				String title = elementTitle.text();
				String subTitle = EMPTY;

				Elements elementSubTitle = doc.getElementsByAttributeValue("data-test", "product-sub-title");
				for (Element element : elementSubTitle) {
					subTitle = element.text();
					break;
				}
				Elements elementPrice = doc.getElementsByAttributeValue("data-test", "product-price");
				String price = EMPTY;
				for (Element element : elementPrice) {
					price = element.text();
					break;
				}
				Elements estyleCode = doc.getElementsByClass("description-preview__style-color");
				String styleCode = EMPTY;
				for (Element element : estyleCode) {
					String fullStyleText = element.text();
					int lastIndex = fullStyleText.lastIndexOf(":");
					styleCode = (fullStyleText.substring(lastIndex + 1, fullStyleText.length()).trim());
				}
				String handle = title.concat("-").concat(styleCode).replace(SPACE, "-").toLowerCase();
				title = title.concat(SPACE).concat(subTitle).concat(SPACE).concat(styleCode);
				String des = getHTMLDesNike(doc);
				des = des.replace("\"", "\"\"");
				handle = handle.replaceAll("[^a-zA-Z0-9-]", "");
				export.setHandle(handle);
				if (k == 0) {
					export.setTitle(title);
					export.setBody("\"".concat(des).concat("\""));
					export.setWoocommerce(sb.toString());
				} else {
					export.setTitle(EMPTY);
					export.setBody(EMPTY);
				}
				k++;

				export.setVendor("Nike");
				export.setType(subTitle);
				export.setPublished(TRUE);
				export.setOption1name("Size");
				String valueSize = "US " + j;
				j = j + 0.5;
				if (valueSize.contains(".0")) {
					valueSize = valueSize.replace(".0", "");
				}
				export.setOption1value(valueSize);
				export.setVariantSKU(EMPTY);
				export.setVariantTracker("shopify");
				export.setQuantity(QUANTITY);
				export.setVariantPolicy("deny");
				export.setVariantFulfillment("manual");
				export.setPrice(price);
				export.setCompare(price);
				export.setRequireShipping(TRUE);
				export.setVariantTax(TRUE);
				export.setVariantBarcode(EMPTY);
				export.setImageURL(EMPTY);
				export.setImagePosition(EMPTY);
				export.setWeightUnit("lb");
				lsExport.add(export);

			}
		}
		return lsExport;
	}

	public static String getHTMLDesNike(Document doc) {
		Elements eDes = doc.getElementsByClass("description-preview");
		String des = EMPTY;
		for (Element element : eDes) {
			des = element.html();
			break;
		}
		return des;
	}

	private static List<String> getAllLinkImageMulti(String firstLink, String source) {
		List<String> listImageId = getListImageId(source);
		int lastIndex = firstLink.lastIndexOf("/");
		String firstImageID = firstLink.substring(lastIndex - 20, lastIndex);
		List<String> listImageURL = new ArrayList<String>();
		listImageURL.add(firstLink);
		int indexFirstImage = listImageId.indexOf(firstImageID);
		List<String> listImageIdMulti = new ArrayList<String>();
		listImageIdMulti.add(listImageId.get(indexFirstImage + 1));
		listImageIdMulti.add(listImageId.get(indexFirstImage + 2));
		listImageIdMulti.add(listImageId.get(indexFirstImage + 3));
		listImageIdMulti.add(listImageId.get(indexFirstImage + 4));
		for (String ImageId : listImageIdMulti) {
			if (!firstImageID.equalsIgnoreCase(ImageId)) {
				String newURL = firstLink.replace(firstImageID, ImageId);
				listImageURL.add(newURL);
			}
		}
		return listImageURL;
	}

	private static List<String> getListImageId(String source) {
		List<String> lsImagesId = new ArrayList<String>();
		for (int index = source.indexOf(portraitId); index >= 0; index = source.indexOf(portraitId, index + 1)) {
			String imageid = source.substring(index + 13, index + 33);
			if (!imageid.contains("squarishURL")) {
				lsImagesId.add(imageid);
			}

		}
		return lsImagesId;
	}

	private static String getURLSource(InputStream inputStream) throws IOException {
		StringBuilder stringBuilder = new StringBuilder();
		try {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
			String inputLine;
			while ((inputLine = bufferedReader.readLine()) != null) {
				if (inputLine.contains(portraitId)) {
					stringBuilder.append(inputLine.trim());
				}
			}

		} catch (Exception e) {

			e.printStackTrace();
		}
		return stringBuilder.toString();
	}

	public static void refeed() {
		try {
			String pathsps = "C:\\Users\\VIET IT\\Downloads\\" + folder + "\\SupFeedKoa.xml";
			File fileSPS = new File(pathsps);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
			// parse XML file
			DocumentBuilder db = dbf.newDocumentBuilder();
			org.w3c.dom.Document supdoc = db.newDocument();

			org.w3c.dom.Document docSPS = db.parse(fileSPS);
			docSPS.getDocumentElement().normalize();
			NodeList listSPS = docSPS.getElementsByTagName("item");
			List<XMLProduct> lsProductSPS = getListProductInFile(listSPS, Constants.SPS);
			org.w3c.dom.Element rootElement = supdoc.createElement("channel");
			supdoc.appendChild(rootElement);
			if (!lsProductSPS.isEmpty()) {
				for (int j = 0; j < lsProductSPS.size(); j++) {
					XMLProduct productsps = lsProductSPS.get(j);
					String id = productsps.getId();
					String title = productsps.getTitle();
					String description = productsps.getDescription();
					String gtin = productsps.getGtin();
					org.w3c.dom.Element eItem = supdoc.createElement("item");
					rootElement.appendChild(eItem);
					org.w3c.dom.Element eId = supdoc.createElement("g:id");
					eId.setTextContent(id);
					eItem.appendChild(eId);
					org.w3c.dom.Element etitle = supdoc.createElement("g:title");
					etitle.setTextContent(title);
					org.w3c.dom.Element eDescription = supdoc.createElement("g:description");
					eDescription.setTextContent(description);
					org.w3c.dom.Element etgtin = supdoc.createElement("g:tin");
					etgtin.setTextContent(gtin);
					eItem.appendChild(etitle);
					eItem.appendChild(eDescription);
					eItem.appendChild(etgtin);
				}
			}
			FileOutputStream output = new FileOutputStream(
					"C:\\Users\\VIET IT\\Downloads\\" + folder + "\\resupfeed.xml");
			writeXml(supdoc, output);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void wooSPSUpdate() {
		try {
			Gson gson = new Gson();
			boolean isFirstFeed = false;
			String path = "C:\\Users\\VIET IT\\Downloads\\" + folder + "\\tm.xml";
			String pathsps = "C:\\Users\\VIET IT\\Downloads\\" + folder + "\\sps.xml";
			File fileStore = new File(path);
			File fileSPS = new File(pathsps);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);

			// parse XML file
			DocumentBuilder db = dbf.newDocumentBuilder();
			org.w3c.dom.Document supdoc = db.newDocument();

			org.w3c.dom.Document doc = db.parse(fileStore);
			doc.getDocumentElement().normalize();

			org.w3c.dom.Document docSPS = db.parse(fileSPS);
			docSPS.getDocumentElement().normalize();

			NodeList list = doc.getElementsByTagName("item");
			NodeList listSPS = docSPS.getElementsByTagName("item");
			List<WooLink> lsChangeLinks = new ArrayList<WooLink>();
			List<WooLink> lsOldChangeLinks = new ArrayList<WooLink>();
			Set<String> sTMGrId = new HashSet<String>();
			Set<String> sSPS = new HashSet<String>();
			Set<String> sTMGrIdChanged = new HashSet<String>();
			Set<String> sSPSChanged = new HashSet<String>();
			if (!isFirstFeed) {
				readExcel(sTMGrIdChanged, sSPSChanged, lsOldChangeLinks);
			}
			Map<String, String> mapGtinImage = readFileShopify();
			List<XMLProduct> lsProductTM = getListProductInFile(list, sTMGrId, sTMGrIdChanged, sSPSChanged,
					Constants.WOO);
			List<XMLProduct> lsProductSPS = getListProductInFile(listSPS, sSPS, sTMGrIdChanged, sSPSChanged,
					Constants.SPS);

			org.w3c.dom.Element rootElement = supdoc.createElement("channel");
			supdoc.appendChild(rootElement);
			List<String> listgrTM = new ArrayList<>(sTMGrId);
			List<String> listgrSPS = new ArrayList<>(sSPS);
			List<ProductPrice> lsProductPrice = new ArrayList<ProductPrice>();
			for (int i = 0; i < listgrTM.size(); i++) {
				WooLink woolink = new WooLink();
				String gridTM = listgrTM.get(i);
				woolink.setId(gridTM);
				String gridSPS = listgrSPS.get(i);
				woolink.setSpsID(gridSPS);
				List<XMLProduct> lsSameIDTM = lsProductTM.stream()
						.filter(product -> gridTM.equalsIgnoreCase(product.getItem_group_id()))
						.collect(Collectors.toList());
				List<XMLProduct> lsSameIDSPS = lsProductSPS.stream()
						.filter(productsps -> gridSPS.equalsIgnoreCase(productsps.getItem_group_id()))
						.collect(Collectors.toList());
				if (!lsProductSPS.isEmpty()) {
					XMLProduct spsProduct = lsSameIDSPS.get(0);
					String spslink = spsProduct.getLink();
//					if (spslink.contains("#!") && spslink.contains("?")) {
//						String slug = spslink.substring(spslink.indexOf("#!")+3, spslink.indexOf("&"));
//						woolink.setSlug(slug);
//						woolink.setPermalink(domainProduct + slug);
//					}
					if (spslink.contains("product") && spslink.contains("?")) {
						String slug = spslink.substring(spslink.indexOf("product/") + 8, spslink.indexOf("?"));
						woolink.setSlug(slug);
						woolink.setPermalink(domainProduct + slug);
					}

				}
				if (!lsSameIDTM.isEmpty()) {
					XMLProduct tmProduct = lsSameIDTM.get(0);
					String tmname = tmProduct.getTitle();
					woolink.setName(tmname);
				}
				String tmLinkShopify = null;
				for (int j = 0; j < lsSameIDTM.size(); j++) {
					XMLProduct productTM = lsSameIDTM.get(j);
					String gtin = productTM.getGtin();
					tmLinkShopify = mapGtinImage.get(gtin);
					if (tmLinkShopify != null) {
						break;
					}
				}
				String regular_TM = null;
				for (int j = 0; j < lsSameIDTM.size(); j++) {
					XMLProduct productTM = lsSameIDTM.get(j);
					XMLProduct productsps = lsSameIDSPS.get(j);
					org.w3c.dom.Element eItem = supdoc.createElement("item");
					rootElement.appendChild(eItem);
					org.w3c.dom.Element eId = supdoc.createElement("g:id");
					org.w3c.dom.Element eGroupID = supdoc.createElement("g:item_group_id");
					eId.setTextContent(productsps.getId());
					String linksps = productsps.getLink();
					String regular_Price_SPS = productsps.getPrice();
					if (regular_Price_SPS.contains(" ")) {
						regular_Price_SPS = regular_Price_SPS
								.substring(regular_Price_SPS.indexOf(" "), regular_Price_SPS.length()).trim();
					}
					regular_TM = regular_Price_SPS;
					if (!linksps.contains("utm_source")) {
						linksps = linksps.concat("&utm_source=google");
					}
					eGroupID.setTextContent(gridSPS);
					eItem.appendChild(eId);
//					eItem.appendChild(eGroupID);
					org.w3c.dom.Element etitle = supdoc.createElement("g:title");
					etitle.setTextContent(productTM.getTitle());
					org.w3c.dom.Element eDescription = supdoc.createElement("g:description");
					eDescription.setTextContent(productTM.getDescription());
					eItem.appendChild(etitle);
					eItem.appendChild(eDescription);
//					org.w3c.dom.Element elink = supdoc.createElement("g:link");
//					elink.setTextContent(linksps);
//					eItem.appendChild(elink);
//					org.w3c.dom.Element eprice = supdoc.createElement("g:price");
//					eprice.setTextContent(productTM.getPrice());

					String gtinTM = productTM.getGtin();
					if (gtinTM != null) {
						if (gtinTM.contains("'")) {
							gtinTM = gtinTM.replace("'", Constants.EMPTY);
						}
						org.w3c.dom.Element eGtin = supdoc.createElement("g:gtin");
						eGtin.setTextContent(gtinTM);
						if (!isFirstFeed) {
							eItem.appendChild(eGtin);
						}
					}
					org.w3c.dom.Element eLink = supdoc.createElement("g:link");
					String linkTM = productTM.getLink();
					if (linkTM.contains("utm_source")) {
						linkTM = linkTM.substring(0, linkTM.indexOf("utm_source"));
					}
					eLink.setTextContent(linkTM);
//					eItem.appendChild(eLink);
					org.w3c.dom.Element eimage_link = supdoc.createElement("g:image_link");
					if (tmLinkShopify != null) {
						eimage_link.setTextContent(tmLinkShopify);
					} else {
						eimage_link.setTextContent(productTM.getImage_link());
					}
					org.w3c.dom.Element eColor = supdoc.createElement("g:color");
					String color = productTM.getColor();
					eColor.setTextContent(color);

					org.w3c.dom.Element eSize = supdoc.createElement("g:size");
					String size = productTM.getSize();
					eSize.setTextContent(size);

					org.w3c.dom.Element eGender = supdoc.createElement("g:gender");
					eGender.setTextContent("unisex");

					org.w3c.dom.Element eSexGroup = supdoc.createElement("g:age_group");
					eSexGroup.setTextContent("adult");

					if (!isFirstFeed) {
						ProductPrice variant = new ProductPrice(Integer.valueOf(productTM.getId()), regular_Price_SPS,
								Constants.VARIANT, gridTM);
						lsProductPrice.add(variant);
					}

					if (!isFirstFeed) {
//						eItem.appendChild(eprice);
						eItem.appendChild(eimage_link);
						eItem.appendChild(eColor);
						eItem.appendChild(eSize);
						eItem.appendChild(eGender);
						eItem.appendChild(eSexGroup);
					}

					if (j == lsSameIDSPS.size() - 1 || j >= 11) {
						break;
					}

				}
				if (i == listgrSPS.size() - 1) {
					break;
				}
				if (!isFirstFeed) {
					ProductPrice tmPrice = new ProductPrice(Integer.valueOf(gridTM), regular_TM, Constants.PARENT,
							gridTM);
					lsProductPrice.add(tmPrice);
				}
				lsChangeLinks.add(woolink);
			}
			FileOutputStream output = new FileOutputStream(
					"C:\\Users\\VIET IT\\Downloads\\" + folder + "\\supfeed.xml");
			writeXml(supdoc, output);
			if (!isFirstFeed && !lsProductPrice.isEmpty()) {
				for (WooLink woo : lsChangeLinks) {
					// changePermalink(woo.getSlug(), woo.getPermalink(), domain, woo.getId(),
					// gson);
				}
				for (ProductPrice woo : lsProductPrice) {
					// changePrice(domain, woo, gson);
				}

			}
			lsChangeLinks.addAll(lsOldChangeLinks);
			if (!lsProductPrice.isEmpty() && !sTMGrId.isEmpty()) {
				writeExcel(lsChangeLinks);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void ebook(String link) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "E:\\Metamask\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.navigate().to(link);
		Thread.sleep(10000);
		driver.quit();
	}

	private static void spsUpdate() {
		try {
			Gson gson = new Gson();
			boolean isFirstFeed = false;
			String path = "C:\\Users\\VIET IT\\Downloads\\" + folder + "\\tm.xml";
			String pathsps = "C:\\Users\\VIET IT\\Downloads\\" + folder + "\\sps";

			File fileStore = new File(path);
			File fileSPS = new File(pathsps);

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);

			// parse XML file
			DocumentBuilder db = dbf.newDocumentBuilder();
			org.w3c.dom.Document supdoc = db.newDocument();

			org.w3c.dom.Document doc = db.parse(fileStore);
			doc.getDocumentElement().normalize();

			org.w3c.dom.Document docSPS = db.parse(fileSPS);
			docSPS.getDocumentElement().normalize();

			NodeList list = doc.getElementsByTagName("item");
			NodeList listSPS = docSPS.getElementsByTagName("item");
			List<WooLink> lsChangeLinks = new ArrayList<WooLink>();
			List<WooLink> lsOldChangeLinks = new ArrayList<WooLink>();
			Set<String> sTMGrId = new HashSet<String>();
			Set<String> sSPS = new HashSet<String>();
			Set<String> sTMGrIdChanged = new HashSet<String>();
			Set<String> sSPSChanged = new HashSet<String>();
			if (!isFirstFeed) {
				readExcel(sTMGrIdChanged, sSPSChanged, lsOldChangeLinks);
			}
			Map<String, String> mapGtinImage = readFileShopify();
			List<XMLProduct> lsProductTM = getListProductInFile(list, sTMGrId, sTMGrIdChanged, sSPSChanged,
					Constants.WOO);
			List<XMLProduct> lsProductSPS = getListProductInFile(listSPS, sSPS, sTMGrIdChanged, sSPSChanged,
					Constants.SPS);

			org.w3c.dom.Element rootElement = supdoc.createElement("channel");
			supdoc.appendChild(rootElement);
			List<String> listgrTM = new ArrayList<>(sTMGrId);
			List<String> listgrSPS = new ArrayList<>(sSPS);
			for (int i = 0; i < listgrTM.size(); i++) {
				WooLink woolink = new WooLink();
				String gridTM = listgrTM.get(i);
				woolink.setId(gridTM);
				String gridSPS = listgrSPS.get(i);
				woolink.setSpsID(gridSPS);
				List<XMLProduct> lsSameIDTM = lsProductTM.stream()
						.filter(product -> gridTM.equalsIgnoreCase(product.getItem_group_id()))
						.collect(Collectors.toList());
				List<XMLProduct> lsSameIDSPS = lsProductSPS.stream()
						.filter(productsps -> gridSPS.equalsIgnoreCase(productsps.getItem_group_id()))
						.collect(Collectors.toList());
				if (!lsProductSPS.isEmpty()) {
					XMLProduct spsProduct = lsSameIDSPS.get(0);
					String spslink = spsProduct.getLink();
					if (spslink.contains("#!") && spslink.contains("?")) {
						String slug = spslink.substring(spslink.indexOf("#!") + 3, spslink.indexOf("&"));
						woolink.setSlug(slug);
						woolink.setPermalink(domainProduct + slug);
					}
				}
				if (!lsSameIDTM.isEmpty()) {
					XMLProduct tmProduct = lsSameIDTM.get(0);
					String tmname = tmProduct.getTitle();
					woolink.setName(tmname);
				}
				String tmLinkShopify = null;
				for (int j = 0; j < lsSameIDTM.size(); j++) {
					XMLProduct productTM = lsSameIDTM.get(j);
					String gtin = productTM.getGtin();
					tmLinkShopify = mapGtinImage.get(gtin);
					if (tmLinkShopify != null) {
						break;
					}
				}
				for (int j = 0; j < lsSameIDTM.size(); j++) {
					XMLProduct productTM = lsSameIDTM.get(j);
					XMLProduct productsps = lsSameIDSPS.get(j);
					org.w3c.dom.Element eItem = supdoc.createElement("item");
					rootElement.appendChild(eItem);
					org.w3c.dom.Element eId = supdoc.createElement("g:id");
					org.w3c.dom.Element eGroupID = supdoc.createElement("g:item_group_id");
					eId.setTextContent(productsps.getId());
					String linksps = productsps.getLink();
					if (!linksps.contains("utm_source")) {
						linksps = linksps.concat("&utm_source=google");
					}
					eGroupID.setTextContent(gridSPS);
					eItem.appendChild(eId);
//					eItem.appendChild(eGroupID);
					org.w3c.dom.Element etitle = supdoc.createElement("g:title");
					etitle.setTextContent(productTM.getTitle());
					org.w3c.dom.Element eDescription = supdoc.createElement("g:description");
					eDescription.setTextContent(productTM.getDescription());
					eItem.appendChild(etitle);
					eItem.appendChild(eDescription);
					org.w3c.dom.Element elink = supdoc.createElement("g:link");
					elink.setTextContent(linksps);
					eItem.appendChild(elink);
					org.w3c.dom.Element eprice = supdoc.createElement("g:price");
					eprice.setTextContent(productTM.getPrice());

					String gtinTM = productTM.getGtin();
					if (gtinTM != null) {
						if (gtinTM.contains("'")) {
							gtinTM = gtinTM.replace("'", Constants.EMPTY);
						}
						org.w3c.dom.Element eGtin = supdoc.createElement("g:gtin");
						eGtin.setTextContent(gtinTM);
						if (!isFirstFeed) {
							// eItem.appendChild(eGtin);
						}
					}
					org.w3c.dom.Element eLink = supdoc.createElement("g:link");
					String linkTM = productTM.getLink();
					if (linkTM.contains("utm_source")) {
						linkTM = linkTM.substring(0, linkTM.indexOf("utm_source"));
					}
					eLink.setTextContent(linkTM);
//					eItem.appendChild(eLink);
					org.w3c.dom.Element eimage_link = supdoc.createElement("g:image_link");
					if (tmLinkShopify != null) {
						eimage_link.setTextContent(tmLinkShopify);
					} else {
						eimage_link.setTextContent(productTM.getImage_link());
					}
					if (!isFirstFeed) {
						eItem.appendChild(eprice);
						eItem.appendChild(eimage_link);
					}

					if (j == lsSameIDSPS.size() - 1 || j >= 11) {
						break;
					}
				}
				if (i == listgrSPS.size() - 1) {
					break;
				}
				lsChangeLinks.add(woolink);
			}
			FileOutputStream output = new FileOutputStream(
					"C:\\Users\\VIET IT\\Downloads\\" + folder + "\\supfeed.xml");
			writeXml(supdoc, output);
			if (!isFirstFeed && !sTMGrId.isEmpty()) {
				for (WooLink woo : lsChangeLinks) {
					changePermalink(woo.getSlug(), woo.getPermalink(), domain, woo.getId(), gson);
				}
			}
			lsChangeLinks.addAll(lsOldChangeLinks);
			if (!lsChangeLinks.isEmpty() && !sTMGrId.isEmpty()) {
				writeExcel(lsChangeLinks);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public static List<String> getLinkChewy() throws InterruptedException {
		List<String> lsLink = new ArrayList<String>();
		System.setProperty("webdriver.chrome.driver", "E:\\Metamask\\chromedriver.exe");
		String link = "https://www.chewy.com/b/brushless-teeth-cleaning-2600";
		WebDriver driver = new ChromeDriver();
		driver.navigate().to(link);
		String xpathProducts = "/html/body/div[1]/div[4]/div[2]/div[1]/div/div[3]";
		WebElement elementProducts = driver.findElement(By.xpath(xpathProducts));
		List<WebElement> eWebElement = elementProducts.findElements(By.className("product-card-image"));
		if (!eWebElement.isEmpty()) {
			for (WebElement webElement : eWebElement) {
				String href = webElement.getAttribute("href");
				lsLink.add(href);
			}
		}
		Thread.sleep(2000);
		driver.quit();
		return lsLink;
	}

	public static List<ChewyProduct> chewy(List<String> links) throws InterruptedException {
		List<ChewyProduct> lsChewy = new ArrayList<ChewyProduct>();
		System.setProperty("webdriver.chrome.driver", "E:\\Metamask\\chromedriver.exe");
//		List<String> links = new ArrayList<String>();
//		links.add("https://www.chewy.com/revolution-plus-topical-solution-cats/dp/185730");
//		links.add("https://www.chewy.com/frisco-chick-in-nest-2-in-1-plush/dp/331284");
//		links.add("https://www.chewy.com/frisco-baseball-2-in-1-mitt-ball/dp/279221");
//		links.add("https://www.chewy.com/nylabone-power-chew-textured-chew/dp/42981");
		for (String string : links) {
			ChewyProduct chewyProduct = new ChewyProduct();
			WebDriver driver = new ChromeDriver();
			driver.navigate().to(string);
			String pathpath2 = "/html/body/div[1]/div/div[3]/div/div[1]/div[6]/div/div/div";
			String price3 = "/html/body/div[1]/div/div[3]/div/div[1]/div[6]/div/div/div/div[2]/div[2]";
			String descriptionpath = "/html/body/div[1]/div/div[3]/div/div[4]/div/div[1]/div/div[1]/div[2]";
			String descriptionPath2 = "/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/div[1]/div[2]/section";
			String categoryPath = "/html/body/div[1]/div/div[3]/div/nav/ol/li[3]/a";
			String category = Constants.EMPTY;
			String title = null;
			String description = null;
			try {
				WebElement elementTitle = driver
						.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[2]/div[1]/div/h1"));
				title = elementTitle.getText();

			} catch (Exception e) {
				title = driver.getTitle();
			}

			String xpathImage = "/html/body/div[1]/div/div[3]/div/div[1]/div[2]/div/div[2]/div[1]/section/div[2]/div/div[1]/div/img";
			WebElement image = driver.findElement(By.xpath(xpathImage));
			WebElement eprice = null;
			try {
				eprice = driver.findElement(By.xpath(price3));
			} catch (Exception e) {
				eprice = driver.findElement(By.xpath(pathpath2));
			}
			try {
				WebElement eDescription = driver.findElement(By.xpath(descriptionpath));
				description = eDescription.getText();
			} catch (Exception e) {
				WebElement eDescription = driver.findElement(By.xpath(descriptionPath2));
				description = eDescription.getText();
			}
			String imageLink = image.getAttribute("src");
			if (title != null && title.contains("- Chewy.com")) {
				title = title.replace("- Chewy.com", "").trim();
			}
			try {
				WebElement elementCategory = driver.findElement(By.xpath(categoryPath));
				category = elementCategory.getText();
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			if (eprice != null) {
				String price = eprice.getText();
				if (price.contains("$") && price.contains(".")) {
					price = price.substring(price.indexOf("$") + 1, price.indexOf("."));
				}
				chewyProduct.setPrice(price);
			}
			chewyProduct.setTitle(title);
			chewyProduct.setImagelink(imageLink);
			chewyProduct.setDescription(description);
			chewyProduct.setCategory(category);
			lsChewy.add(chewyProduct);
			Thread.sleep(2000);
			driver.quit();
		}
		return lsChewy;
	}

	private static Map<String, String> readFileShopify() throws IOException {
		Map<String, String> mapGtinImage = new HashMap<String, String>();
		String pathShopify = "C:\\Users\\VIET IT\\Desktop\\AWS (1)\\Drop\\Product\\shopify_image.xlsx";
		File fileShopify = new File(pathShopify);
		FileInputStream fis = new FileInputStream(fileShopify);
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		XSSFSheet sheet = wb.getSheetAt(0);
		Iterator<Row> itr = sheet.iterator();
		while (itr.hasNext()) {
			Row row = itr.next();
			Iterator<Cell> cellIterator = row.cellIterator();
			String gtin = null;
			String imagelink = null;
			while (cellIterator.hasNext()) {
				Cell currentCell = cellIterator.next();
				int index = currentCell.getColumnIndex();
				if (index == 0) {
					if (currentCell.getCellTypeEnum() == CellType.STRING) {
						gtin = currentCell.getStringCellValue();
					} else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
						gtin = String.valueOf(currentCell.getNumericCellValue());
					}
				} else if (index == 1) {
					if (currentCell.getCellTypeEnum() == CellType.STRING) {
						imagelink = currentCell.getStringCellValue();
					} else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
						imagelink = String.valueOf(currentCell.getNumericCellValue());
					}
				}
			}
			if (gtin != null && imagelink != null) {
				if (gtin.contains("'")) {
					gtin = gtin.replace("'", Constants.EMPTY);
				}
				mapGtinImage.put(gtin, imagelink);
			}
		}
		return mapGtinImage;
	}

	private static List<XMLProduct> getListProductInFile(NodeList listsp, Set<String> sTMGrId,
			Set<String> sTMGrIdChange, Set<String> spsChanged, int typeProduct) {
		List<XMLProduct> lsProduct = new ArrayList<XMLProduct>();
		for (int temp = 0; temp < listsp.getLength(); temp++) {

			Node node = listsp.item(temp);

			if (node.getNodeType() == Node.ELEMENT_NODE) {

				org.w3c.dom.Element element = (org.w3c.dom.Element) node;

				// get text
				String id = element.getElementsByTagName("g:id").item(0).getTextContent();
				String title = element.getElementsByTagName("g:title").item(0).getTextContent();
				String description = element.getElementsByTagName("g:description").item(0).getTextContent();
				String link = element.getElementsByTagName("g:link").item(0).getTextContent();
				String price = element.getElementsByTagName("g:price").item(0).getTextContent();
				String image_link = element.getElementsByTagName("g:image_link").item(0).getTextContent();
				String item_group_id = element.getElementsByTagName("g:item_group_id").item(0).getTextContent();
				String gtin = element.getElementsByTagName("g:gtin").item(0) == null ? ""
						: element.getElementsByTagName("g:gtin").item(0).getTextContent();
				String size = element.getElementsByTagName("g:size").item(0) == null ? ""
						: element.getElementsByTagName("g:size").item(0).getTextContent();
				String color = element.getElementsByTagName("g:color").item(0) == null ? ""
						: element.getElementsByTagName("g:color").item(0).getTextContent();
				String gender = element.getElementsByTagName("g:gender").item(0) == null ? ""
						: element.getElementsByTagName("g:gender").item(0).getTextContent();
				String age_group = element.getElementsByTagName("g:age_group").item(0) == null ? ""
						: element.getElementsByTagName("g:age_group").item(0).getTextContent();
				if (Constants.WOO == typeProduct) {
					if (!sTMGrIdChange.contains(item_group_id)) {
						sTMGrId.add(item_group_id);
					}
				} else {
					if (!spsChanged.contains(item_group_id)) {
						sTMGrId.add(item_group_id);
					}
				}
				XMLProduct productTM = new XMLProduct(id, title, description, link, price, image_link, item_group_id,
						gtin, size, gender, age_group, color);
				lsProduct.add(productTM);
			}
		}
		return lsProduct;
	}

	private static List<XMLProduct> getListProductInFile(NodeList listsp, int typeProduct) {
		List<XMLProduct> lsProduct = new ArrayList<XMLProduct>();
		for (int temp = 0; temp < listsp.getLength(); temp++) {

			Node node = listsp.item(temp);

			if (node.getNodeType() == Node.ELEMENT_NODE) {

				org.w3c.dom.Element element = (org.w3c.dom.Element) node;

				// get text
				String id = element.getElementsByTagName("g:id").item(0).getTextContent();
				String title = element.getElementsByTagName("g:title").item(0).getTextContent();
				String description = element.getElementsByTagName("g:description").item(0).getTextContent();
				String gtin = element.getElementsByTagName("g:gtin").item(0).getTextContent();
				XMLProduct productTM = new XMLProduct(id, title, description, null, null, null, null, gtin, null, null,
						null, null);
				lsProduct.add(productTM);
			}
		}
		return lsProduct;
	}

	private static void exportChewy(List<ChewyProduct> lsChewy) throws IOException {
		String[] COLUMNs = { "Handle", "Title", "Body (HTML)", "Vendor", "Type", "Tag", "Published", "Option1 Name",
				"Option1 Value", "Variant Inventory Tracker", "Variant Inventory Qty", "Variant Price",
				"Variant Compare At Price", "Variant Barcode", "Image Src", "Image Position", "Status" };
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("Chewy");
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setColor(IndexedColors.BLACK.getIndex());
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		// Row for Header
		Row headerRow = sheet.createRow(0);
		// Header
		for (int col = 0; col < COLUMNs.length; col++) {
			Cell cell = headerRow.createCell(col);
			cell.setCellValue(COLUMNs[col]);
			cell.setCellStyle(headerCellStyle);
		}
		int rowIdx = 1;
		for (ChewyProduct product : lsChewy) {
			Row row = sheet.createRow(rowIdx++);
			String title = product.getTitle().trim();
			String handle = title.replace(" ", "-");
			row.createCell(0).setCellValue(handle);
			row.createCell(1).setCellValue(title);
			row.createCell(2).setCellValue(product.getDescription().trim());
			row.createCell(3).setCellValue("Chewy");
			row.createCell(4).setCellValue(product.getCategory());
			row.createCell(5).setCellValue(Constants.EMPTY);
			row.createCell(6).setCellValue("TRUE");
			row.createCell(7).setCellValue(Constants.EMPTY);
			row.createCell(8).setCellValue(Constants.EMPTY);
			row.createCell(9).setCellValue(Constants.EMPTY);
			row.createCell(10).setCellValue(21);
			row.createCell(11).setCellValue(product.getPrice());
			row.createCell(12).setCellValue(Constants.EMPTY);
			row.createCell(13).setCellValue(Constants.EMPTY);
			row.createCell(14).setCellValue(product.getImagelink());
			row.createCell(15).setCellValue(Constants.EMPTY);
			row.createCell(16).setCellValue("ACTIVE");
		}
		Date newdate = Calendar.getInstance().getTime();
		String pattern = "HHmm";
		DateFormat df = new SimpleDateFormat(pattern);
		String pathFileSaved = "C:\\chewy\\" + df.format(newdate) + "product.xlsx";
		FileOutputStream out = new FileOutputStream(new File(pathFileSaved));
		workbook.write(out);
		workbook.close();
	}

	private static void writeExcel(List<WooLink> lsChangeLinks) throws IOException {
		String[] COLUMNs = { "TM ID", "Name", "Slug", "Link", "SPSID" };
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("MapProductLink");
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setColor(IndexedColors.BLACK.getIndex());
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		// Row for Header
		Row headerRow = sheet.createRow(0);
		// Header
		for (int col = 0; col < COLUMNs.length; col++) {
			Cell cell = headerRow.createCell(col);
			cell.setCellValue(COLUMNs[col]);
			cell.setCellStyle(headerCellStyle);
		}
		int rowIdx = 1;
		for (WooLink woo : lsChangeLinks) {
			Row row = sheet.createRow(rowIdx++);

			row.createCell(0).setCellValue(woo.getId());
			row.createCell(1).setCellValue(woo.getName());
			row.createCell(2).setCellValue(woo.getSlug());
			row.createCell(3).setCellValue(woo.getPermalink());
			row.createCell(4).setCellValue(woo.getSpsID());
		}
		FileOutputStream out = new FileOutputStream(
				new File("C:\\Users\\VIET IT\\Downloads\\" + folder + "\\mapProduct.xlsx"));
		workbook.write(out);
		workbook.close();

	}

	private static void readExcel(Set<String> tmGrid, Set<String> spsGrId, List<WooLink> changeLink) {
		String pathChanged = "C:\\Users\\VIET IT\\Downloads\\" + folder + "\\mapProduct.xlsx";
		try {
			File fileChanged = new File(pathChanged);
			FileInputStream fis = new FileInputStream(fileChanged);
			XSSFWorkbook wb = new XSSFWorkbook(fis);
			XSSFSheet sheet = wb.getSheetAt(0);
			Iterator<Row> itr = sheet.iterator();
			while (itr.hasNext()) {
				Row currentRow = itr.next();
				int rowindex = currentRow.getRowNum();
				if (rowindex == 0) {
					continue;
				}
				Iterator<Cell> cellIterator = currentRow.cellIterator();
				WooLink woolink = new WooLink();
				while (cellIterator.hasNext()) {
					Cell currentCell = cellIterator.next();
					int index = currentCell.getColumnIndex();
					switch (index) {
					case 0:
						String tmid = Constants.EMPTY;
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							tmid = currentCell.getStringCellValue();
						} else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
							tmid = String.valueOf((int) currentCell.getNumericCellValue());
						}
						woolink.setId(tmid);
						tmGrid.add(tmid);
						break;
					case 1:
						String name = currentCell.getStringCellValue();
						woolink.setName(name);
						break;
					case 2:
						String slug = currentCell.getStringCellValue();
						woolink.setSlug(slug);
						break;
					case 3:
						String link = currentCell.getStringCellValue();
						woolink.setPermalink(link);
						break;
					case 4:
						String spsid = Constants.EMPTY;
						if (currentCell.getCellTypeEnum() == CellType.STRING) {
							spsid = currentCell.getStringCellValue();
						} else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
							spsid = String.valueOf((int) currentCell.getNumericCellValue());
						}
						woolink.setSpsID(spsid);
						spsGrId.add(spsid);
						break;
					default:
						break;
					}
				}
				changeLink.add(woolink);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void writeXml(org.w3c.dom.Document doc, OutputStream output) throws TransformerException {

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(output);

		transformer.transform(source, result);

	}

	private static String getWooInfo(String urlWoo) {
		String responseString = Constants.EMPTY;
		try {
			URL url = new URL(urlWoo);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.addRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
			conn.setRequestMethod("GET");

			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String output;

			StringBuffer response = new StringBuffer();
			while ((output = in.readLine()) != null) {
				response.append(output);
			}

			in.close();
			// printing result from response
			responseString = response.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseString;
	}

	private static boolean checkExistProxy(String url, HttpClient client) {
		try {

			HttpGet httpGet = new HttpGet(url);
			HttpResponse response = client.execute(httpGet);
			if (response.getStatusLine().getStatusCode() != 200) {
			}
			BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));

			String output;
			String saleAPI = null;
			while ((output = br.readLine()) != null) {
				saleAPI = output;
			}
			if (saleAPI == null) {
				return false;
			} else {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private static void changePermalink(String slug, String permalinkdomain, String domain, String id, Gson gson)
			throws ClientProtocolException, IOException {
		Permalink permalink = new Permalink(slug, permalinkdomain);
		String jsonDataPut = gson.toJson(permalink);
		StringEntity paramsPut = new StringEntity(jsonDataPut);
		HttpClient clientPut = HttpClientBuilder.create().build();
		String urlPut = "https://" + domain + "/wp-json/wc/v3/products/" + id + "?consumer_key=" + ck
				+ "&consumer_secret=" + cs;
		HttpPut httpPut = new HttpPut(urlPut);
		httpPut.setEntity(paramsPut);
		httpPut.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
		HttpResponse responsePut = clientPut.execute(httpPut);
		int statusCodePut = responsePut.getStatusLine().getStatusCode();
	}

	private static void changePrice(String domain, ProductPrice productPrice, Gson gson)
			throws ClientProtocolException, IOException {
		int type = productPrice.getType();
		String urlPut = null;
		if (Constants.PARENT == type) {
			urlPut = "https://" + domain + "/wp-json/wc/v3/products/" + productPrice.getParent() + "?consumer_key=" + ck
					+ "&consumer_secret=" + cs;
		} else {
			urlPut = "https://" + domain + "/wp-json/wc/v3/products/" + productPrice.getParent() + "/variations/"
					+ productPrice.getId() + "?consumer_key=" + ck + "&consumer_secret=" + cs;
		}
		ProductPrice ppWoo = new ProductPrice(productPrice.getId(), productPrice.getRegular_price());
		String jsonDataPut = gson.toJson(ppWoo);
		StringEntity paramsPut = new StringEntity(jsonDataPut);
		HttpClient clientPut = HttpClientBuilder.create().build();
		HttpPut httpPut = new HttpPut(urlPut);
		httpPut.setEntity(paramsPut);
		httpPut.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
		HttpResponse responsePut = clientPut.execute(httpPut);
		int statusCodePut = responsePut.getStatusLine().getStatusCode();
	}

	private static List<Export> addExportProductColumbia(List<String> ImageURLs, Document doc) {
		List<Export> lsExport = new ArrayList<Export>();
		String productname = "";
		String description = "";
		String category = "";
		String price = "";
		String id = "";
		Elements eName = doc.getElementsByClass("product-name");
		for (Element element : eName) {
			productname = element.html();
		}
		Elements eDes = doc.getElementsByClass("product__accordion__section__details");
		for (Element element : eDes) {
			description = element.html();
		}
		Elements eBreadCum = doc.getElementsByClass("breadcrumb-item");
		Element eLast = eBreadCum.last();
		Elements elastLink = eLast.getElementsByTag("a");
		for (Element element : elastLink) {
			category = element.html();
		}
		Elements ePrice = doc.getElementsByClass("price");
		for (Element element : ePrice) {
			Elements eValue = element.getElementsByClass("value");
			for (Element element2 : eValue) {
				price = element2.attr("content");
			}
			break;
		}
		Elements eid = doc.getElementsByClass("product-id");
		for (Element element : eid) {
			id = element.html();
		}
		String handle = productname.concat("-").concat(id).replace(" ", "-").toLowerCase();
		handle = handle.replaceAll("[^a-zA-Z0-9-]", "");
		Elements eSizeAtt = doc.getElementsByClass("js-size-attribute");
		List<String> sizes = new ArrayList<String>();
		for (Element element : eSizeAtt) {
			Elements eLink = element.getElementsByTag("a");
			for (Element e : eLink) {
				String size = e.attr("aria-label");
				if (size.contains("Selected")) {
					size = size.replace("Selected", "").trim();
				}
				System.out.println(size);
				sizes.add(size);
			}
		}
		int k = 0;
		int j = 0;
		for (int i = 0; i < sizes.size(); i++) {

			Export export = new Export();
			export.setHandle(handle);
			if (k == 0) {
				export.setTitle(productname);
				export.setBody("\"".concat(description).concat("\""));
			} else {
				export.setTitle("");
				export.setBody("");
			}
			k++;
			export.setVendor("Columbia");
			export.setType(category);
			export.setPublished("TRUE");
			export.setOption1name("Size");
			export.setOption1value(sizes.get(i));
			export.setVariantSKU("");
			export.setVariantTracker("shopify");
			export.setQuantity("21");
			export.setVariantPolicy("deny");
			export.setVariantFulfillment("manual");
			export.setPrice(price);
			export.setCompare(price);
			export.setRequireShipping("true");
			export.setVariantTax("true");
			export.setVariantBarcode("");
			export.setImageURL(ImageURLs.get(i));

			export.setImagePosition(String.valueOf(++j));
			export.setWeightUnit("lb");
			lsExport.add(export);
		}
		return lsExport;
	}

	public static void listFilesForFolder(final File folder) {
		for (final File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				listFilesForFolder(fileEntry);
			} else {
				String filename = fileEntry.getName();
				if (filename.contains("lot") || filename.contains("card") || filename.contains("Lot")
						|| filename.contains("Card")) {
					if (filename.contains(".mrd") && !filename.contains(".BK") && !filename.contains(".BAK")
							&& !filename.contains(".bak")) {
						System.out.println(fileEntry.getName());
					}
				}
			}
		}
	}

}
